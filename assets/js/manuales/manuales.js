$(document).ready(function() {

	 $(".quitarDoc").click(function(e){

         var id  = e.currentTarget.attributes.id.value;

            swal({   title: "Alerta  "+name_pag+"!",  
              text: "¿ESTÁ SEGURO DE QUITAR ESTE DOCUMENTO?",   
              type: "warning",   
              showCancelButton: true,   
              confirmButtonText: "OK",   
              closeOnConfirm: false }, 

              function(isConfirm){   
                if (isConfirm) {  

                      // alert(state);
                      data = {
                            'id' : id
                          };

                        $('#tabManuales').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');
                        
                        postUrl = base_url + "index.php/ManualesDoc/StateChange";
                        $.ajax({
                          type: "POST",
                          url: postUrl,
                          data: data,
                          dataType: "text",
                          success: function(result) { 

                            // console.log(result);

                              if (result=='NOK') {
                                 swal({   title: "Alerta  "+name_pag+"!",  
                                  text: "NO SE PUDO QUITAR EL DOCUMENTO",   
                                  type: "warning",   
                                  showCancelButton: false,   
                                  confirmButtonText: "OK",   
                                  closeOnConfirm: false }, 
                                  function(isConfirm){   
                                    if (isConfirm) {   
                                      location.reload();
                                    } else {   
                                     location.reload();   } });

                                
                              }else{
                                swal("Alerta  "+name_pag+"!", "DOCUMENTO QUITADO CORRECTAMENTE", "success")
                                $("#container").html(result);   
                              };     


                        },
                          error: function(xhr, ajaxOptions, thrownError) {}
                      });

                } else {   
                  return;
                } });          

    });




     $(".descManual").change(function(e){

        $("#container").css("cursor", "progress");
        var id = e.currentTarget.attributes.id.value;
        var desc =  $(this).val();

        swal({   title: "Alerta  "+name_pag+"!",  
        text: "¿ESTÁ SEGURO DE ACTUALIZAR ESTE CAMPO?",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonText: "SI",   
        cancelButtonText: "NO",   
        closeOnConfirm: false }, 

        function(isConfirm){     
          if (isConfirm) {    

            data = {
              'id' : id,
              'desc' : desc
            };
          
              postUrl = base_url + "index.php/ManualesDoc/actDescManuales";
              $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                dataType: "text",
                success: function(result) { 

                  // console.log(result);
                  $("#container").css("cursor", "default");

                    if (result=='NOK') {
                       swal({   title: "Alerta  "+name_pag+"!",  
                        text: "EN ESTE MOMENTO NO SE PUEDE EDITAR ESTE CAMPO",   
                        type: "warning",   
                        showCancelButton: false,   
                        confirmButtonText: "OK",   
                        closeOnConfirm: false }, 
                        function(isConfirm){   
                          if (isConfirm) {   
                            return;
                          } else {   
                           return;  } });
                      
                    }else{
                      swal("Alerta  "+name_pag+"!", "CAMPO ACTUALIZADO CORRECTAMENTE", "success")
                    };     

              },
                error: function(xhr, ajaxOptions, thrownError) {}
            });

          } else {   
            $("#container").css("cursor", "default");
            return;
          } });          


    });



////////////////////////////////////////////


  $("#form1").submit(function (e) {       

    $("#cargaDoc").attr("disabled", true);
    $("#tabManuales2").css("cursor", "progress");
    $("#barra").show();  

            e.preventDefault();

            var parametros=new FormData($(this)[0])

            $.ajax({
                type: "POST",
                url: base_url + "index.php/ManualesDoc/upload",
                data: parametros,
                contentType: false, //importante enviar este parametro en false
                processData: false, //importante enviar este parametro en false
                success: function (data) {                       
// console.log(data);    
// alert(data);
                  $("#cargaDoc").attr("disabled", false);
                  $("#tabManuales2").css("cursor", "default");
                  $("#barra").hide();  

                  if(data=='OK'){

                      swal({   
                      title: "Alerta  "+name_pag+"!",  
                      text: "EL DOCUMENTO SE CARGÓ CORRECTAMENTE",   
                      type: "success",   
                      showCancelButton: false,   
                      confirmButtonText: "OK",   
                      closeOnConfirm: false }, 
                      function(isConfirm){   
                        if (isConfirm) { location.reload();
                        } else { location.reload();   } });

                  }else if(data=='1'){
                     swal("Alerta  "+name_pag+"!", "EL DOCUMENTO NO TIENE UN FORMATO PERMITIDO (SOLO SE ACEPTAN PDF, CSV, DOC, DOCX)", "error")
                  }else if(data=='2'){
                     swal("Alerta  "+name_pag+"!", "EL DOCUMENTO EXCEDE EL TAMAÑO PERMITIDO", "error")
                  }else if(data=='3'){
                     swal("Alerta  "+name_pag+"!", "EL DOCUMENTO ES MAS GRANDE DE LO PERMITIDO", "error")
                  }else if(data=='4'){
                     swal("Alerta  "+name_pag+"!", "EL DOCUMENTO NO SE PUDO PROCESAR COMPLETAMENTE", "error")
                  }else if(data=='5'){
                     swal("Alerta  "+name_pag+"!", "EL DOCUMENTO NO SE PUDO PROCESAR", "error")
                  }else if(data=='6'){
                     swal("Alerta  "+name_pag+"!", "ERROR AL CARGAR EL DOCUMENTO", "error")
                  }else if(data=='7'){
                     swal("Alerta  "+name_pag+"!", "NO SE PUDO MOVER DE CARPETA EL DOCUMENTO", "error")
                  }else if(data=='8'){
                     swal("Alerta  "+name_pag+"!", "¡ERROR...!   ES PROBABLE QUE EL DOCUMENTO ESTÉ INFECTADO CON UN VIRUS O FUE MAL CREADO", "error")
                  }else if(data=='9'){
                     swal("Alerta  "+name_pag+"!", "EL DOCUMENTO YA EXISTE", "error")
                  }else if(data=='10'){
                     swal("Alerta  "+name_pag+"!", "EL NOMBRE DEL DOCUMENTO YA EXISTE", "error")
                  }else{
                     swal("Alerta  "+name_pag+"!", "EL DOCUMENTO NO CUMPLE CON LOS REQUISITOS PARA SER CARGADO", "error")
                  }

                // $("#container").css("display", "none");

                },
                error: function (r) {
                    
                    swal("Alerta  "+name_pag+"!", "ERROR DE CARGA, VUELVA A INTENTARLO", "warning")
                    $("#container").css("display", "none");
                }
            });
   
        })



//
//   $(".DLWD").click(function(e){
//
//         var valor  = e.currentTarget.attributes.valor.value;
//         var op = 1;
//
//        data = {
//              'valor' : valor,
//              'op'  : op
//            };
//
//          postUrl = base_url + "index.php/ManualesDoc/Trace1";
//          $.ajax({
//            type: "POST",
//            url: postUrl,
//            data: data,
//            dataType: "text",
//            success: function(result) { 
//
//          },
//            error: function(xhr, ajaxOptions, thrownError) {}
//        });
//    });




});