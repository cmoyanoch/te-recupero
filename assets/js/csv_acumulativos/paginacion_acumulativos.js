$(document).ready(function() {


   $("#adelante").click(function(e) { 

        var seleccionados = new Array();
        $(".reportSeleccionado").each(function(){
             seleccionados.push($(this).val());     
        });
        // console.log(seleccionados); 


        var adelante  = parseInt($("#cp").text().trim());
        var FiltroF  = $("#FiltroF").val(); 
        var fIni = $("#datepicker_Desde").val();
        var fFin = $("#datepicker_Hasta").val(); 
        var id = $("#idTipoRep").val(); 
        var uno = parseInt(1);
        adelante = parseInt(adelante + 1);

        // alert(estado);
         $("#cp").text(adelante);


         if (FiltroF!="1" || fIni=="" || fFin==""){ 
          var fIni  = "";
          var fFin  = "";
         }                  

           data = {
              'paginador' : adelante,
              'fIni'      : fIni,
              'fFin'      : fFin,
              'seleccionados' : seleccionados,
              'id'            : id
              };

         $('#tab_acumulativos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');

          postUrl = base_url + "index.php/Reportes/acumulativos_pag";

            $.ajax({
            type: "POST",
            url: postUrl,
            data: data,
            dataType: "text",
            success: function(result) {

                if (result=='NOK') {

                    swal({   title: "Alerta  "+name_pag+"!",  
                    text: "NO SE ENCUENTRAN DATOS",   
                    type: "error",   
                    showCancelButton: false,   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false }, 
                    function(isConfirm){   
                    if (isConfirm) {  location.reload();
                    } else {   location.reload();   } });
                }else{

                    $("#tab_acumulativos").html(result);   

                    $("#FiltroF").attr("value",FiltroF);
                    $("#datepicker_Desde").val(fIni);
                    $("#datepicker_Hasta").val(fFin);        

//                    if (fIni!="" && fFin!="") {
                      // $( "#datepicker_Desde" ).prop( "disabled", true );
                      // $( "#datepicker_Hasta" ).prop( "disabled", true );                   
//                    };

            };       
          },
            error: function(xhr, ajaxOptions, thrownError) {}
        });    
      
  });  



    $("#atras").click(function(e) { 

        var seleccionados = new Array();
        $(".reportSeleccionado").each(function(){
             seleccionados.push($(this).val());     
        });      

        var adelante  = parseInt($("#cp").text().trim());
        var FiltroF  = $("#FiltroF").val(); 
        var fIni = $("#datepicker_Desde").val();
        var fFin = $("#datepicker_Hasta").val(); 
        var id = $("#idTipoRep").val(); 
        var uno = parseInt(1);
        adelante = parseInt(adelante - 1);

        // alert(estado);
         $("#cp").text(adelante);


        if (FiltroF!="1" || fIni=="" || fFin=="")  { 
         var fIni  = "";
         var fFin  = "";
        }                  

            data = {
             'paginador' : adelante,
             'fIni'      : fIni,
             'fFin'      : fFin,
             'seleccionados' : seleccionados,
             'id'            : id
             };

            $('#tab_acumulativos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');

            postUrl = base_url + "index.php/Reportes/acumulativos_pag";

            $.ajax({
            type: "POST",
            url: postUrl,
            data: data,
            dataType: "text",
            success: function(result) { 

                if (result=='NOK') {

                    swal({   title: "Alerta  "+name_pag+"!",  
                    text: "NO SE ENCUENTRAN DATOS",   
                    type: "error",   
                    showCancelButton: false,   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false }, 
                    function(isConfirm){   
                    if (isConfirm) {  location.reload();
                    } else {   location.reload();   } });

                 }else{

                    $("#tab_acumulativos").html(result);    

                    $("#FiltroF").attr("value",FiltroF);
                    $("#datepicker_Desde").val(fIni);
                    $("#datepicker_Hasta").val(fFin);        

//                    if (fIni!="" && fFin!="") {
                      // $( "#datepicker_Desde" ).prop( "disabled", true );
                      // $( "#datepicker_Hasta" ).prop( "disabled", true );                   
//                    };


                 };       
            },
              error: function(xhr, ajaxOptions, thrownError) {}
          });               
    });  




     $.datepicker.regional['es'] = {
             closeText: 'Cerrar',
             prevText: '<Ant',
             nextText: 'Sig>',
             currentText: 'Hoy',
             monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
             monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
             dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
             dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
             dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
             weekHeader: 'Sm',
             dateFormat: 'dd/mm/yy',
             firstDay: 1,
             isRTL: false,
             showMonthAfterYear: false,
             yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);



    $( "#datepicker_Desde" ).datepicker();
    $( "#datepicker_Hasta" ).datepicker();



  $("#filtrar_acumulativos").click(function(){   

    var seleccionados = new Array();
      $(".reportSeleccionado").each(function(){
           seleccionados.push($(this).val());     
      });

    var fIni  = $("#datepicker_Desde").val();
    var fFin  = $("#datepicker_Hasta").val();
    var id = $("#idTipoRep").val(); 

    if (fIni!="" && fFin!="") {

      var FiltroF  = "1";

      data = {          
        "fIni" : fIni,
        "fFin" : fFin,
        "seleccionados" : seleccionados,
        'id'            : id
      };

// console.log(data); return;

     $('#tab_acumulativos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');
   
      postUrl = base_url + "index.php/Reportes/acumulativos_fecha";
        $.ajax({
          type: "POST",
          url: postUrl,
          data: data,
          dataType: "text",
          success: function(result) {
            // console.log(result);

            $("#tab_acumulativos").html(result); 

            $("#FiltroF").attr("value",FiltroF);
            $("#datepicker_Desde").val(fIni);
            $("#datepicker_Hasta").val(fFin);

            // $( "#datepicker_Desde" ).prop( "disabled", true );
            // $( "#datepicker_Hasta" ).prop( "disabled", true ); 

          },
            error: function(xhr, ajaxOptions, thrownError) {

            }
        });


      }else{
        swal("Alerta  "+name_pag+"!", "DEBE INGRESAR UNA FECHA INICIAL Y UNA FINAL.", "warning")
      }


  }); 








    $("#reCargar").click(function(){
      location.reload();
   }); 

 
  
    $(".todosDoc").change(function(){

      var archivos = new Array();
      var op = '1';
      var id = $("#idTipoRep").val(); 

      swal({
        title: "Alerta  "+name_pag+"!",  
        text: "¿Desea descargar la totalidad de los reportes?",
        type: "warning",
        showCancelButton: true,
        // confirmButtonColor: "#DD6B55", 
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        closeOnCancel: false
      },
      function(isConfirm){
        if (isConfirm) {

          data = {
            'archivos' : archivos,
            'op'  : op,
            'id'  : id
           };    

           // $('#tab_acumulativos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="http://www.jose-aguilar.com/blog/wp-content/uploads/2012/03/loading.gif" style="margin-top: -268px;"/></p>');
            
              postUrl = base_url + "index.php/Reportes/acumulativos_comprimir";
              $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                dataType: "text",
                success: function(result) {  
    // console.log(result);

                  if (result!=0) {

                    // window.open(base_url+'/descarga_zip/'+result, '_blank');

                    swal({   title: "La descarga está disponible!",  
                    text: "Presione OK para comenzar",   
                    type: "success",   
                    showCancelButton: false,   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false }, 
                    function(isConfirm){   
                      if (isConfirm) {    
                        window.open(base_url+'/descarga_zip/'+result, '_blank');
                        location.reload();
                      } else {    swal("Cancelado", "", "warning"); $('#selTodos').attr('checked', false);  } });

                  }else{

                    swal({   title: "Alerta  "+name_pag+"!",  
                    text: "En este momento no se pueden descargar los documentos, inténtelo mas tarde.",   
                    type: "warning",   
                    showCancelButton: false,   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false }, 
                    function(isConfirm){   
                      if (isConfirm) {    location.reload();
                      } else {    location.reload();   } });

                  };
                  
                },
                  error: function(xhr, ajaxOptions, thrownError) {}
            });

        } else {

          swal("Cancelado", "", "warning");


          $('#selTodos').attr('checked', false); 
          // location.reload(); 

        }
      });      


    });

/////////////////////////////////////////////////////////////////////////

    $('.select_acumulativo').click(function(e) {
    
        if ($(this).is(':checked')) {
            
            nombre1 = e.currentTarget.attributes.nombre.value;   
            nombre = nombre1.toUpperCase();  
            id = e.currentTarget.attributes.id.value;
            $("#"+id).attr('disabled', true);

            $("#cargarSeleccionados").append('<tr><td><input type="checkbox" checked  class="reportSeleccionado" value="'+nombre1+'" ></td><td>'+nombre+'</td></tr>');
        }
                  
    });



    $('.D_acumulativos').click(function(e) { 

      var op = 6;
    
       data = {
            'op'  : op
            };

        postUrl = base_url + "index.php/Reportes/Trace";

        $.ajax({
          type: "POST",
          url: postUrl,
          data: data,
          dataType: "text",
          success: function(result) { 


          },
            error: function(xhr, ajaxOptions, thrownError) {}
        });   
                  
    });




});
