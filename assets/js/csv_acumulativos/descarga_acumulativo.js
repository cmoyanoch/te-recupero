$(document).ready(function() {


  $('#descargar_acumulativo').click(function() {

      var archivos = new Array();
      var op = '0';
      var id = $("#idTipoRep").val(); 

        $(".reportSeleccionado:checkbox:checked").each(function(){
             archivos.push($(this).val());     
        });

      if(archivos!=""){


        data = {
          'archivos' : archivos,
          'op'	: op,
          'id'  : id
         };    

//            console.log(data); return;
          postUrl = base_url + "index.php/Reportes/acumulativos_comprimir";
          $.ajax({
            type: "POST",
            url: postUrl,
            data: data,
            dataType: "text",
            success: function(result) {  
// console.log(result);  return;

              if (result!=0) {

                    swal({   title: "La descarga está disponible!",  
                    text: "Presione OK para comenzar",   
                    type: "success",   
                    showCancelButton: false,   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false }, 
                    function(isConfirm){   
                      if (isConfirm) {    
                        window.open(base_url+'/descarga_zip/'+result, '_blank');
                        location.reload();
                      } else {    swal("Cancelado", "", "warning"); $('#selTodos').attr('checked', false);  } });


              }else{

                    swal({   title: "Alerta  "+name_pag+"!",  
                    text: "En este momento no se pueden descargar los documentos, inténtelo mas tarde.",   
                    type: "warning",   
                    showCancelButton: false,   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false }, 
                    function(isConfirm){   
                      if (isConfirm) {    location.reload();
                      } else {    location.reload();   } });

              };
              
            },
              error: function(xhr, ajaxOptions, thrownError) {}
        });

      }else{
         swal("Alerta  "+name_pag+"!", "Debe seleccionar al menos un documento para descargar. Para obtener todos los archivos debe marcar el check de la cabecera de la tabla principal.", "info");
      }
  
    });

  


});
