jQuery(function($){ 
  $.datepicker.regional['es'] = {
             closeText: 'Cerrar',
             prevText: '<Ant',
             nextText: 'Sig>',
             currentText: 'Hoy',
             monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
             monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
             dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
             dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
             dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
             weekHeader: 'Sm',
             dateFormat: 'dd/mm/yy',
             firstDay: 1,
             isRTL: false,
             showMonthAfterYear: false,
             // minDate: "-30D",
             maxDate: "0D",
             yearSuffix: ''
             
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);


    $("#datepickerInicio").datepicker();
    $("#datepickerFinal").datepicker();
    // alert('hola');



    $("#FiltrarLF").click(function(){ 
      
      var datepickerInicio  = $("#datepickerInicio").val();
      var datepickerFinal   = $("#datepickerFinal").val();

      if (datepickerInicio == "") {
          datepickerInicio = "0";
      }
      if (datepickerFinal == "") {
          datepickerFinal = "0";
      }

      data = {
          
            "datepickerInicio" : datepickerInicio,
            "datepickerFinal" : datepickerFinal
      };
       console.log(data);

     $('#tabForos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');
      postUrl = base_url + "index.php/Foros/buscarLFfecha";
        $.ajax({
          type: "POST",
          url: postUrl,
          data: data,
          dataType: "text",
          success: function(result) {
            $("#tabForos").html(result);   
            $("#SWfiltro").attr("value","1");
            if(datepickerInicio!=0){
              $("#datepickerInicio").attr("value",datepickerInicio);
            }
            if(datepickerFinal!=0){
             $("#datepickerFinal").attr("value",datepickerFinal);
           }
          },
            error: function(xhr, ajaxOptions, thrownError) {

            }
        });


  }); 



});
