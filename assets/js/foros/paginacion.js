$(document).ready(function() {


	 $("#adelante").click(function(e) { 

      var SWfiltro = $("#SWfiltro").val();

      if(SWfiltro==0){

          var adelante  = parseInt($("#cp").text().trim());
          var uno = parseInt(1);
          adelante = parseInt(adelante + 1);
          // alert(estado);
           $("#cp").text(adelante);
           
               
           data = {
                  'paginador' : adelante
                };

            $('#tabForos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');

            postUrl = base_url + "index.php/Foros/PaginarForos";

            $.ajax({
              type: "POST",
              url: postUrl,
              data: data,
              dataType: "text",
              success: function(result) {

                   if (result=='NOK') {
                      // alert('NO SE ENCUENTRAN DATOS, REFRESQUE LA PÁGINA E INTÉNTELO NUEVAMENTE');
                      // location.reload();
                      swal({   title: "Alerta  "+name_pag+"!",  
                      text: "NO HAY MAS REGISTROS EN ESTA TABLA",   
                      type: "warning",   
                      showCancelButton: false,   
                      confirmButtonText: "OK",   
                      closeOnConfirm: false }, 
                      function(isConfirm){   
                        if (isConfirm) {    location.reload();
                        } else {    location.reload();   } });

                    }else{

                        $("#tabForos").html(result);   
                        // $("#swBuscar").attr("value","0");
                        
                    };       



              },
                error: function(xhr, ajaxOptions, thrownError) {}
            });    


      }else if(SWfiltro==1){

          var op = SWfiltro; 
          var adelante  = parseInt($("#cp").text().trim());
          var datepickerInicio = $("#datepickerInicio").val();
          var datepickerFinal = $("#datepickerFinal").val();
          
          var uno = parseInt(1);
          adelante = parseInt(adelante + 1);
          // alert(estado);
           $("#cp").text(adelante);
           
               
           data = {
                'datepickerInicio' : datepickerInicio,
                'datepickerFinal' : datepickerFinal,
                'paginador' : adelante
                };


            // alert(id_estado);
            $('#tabForos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');

            postUrl = base_url + "index.php/Foros/PaginarForos2";

            $.ajax({
              type: "POST",
              url: postUrl,
              data: data,
              dataType: "text",
              success: function(result) { 

                   if (result=='NOK') {
                      // alert('NO SE ENCUENTRAN DATOS, REFRESQUE LA PÁGINA E INTÉNTELO NUEVAMENTE');
                      // location.reload();
                      swal({   title: "Alerta  "+name_pag+"!",  
                      text: "NO SE ENCONTRARON REGISTROS QUE CUMPLAN CON LOS PARÁMETROS DE BUSQUEDA",   
                      type: "warning",   
                      showCancelButton: false,   
                      confirmButtonText: "OK",   
                      closeOnConfirm: false }, 
                      function(isConfirm){   
                        if (isConfirm) {    location.reload();   
                        } else {    location.reload();   } });

                    }else{

                        $("#tabForos").html(result); 
                        $("#SWfiltro").attr("value",op);  
                       
                    };   

              },
                error: function(xhr, ajaxOptions, thrownError) {}
            });    



        }else if(SWfiltro==2){

          var op = SWfiltro; 
          var adelante  = parseInt($("#cp").text().trim());
          var titulo = $("#buscarEnTabla").val();
          
          var uno = parseInt(1);
          adelante = parseInt(adelante + 1);
          // alert(estado);
           $("#cp").text(adelante);
           
               
           data = {
                'titulo' : titulo,
                'paginador' : adelante
                };


            // alert(id_estado);
            $('#tabForos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');

            postUrl = base_url + "index.php/Foros/PaginarForos3";

            $.ajax({
              type: "POST",
              url: postUrl,
              data: data,
              dataType: "text",
              success: function(result) { 

                   if (result=='NOK') {
                      // alert('NO SE ENCUENTRAN DATOS, REFRESQUE LA PÁGINA E INTÉNTELO NUEVAMENTE');
                      // location.reload();
                      swal({   title: "Alerta  "+name_pag+"!",  
                      text: "NO SE ENCONTRARON REGISTROS QUE CUMPLAN CON LOS PARÁMETROS DE BUSQUEDA",   
                      type: "warning",   
                      showCancelButton: false,   
                      confirmButtonText: "OK",   
                      closeOnConfirm: false }, 
                      function(isConfirm){   
                        if (isConfirm) {    location.reload();   
                        } else {    location.reload();   } });

                    }else{

                        $("#tabForos").html(result); 
                        $("#SWfiltro").attr("value",op);  
                       
                    };   

              },
                error: function(xhr, ajaxOptions, thrownError) {}
            });    



        }

      
  });  



    $("#atras").click(function(e) { 

      var SWfiltro = $("#SWfiltro").val();

      if(SWfiltro==0){
     
          var adelante  = parseInt($("#cp").text().trim());
          var uno = parseInt(1);
          adelante = parseInt(adelante - 1);
          // alert(estado);
           $("#cp").text(adelante);
           
               
           data = {
                  'paginador' : adelante
                };

            $('#tabForos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');

            postUrl = base_url + "index.php/Foros/PaginarForos";

            $.ajax({
              type: "POST",
              url: postUrl,
              data: data,
              dataType: "text",
              success: function(result) {

                   if (result=='NOK') {
                      // alert('NO SE ENCUENTRAN DATOS, REFRESQUE LA PÁGINA E INTÉNTELO NUEVAMENTE');
                      // location.reload();
                      swal({   title: "Alerta  "+name_pag+"!",  
                      text: "NO HAY MAS REGISTROS EN ESTA TABLA",   
                      type: "warning",   
                      showCancelButton: false,   
                      confirmButtonText: "OK",   
                      closeOnConfirm: false }, 
                      function(isConfirm){   
                        if (isConfirm) {    location.reload();
                        } else {    location.reload();   } });

                    }else{

                        $("#tabForos").html(result);   

                        
                    };       



              },
                error: function(xhr, ajaxOptions, thrownError) {}
            });    


         }else if(SWfiltro==1){

          var op = SWfiltro; 
          var adelante  = parseInt($("#cp").text().trim());
          var datepickerInicio = $("#datepickerInicio").val();
          var datepickerFinal = $("#datepickerFinal").val();
          
          var uno = parseInt(1);
          adelante = parseInt(adelante - 1);
          // alert(estado);
           $("#cp").text(adelante);
           
               
           data = {
                'datepickerInicio' : datepickerInicio,
                'datepickerFinal' : datepickerFinal,
                'paginador' : adelante
                };


            // alert(id_estado);
            $('#tabForos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');

            postUrl = base_url + "index.php/Foros/PaginarForos2";

            $.ajax({
              type: "POST",
              url: postUrl,
              data: data,
              dataType: "text",
              success: function(result) { 

                   if (result=='NOK') {
                      // alert('NO SE ENCUENTRAN DATOS, REFRESQUE LA PÁGINA E INTÉNTELO NUEVAMENTE');
                      // location.reload();
                      swal({   title: "Alerta  "+name_pag+"!",  
                      text: "NO SE ENCONTRARON REGISTROS QUE CUMPLAN CON LOS PARÁMETROS DE BUSQUEDA",   
                      type: "warning",   
                      showCancelButton: false,   
                      confirmButtonText: "OK",   
                      closeOnConfirm: false }, 
                      function(isConfirm){   
                        if (isConfirm) {    location.reload();   
                        } else {    location.reload();   } });

                    }else{

                        $("#tabForos").html(result); 
                        $("#SWfiltro").attr("value",op);  
                       
                    };   

              },
                error: function(xhr, ajaxOptions, thrownError) {}
            });    




        }else if(SWfiltro==2){

          var op = SWfiltro; 
          var adelante  = parseInt($("#cp").text().trim());
          var titulo = $("#buscarEnTabla").val();
          
          var uno = parseInt(1);
          adelante = parseInt(adelante - 1);
          // alert(estado);
           $("#cp").text(adelante);
           
               
           data = {
                'titulo' : titulo,
                'paginador' : adelante
                };


            // alert(id_estado);
            $('#tabForos').html('<p style="text-align: -webkit-center;margin-top: 198px;margin-left: 125px;"><img src="'+base_url_gif+'" /></p>');

            postUrl = base_url + "index.php/Foros/PaginarForos3";

            $.ajax({
              type: "POST",
              url: postUrl,
              data: data,
              dataType: "text",
              success: function(result) { 

                   if (result=='NOK') {
                      // alert('NO SE ENCUENTRAN DATOS, REFRESQUE LA PÁGINA E INTÉNTELO NUEVAMENTE');
                      // location.reload();
                      swal({   title: "Alerta  "+name_pag+"!",  
                      text: "NO SE ENCONTRARON REGISTROS QUE CUMPLAN CON LOS PARÁMETROS DE BUSQUEDA",   
                      type: "warning",   
                      showCancelButton: false,   
                      confirmButtonText: "OK",   
                      closeOnConfirm: false }, 
                      function(isConfirm){   
                        if (isConfirm) {    location.reload();   
                        } else {    location.reload();   } });

                    }else{

                        $("#tabForos").html(result); 
                        $("#SWfiltro").attr("value",op);  
                       
                    };   

              },
                error: function(xhr, ajaxOptions, thrownError) {}
            });    



        }




      
    });  






	

});
