$(document).ready(function() {
     
    $("#reCargar").click(function(e){        
        location.reload();     
    });



    $(".btnEliminar").click(function(e){   


   	var id = e.currentTarget.attributes.id.value; 

          swal({   title: "Alerta  "+name_pag+"!",   
            text: "¿Está seguro de eliminar este foro?",   
            type: "warning",   
            showCancelButton: true,   
            cancelButtonText: "No",   
            confirmButtonText: "Si", 
            showLoaderOnConfirm: true,  
            closeOnConfirm: false }, 
            function(){   

            data = {
              'id' : id
             };    

            
              postUrl = base_url + "index.php/Foros/EliminarForo";
              $.ajax({
              type: "POST",
              url: postUrl,
              data: data,
              dataType: "text",
              success: function(result) {  

                if(result!="NOK"){

                swal("Hecho...!","FORO ELIMINADO CORRECTAMENTE!", "success")
                $("#tabForos").html(result);   
                 

               }else{

                swal({   
                  title: "Alerta  "+name_pag+"!",  
                  text: "HA OCURIDO UN PROBLEMA AL ELIMINAR EL FORO, INTÉNTELO NUEVAMENTE",   
                  type: "error",   
                  showCancelButton: false,   
                  confirmButtonText: "OK",   
                  closeOnConfirm: false }, 
                  function(isConfirm){   
                    if (isConfirm) {   
                      return;
                    } else {   
                     return;   } });


               }


              },
                error: function(xhr, ajaxOptions, thrownError) {}
            });

         });        

  });    




$(".btnEditar").click(function(e){   

	$("#divBloqueo").css("display", "block");

   	var id = e.currentTarget.attributes.id.value; 	

            data = {
              'id' : id
             };    
            
              postUrl = base_url + "index.php/Foros/EditarForo";
              $.ajax({
              type: "POST",
              url: postUrl,
              data: data,
              dataType: "text",
              success: function(result) {    

               $("#ModuloForos").html(result);
               $("#modalEditForo").modal("show");
  			   $("#divBloqueo").css("display", "none");      

              },
                error: function(xhr, ajaxOptions, thrownError) {}
            });


  });    



     $("#TablaForos tr.trDetalleForos td.tdForos").click(function(e){ 

	$("#divBloqueo").css("display", "block");

        var id = e.currentTarget.attributes.id.value;
        // alert(id);return;

        data = {
              'id' : id
            };
        // alert(id);
        postUrl = base_url + "index.php/Foros/VerForo";
        $.ajax({
          type: "POST",
          url: postUrl,
          data: data,
          dataType: "text",
          success: function(result) { 

                // console.log(result);           
               
               $("#ModuloForos").html(result);
               $("#ModuloVerForos").modal("show");
  			   $("#divBloqueo").css("display", "none");      

          },
            error: function(xhr, ajaxOptions, thrownError) {}
        });


    });






});
