var notifi={
    selectEmp:$("#selectEmpresasTec"),
    searchTec:$("#searchTec"),
    selectTecDest:$("#select-destinatario"),
    checkAll:null,
    btnSend:$("#sendMSJ"),
    allCheck:$("#allUserSend"),
    msj:$("#msjNoti"),
    init:function(){
        this.selectTecDest.multiSelect({
            selectableHeader: "<div class='custom-header'><b>Usuarios: </b></div><p></p><input type='text' style='width: 100%;' class='search-input form-control' autocomplete='off' placeholder='Buscador'>",
            selectionHeader: "<div class='custom-header'><b>Seleccionados: </b></div><p></p><input type='text' style='width: 100%;' class='search-input form-control' autocomplete='off' placeholder='Buscador'>",
            afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });
        this.selectEmp.select2().on("change",function(){
            notifi.loadTec($(this).val());
        });
        this.btnSend.on("click",function(){
            notifi.enviarNoti();
        });
        this.searchTec.on("keyup",function(){
            notifi.buscarTec($(this).val());
        });
        this.msj.on("keyup",function(){
            var text =  $(this).val().length;
            // var textCant =  $("#cantidadTexto").text();
            var resto  = 700 -  parseInt(text);
            $("#cantidadTexto").text(resto);
        });
    },
    loadTec:function(value){
        //console.log(value);
        var notPost=["clist","-1"];
        if(notPost.indexOf(value)===-1) {
            var word=this.searchTec.val()==""?null:this.searchTec.val();
            postUrl = base_url + "index.php/Notificaciones/buscarTecnicosEmpresa";
            $.ajax({
                type: "POST",
                url: postUrl,
                data: {idemp: value,setec:word},
                dataType: "text",
                success: function (result) {

// console.log(result);
                    
                    var data = $.parseJSON(result);
                    var options="";
                    if(value!="all") {
                        $.each(data, function (index, value) {
                            options += "<option value='" + value.id + "'>" + value.Nombre + "</option>";
                        });
                    }else{
                        var group="";
                        $.each(data, function (index, value) {

                            var opt="";
                            $.each(value, function (indx, tec) {
                                opt+="<option value='"+tec.id+"'>"+tec.Nombre+"</option>";
                            });
                            group+="<optgroup label='"+index+"'>"+opt+"</optgroup>";
                        });
                        options=group;
                    }
                    notifi.selectTecDest.html(options);
                    notifi.selectTecDest.multiSelect("refresh");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                }
            });
        }else{
            notifi.selectTecDest.html("");
            notifi.selectTecDest.multiSelect("refresh");
        }
    },
    buscarTec:function(word){
        if(word.length>2) {
            postUrl = base_url + "index.php/Notificaciones/buscarTecnicosEmpresa";
            $.ajax({
                type: "POST",
                url: postUrl,
                data: {idemp: "all", setec: word},
                dataType: "text",
                success: function (result) {
//                    console.log(result); return;
// console.log(result);
                    
                    var data = $.parseJSON(result);
                    var options = "";

                    var group = "";
                    $.each(data, function (index, value) {

                        var opt = "";
                        $.each(value, function (indx, tec) {
                            opt += "<option value='" + tec.id + "'>" + tec.Nombre + "</option>";
                        });
                        group += "<optgroup label='" + index + "'>" + opt + "</optgroup>";
                    });
                    options = group;

                    notifi.selectTecDest.html(options);
                    notifi.selectTecDest.multiSelect("refresh");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                }
            });
        }else{
            notifi.selectTecDest.html("");
            notifi.selectTecDest.multiSelect("refresh");
        }
    },
    enviarNoti:function(){
//        console.log(this.allCheck.is(":checked"));
        valor = $('#msjNoti').val();
        $('#sendMSJ').attr("disabled", true);


        if( valor.length == "" && valor.length == null ){
            swal("Alerta ","Ingrese un Mensaje para enviar...", "warning");
            $('#sendMSJ').attr("disabled", false);
            return;
        }

        data = {
            'text' : this.msj.val(),
            'id_usuarios' : this.selectTecDest.val(),
            'todos' : this.allCheck.is(":checked")?1:0
        };
        if(data.id_usuarios!=null||data.todos==1) {
            postUrl = base_url + "index.php/Notificaciones/sendNotifications";
            $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                dataType: "text",
                success: function (result) {
// console.log(result);
// return;
//                    console.log(result);
                    if (result != "NOK") {

                        swal({
                                title: "Alerta  "+name_pag+"!",
                                text: result,
                                type: "info",
                                showCancelButton: false,
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    location.reload();
                                } else {
                                    location.reload();
                                }
                            });

                    } else {

                        //$('#enviarMensaje').attr("disabled", false);
                        swal({
                                title: "Alerta  "+name_pag+"!",
                                text: result,
                                type: "info",
                                showCancelButton: false,
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    location.reload();
                                } else {
                                    location.reload();
                                }
                            });

                    }
                    ;

                },
                error: function (xhr, ajaxOptions, thrownError) {
                }
            });
        }
    }
};
$(document).ready(function() {
    notifi.init();
});


