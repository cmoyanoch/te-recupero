$(document).ready(function() {


  $("#form1").submit(function (e) {

    $("#cargaCap").attr("disabled", true);
    $("#contCap").css("cursor", "progress");
    $("#barraCap").show();  

          e.preventDefault();
 
          var parametros=new FormData($(this)[0])
   // console.log(parametros);   return;     
          $.ajax({
              type: "POST",
              url: base_url + "index.php/Capsulas/uploadCap",
              data: parametros,
              contentType: false, //importante enviar este parametro en false
              processData: false, //importante enviar este parametro en false
              success: function (data) {                       
// console.log(data); 

                $("#cargaCap").attr("disabled", false);
                $("#userfile").attr("disabled", false);
                $("#descriptionCap").attr("disabled", false);
                $("#contCap").css("cursor", "default");
                $("#barraCap").hide();  

                if(data=='OK'){

                    swal({   
                    title: "Alerta  "+name_pag+"!",  
                    text: "EL ARCHIVO SE CARGÓ CORRECTAMENTE",   
                    type: "success",   
                    showCancelButton: false,   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false }, 
                    function(isConfirm){   
                      if (isConfirm) { location.reload();
                      } else { location.reload();   } });

                }else if(data=='1'){
                    swal("Alerta  "+name_pag+"!", "EL ARCHIVO NO TIENE UN FORMATO PERMITIDO (SOLO SE ACEPTAN mp4, mp3, wma, wav, m4a, avi, wmv y flv)", "error");
                }else if(data=='2'){
                   swal("Alerta  "+name_pag+"!", "EL ARCHIVO EXCEDE EL TAMAÑO PERMITIDO", "error");
                }else if(data=='3'){
                   swal("Alerta  "+name_pag+"!", "EL ARCHIVO ES MAS GRANDE DE LO PERMITIDO", "error");
                }else if(data=='4'){
                   swal("Alerta  "+name_pag+"!", "EL ARCHIVO NO SE PUDO PROCESAR COMPLETAMENTE", "error");
                }else if(data=='5'){
                   swal("Alerta  "+name_pag+"!", "EL ARCHIVO NO SE PUDO PROCESAR", "error");
                }else if(data=='6'){
                   swal("Alerta  "+name_pag+"!", "ERROR AL CARGAR EL ARCHIVO", "error");
                }else if(data=='7'){
                   swal("Alerta  "+name_pag+"!", "NO SE PUDO MOVER DE CARPETA EL ARCHIVO", "error");
                }else if(data=='8'){
                   swal("Alerta  "+name_pag+"!", "¡ERROR...!   ES PROBABLE QUE EL ARCHIVO ESTÉ INFECTADO CON UN VIRUS O FUE MAL CREADO", "error");
                }else if(data=='9'){
                   swal("Alerta  "+name_pag+"!", "EL ARCHIVO YA EXISTE", "error");
                }else{
                   swal("Alerta  "+name_pag+"!", "EL ARCHIVO NO CUMPLE CON LOS REQUISITOS PARA SER CARGADO", "error");
                }

              // $("#container").css("display", "none");

              },
              error: function (r) {
                  
                  swal("Alerta  "+name_pag+"!", "ERROR DE CARGA, VUELVA A INTENTARLO", "error")
                  $("#contCap").css("display", "none");
              }
          });
   
        })


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////77



     $(".descCap").change(function(e){

        $("#container").css("cursor", "progress");
        var id = e.currentTarget.attributes.id.value;
        var desc =  $(this).val();

        swal({   title: "Alerta  "+name_pag+"!",  
        text: "¿ESTÁ SEGURO DE ACTUALIZAR ESTE CAMPO?",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonText: "SI",   
        cancelButtonText: "NO",   
        closeOnConfirm: false }, 

        function(isConfirm){     
          if (isConfirm) {    

            data = {
              'id' : id,
              'desc' : desc
            };
          
              postUrl = base_url + "index.php/Capsulas/actDescCap";
              $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                dataType: "text",
                success: function(result) { 

//                   console.log(result);
                  $("#container").css("cursor", "default");

                    if (result=='NOK') {
                       swal({   title: "Alerta  "+name_pag+"!",  
                        text: "EN ESTE MOMENTO NO SE PUEDE EDITAR ESTE CAMPO",   
                        type: "warning",   
                        showCancelButton: false,   
                        confirmButtonText: "OK",   
                        closeOnConfirm: false }, 
                        function(isConfirm){   
                          if (isConfirm) {   
                            return;
                          } else {   
                           return;  } });
                      
                    }else{
                      swal("Alerta  "+name_pag+"!", "CAMPO ACTUALIZADO CORRECTAMENTE", "success")
                    };     

              },
                error: function(xhr, ajaxOptions, thrownError) {}
            });

          } else {   
            $("#container").css("cursor", "default");
            return;
          } });          


    });








});