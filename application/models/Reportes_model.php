<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes_model extends CI_Model {

private $bd_;

    function __construct()
    {
      // Call the Model constructor
      parent::__construct();    
    }

        
        
        public function acumulativo_model($sumador,$fIni,$fFin,$seleccionados,$id){

            $num1 = 0;
            $num2 = PAGINADO_CANT;
            $num1 = $num1 + $sumador;
            $num2 = $num2 + $sumador;

            try{

                $this->db->select('r.Id, r.ReportName');
                $this->db->from('' . smw_tre_cl_smartway . '.MNG_DATA_ReportName r');
                $this->db->where('r.TypeFile = "'.$id.'" ');
                $this->db->order_by('r.Date', "desc");

            if ($fIni!="" && $fFin!="") {
                    $this->db->where('(r.Date BETWEEN "'.$fIni.'" AND "'.$fFin.'" )');
            }

            if(!empty($seleccionados)){
                foreach ($seleccionados as $value) {
                    $this->db->where('r.ReportName != "'.$value.'" ');
                }
            }

            if ($sumador==0) {
                $this->db->limit($num2);
            }else{
                $this->db->limit(PAGINADO_CANT,$num1);
            }

            $query = $this->db->get()->result_array();

            // print_r($this->db->last_query());
            // return print_r($query);
            return $query;

            }catch(Exception $e){
                    throw new Exception($e->getMessage(), 500);		
            }

       }


	public function acumulativo_model_cont($fIni,$fFin,$seleccionados,$id){

            try{

            $this->db->select('count(1) as cantReg');
            $this->db->from('' . smw_tre_cl_smartway . '.MNG_DATA_ReportName r');
            $this->db->where('r.TypeFile = "'.$id.'" ');

            if(!empty($seleccionados)){
                foreach ($seleccionados as $value) {
                    $this->db->where('r.ReportName != "'.$value.'" ');
                }
            }

            if ($fIni!="" && $fFin!="") {
                    $this->db->where('(r.Date BETWEEN "'.$fIni.'" AND "'.$fFin.'" )');
            }

            $query = $this->db->get()->row_array();

            // print_r($this->db->last_query());
            // return print_r($query);
            return $query;


            }catch(Exception $e){
                    throw new Exception($e->getMessage(), 500);		
            }

	}
        
        
        
        
	 public function acumulativo_mode_zip($id){

            try{

            $this->db->select('r.ReportName');
            $this->db->from('' . smw_tre_cl_smartway . '.MNG_DATA_ReportName r');
            $this->db->where('r.TypeFile = "'.$id.'" ');
            $this->db->order_by('r.ReportName', "desc");

            $query = $this->db->get()->result_array();

            // print_r($this->db->last_query());
            // return print_r($query);
            return $query;


            }catch(Exception $e){
                    throw new Exception($e->getMessage(), 500);		
            }

	}
                     
        
                

}