<?php
	
	class Tecnicos_model extends CI_Model
	{
		public function __construct(){
			$this->load->database();
			$this->tables      = $this->config->item('tables', 'ion_auth');
			$this->store_salt  = $this->config->item('store_salt', 'ion_auth');
			$this->hash_method = $this->config->item('hash_method', 'ion_auth');
		}
		
		public function create($tec){	


			$this->db->trans_begin();


			$name         = $tec['name'];
			$rut          = $tec['rut'];
			$direc        = $tec['direc'];	
			
			$query = $this->db->query("INSERT INTO " . smw_tre_cl_smartway . ".TRAZER_MAS_CLIENTES
												(nombre_cliente, rut_cliente, direccion_cliente)
												VALUES
												('$name', '$rut', '$direc')");
			$id = $this->db->insert_id();			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
		

			$this->db->trans_commit();
			return TRUE;
		}
		
		public function edit($tec){
			
	
			$tec_data = array(
				'nombre_cliente' => $tec['name'],
				'rut_cliente' => $tec['rut'],
				'direccion_cliente' => $tec['direc']
			);

			$this->db->set($tec_data);
			$this->db->where('id', $tec['id']);
			$this->db->update(smw_tre_cl_smartway . '.TRAZER_MAS_CLIENTES');


			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
			

			$this->db->trans_commit();			
			
			return TRUE;
		}
		
		public function get($id){
			$this->db->select('id ,  name, mail,nombre_moebius,imei,phone,id_mas_group, id_zona, rut, patentTruck, numberTruck,external_id');
			$this->db->where('id', $id);
			$this->db->where('is_delete', 0);
			
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_USER');
			$result = $query->row_array();
			
			return $result;
		}
		
		public function find($value = "", $limit = 0, $page = NULL){
			try {
				
				$this->db->select('id_cliente AS id, nombre_cliente AS nomb,rut_cliente AS rut, direccion_cliente AS direc, id_status AS status ');
				$this->db->from('' . smw_tre_cl_smartway . '.TRAZER_MAS_CLIENTES');
				$this->db->order_by('nombre_cliente', 'asc');
				$this->db->where('id_status = "0" ');
				
				if ($value != "" && $value != null){
					$this->db->where(" ( nombre_cliente like '%" . $value . "%' or
												rut_cliente like '%" . $value . "%' or 
												 direccion_cliente like '%" . $value . "%' )");
				}
				
				if ($page == 1){
					$this->db->limit($limit);
				} else {
					$this->db->limit($limit, (($page - 1) * $limit));
				}
				
				$query = $this->db->get()->result_array();
				
				return $query;
				
			} catch (Exception $e) {
				throw new Exception($e->getMessage(), 500);
			}
		}
		
		public function count($value = ""){
			$this->db->like('nombre_cliente', $value);
			return $this->db->count_all_results(smw_tre_cl_smartway . '.TRAZER_MAS_CLIENTES');
		}
		
		public function delete($id){
			$this->db->trans_begin();
			$this->db->set('id_status', 1);
			$this->db->where('id_cliente', $id);
			$this->db->update(smw_tre_cl_smartway . '.TRAZER_MAS_CLIENTES');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
			$this->db->trans_commit();
			return true;
		}
		
		public function activate($user){ return true; }

		
		public function deactivate($user){ return true; }

		
		public function get_authorizations(){
			$id = $this->ion_auth->user()->row()->id;
			$this->db->select('description');
			$this->db->where('user_id', $id);
			$this->db->join('users_groups', 'groups.id = users_groups.group_id');
			$query = $this->db->get('groups')->result_array();
			$result = array_column($query, 'description');
			return $result;
		}
		
		public function change_password($user){

			$id = $this->ion_auth->user()->row()->id;
			$this->db->trans_begin();
			$salt = $this->store_salt ? $this->salt() : FALSE;
			$password = $this->hash_password($user['password'], $salt);
			
			$data = array(
				'password' => $password,
			);
			$this->db->set($data);
			$this->db->where('id', $id);
			$this->db->update('users');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
			$this->db->trans_commit();
			return true;
		}
		
		/**
		 * Misc functions
		 *
		 * Hash password : Hashes the password to be stored in the database.
		 * Hash password db : This function takes a password and validates it
		 * against an entry in the users table.
		 * Salt : Generates a random salt value.
		 *
		 * @author Mathew
		 */
		
		/**
		 * Hashes the password to be stored in the database.
		 *
		 * @return void
		 * @author Mathew
		 **/
		public function hash_password($password, $salt = false, $use_sha1_override = FALSE){
			if (empty($password)){
				return FALSE;
			}
			
			if ($use_sha1_override === FALSE && $this->hash_method == 'bcrypt'){
				return $this->bcrypt->hash($password);
			}
			
			if ($this->store_salt && $salt){
				return sha1($password . $salt);
			} else {
				$salt = $this->salt();
				return $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
			}
		}
		
		public function get_group_dropdown(){
			$result = [];
			
			$group = array(608, 609, 610, 89, 606, 607, 3, 5, 1, 2, 9, 10, 4, 6, 7, 8, 11, 12, 13, 313, 314, 576, 580, 581, 584, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605);
			$this->db->select('id, name');
			$this->db->where_in('id', $group);
			$this->db->order_by(smw_tre_cl_smartway . '.TRAZER_MAS_GRUOP.name');
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_GRUOP');
			$result = $result + array_column($query->result_array(), 'name', 'id');
			return $result;
		}
		
		protected function _prepare_ip($ip_address){
			      return $ip_address;
		}
		
		public function validEmail($tec){
			$this->db->select('mail');
			$this->db->where("mail = '" . $tec['mail'] . "'");
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_USER');
			$result = $query->result_array();
			
			return $result;
		}
		
		public function validPhone($tec){
			$this->db->select('phone');
			$this->db->where("phone = '" . $tec['phone'] . "'");
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_USER');
			$result = $query->result_array();
			
			return $result;
		}
		
		public function validImei($tec){
			$this->db->select('imei');
			$this->db->where("imei = '" . $tec['imei'] . "'");
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_USER');
			$result = $query->result_array();
			
			return $result;
		}
		
		public function setInactive($idTec, $type, $commentary){
			
			$sql = " UPDATE ".smw_tre_cl_smartway.".TRAZER_MAS_USER
						SET
							inactiveType       = ".$type.",
							inactiveCommentary = '".$commentary."'
						WHERE
							id = ".$idTec."";
			$query  = $this->db->query($sql);
			
			$response = "NOK";
			if($query)
				$response = "OK";
			
			return $response;
		}
		
		public function setActive($idTec){
			
			$sql = " UPDATE ".smw_tre_cl_smartway.".TRAZER_MAS_USER
						SET
							inactiveType       = 0,
							inactiveCommentary = ''
						WHERE
							id = ".$idTec."";
			$query  = $this->db->query($sql);
			
			$response = "NOK";
			if($query)
				$response = "OK";
			
			return $response;
		}

	}

?>