<?php

class Grilla_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}
	
	public function get(){

		$fecha = date('Y-m-d 00:00:00');

	//	$fecha = $fecha.' 00:00:00';

		$ids = $this->getIds();

		$ids = str_replace('*,', '', $ids);

		$sql = "SELECT 
					i.incident,
					i.idTec,
					i.id_area,
					u.name, 
					u.numberTruck,
					u.external_id,
				    i.id_bahia,
					u.updatetimecoord,
					u.coordx,
					u.coordy,
					g.name as empresa,
					p.name AS estado,
					i.coordX coordEmpX,
					i.coordY coordEmpY,
					i.datecreation,
					e.radio_cerco,
					e.radio_cerco_2,
					e.radio_cerco_3,
					e.radio_cerco_4 	
				FROM  smw_tre_cl_smartway.TRAZER_DATA_INCIDENT i 
				INNER JOIN  smw_tre_cl_smartway.TRAZER_MAS_USER u ON i.idTec = u.id
				INNER JOIN  smw_tre_cl_smartway.TRAZER_MAS_GRUOP g ON u.id_grupo = g.id 
				INNER JOIN  smw_tre_cl_smartway.TRAZER_MAS_STATUS p ON i.idstatus = p.id 
				INNER JOIN smw_tre_cl_smartway.TRAZER_DATA_EMPRESA e ON i.id_customer = e.id_empresa
				WHERE i.id_bahia IS NOT NULL AND i.id IN(".$ids.");";

		$query = $this->db->query($sql);
		$querys = $query->result_array();
		return $querys;
	}

	public function getHercules($search,$truck){

		$fecha = date('Y-m-d 00:00:00');

	//	$fecha = $fecha.' 00:00:00';

		$ids = $this->getIdsHercules($search,$truck);

		if($ids!='NOK')
		{

		$ids = str_replace('*,', '', $ids);


		$sql = "SELECT DISTINCT
					i.incident,
					i.idTec,
					i.id_area,
				    i.id_bahia,
				    i.updatetime,
					p.name AS estado,
					i.coordX coordEmpX,
					i.coordY coordEmpY,
					i.datecreation,
					e.radio_cerco,
					e.radio_cerco_2,
					e.radio_cerco_3,
					e.radio_cerco_4,
					(SELECT U.numberTruck FROM smw_tre_cl_smartway.TRAZER_MAS_USER U WHERE i.idTec = U.id)	AS numberTruck
				FROM  smw_tre_cl_smartway.TRAZER_DATA_INCIDENT i 
				INNER JOIN  smw_tre_cl_smartway.TRAZER_MAS_STATUS p ON i.idstatus = p.id 
				INNER JOIN smw_tre_cl_smartway.TRAZER_DATA_EMPRESA e ON i.id_customer = e.id_empresa
				WHERE i.id_bahia IS NOT NULL AND i.id IN(".$ids.") ORDER BY idTec DESC;";

		$query = $this->db->query($sql);
		$querys = $query->result_array();
		return $querys;
		}
		else
		{
			return 'NOK';
		}
	}


	public function getDailyCharges($idtec){

		$fecha = date('Y-m-d 19:00:00',strtotime("-1 days"));

	//	$fecha = $fecha.' 00:00:00';

		$sql = "SELECT i.incident,p.name as estado,i.title,i.datecreation,i.id_bahia,i.id_area FROM smw_tre_cl_smartway.TRAZER_DATA_INCIDENT i
				INNER JOIN  smw_tre_cl_smartway.TRAZER_MAS_STATUS p ON i.idstatus = p.id
				WHERE i.idTec =".$idtec." AND i.datecreation>'".$fecha."'  ORDER BY i.incident DESC;";

		$query = $this->db->query($sql);
		$querys = $query->result_array();
		return $querys;
	}


	public function getIds()
	{
		
		$fecha = date('Y-m-d 00:00:00');

		$sql = "SELECT  id,MIN(id_area) AS incident,idTec FROM smw_tre_cl_smartway.TRAZER_DATA_INCIDENT WHERE idstatus IN(11, 4 ,16, 3,17) GROUP BY idTec;";

		$query = $this->db->query($sql);
		$querys = $query->result_array();

		$idsH = $this->getIdsHercules2();

		$temp='';

		foreach ($querys as $key => $valor) {
			
			foreach ($idsH as $key2 => $valor2) {
				
				if($valor['idTec']==$valor2['idTec'])
				{
					$valor = $valor2;
					$querys[$key]= $valor2;
				}

			}
		}

		//var_dump($querys);
		
		$ids='*';

		foreach ($querys as $valor) {
			
			$ids = $ids.','.$valor["id"];
		}

		return $ids;
	}


	public function getIdsHercules($search,$truck)
	{
		
		$fecha = date('Y-m-d 19:00:00',strtotime("-1 days"));
		//$fecha = date('Y-m-d 00:00:00');

		$sql = "SELECT DISTINCT I.id,I.id_area AS incident,I.idTec FROM smw_tre_cl_smartway.TRAZER_DATA_INCIDENT I ";

		if(!empty($truck)&&!is_null($truck))
		{
			$sql.=" INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_USER U ON I.idTec = U.id ";
		}

		$sql.=" WHERE idstatus IN(11, 4 ,16, 3,17,5) AND datecreation>'$fecha' ";


		if(!empty($truck)&&!is_null($truck))
		{
			$sql.=" AND U.numberTruck = '".$truck."' ";
		}

		if(!empty($search)&&!is_null($search))
		{
			$sql.=" AND I.incident LIKE '%".$search."%' ";
		}

		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
				$querys = $query->result_array();

				$ids='*';

				foreach ($querys as $valor) {
							
					$ids = $ids.','.$valor["id"];
				}

				return $ids;
		}
		else
		{
				$response = 'NOK';

				return $response;
		}

	}


	public function getIdsHercules2()
	{
		
		$fecha = date('Y-m-d 00:00:00');

		$sql = "SELECT DISTINCT id,MIN(id_area) AS incident,idTec FROM smw_tre_cl_smartway.TRAZER_DATA_INCIDENT WHERE idstatus IN(11, 4 ,16, 3,17) AND id_area=99 GROUP BY idTec;";

		$query = $this->db->query($sql);
		$querys = $query->result_array();

		return $this->unique_multidim_array($querys,'idTec');
	}


	public function cerrarTicket($id)
	{

		$sql = "UPDATE smw_tre_cl_smartway.TRAZER_DATA_INCIDENT SET idstatus=5 WHERE incident = '".$id."'";

		$query = $this->db->query($sql);
		
		if($query)
		{
			return 'OK';
		}
		else
		{
			return 'NOK';
		}


	}


	
	function unique_multidim_array($array, $key) {
	    $temp_array = array();
	    $i = 0;
	    $key_array = array();
	   
	    foreach($array as $val) {
	        if (!in_array($val[$key], $key_array)) {
	            $key_array[$i] = $val[$key];
	            $temp_array[$i] = $val;
	        }
	        $i++;
	    }
	    return $temp_array;
	}


	
}

?>