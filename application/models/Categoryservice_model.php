<?php

class Categoryservice_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	/*Guarda una nueva categoría*/
	public function create($categoryservice){
		$this->db->trans_begin();			
		$category_data = array(
			'nombre' => $categoryservice['nombre']
			//'cat_category_id' => $categoryservice['id'] ? $categoryservice['id'] : NULL
		);
		$this->db->insert(smw_tre_cl_smartway.'.TRAZER_MAS_SERVICIO' ,$category_data);
		//new dBug($this->db->trans_status());
		//new dBug($this->db->last_query());exit();
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Edita una categoría*/
	public function edit($categoryservice){
		//new dbug($categoryservice['id']);exit();
		$this->db->trans_begin();			
		$category_data = array(
			'nombre' => $categoryservice['nombre'],
			'id' => $categoryservice['id'] ? $categoryservice['id'] : NULL
		);
		$this->db->set($category_data);
		$this->db->where('id', $categoryservice['id']);
		$this->db->update(smw_tre_cl_smartway.'.TRAZER_MAS_SERVICIO');
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Obtiene una categoría por el ID*/
	public function get($id){
		$this->db->where('id', $id);
		$query = $this->db->get(smw_tre_cl_smartway.'.TRAZER_MAS_SERVICIO');
		return $query->result();
	}

	/*Obtiene una categoría por el ID*/
	public function get_all(){
		$query = $this->db->get(smw_tre_cl_smartway.'.TRAZER_MAS_SERVICIO');
		return $query->result();
	}

	/*Lista las categorías creadas. Si $value tiene un valor filtra el listado considerando el nombre de la categoría*/
	public function find($value ="", $limit = 0, $page = NULL){	

		$this->db->select('*');		
		$this->db->limit($limit, (($page-1)*$limit));		
		$this->db->like('nombre', $value);
		$this->db->order_by('nombre');
		$query = $this->db->get(smw_tre_cl_smartway.'.TRAZER_MAS_SERVICIO');


		//$query = $this->db->get('category_service');
		$result =  $query->result();		
		foreach ($result as $key => $value) {			
			//$result[$key]['can_delete'] = $this->canDelete($value['id']);
		}
		return $result;
	}

	/*Cuenta el total de categorías creadas en el sistema */
	public function count($value = ""){
		$this->db->like('nombre', $value);				
		return $this->db->count_all_results(smw_tre_cl_smartway.'.TRAZER_MAS_SERVICIO');
	}

	/*Elimina la categoría seleccionada*/
	public function delete($category){
		$this->db->trans_begin();
		foreach ($category as $keyCategory => $category_id) {
			if ($this->canDelete($category_id)){
				$this->db->where('id', $category_id);
				$this->db->delete(smw_tre_cl_smartway.'.TRAZER_MAS_SERVICIO');	
			}			
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}

	/*Se determina si la categoría se puede eliminar en base a su relación con otras categorías y otras preguntas */
	public function canDelete($id){
		$this->db->where('id', $id);
		$this->db->or_where('question.category_id', $id);
		$this->db->join('question', 'category.category_id = question.category_id', 'LEFT');
		$result = $this->db->count_all_results(smw_tre_cl_smartway.'.TRAZER_MAS_SERVICIO');
		if($result == 0){
			return true;
		}else{
			return false;
		}
	}

	/*Obtiene las categorías padres posibles*/
	public function get_categories_dropdown(){
		$result[''] = 'Seleccione una categoría padre';
		$this->db->select('id, nombre');
                $this->db->order_by('category_service.nombre');
		$query = $this->db->get(smw_tre_cl_smartway.'.TRAZER_MAS_SERVICIO');		
		$result = $result + array_column($query->result_array(), 'nombre', 'id');
		return $result;	
	}
}
?>