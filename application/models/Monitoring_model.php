<?php
	
	class Monitoring_model extends CI_Model
	{
		
		public function __construct()
		{
			$this->load->database();
		}
		
		public function getStates()
		{
			
			$_IN = $this->getGroupUser('AA');
			
			$this->db->select(' (SELECT COUNT(id) FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT I WHERE idstatus = "Abierto"  AND id_group IN ( ' . $_IN . ' ) ) abierto,
                            (SELECT COUNT(id) FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT I WHERE idstatus = "Recibido" AND id_group IN ( ' . $_IN . ' ) ) recibido,
                            (SELECT COUNT(id) FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT I WHERE idstatus = "En Progeso" AND id_group IN ( ' . $_IN . ' ) ) progress,
                            (SELECT COUNT(id) FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT I WHERE idstatus = "En El Lugar" AND id_group IN ( ' . $_IN . ' ) ) insite,
                            (SELECT COUNT(id) FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT I WHERE idstatus = "Resuelto" AND id_group IN ( ' . $_IN . ' ) ) resuelto,
                            (SELECT COUNT(id) FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT I WHERE idstatus = "Cerrado" AND id_group IN ( ' . $_IN . ' ) ) cerrado');
			
			$query = $this->db->get('' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT I');
			
			$result = $query->result_array();
			
			
			//print_r( $this->db->last_query());
			return $result;
		}
		
		public function getSinAsignar()
		{
			$_IN = $this->getGroupUser('AA');
			
			$this->db->select('(SELECT COUNT(id) FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT WHERE iduser IS NULL AND id_group IN ( ' . $_IN . ' ) ) sinasignar,
			                     (SELECT COUNT(id) FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT WHERE iduser != "(NULL)" AND id_group IN ( ' . $_IN . ' ) ) asignados');
			
			$query = $this->db->get('' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT I');
			$result = $query->result_array();
			
			return $result;
		}
		
		public function getSla()
		{
			$_IN = $this->getGroupUser('BB');
			$this->db->select('I.id, I.incident, I.idstatus, I.title,I.address,I.response,I.datecreation,
                            I.updatetime,I.dateclose,I.iduser,I.coordX,I.coordY,I.sla,id_form, 
                            (SELECT T.headId FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_TRAZABILITY T WHERE I.incident = T.idticket
                            ORDER BY T.UpdateTime DESC LIMIT 1) 
                            HeadId,(SELECT USR.imei FROM ' . smw_tre_cl_smartway . '.TRAZER_MAS_USER USR WHERE USR.id = I.iduser LIMIT 1) imei,
                            (SELECT USR.name FROM ' . smw_tre_cl_smartway . '.TRAZER_MAS_USER USR WHERE USR.id = I.iduser LIMIT 1) name');
			$this->db->where_in("id_group ", $_IN);
			$this->db->order_by('I.idstatus ASC');
			$query = $this->db->get('' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT I');
			$result = $query->result_array();
			
			return $result;
		}
		
		public function getZoneDashInc($idzona = null, $periodo, $_CONFIG_USER)
		{
			$sql = " SELECT t2.id_zona as id_zona, t3.name as zona,
								
						SUM(datediff(now(), t1.datecreation ) < $periodo ) AS 'Total Mensual',
						SUM(datediff(now(), t1.datecreation)  < $periodo  AND ( t1.idstatus = 5 or t1.idstatus = 6 ) ) AS 'Total Cerrado',
						
						
						SUM( ( ( t1.sla_ca IS NULL OR t1.sla_ca = '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus = 5 or idstatus = 6 ) ) OR
							 ( ( t1.sla_ca  IS NOT NULL OR t1.sla_ca <> '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus = 5 or idstatus = 6 ) ) AND
							   (t1.dateclose < t1.sla_ca ))	AS 'Cerrado Dentro SLA',
							   
						SUM( ((t1.dateclose is not null and t1.dateclose <> '') and (t1.sla_ca is not null and t1.sla_ca <> '') ) AND (t1.dateclose  > t1.sla_ca  ) AND
						      (  t1.idstatus = 5 or t1.idstatus = 6  ) AND  ( datediff(now(), t1.datecreation ) < $periodo )) AS 'Cerrado Fuera SLA',
							
						SUM(datediff(now(), t1.datecreation)  < $periodo  AND ( t1.idstatus <> 5 and t1.idstatus <> 6 ) ) AS 'Total Pendiente',
						
						SUM(t1.type_incident=1 AND datediff(now(), t1.datecreation ) < $periodo ) AS 'Inc',
						SUM(t1.type_incident=2 AND datediff(now(), t1.datecreation ) < $periodo ) AS 'Req',
						SUM( ( ( t1.sla_ca IS NULL OR t1.sla_ca = '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus <> 5 and idstatus <> 6 ) ) OR
						   ( ( t1.sla_ca  IS NOT NULL OR t1.sla_ca <> '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus <> 5 and idstatus <> 6 ) ) AND
						   (t1.dateclose < t1.sla_ca ))	AS 'Dentro SLA',
						   
						SUM( ((t1.dateclose is not null and t1.dateclose <> '') and (t1.sla_ca is not null and t1.sla_ca <> '') ) AND (t1.dateclose  > t1.sla_ca  ) AND
						      (  idstatus <> 5 and idstatus <> 6  ) AND  ( datediff(now(), t1.datecreation ) < $periodo )) AS 'Fuera SLA',


						SUM(datediff(now(), t1.datecreation) < 1 ) AS 'Hoy',
						SUM(datediff(now(), t1.datecreation) > 1   AND datediff(now(), t1.datecreation) <= 3 ) AS '1 a 3 dias',
						SUM(datediff(now(), t1.datecreation) >= 4  AND datediff(now(), t1.datecreation) <= 7  ) AS '4 a 7 dias',
						SUM(datediff(now(), t1.datecreation) >= 8  AND datediff(now(), t1.datecreation) <= 14 ) AS '2 Sem',
						SUM(datediff(now(), t1.datecreation) >= 15 AND datediff(now(), t1.datecreation) <= 21 ) AS '3 Sem'
						
					FROM
						smw_tre_cl_smartway.TRAZER_DATA_INCIDENT t1
					 
						INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_GRUOP          t2 ON t1.id_group     = t2.id
						INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_ZONA           t3 ON t2.id_zona      = t3.id_zona
						LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_MAS_USER      t5 ON t1.idTec        = t5.id
						LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_DATA_EMPRESA  t6 ON t1.id_customer  = t6.id_empresa
						
					WHERE
						t1.persistent_id IS NOT NULL    ";
			
			if ($idzona){
				$sql .= " AND t2.id_zona = " . $idzona;
			}
			
			$sql .= " AND t1.id_group    IN ( ".$_CONFIG_USER['GROUP']." )
						 AND t1.id_customer IN ( ".$_CONFIG_USER['CUST'] ." )
			          GROUP BY t2.id_zona ORDER BY t3.name ASC ";
			
			$query  = $this->db->query($sql);
			$result = $query->result_array();
			
			return $result;
			
		}
		
		public function getClientDashInc($idClient = null, $periodo, $_CONFIG_USER)
		{

			$sql = " SELECT t1.id_customer as id_cust, t6.nombre_empresa as empresa,
							SUM(datediff(now(), t1.datecreation ) < $periodo ) AS 'Total Mensual',
							SUM(datediff(now(), t1.datecreation)  < $periodo  AND ( t1.idstatus = 5 or t1.idstatus = 6 ) ) AS 'Total Cerrado',
							SUM( ( ( t1.sla_ca IS NULL OR t1.sla_ca = '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus = 5 or idstatus = 6 ) ) OR
							 ( ( t1.sla_ca  IS NOT NULL OR t1.sla_ca <> '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus = 5 or idstatus = 6 ) ) AND
							   (t1.dateclose < t1.sla_ca ))	AS 'Cerrado Dentro SLA',
							SUM( ((t1.dateclose is not null and t1.dateclose <> '') and (t1.sla_ca is not null and t1.sla_ca <> '') ) AND (t1.dateclose  > t1.sla_ca  ) AND
							  (  t1.idstatus = 5 or t1.idstatus = 6  ) AND  ( datediff(now(), t1.datecreation ) < $periodo )) AS 'Cerrado Fuera SLA',
							SUM(datediff(now(), t1.datecreation)  < $periodo  AND ( t1.idstatus <> 5 and t1.idstatus <> 6 ) ) AS 'Total Pendiente',
							SUM(t1.type_incident=1 AND datediff(now(), t1.datecreation ) < $periodo ) AS 'Inc',
							SUM(t1.type_incident=2 AND datediff(now(), t1.datecreation ) < $periodo ) AS 'Req',
							SUM( ( ( t1.sla_ca IS NULL OR t1.sla_ca = '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus <> 5 and idstatus <> 6 ) ) OR
							   ( ( t1.sla_ca  IS NOT NULL OR t1.sla_ca <> '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus <> 5 and idstatus <> 6 ) ) AND
							   (t1.dateclose < t1.sla_ca ))	AS 'Dentro SLA',
							SUM( ((t1.dateclose is not null and t1.dateclose <> '') and (t1.sla_ca is not null and t1.sla_ca <> '') ) AND (t1.dateclose  > t1.sla_ca  ) AND
							  (  idstatus <> 5 and idstatus <> 6  ) AND  ( datediff(now(), t1.datecreation ) < $periodo )) AS 'Fuera SLA',
							SUM(datediff(now(), t1.datecreation) <   1 ) AS 'Hoy',
							SUM(datediff(now(), t1.datecreation) >   1   AND datediff(now(), t1.datecreation) <= 3  ) AS '1 a 3 dias',
							SUM(datediff(now(), t1.datecreation) >=  4   AND datediff(now(), t1.datecreation) <= 7  ) AS '4 a 7 dias',
							SUM(datediff(now(), t1.datecreation) >=  8   AND datediff(now(), t1.datecreation) <= 14 ) AS '2 Sem',
							SUM(datediff(now(), t1.datecreation) >= 15   AND datediff(now(), t1.datecreation) <= 21 ) AS '3 Sem'
							
						FROM
							smw_tre_cl_smartway.TRAZER_DATA_INCIDENT t1
						 
							INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_GRUOP          t2 ON t1.id_group     = t2.id
							INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_ZONA           t3 ON t2.id_zona      = t3.id_zona
							LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_MAS_USER      t5 ON t1.idTec        = t5.id
							LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_DATA_EMPRESA  t6 ON t1.id_customer  = t6.id_empresa
						WHERE
							t1.persistent_id IS NOT NULL ";
			
			if ($idClient){
				$sql .= " AND t1.id_customer = " . $idClient;
			}
			
			$sql .= " AND t1.id_group    IN  ( ". $_CONFIG_USER['GROUP'] ." )
			          AND t1.id_customer IN  ( ". $_CONFIG_USER['CUST']  ." )
						
						 GROUP BY t1.id_customer ORDER BY t6.nombre_empresa ASC  ";

			$query  = $this->db->query($sql);
			$result = $query->result_array();

			return $result;
			
		}
		
		public function getGroupDashInc($idzone, $idempresa = null, $periodo, $customer, $modo, $_CONFIG_USER = '')
		{
			//$addFilter = " INNER JOIN smw_tre_cl_smartway.TRAZER_DATA_EMPRESA  t6 ON t1.id_customer  = t6.id_empresa ";
			
			$sql = " SELECT t1.id_group, t2.name 'group',
						SUM(datediff(now(), t1.datecreation ) < $periodo ) AS 'Total Mensual',
						SUM(datediff(now(), t1.datecreation)  < $periodo  AND ( t1.idstatus = 5 or t1.idstatus = 6 ) ) AS 'Total Cerrado',
						SUM( ( ( t1.sla_ca IS NULL OR t1.sla_ca = '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus = 5 or idstatus = 6 ) ) OR
							 ( ( t1.sla_ca  IS NOT NULL OR t1.sla_ca <> '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus = 5 or idstatus = 6 ) ) AND
							   (t1.dateclose < t1.sla_ca ))	AS 'Cerrado Dentro SLA',
						SUM( ((t1.dateclose is not null and t1.dateclose <> '') and (t1.sla_ca is not null and t1.sla_ca <> '') ) AND (t1.dateclose  > t1.sla_ca  ) AND
						      (  t1.idstatus = 5 or t1.idstatus = 6  ) AND  ( datediff(now(), t1.datecreation ) < $periodo )) AS 'Cerrado Fuera SLA',
						SUM(datediff(now(), t1.datecreation)  < $periodo  AND ( t1.idstatus <> 5 and t1.idstatus <> 6 ) ) AS 'Total Pendiente',
						SUM(t1.type_incident=1 AND datediff(now(), t1.datecreation ) < $periodo ) AS 'Inc',
						SUM(t1.type_incident=2 AND datediff(now(), t1.datecreation ) < $periodo ) AS 'Req',
						SUM( ( ( t1.sla_ca IS NULL OR t1.sla_ca = '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus <> 5 and idstatus <> 6 ) ) OR
						   ( ( t1.sla_ca  IS NOT NULL OR t1.sla_ca <> '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus <> 5 and idstatus <> 6 ) ) AND
						   (t1.dateclose < t1.sla_ca ))	AS 'Dentro SLA',
						   
						SUM( ((t1.dateclose is not null and t1.dateclose <> '') and (t1.sla_ca is not null and t1.sla_ca <> '') ) AND (t1.dateclose  > t1.sla_ca  ) AND
						      (  idstatus <> 5 and idstatus <> 6  ) AND  ( datediff(now(), t1.datecreation ) < $periodo )) AS 'Fuera SLA',
						SUM(datediff(now(), t1.datecreation) < 1 ) AS 'Hoy',
						SUM(datediff(now(), t1.datecreation) > 1   AND datediff(now(), t1.datecreation) <= 3 ) AS '1 a 3 dias',
						SUM(datediff(now(), t1.datecreation) >= 4  AND datediff(now(), t1.datecreation) <= 7  ) AS '4 a 7 dias',
						SUM(datediff(now(), t1.datecreation) >= 8  AND datediff(now(), t1.datecreation) <= 14 ) AS '2 Sem',
						SUM(datediff(now(), t1.datecreation) >= 15 AND datediff(now(), t1.datecreation) <= 21 ) AS '3 Sem'
						
						FROM smw_tre_cl_smartway.TRAZER_DATA_INCIDENT t1
							
							INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_GRUOP          t2  ON t1.id_group     = t2.id
							INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_ZONA           t3  ON t2.id_zona      = t3.id_zona
							LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_MAS_USER      t5  ON t1.idTec        = t5.id
							LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_DATA_EMPRESA  t6  ON t1.id_customer  = t6.id_empresa";
			/*
			if($modo == "customer")
				$sql .=  $addFilter;
				*/
			$sql .= " WHERE t1.persistent_id IS NOT NULL ";
			
			if ($customer){
				$sql .= " AND t1.id_customer = " . $customer;
			}
			
			if ($idzone){
				$sql .= " AND t2.id_zona = " . $idzone;
			}
			$sql .= " AND datediff(now(), t1.datecreation ) < $periodo
			          AND t1.id_group    IN ( ".$_CONFIG_USER['GROUP']." )
			          AND t1.id_customer IN ( ".$_CONFIG_USER['CUST'] ." ) ";
			
			$sql .= " GROUP BY t1.id_group ORDER BY t2.name ASC ";

			$query  = $this->db->query($sql);
			$result = $query->result_array();
			
			return $result;
		}
		
		public function getTecnicoDashInc($idgroup, $idempresa = null, $periodo, $modo, $idcustomer , $_CONFIG_USER)
		{
			// $addFilter = " LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_DATA_EMPRESA  t6 ON t1.id_customer  = t6.id_empresa ";
			
			$sql = " SELECT t1.id_group, t2.name as 'group' , t5.name as  tecnico,
 
						SUM(datediff(now(), t1.datecreation ) < $periodo ) AS 'Total Mensual',
						SUM(datediff(now(), t1.datecreation)  < $periodo  AND ( t1.idstatus = 5 or t1.idstatus = 6 ) ) AS 'Total Cerrado',
						
						
						SUM( ( ( t1.sla_ca IS NULL OR t1.sla_ca = '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus = 5 or idstatus = 6 ) ) OR
							 ( ( t1.sla_ca  IS NOT NULL OR t1.sla_ca <> '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus = 5 or idstatus = 6 ) ) AND
							   (t1.dateclose < t1.sla_ca ))	AS 'Cerrado Dentro SLA',
							   
						SUM( ((t1.dateclose is not null and t1.dateclose <> '') and (t1.sla_ca is not null and t1.sla_ca <> '') ) AND (t1.dateclose  > t1.sla_ca  ) AND
						      (  t1.idstatus = 5 or t1.idstatus = 6  ) AND  ( datediff(now(), t1.datecreation ) < $periodo )) AS 'Cerrado Fuera SLA',
							
						SUM(datediff(now(), t1.datecreation)  < $periodo  AND ( t1.idstatus <> 5 and t1.idstatus <> 6 ) ) AS 'Total Pendiente',
						
						SUM(t1.type_incident=1 AND datediff(now(), t1.datecreation ) < $periodo ) AS 'Inc',
						SUM(t1.type_incident=2 AND datediff(now(), t1.datecreation ) < $periodo ) AS 'Req',
						SUM( ( ( t1.sla_ca IS NULL OR t1.sla_ca = '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus <> 5 and idstatus <> 6 ) ) OR
						   ( ( t1.sla_ca  IS NOT NULL OR t1.sla_ca <> '' )  AND  (datediff(now(), t1.datecreation ) < $periodo ) AND ( idstatus <> 5 and idstatus <> 6 ) ) AND
						   (t1.dateclose < t1.sla_ca ))	AS 'Dentro SLA',
						   
						SUM( ((t1.dateclose is not null and t1.dateclose <> '') and (t1.sla_ca is not null and t1.sla_ca <> '') ) AND (t1.dateclose  > t1.sla_ca  ) AND
						      (  idstatus <> 5 and idstatus <> 6  ) AND  ( datediff(now(), t1.datecreation ) < $periodo )) AS 'Fuera SLA',
											
						SUM(datediff(now(), t1.datecreation) < 1 ) AS 'Hoy',
						SUM(datediff(now(), t1.datecreation) > 1   AND datediff(now(), t1.datecreation) <= 3  ) AS '1 a 3 dias',
						SUM(datediff(now(), t1.datecreation) >= 4  AND datediff(now(), t1.datecreation) <= 7  ) AS '4 a 7 dias',
						SUM(datediff(now(), t1.datecreation) >= 8  AND datediff(now(), t1.datecreation) <= 14 ) AS '2 Sem',
						SUM(datediff(now(), t1.datecreation) >= 15 AND datediff(now(), t1.datecreation) <= 21 ) AS '3 Sem'
						
						FROM smw_tre_cl_smartway.TRAZER_DATA_INCIDENT t1
						
							INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_GRUOP       t2 ON t1.id_group = t2.id
							INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_ZONA        t3 ON t2.id_zona  = t3.id_zona
							LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_MAS_USER   t5 ON t1.idTec   = t5.id ";
		/*	if($modo == "customer")
				$sql .=  $addFilter;
			*/
			$sql .= " WHERE t1.persistent_id IS NOT NULL ";
			
			if ($idgroup){
				$sql .= " AND t1.id_group  = " . $idgroup;
			}
			
			if($idcustomer){
				$sql .= " AND t1.id_customer = " . $idcustomer;
			}
			
			$sql .= " AND	datediff(now(), t1.datecreation ) < $periodo
			          AND t1.id_group IN ( ".$_CONFIG_USER['GROUP']." )
						 GROUP BY t1.idTec ORDER BY t5.name ASC ";

			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			
			return $result;
		}
		
		public function getGroupUser($type)
		{
			
			// echo $type;
			
			$idUser = $this->session->userdata('id_perfil');
			
			
			$this->db->select('id_group');
			$this->db->where(" id_cordinador ", $idUser);
			
			$query = $this->db->get(" " . smw_tre_cl_smartway . ".TRAZER_GRUPO_CORDINADOR ");
			//print_r($this->db->last_query());
			$result1 = $query->result_array();
			
			
			if (!(int)$result1[0]['id_group']){
				$this->db->distinct('  id_group ');
				$this->db->select('  id_group ');
				$query = $this->db->get("" . smw_tre_cl_smartway . ".TRAZER_GRUPO_CORDINADOR ");
				$result1 = $query->result_array();
			}
			
			$IN = "";
			foreach ($result1 AS $row)
				if ((int)$row['id_group'])
					$IN .= $row['id_group'] . ", ";
			
			if ($type == 'AA'){
				// print_r($IN);
				$_IN = substr($IN, 0, -2);
				
			}
			if ($type == 'BB'){
				
				$_IN = substr($IN, 0, -2);
				$split = explode(',', $_IN);
				$_IN = $split;
			}
			//exit();
			return $_IN;
		}
		
		public function getZoneFilter($filter = "")
		{
			$result[''] = 'Seleccione una Zona';
			$this->db->select(' id_zona, name ');
			if ($filter != ""){
				$this->db->where("id", $filter);
			}
			$this->db->order_by('name ASC');
			$query = $this->db->get("" . smw_tre_cl_smartway . ".TRAZER_MAS_ZONA ");
			
			$result = $query->result_array();
			
			return $result;
		}
		
		public function getClientFilter($filter = "")
		{
			$result[''] = 'Seleccione una Cliente';
			$this->db->select(' id_empresa, nombre_empresa ');
			
			$this->db->where_in("id_empresa", $filter);
			
			$this->db->order_by('nombre_empresa ASC');
			$query = $this->db->get("" . smw_tre_cl_smartway . ".TRAZER_DATA_EMPRESA ");
			
			$result = $query->result_array();
			
			return $result;
		}
		
		public function getClient($_CONFIG_USER)
		{
			$sql = " SELECT id_empresa, nombre_empresa
					   FROM  ". smw_tre_cl_smartway .".TRAZER_DATA_EMPRESA
					   WHERE
					   id_empresa IN ( ".$_CONFIG_USER['CUST']." )
						ORDER BY nombre_empresa ";
					$query  = $this->db->query($sql);
			$result = $query->result_array();

			return $result;
		}
		
		public function getZone($filter = "")
		{
			
			$this->db->select(' id_zona, name ');
			if ($filter != ""){
				$this->db->where("id", $filter);
			}
			$this->db->order_by('name ASC');
			$query = $this->db->get("" . smw_tre_cl_smartway . ".TRAZER_MAS_ZONA ");
			$result = $query->result_array();
			
			return $result;
		}
		
		public function getGrupoResolutor($filter = "")
		{
			
			$this->db->select(' id, name, description, id_zona, external_id ');
			
			if ($filter != ""){
				$this->db->where("id", $filter);
			}
			$this->db->order_by('name ASC');
			$query = $this->db->get("" . smw_tre_cl_smartway . ".TRAZER_MAS_GRUOP ");
			
			$result = $query->result_array();
			
			
			return $result;
		}

		public function getInitialReport($fechaini,$fechafinal,$idtec)
		{	

			$sql = "SELECT DATE_FORMAT(U.datecreation, '%Y-%m-%d') AS fecha,COUNT(DISTINCT U.id) AS cantidad FROM smw_tre_cl_smartway.TRAZER_DATA_INCIDENT U INNER JOIN  smw_tre_cl_smartway.TRAZER_MAS_USER A ON U.idTec = A.id WHERE DATE_FORMAT(datecreation, '%Y-%m-%d') <= '$fechafinal' AND DATE_FORMAT(datecreation, '%Y-%m-%d') >= '$fechaini' AND U.idTec=$idtec	GROUP BY DATE_FORMAT(datecreation, '%Y-%m-%d') ORDER BY U.datecreation DESC";

			$query = $this->db->query($sql);
			$result = $query->result_array();

			return $result;
		}


		public function getTecnicos()
		{
			$sql = "SELECT id,numberTruck,name FROM smw_tre_cl_smartway.TRAZER_MAS_USER";

			$query = $this->db->query($sql);
			$result = $query->result_array();

			return $result;
		}
		
	}

?>	