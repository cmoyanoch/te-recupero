<?php

class Sla_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	/*Guarda una nueva categoría*/
	public function create($sla){
		$this->db->trans_begin();			
		$sla_data = array(
			'name' => $sla['name'],
			'empresa' => $sla['empresa'],
			'contrato' => $sla['contrato'],
			'description' => $sla['description'] ? $sla['description'] : NULL
		);
		$this->db->insert('sla' ,$sla_data);
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Edita una categoría*/
	public function edit($sla){
		$this->db->trans_begin();	

		$sla_data = array(
			'name' => $sla['name'],
			'description' => $sla['description']
			
		);
		$this->db->set($sla_data);
		$this->db->where('id', $sla['id']);
		$this->db->update('sla');
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Obtiene una categoría por el ID*/
	public function get($id){
		$this->db->where('id', $id);
		$query = $this->db->get('sla');
		return $query->row_array();
	}

	/*Lista las categorías creadas. Si $value tiene un valor filtra el listado considerando el nombre de la categoría*/
	public function find($value ="", $limit = 0, $page = NULL){	
		$this->db->select('sla.id, sla.name, sla.description, sla.empresa, sla.contrato');		
		$this->db->limit($limit, (($page-1)*$limit));		
		$this->db->like('sla.name', $value);
		// $this->db->join('category categoryB', 'categoryA.cat_category_id = categoryB.category_id', 'LEFT');
		$this->db->order_by('sla.description');
		$query = $this->db->get('sla');
		$result =  $query->result_array();		
		foreach ($result as $key => $value) {			
			$result[$key]['can_delete'] = $this->canDelete($value['category_id']);
		}

		return $result;
	}

	/*Cuenta el total de categorías creadas en el sistema */
	public function count($value = ""){
		$this->db->like('name', $value);				
		return $this->db->count_all_results('sla');
	}

	/*Elimina la categoría seleccionada*/
	public function delete($category){
		$this->db->trans_begin();
		foreach ($category as $keyCategory => $category_id) {
			if ($this->canDelete($category_id)){
				$this->db->where('category_id', $category_id);
				$this->db->delete('category');	
			}			
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}

	/*Se determina si la categoría se puede eliminar en base a su relación con otras categorías y otras preguntas */
	public function canDelete($id){
		$this->db->where('cat_category_id', $id);
		$this->db->or_where('question.category_id', $id);
		$this->db->join('question', 'category.category_id = question.category_id', 'LEFT');
		$result = $this->db->count_all_results('category');
		if($result == 0){
			return true;
		}else{
			return false;
		}
	}

	/*Obtiene las categorías padres posibles*/
	public function get_categories_dropdown(){
		$result[''] = 'Seleccione una categoría padre';
		$this->db->select('category_id, name');
                $this->db->order_by('category.name');
		$query = $this->db->get('category');		
		$result = $result + array_column($query->result_array(), 'name', 'category_id');
		return $result;	
	}
	
}

?>