<?php

class Priority_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	public function getData() {
		$this->db->order_by('CAST( SUBSTRING(NAME,1,LENGTH(NAME)-1) AS INTEGER) ');
		$query = $this->db->get(smw_tre_cl_smartway.'.TRAZER_MAS_PRIORITY');
		$result =  $query->result_array();	
		return $result;
	}

	public function updateData($id, $name) {
		$this->db->trans_begin();
			
		$query = $this->db->query("UPDATE " . smw_tre_cl_smartway . ".TRAZER_MAS_PRIORITY
            SET name = '{$name}' WHERE id = $id");

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('success' => 0, "message" => "Error al actualizar registro");
		}
		$this->db->trans_commit();
		return array('success' => 1, "message" => "Registro actualizado con exito");
	}
	

	public function deleteData($id) {
		$this->db->trans_begin();

		$query = $this->db->query("DELETE FROM " . smw_tre_cl_smartway . ".TRAZER_MAS_PRIORITY WHERE id = $id");

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('success' => 0, "message" => "Error al eliminar registro");
		}
		$this->db->trans_commit();
		return array('success' => 1, "message" => "Registro eliminado con exito");
	}

	public function createData($name) {
		$this->db->trans_begin();

		$query = $this->db->query("INSERT INTO " . smw_tre_cl_smartway . ".TRAZER_MAS_PRIORITY (NAME) values('{$name}')");

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('success' => 0, "message" => "Error al insertar registro");
		}
		$this->db->trans_commit();
		return array('success' => 1, "message" => "Registro insertado con exito");
	}
}

?>