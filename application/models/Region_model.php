<?php

class Region_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	/*Guarda una nueva categoría*/
	public function create($region){
		$this->db->trans_begin();			
		$region_data = array(
			'name' => $region['name']
		);
		$this->db->insert('region' ,$region_data);
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Edita una categoría*/
	public function edit($region){
		$this->db->trans_begin();			
		$region_data = array(
			'name' => $region['name']
		);
		$this->db->set($region_data);
		$this->db->where('region_id', $region['region_id']);
		$this->db->update('region');
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Obtiene una categoría por el ID*/
	public function get($id){
		$this->db->where('region_id', $id);
		$query = $this->db->get('region');
		return $query->row_array();
	}

	/*Lista las categorías creadas. Si $value tiene un valor filtra el listado considerando el nombre de la categoría*/
	public function find($value ="", $limit = 0, $page = NULL){		
		$this->db->select('regionA.region_id, regionA.name');		
		$this->db->limit($limit, (($page-1)*$limit));		
		$this->db->like('regionA.name', $value);
		$this->db->order_by('regionA.name');
		$query = $this->db->get('region regionA');
		$result =  $query->result_array();		
		foreach ($result as $key => $value) {			
			$result[$key]['can_delete'] = $this->canDelete($value['region_id']);
		}
		return $result;
	}

	/*Cuenta el total de categorías creadas en el sistema */
	public function count($value = ""){
		$this->db->like('name', $value);				
		return $this->db->count_all_results('region');
	}

	/*Elimina la categoría seleccionada*/
	public function delete($region){
		$this->db->trans_begin();
		foreach ($region as $keyCategory => $region_id) {
			if ($this->canDelete($region_id)){
				$this->db->where('region_id', $region_id);
				$this->db->delete('region');	
			}			
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}

	/*Se determina si la categoría se puede eliminar en base a su relación con otras categorías y otras preguntas */
	public function canDelete($id){
                //TMP
                return true;
                //TMP
		$this->db->where('cat_region_id', $id);
		$this->db->or_where('question.region_id', $id);
		$this->db->join('question', 'region.region_id = question.region_id', 'LEFT');
		$result = $this->db->count_all_results('region');
		if($result == 0){
			return true;
		}else{
			return false;
		}
	}
	
}

?>