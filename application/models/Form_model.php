<?php

class Form_model extends CI_Model {

	public function __construct(){
		$this->load->database();

	}

	/*Guarda en la base de datos la información del formulario creado */
	public function create($form){						
		$this->db->trans_begin();
		$user = $this->ion_auth->user()->row();			
		//Se registran los datos del formulario
		$form_data = array(
			'created_date' => date("Y-m-d H:i:s"),
			'user_id' => $user->id,
			'modificated_date' => NULL,
			'active' => FALSE,
			'title' => ucfirst($form['title']),	
			'description' => ucfirst($form['description'])//,
                        //'company_id' => (int)$form["company_id"],
                        //'country_id' => (int)$form["country_id"]
		);
		$this->db->insert('form' ,$form_data);
		$form_id = $this->db->insert_id();
		//Se registran los datos generales de cada formulario
		$section_ids = array();
		foreach ($form['section'] as $keySection => $section) {
			$section_data = array(
				'form_id' => $form_id
			);
			$this->db->insert('section', $section_data);
			$section_ids[$keySection] = $this->db->insert_id();
		}
		//Se registran los datos de la pregunta
		$question_ids = array();			
		foreach ($form['section'] as $keySection => $section) {	
			$question_position = 1;			
			foreach ($form['section'][$keySection]['question'] as $keyQuestion => $question) {					
				$question_data = array(
					'category_id'=> $question['category_id'] != '' ? $question['category_id'] : NULL,
					'input_type_id' => $question['input_type_id'],
					'section_id' => $section_ids[$keySection],
					'position' => $question_position,
					'text' => $question['text'],
					'required' => isset($question['required']) ? TRUE: FALSE,
                                        'question_type_id'=> $question['question_type_id'] != '' ? $question['question_type_id'] : NULL,
				);
				$question_position ++;
				$this->db->insert('question', $question_data);
				$question_ids[$keyQuestion] = $this->db->insert_id();
				//Si la pregunta posee restricciones
				if(isset($form['section'][$keySection]['question'][$keyQuestion]['restriction'])){
					foreach ($form['section'][$keySection]['question'][$keyQuestion]['restriction'] as $keyRestriction => $restriction) {
						if($keyRestriction != 'section'){
							$restriction_data = array(
								'question_id'=> $question_ids[$keyQuestion],
								'restriction_id' => $keyRestriction,
								'value'=> $restriction
							);
							$this->db->insert('restriction_question', $restriction_data);
						//Si dentro de las restricciones se encuentra el redireccionamiento a una sección.
						}else{
							$section_question_data = array(
								'question_id'=> $question_ids[$keyQuestion],
								'section_id' => $section_ids[$restriction]
							);
							$this->db->insert('section_question', $section_question_data);	
						}
					}	
				}
				if(isset($form['section'][$keySection]['question'][$keyQuestion]['option'])){
					foreach ($form['section'][$keySection]['question'][$keyQuestion]['option'] as $keyOption => $option){
						$option_data = array(									
							'question_id' => $question_ids[$keyQuestion],
							'text' => $option['text']
						);
						$this->db->insert('option', $option_data);
						if($option['section'] != ''){
							$section_option_data = array(
								'option_id'=> $this->db->insert_id(),
								'section_id' => $section_ids[$option['section']]
							);
							$this->db->insert('section_option', $section_option_data);
						}						
					}
				}					
			}
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Obtiene la fecha de modificación del formulario*/
	public function getCreatedDate($id){
		$this->db->select("created_date");
		$this->db->where('form_id',$id);
		$queryForm = $this->db->get('form');
		return $queryForm->row_array();
	}

	/* Edita los datos de un formulario */
	public function edit($id, $form){
		$this->db->trans_begin();		
		$aux = $this->getCreatedDate($id);
		$created_date = $aux["created_date"];
		$user = $this->ion_auth->user()->row();		

		//Se registran los nuevos datos del formulario		
		$form_data = array(
			'user_id' => $user->id,
			'modificated_date' => date("Y-m-d H:i:s"),
			'active' => FALSE,
			'title' => ucfirst($form['title']),	
			'description' => ucfirst($form['description'])
		);
                $this->db->where('form_id', $id);
                $this->db->update('form' ,$form_data);
                $form_id = $id;

		//Se registran los datos generales de cada formulario
		$section_ids = array();
		foreach ($form['section'] as $keySection => $section) {
			$section_data = array(
				'form_id' => $form_id
			);
                        $insertChk = false;
                        $query = $this->db->query("SELECT section_id FROM ".DYNAMIC_FORMS.".section WHERE form_id = {$form_id} AND section_id = {$keySection};");
			$chkArr = $query->result_array();
                        if(!isset($chkArr[0]))
                            $insertChk = true;
                        foreach ($query->result_array() as $row) {
                            if(!(int)$row["section_id"])
                                $insertChk = true;
                        }
                        if($insertChk) {
                            $this->db->insert('section', $section_data);
                            $section_ids[$keySection] = $this->db->insert_id();
                        } else {
                            $section_ids[$keySection] = $keySection;
                        }

		}
                
		//Se registran los datos de la pregunta
		$question_ids = array();			
		foreach ($form['section'] as $keySection => $section) {	
			$question_position = 1;			
			foreach ($form['section'][$keySection]['question'] as $keyQuestion => $question) {					
				$question_data = array(
					'category_id'=> $question['category_id'] != '' ? $question['category_id'] : NULL,
					'input_type_id' => $question['input_type_id'],
					'section_id' => $section_ids[$keySection],
					'position' => $question_position,
					'text' => $question['text'],
					'required' => isset($question['required']) ? TRUE: FALSE,
                                        'question_type_id'=> $question['question_type_id'] != '' ? $question['question_type_id'] : NULL,
				);
                                
                                $insertChk = false;
                                $query = $this->db->query("SELECT question_id FROM ".DYNAMIC_FORMS.".question WHERE section_id = {$keySection} AND question_id = {$keyQuestion};");
                                $chkArr = $query->result_array();
                                if(!isset($chkArr[0]))
                                    $insertChk = true;
                                foreach ($query->result_array() as $row) {
                                    if(!(int)$row["question_id"])
                                        $insertChk = true;
                                }
                                if($insertChk) {
                                    $query = $this->db->query("SELECT MAX(question_id) 'maxId' FROM ".DYNAMIC_FORMS.".question;");
                                    foreach ($query->result_array() as $row) {
                                        $maxId = $row["maxId"];
                                    }
                                    $question_data["question_id"] = $maxId+1;
                                    $this->db->insert('question', $question_data);
                                    $question_ids[$keyQuestion] = $this->db->insert_id();
                                } else {
                                    $this->db->where('question_id', $keyQuestion);
                                    $this->db->update(DYNAMIC_FORMS.'.question', $question_data);
                                    $question_ids[$keyQuestion] = $keyQuestion;
                                }
				$question_position ++;
				
				//Si la pregunta posee restricciones
				if(isset($form['section'][$keySection]['question'][$keyQuestion]['restriction'])){
					foreach ($form['section'][$keySection]['question'][$keyQuestion]['restriction'] as $keyRestriction => $restriction) {
						if($keyRestriction != 'section'){
							$restriction_data = array(
								'question_id'=> $question_ids[$keyQuestion],
								'restriction_id' => $keyRestriction,
								'value'=> $restriction
							);
                                                        
                                                        $insertChk = false;
                                                        $restriction_question_id = 0;
                                                        $query = $this->db->query("SELECT restriction_question_id FROM ".DYNAMIC_FORMS.".restriction_question WHERE question_id = {$question_ids[$keyQuestion]} AND restriction_id = {$keyRestriction};");
                                                        $chkArr = $query->result_array();
                                                        if(!isset($chkArr[0]))
                                                            $insertChk = true;
                                                        foreach ($query->result_array() as $row) {
                                                            if(!(int)$row["restriction_question_id"]) {
                                                                $insertChk = true;
                                                            } else {
                                                                $restriction_question_id = $row["restriction_question_id"];
                                                            }
                                                        }
                                                        if($insertChk) {
                                                            $this->db->insert('restriction_question', $restriction_data);
                                                        } else {
                                                            $this->db->where('restriction_question_id', $restriction_question_id);
                                                            $this->db->update(DYNAMIC_FORMS.'.restriction_question', $restriction_data);
                                                        }
						//Si dentro de las restricciones se encuentra el redireccionamiento a una sección.
						}else{
							
							$section_question_data = array(
								'question_id'=> $question_ids[$keyQuestion],
								'section_id' => $section_ids[$restriction]
							);
                                                        $insertChk = false;
                                                        $section_question_id = 0;
                                                        $query = $this->db->query("SELECT section_question_id FROM ".DYNAMIC_FORMS.".section_question WHERE question_id = {$question_ids[$keyQuestion]} AND section_id = {$section_ids[$restriction]};");
                                                        $chkArr = $query->result_array();
                                                        if(!isset($chkArr[0]))
                                                            $insertChk = true;
                                                        foreach ($query->result_array() as $row) {
                                                            if(!(int)$row["section_question_id"]) {
                                                                $insertChk = true;
                                                            } else {
                                                                $section_question_id = $row["section_question_id"];
                                                            }
                                                        }
                                                        if($insertChk) {
                                                            $this->db->insert('section_question', $section_question_data);	
                                                        } else {
                                                            $this->db->where('section_question_id', $section_question_id);
                                                            $this->db->update(DYNAMIC_FORMS.'.section_question', $section_question_data);
                                                        }
						}
					}	
				}

				if(isset($form['section'][$keySection]['question'][$keyQuestion]['option'])){
					foreach ($form['section'][$keySection]['question'][$keyQuestion]['option'] as $keyOption => $option){
						$option_data = array(									
							'question_id' => $question_ids[$keyQuestion],
							'text' => $option['text']
						);
                                                $insertChk = false;
                                                $option_id = 0;
                                                $query = $this->db->query("SELECT option_id FROM ".DYNAMIC_FORMS.".option WHERE question_id = {$question_ids[$keyQuestion]} AND option_id = {$keyOption}");
                                                $chkArr = $query->result_array();
                                                if(!isset($chkArr[0]))
                                                    $insertChk = true;
                                                foreach ($query->result_array() as $row) {
                                                    if(!(int)$row["option_id"]) {
                                                        $insertChk = true;
                                                    } else {
                                                        $option_id = $row["option_id"];
                                                    }
                                                }
                                                if($insertChk) {
                                                    $this->db->insert('option', $option_data);
                                                    $option_id = $this->db->insert_id();
                                                } else {
                                                    $this->db->where('option_id', $option_id);
                                                    $this->db->update(DYNAMIC_FORMS.'.option', $option_data);
                                                }
                                                
						
						if($option['section'] != ''){
							$section_option_data = array(
								'option_id'=> $option_id,
								'section_id' => $section_ids[$option['section']]
							);
                                                        
                                                        $insertChk = false;
                                                        $section_option_id = 0;
                                                        $query = $this->db->query("SELECT section_option_id FROM ".DYNAMIC_FORMS.".section_option WHERE option_id = {$option_id} AND section_id = {$section_ids[$option['section']]}");
                                                        $chkArr = $query->result_array();
                                                        if(!isset($chkArr[0]))
                                                            $insertChk = true;
                                                        foreach ($query->result_array() as $row) {
                                                            if(!(int)$row["section_option_id"]) {
                                                                $insertChk = true;
                                                            } else {
                                                                $section_option_id = $row["section_option_id"];
                                                            }
                                                        }
                                                        if($insertChk) {
                                                            $this->db->insert('section_option', $section_option_data);
                                                        } else {
                                                            $this->db->where('section_option_id', $section_option_id);
                                                            $this->db->update(DYNAMIC_FORMS.'.section_option', $section_option_data);
                                                        }
						}						
					}
				}					
			}
		}

		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Lista los formularios creados. Si $value tiene un valor filtra el listado considerando el título, la descripción, el usaurio o el estado (activo o inactivo)*/
	public function find($value ="", $limit = 0, $page = NULL){		
		$this->db->select('form.form_id, DATE_FORMAT(form.created_date, "%d-%m-%Y") as created, DATE_FORMAT(form.modificated_date, "%d-%m-%Y") as modificated, form.title, form.description, IF(form.active = 1, "Activo", "Inactivo") as active, CONCAT_WS(" ", user.first_name, user.last_name) as user');		
		$this->db->limit($limit, (($page-1)*$limit));
		$like = array('title' => $value, 'description' => $value, 'user.first_name'=> $value, 'user.last_name' => $value);
		$this->db->or_like($like);
		if (strpos($value, 'activo') !== FALSE && $value != 'inactivo')  {
			$this->db->or_like('form.active', 1);
		}
		if (strpos($value, 'inactivo') !== FALSE) {
			$this->db->or_like('form.active', 0);
		}
		$this->db->join('user', 'user.user_id = form.user_id', 'LEFT');
		$this->db->order_by('active ASC,created_date DESC, modificated_date DESC');
		$query = $this->db->get('form');
		return $query->result_array();
	}
	
	
	/*Obtiene toda la información de un formulario*/
	public function get($id){
		//Se obtiene la información básica del formulario
		$this->db->select('form_id , title, description'); // , company_id, country_id
		$this->db->where('form_id',$id);
		$queryForm = $this->db->get('form');
		$form['form'] = $queryForm->row_array();		
		//Se obtienen las secciones del formulario
		$this->db->select('section_id as section_index');
		$this->db->where('form_id',$id);
                $this->db->where('status',1);
		$querySection = $this->db->get('section');
		$queryResultSection = $querySection->result_array();
		$form['form']['section'] = array_column($queryResultSection, null, 'section_index');
		foreach ($form['form']['section'] as $keySection => $section) {
			$this->db->select('question_id, text, required, input_type_id, category_id, question_type_id');
			$this->db->where('section_id', $section['section_index']);
                        $this->db->where('status', 1);
                        $this->db->order_by('position', 'ASC');
			$queryQuestion = $this->db->get('question');
			$queryResultQuestion = $queryQuestion->result_array();
			$form['form']['section'][$keySection]['question'] =  array_column($queryResultQuestion, null, 'question_id');
			foreach ($form['form']['section'][$keySection]['question'] as $keyQuestion => $question) {
				//Se incluyen las opciones
				$this->db->select('option.option_id, text, section_option.section_id as section');
				$this->db->where('question_id', $question['question_id']);
                                $this->db->where('status', 1);
				$this->db->join('section_option', 'option.option_id = section_option.option_id', 'LEFT');
				$queryOption = $this->db->get('option');
				$queryOptionResult = $queryOption->result_array();
				if($queryOptionResult){
					$form['form']['section'][$keySection]['question'][$keyQuestion]['option'] = array_column($queryOptionResult, null,'option_id');
				}
				//Se incluyen las restricciones
				$this->db->select('restriction_id, value');
				$this->db->where('question_id', $question['question_id']);								
				$queryRestriction = $this->db->get('restriction_question');
				$queryRestrictionResult = $queryRestriction->result_array();				
				if($queryRestrictionResult){
					$form['form']['section'][$keySection]['question'][$keyQuestion]['restriction'] = array_column($queryRestrictionResult, 'value', 'restriction_id');
					$this->db->select('section_id');
					$this->db->where('question_id', $question['question_id']);
					$querySectionRestriction = $this->db->get('section_question');
					$querySectionRestrictionResult = $querySectionRestriction->row_array();
					if($querySectionRestrictionResult){
						$form['form']['section'][$keySection]['question'][$keyQuestion]['restriction']['section']= $querySectionRestrictionResult['section_id'];
					}

				}				
			}
		}
		return $form;
		
	}

	/*Cuenta el total de formularios creados en el sistema */
	public function count($value = ""){
		$like = array('title' => $value, 'description' => $value, 'user.first_name'=> $value, 'user.last_name' => $value);
		$this->db->or_like($like);
		if (strpos($value, 'activo') !== FALSE && $value != 'inactivo')  {
			$this->db->or_like('form.active', 1);
		}
		if (strpos($value, 'inactivo') !== FALSE) {
			$this->db->or_like('form.active', 0);
		}
		$this->db->join('user', 'user.user_id = form.user_id', 'LEFT');
		return $this->db->count_all_results('form');
	}

	/*Cambia el estado de uno o varios formularios a activo*/
	public function activate($form){
		$this->db->trans_begin();
		foreach ($form as $keyForm => $form_id) {
			$this->db->set('active', TRUE);
			$this->db->where('form_id', $form_id);
			$this->db->update('form');
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}

	/*Cambia el estado de uno o varios formularios a inactivo */
	public function deactivate($form){
		$this->db->trans_begin();
		foreach ($form as $keyForm => $form_id) {
			$this->db->set('active', FALSE);
			$this->db->where('form_id', $form_id);
			$this->db->update('form');
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}

	/*Obtiene los tipos de input posibles para un campo*/
	public function get_input_types_dropdown(){				
		$result = array(''=>'Seleccione un tipo de input');
		$query = $this->db->get('input_type');			
		$result = $result + array_column($query->result_array(), 'name', 'input_type_id');		
		return $result;
	}
        
        /*Obtiene los tipos de input posibles para un campo*/
	public function get_countries_dropdown(){				
		$result = array(''=>'Seleccione un país');
		$query = $this->db->get('country');			
		$result = $result + array_column($query->result_array(), 'name', 'country_id');		
		return $result;
	}
        
        /*Obtiene los tipos de input posibles para un campo*/
	public function get_companies_dropdown(){				
		$result = array(''=>'Seleccione una empresa');
		$query = $this->db->get('company');			
		$result = $result + array_column($query->result_array(), 'name', 'company_id');		
		return $result;
	}

	/*Obtiene las categorías posible de una pregunta*/
	public function get_categories_dropdown(){
		$result[''] = 'Seleccione una categoría';
		$this->db->select('category_id, name');
                $this->db->order_by('category.name');
		$query = $this->db->get('category');		
		$result = $result + array_column($query->result_array(), 'name', 'category_id');
		return $result;	
	}
        
        /*Obtiene los tipos asociados a una posible pregunta*/
	public function get_question_type_dropdown(){
		$result[''] = 'Seleccione un tipo';
		$this->db->select('question_type_id, name');
                $this->db->order_by('question_type.name');
		$query = $this->db->get('question_type');		
		$result = $result + array_column($query->result_array(), 'name', 'question_type_id');
		return $result;	
	}

	/* Determina si un formulario es editable */
	public function isEditable($id){
		$this->db->select('active');
		$this->db->where('form_id', $id);
		$query = $this->db->get('form');
		$result = $query->row_array();		
                $result = $result['active'];
		if ($result){
			return false;
		}else{
			return true;
		}
	}

        public function deleteOpt($optId, $queId) {
		$opt_data = array(
			'status' => 0
		);
                $this->db->where('option_id', $optId);
                $this->db->where('question_id', $queId);
                $this->db->update('option' ,$opt_data);
	}
        
        public function deleteQue($queId, $secId) {
		$opt_data = array(
			'status' => 0
		);
                $this->db->where('question_id', $queId);
                $this->db->where('section_id', $secId);
                $this->db->update('question' ,$opt_data);
                
	}
        
        public function deleteSec($secId) {
		$opt_data = array(
			'status' => 0
		);
                $this->db->where('section_id', $secId);
                $this->db->update('section' ,$opt_data);
                
	}
}

?>