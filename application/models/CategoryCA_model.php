<?php
/**
 * Created by PhpStorm.
 * User: soporte
 * Date: 27-02-18
 * Time: 11:43
 */

Class CategoryCA_model Extends CI_Model{

    /**
     * CategoryCA_model constructor.
     */
    public function __construct(){
        $this->load->database();
    }

    public function getListCategoryCA(){
        $sql = "SELECT id, name, name_short, hour, Status, CreateTime, UpdateTime 
                FROM  smw_tre_cl_smartway.TRAZER_MAS_CATEGORY_CA 
                WHERE 
                Status = 1 ";
        $result = $this->db->query($sql)->result_array();

        return $result;
    }
}