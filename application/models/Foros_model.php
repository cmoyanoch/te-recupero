<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Foros_model extends CI_Model {

private $bd_;

  function __construct()
  {
    // Call the Model constructor
    parent::__construct();
    
  }


  public function ObtieneListaForos($fechaInicio, $fechaFinal, $sumador, $titulo){	
  	$num1 = 0;
	$num2 = PAGINADO_CANT;
	$num1 = $num1 + $sumador;
	$num2 = $num2 + $sumador;

	try{					


          $query1 = '
                    SELECT id, Nombre, title, description, DATETIME AS fecha , datetimeLastAnswer, countAnswer   
			FROM
			  (
			      (
			      SELECT df.id, concat(us.first_name , " " , us.last_name) AS Nombre, df.title, df.description, df.datetime, df.datetimeLastAnswer, df.countAnswer 
			      FROM ' . smw_tre_cl_smartway . '.data_forum df 
			      JOIN ' . DYNAMIC_FORMS . '.users us ON us.id = df.iduser	
			      WHERE df.state = 1
			      ';

 				$query2 = '';
			      if($fechaInicio != '0'){
					 $query2 = ' df.datetime > "'.$fechaInicio.' 00:00:00" and';
			      }

			      if($fechaFinal != '0'){
					 $query2 .= ' df.datetime < "'.$fechaFinal.' 23:59:59" and';
			      }

			      if($titulo != '0'){
					 $query2 .= ' df.title like "%'.$titulo.'%" and';
			      }


			      if($fechaInicio != '0' || $fechaFinal != '0' || $titulo != '0'){
					$query1 .= 'and  '.$query2;
			        $query1 = substr($query1, 0, -3); 
			      }



		 $query1 .= '
			      )
			      UNION ALL
			      (
			      SELECT df.id, us.name AS Nombre, df.title, df.description, df.datetime, df.datetimeLastAnswer, df.countAnswer 
			      FROM ' . smw_tre_cl_smartway . '.data_forum df 
			      JOIN ' . smw_tre_cl_smartway . '.TRAZER_MAS_USER us ON us.imei = df.iduser
			      WHERE df.state = 1
			      ';


 				$query3 = '';
			      if($fechaInicio != '0'){
					 $query3 = ' df.datetime > "'.$fechaInicio.' 00:00:00" and';
			      }

			      if($fechaFinal != '0'){
					 $query3 .= ' df.datetime < "'.$fechaFinal.' 23:59:59" and';
			      }

			      if($titulo != '0'){
					 $query3 .= ' df.title like "%'.$titulo.'%" and';
			      }


			      if($fechaInicio != '0' || $fechaFinal != '0' || $titulo != '0'){
					$query1 .= 'and  '.$query3;
			        $query1 = substr($query1, 0, -3); 
			      }



			if($sumador==0){
				$query1 .= '
			     		 )
				 	 ) 
					AS a ORDER BY fecha DESC LIMIT 10
		 		 ';
			}else{
				$query1 .= '
			     		 )
				 	 ) 
					AS a ORDER BY fecha DESC LIMIT '.$num1.' , 10 
		 		 ';
			}

// print_r($query1);

		$query = $this->db->query($query1);      
                $querys = $query->result_array() ; 

		return $querys;

	}catch(Exception $e){
		throw new Exception($e->getMessage(), 500);		
	}
}




public function ObtieneCantListaForos($fechaInicio, $fechaFinal, $titulo){

	try{					

          $query1 = '
                    SELECT count(1) as cantReg 
			FROM
			  (
			      (
			      SELECT df.id, concat(us.first_name , " " , us.last_name) AS Nombre, df.title, df.description, df.datetime, df.datetimeLastAnswer, df.countAnswer 
			      FROM ' . smw_tre_cl_smartway . '.data_forum df 
			      JOIN ' . DYNAMIC_FORMS . '.users us ON us.id = df.iduser	
			      WHERE df.state = 1
			      ';

 				$query2 = '';
			      if($fechaInicio != '0'){
					 $query2 = ' df.datetime > "'.$fechaInicio.' 00:00:00" and';
			      }

			      if($fechaFinal != '0'){
					 $query2 .= ' df.datetime < "'.$fechaFinal.' 23:59:59" and';
			      }

			      if($titulo != '0'){
					 $query2 .= ' df.title like "%'.$titulo.'%" and';
			      }


			      if($fechaInicio != '0' || $fechaFinal != '0' || $titulo != '0'){
					$query1 .= 'and  '.$query2;
			        $query1 = substr($query1, 0, -3); 
			      }



		 $query1 .= '
			      )
			      UNION ALL
			      (
			      SELECT df.id, us.name AS Nombre, df.title, df.description, df.datetime, df.datetimeLastAnswer, df.countAnswer 
			      FROM ' . smw_tre_cl_smartway . '.data_forum df 
			      JOIN ' . smw_tre_cl_smartway . '.TRAZER_MAS_USER us ON us.imei = df.iduser
			      WHERE df.state = 1
			      ';


 				$query3 = '';
			      if($fechaInicio != '0'){
					 $query3 = ' df.datetime > "'.$fechaInicio.' 00:00:00" and';
			      }

			      if($fechaFinal != '0'){
					 $query3 .= ' df.datetime < "'.$fechaFinal.' 23:59:59" and';
			      }

			      if($titulo != '0'){
					 $query3 .= ' df.title like "%'.$titulo.'%" and';
			      }


			      if($fechaInicio != '0' || $fechaFinal != '0' || $titulo != '0'){
					$query1 .= 'and  '.$query3;
			        $query1 = substr($query1, 0, -3); 
			      }

				$query1 .= ' )	) AS a';

// print_r($query1);

		$query = $this->db->query($query1);      

        $querys = $query->row_array() ; 




		return $querys;


	}catch(Exception $e){

		throw new Exception($e->getMessage(), 500);
		

	}
}





public function EliminaForos_model($id){

	$res = 0;

	try{

		$data= array( 'state' => $res );
	  	$this->db->where('id = "'.$id.'"');

	  	if($this->db->update('' . smw_tre_cl_smartway . '.data_forum', $data)){
	  		return $res= 1;
		}else{
			return print_r($res);
		}

	}catch(Exception $e){
		throw new Exception($e->getMessage(), 500);			
	}

}


public function EliminaRespForos_model($id){

	$res = 0;

	try{

		$data= array( 'state' => $res );
	  	$this->db->where('id = "'.$id.'"');

	  	if($this->db->update('' . smw_tre_cl_smartway . '.data_forum', $data)){
	  		return $res= 1;
		}else{
			return print_r($res);
		}

	}catch(Exception $e){
		throw new Exception($e->getMessage(), 500);			
	}

}





    public function ObtieneForo($id){	

          try{


            $query1 = '
                SELECT id, Nombre, title, description, DATETIME AS fecha , countAnswer   
                  FROM
                    (
                    (
                    SELECT df.id, concat(us.first_name , " " , us.last_name) AS Nombre, df.title, df.description, df.datetime, df.datetimeLastAnswer, df.countAnswer 
                    FROM ' . smw_tre_cl_smartway . '.data_forum df 
                    JOIN ' . DYNAMIC_FORMS . '.users us ON us.id = df.iduser	
                    where df.id = "'.$id.'" 
                    )
                    UNION ALL
                    (
                    SELECT df.id, us.name AS Nombre, df.title, df.description, df.datetime, df.datetimeLastAnswer, df.countAnswer 
                    FROM ' . smw_tre_cl_smartway . '.data_forum df 
                    JOIN ' . smw_tre_cl_smartway . '.TRAZER_MAS_USER us ON us.imei = df.iduser
                    where df.id = "'.$id.'")  ) AS a 
                    ';

                        // print_r($query1);
          $query = $this->db->query($query1);      

          $querys = $query->result_array() ; 
          return $querys;


          }catch(Exception $e){

                  throw new Exception($e->getMessage(), 500);


          }
  }



	public function EditarForo($id,$titulo,$campoObs,$user_id,$fecha){
		$res = 'NOK';

		try{

  			$data= array(
                                    'title' => $titulo,
                                    'description' => $campoObs,
                                    'id_userEdit' => $user_id,
                                    'dateTimeUpdate' => $fecha  						  
						);
          	$this->db->where('id = "'.$id.'"');

          	if($this->db->update('' . smw_tre_cl_smartway . '.data_forum', $data)){
          		// print_r($this->db->last_query());
          		return $res= 'OK';
     		}else{
     			return $res;
     		}

		}catch(Exception $e){

			throw new Exception($e->getMessage(), 500);			

		}
	}



  public function ObtieneComentarios($id){	

	try{

		$this->db->select('daf.id, daf.idforum, daf.answer, us.name as Nombre, daf.datetime');
		$this->db->from('' . smw_tre_cl_smartway . '.data_answer_forum daf ');
		$this->db->join('' . smw_tre_cl_smartway . '.TRAZER_MAS_USER us', 'us.id = daf.iduser');
		$this->db->order_by('daf.datetime','asc');
                $this->db->where('daf.idforum = "'.$id.'" ');	    

		$query = $this->db->get()->result_array();

	    // print_r($this->db->last_query());

		return $query;

	}catch(Exception $e){
		throw new Exception($e->getMessage(), 500);
	}
}


public function BorrarComentario_model($id){

	try{

      	$this->db->where('id = "'.$id.'"');
      	if($this->db->delete('' . smw_tre_cl_smartway . '.data_answer_forum')){
      		$res = 1;	
      		return $res;
 		}else{
 			$res = 0; 	
 			return $res; 		
     	}


	}catch(Exception $e){

		throw new Exception($e->getMessage(), 500);
		

	}
}


	public function RestarResp($countAnswer,$id){
		$res = 'NOK';

		try{

  			$data= array(
                                'countAnswer' => $countAnswer
                              );
          	$this->db->where('id = "'.$id.'"');

          	if($this->db->update('' . smw_tre_cl_smartway . '.data_forum', $data)){
          		// print_r($this->db->last_query());
          		return $res= 'OK';
     		}else{
     			return $res;
     		}

		}catch(Exception $e){

			throw new Exception($e->getMessage(), 500);			

		}
	}




	public function GuardarForo($titulo, $descripcion, $fecha, $id_userGuarda){
		$res = 'NOK';
		$com = "0";
		$tipo = "WEB";

		try{

			$data = array(
			   'title' => $titulo,
			   'description' => $descripcion,
			   'datetime' => $fecha,
			   'countAnswer' => $com,
			   'TypeUser' => $tipo,
			   'iduser' => $id_userGuarda
			);			

          	if($this->db->insert('' . smw_tre_cl_smartway . '.data_forum', $data)){
          		// print_r($this->db->last_query());
          		return $res= 'OK';
     		}else{
     			return $res;
     		}

		}catch(Exception $e){

			throw new Exception($e->getMessage(), 500);			

		}
	}




	public function copiaDoc($FullName){

		$rutaWeb = "/var/www/html/toolbox/portal/v0.08.desa/assets/manuales_temp/";
		$rutaApp = "/var/www/html/toolbox/files/manual/";


		// $cmd2 = "chown apache:tb_web_reports ".$rutaWeb.$FullName." && chmod g+rwx ".$rutaWeb.$FullName." > ".$rutaWeb."log.log ";
		$cmd2 = "chmod 777 ".$rutaWeb."x > ".$rutaWeb."log.log ";
		// print_r($cmd2);
		$valor = shell_exec($cmd2);


		$cmd = "/usr/bin/sshpass -p 'tb_web_reports123' scp -r ".$rutaWeb.$FullName." tb_web_reports@app001-cloud-simpledata:".$rutaApp.$FullName."";
		// $cmd = "/usr/bin/sshpass -p 'tb_web_reports123' scp ".$rutaWeb.$FullName." tb_web_reports@10.143.181.67:".$rutaApp.$FullName."";
		// $cmd = "lftp sftp://cmartinez:cmartinez123@169.55.235.198:".$rutaApp."  -e put ".$rutaWeb.$FullName."; bye ";
		print_r($cmd);
		$valor = shell_exec($cmd);
		return print_r($valor);

	}

}