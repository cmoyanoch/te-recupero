<?php

class Company_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	/*Guarda una nueva categoría*/
	public function create($company){
		$this->db->trans_begin();			
		$company_data = array(
			'name' => $company['name']
		);
		$this->db->insert('company' ,$company_data);
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Edita una categoría*/
	public function edit($company){
		$this->db->trans_begin();			
		$company_data = array(
			'name' => $company['name']
		);
		$this->db->set($company_data);
		$this->db->where('company_id', $company['company_id']);
		$this->db->update('company');
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Obtiene una categoría por el ID*/
	public function get($id){
		$this->db->where('company_id', $id);
		$query = $this->db->get('company');
		return $query->row_array();
	}

	/*Lista las categorías creadas. Si $value tiene un valor filtra el listado considerando el nombre de la categoría*/
	public function find($value ="", $limit = 0, $page = NULL){		
		$this->db->select('companyA.company_id, companyA.name');		
		$this->db->limit($limit, (($page-1)*$limit));		
		$this->db->like('companyA.name', $value);
		$this->db->order_by('companyA.name');
		$query = $this->db->get('company companyA');
		$result =  $query->result_array();		
		foreach ($result as $key => $value) {			
			$result[$key]['can_delete'] = $this->canDelete($value['company_id']);
		}
		return $result;
	}

	/*Cuenta el total de categorías creadas en el sistema */
	public function count($value = ""){
		$this->db->like('name', $value);				
		return $this->db->count_all_results('company');
	}

	/*Elimina la categoría seleccionada*/
	public function delete($company){
		$this->db->trans_begin();
		foreach ($company as $keyCategory => $company_id) {
			if ($this->canDelete($company_id)){
				$this->db->where('company_id', $company_id);
				$this->db->delete('company');	
			}			
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}

	/*Se determina si la categoría se puede eliminar en base a su relación con otras categorías y otras preguntas */
	public function canDelete($id){
                //TMP
                return true;
                //TMP
		$this->db->where('cat_company_id', $id);
		$this->db->or_where('question.company_id', $id);
		$this->db->join('question', 'company.company_id = question.company_id', 'LEFT');
		$result = $this->db->count_all_results('company');
		if($result == 0){
			return true;
		}else{
			return false;
		}
	}
	
}

?>