<?php
	
	class User_model extends CI_Model
	{
		
		public function __construct()
		{
			$this->load->database();
			$this->tables = $this->config->item('tables', 'ion_auth');
			$this->store_salt = $this->config->item('store_salt', 'ion_auth');
			$this->hash_method = $this->config->item('hash_method', 'ion_auth');
		}
		
		public function get_perfil()
		{
			return $this->db->get('perfil')->result();
		}
		
		public function get_perfil_array()
		{
			$this->db->select('id_perfil,nombre_perfil');
			$query = $this->db->get('perfil')->result();
			$arr = array();
			foreach ($query as $row) {
				$arr[$row->id_perfil] = $row->nombre_perfil;
			}
			return $arr;
		}
		
		public function get_perfil_by_id($id)
		{
			return $this->db->get_where('perfil', array('id_perfil' => $id))->result();
		}
		
		public function get_groups()
		{
			return $this->db->get('groups')->result();
		}
		
		public function checkperfilDB($user)
		{
			$this->db->select('groups_id_groups');
			$this->db->where('perfil_id_perfil', $user['id_perfil']);
			$user['authorizations'] = $this->db->get('perfil_groups')->result_array();
			return $user;
		}
		
		public function save_perfil()
		{
			$this->db->trans_begin();
			//new dBug($this->input->post('id_perfil'));exit();
			if ($this->input->post('id_perfil') == 0){
				$perfilAux = array(
					'nombre_perfil' => $this->input->post('nombre_perfil')
				);
				$this->db->insert('perfil', $perfilAux);
				$id_perfil = $this->db->insert_id();
				//new dBug($id_perfil);exit();
				foreach ($this->input->post('permisions') as $row) {
					$perfil_groups = array(
						'perfil_id_perfil' => $id_perfil,
						'groups_id_groups' => $row
					);
					$this->db->insert('perfil_groups', $perfil_groups);
				}
			} else {
				$perfilAux = array(
					'nombre_perfil' => $this->input->post('nombre_perfil')
				);
				$this->db->where('id_perfil', $this->input->post('id_perfil'));
				$this->db->update('perfil', $perfilAux);
				//new dBug($id_perfil);exit();
				$this->db->delete('perfil_groups', array('perfil_id_perfil' => $this->input->post('id_perfil')));
				foreach ($this->input->post('permisions') as $row) {
					$perfil_groups = array(
						'perfil_id_perfil' => $this->input->post('id_perfil'),
						'groups_id_groups' => $row
					);
					$this->db->insert('perfil_groups', $perfil_groups);
				}
			}
			
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
			$this->db->trans_commit();
		}
		
		/*Guarda un usuario*/
		public function create($user)
		{
			
			$this->db->trans_begin();
			$manual_activation = $this->config->item('manual_activation', 'ion_auth');
			$ip_address = $this->_prepare_ip($this->input->ip_address());
			$salt = $this->store_salt ? $this->salt() : FALSE;
			$password = $this->hash_password($user['password'], $salt);
			
			$user_data = array(
				'first_name' => trim($user['first_name']),
				'last_name' => trim($user['last_name']),
				'email' => trim($user['email']),
				'password' => trim($password),
				'username' => trim($user['email']),
				'active' => $manual_activation === false ? 1 : 0,
				'id_perfil' => $user['id_perfil'],
				'ip_address' => $ip_address
			);
			
			if ($this->store_salt){
				$user_data['salt'] = $salt;
			}
			$this->db->insert($this->tables['users'], $user_data);
			$user_id = $this->db->insert_id();
			
			//Se inscribe como miembro
			$this->db->select('id');
			$this->db->where('name', 'members');
			$group_id = $this->db->get('groups')->row()->id;

			$users_groups_data = array(
				'user_id' => $user_id,
				'group_id' => $group_id
			);
			$this->db->insert('users_groups', $users_groups_data);
			
			//Se guardan los permisos de acciones
			//$data['perfil'] = array('0'=> 'Seleccione Perfil ','1'=> 'Técnicos ','2'=> 'Coordinadores ','3'=> 'Supervisores ','4'=> 'Jefes de servicios','5'=> 'Gerentes','6'=> 'Administrador');
			$user = $this->checkperfilDB($user);
			//print_r($user['authorizations']);exit();
			if ($user['authorizations']){
				$this->db->delete('users_groups', array('user_id' => $user_id));
				foreach ($user['authorizations'] as $keyAut) {
					$users_groups_data = array(
						'user_id' => $user_id,
						'group_id' => $keyAut['groups_id_groups']
					);
					$this->db->insert('users_groups', $users_groups_data);
				}
			}
			
			$userAux = array(
				'user_id' => $user_id,
				'first_name' => trim($user['first_name']),
				'last_name' => trim($user['last_name'])
			);
			$this->db->insert('user', $userAux);
			
			
			foreach ($user['id_cliente'] as $cliente) {
				$users_customers_data = array(
					'user_id'    => $user_id,
					'customer_id' => $cliente
				);
				$this->db->insert(DYNAMIC_FORMS . '.users_customers', $users_customers_data);
			}
			
			//relaciono el usuario con un grupo
			foreach ($user['id_grupo'] as $row) {
				$data_user_group = array(
					'id_cordinador' => $user_id,
					'id_group' => $row);
				$this->db->insert(smw_tre_cl_smartway . '.TRAZER_GRUPO_CORDINADOR', $data_user_group);
			}
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
			$this->db->trans_commit();
			return TRUE;
		}
		
		public function get_groups_by_user_id($id)
		{
			$this->db->select('id_group');
			$this->db->where('id_cordinador', $id);
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_GRUPO_CORDINADOR')->result();
			$arr = array();
			foreach ($query as $row) {
				array_push($arr, $row->id_group);
			}
			return $arr;
		}
		
		public function get_customers_by_user_id($id)
		{
			$this->db->select('customer_id');
			$this->db->where('user_id', $id);
			$query = $this->db->get(DYNAMIC_FORMS . '.users_customers')->result();
			$arr = array();
			foreach ($query as $row) {
				array_push($arr, $row->customer_id);
			}
			return $arr;
		}
		
		/* Edita un usuario */
		public function edit($user)
		{
			
			$this->db->trans_begin();
			
			$user_data = array(
				'first_name' => trim($user['first_name']),
				'last_name' => trim($user['last_name']),
				'email' => trim($user['email']),
				'username' => trim($user['email']),
				'id_perfil' => trim($user['id_perfil']),
			);
			if ($this->store_salt){
				$user_data['salt'] = $salt;
			}
			if ($user['password']){
				$salt = $this->store_salt ? $this->salt() : FALSE;
				$password = $this->hash_password($user['password'], $salt);
				$user_data['password'] = $password;
			}
			
			$this->db->set($user_data);
			$this->db->where('id', $user['user_id']);
			$this->db->update('users');
			
			//Se eliminan todos los permisos
			$this->db->where('user_id', $user['user_id']);
			$this->db->delete('users_groups');
			
			//Se inscribe como miembro
			$this->db->select('id');
			$this->db->where('name', 'members');
			$group_id = $this->db->get('groups')->row()->id;
			
			$users_groups_data = array(
				'user_id' => $user['user_id'],
				'group_id' => $group_id
			);
			$this->db->insert('users_groups', $users_groups_data);
			$user = $this->checkperfilDB($user);
			//Se guardan los permisos de acciones
			if ($user['authorizations']){
				$this->db->delete('users_groups', array('user_id' => $user['user_id']));
				foreach ($user['authorizations'] as $keyAut) {
					//new dBug($keyAut['groups_id_groups']);exit();
					$users_groups_data = array(
						'user_id' => $user['user_id'],
						'group_id' => $keyAut['groups_id_groups']
					);
					$this->db->insert('users_groups', $users_groups_data);
				}
			}
			//Se actualiza en la tabla auxiliar de usuarios
			$userAux = array(
				'first_name' => $user['first_name'],
				'last_name' => $user['last_name']
			);
			$this->db->set($userAux);
			$this->db->where('user_id', $user['user_id']);
			$this->db->update('user');
			
			// actualizo el grupo TRAZER_GRUPO_CORDINADOR
			$this->db->delete(smw_tre_cl_smartway . '.TRAZER_GRUPO_CORDINADOR', array('id_cordinador' => $user['user_id']));
			foreach ($user['id_grupo'] as $row) {
				$data_user_group = array(
					'id_cordinador' => $user['user_id'],
					'id_group' => $row);
				$this->db->insert(smw_tre_cl_smartway . '.TRAZER_GRUPO_CORDINADOR', $data_user_group);
			}
			
			
			$this->db->delete(DYNAMIC_FORMS . '.users_customers', array('user_id' => $user['user_id']));
			foreach ($user['id_cliente'] as $cliente) {
				$users_customers_data = array(
					'user_id' => $user['user_id'],
					'customer_id' => $cliente);
				$this->db->insert(DYNAMIC_FORMS . '.users_customers', $users_customers_data);
			}
			
			
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
			$this->db->trans_commit();
			return TRUE;
		}
		
		/*obtiene la información de un usuario*/
		public function get($id)
		{
			$this->db->select('U.id as user_id, first_name, last_name, email, id_perfil, TGC.id_group as id_grupo');
			$this->db->where('U.id', $id);
			$this->db->join(smw_tre_cl_smartway . '.TRAZER_GRUPO_CORDINADOR TGC', 'U.id = TGC.id_cordinador');
			$query = $this->db->get('users U');
			$result = $query->row_array();
			
			//print_r($this->db->last_query());
			
			//Se obtienen los permisos del usuario
			$this->db->select('name');
			$this->db->where('user_id', $id);
			$this->db->join('groups', 'groups.id = users_groups.group_id');
			$queryAux = $this->db->get('users_groups');
			$result['authorizations'] = array_column($queryAux->result_array(), 'name');
			return $result;
		}
		
		public function getpermisosperfil($id)
		{
			//Se obtienen los permisos del usuario
			$this->db->select('groups_id_groups');
			$this->db->where('perfil_id_perfil', $id);
			$queryAux = $this->db->get('perfil_groups')->result();
			//new dBug($queryAux->result());new dBug($this->db->last_query());exit();
			$ar = array();
			foreach ($queryAux as $row) {
				array_push($ar, $row->groups_id_groups);
			}
			return $ar;
		}
		
		/*Lista los usuarios creadas. Si $value tiene un valor filtra el listado considerando el nombre o email*/
		public function find($value = "", $limit = 0, $page = NULL){

			 $user = $this->ion_auth->user()->row();
		
			 $sql  = " SELECT t1.id as user_id, t1.first_name, t1.last_name, t1.email, t1.id_perfil,
						FROM_UNIXTIME(t1.last_login, '%d-%m-%Y') as last_login, IF((t1.active = 0), 'Inactivo', 'Activo') as active, perfil.nombre_perfil
						
						FROM smw_tre_cl_dynamicForms.users t1
						
						LEFT JOIN smw_tre_cl_dynamicForms.perfil ON perfil.id_perfil = t1.id_perfil
						WHERE
						t1.id != '1' ";
			
			 if (strpos($value, 'activo') !== FALSE && $value != 'inactivo'){
				$sql .= " AND t1.active = 1) ";
			 }
			 if (strpos($value, 'inactivo') !== FALSE){
				$sql .= " AND t1.active = 0) ";
			 }
			
			 if($value){
				$sql .= " AND ( t1.first_name like '%$value%'";
				$sql .= " OR t1.last_name  like '%$value%'";
				$sql .= " OR t1.email      like '%$value%' )";
			 }
			
			 $sql .= " ORDER BY t1.active ASC, t1.first_name ASC
			          LIMIT ".(($page - 1) * $limit).",  $limit ";
			
			 $query  = $this->db->query($sql);
			 $result = $query->result_array();

			 return $result;	
		}
		
		
		public function countList ($value = ""){

			$user = $this->ion_auth->user()->row();
			
			$sql  = " SELECT COUNT(*) as CC
						FROM smw_tre_cl_dynamicForms.users t1
						LEFT JOIN smw_tre_cl_dynamicForms.perfil ON perfil.id_perfil = t1.id_perfil
						WHERE ( (t1.id != 1) AND t1.id != ('1') )	
						AND (  t1.first_name LIKE '%%' ESCAPE '!' OR t1.last_name LIKE '%%' ESCAPE '!' )";
			
			if (strpos($value, 'activo') !== FALSE && $value != 'inactivo'){
				$sql .= " AND t1.active = 1) ";
			}
			if (strpos($value, 'inactivo') !== FALSE){
				$sql .= " AND t1.active = 0) ";
			}
			
			$sql .= " ORDER BY t1.active ASC, t1.first_name ASC  ";
			
			$query  = $this->db->query($sql);
			$result = $query->result_array();
			
			return $result;
		}
		
		/*Cuenta el total de usuarios creados en el sistema */
		public function count($value = "")
		{
			$user = $this->ion_auth->user()->row();
			$like = array('first_name' => $value, 'last_name' => $value, 'email' => $value);
			$this->db->where("((id !=1) AND id != ('$user->id'))");
			$this->db->like($like);
			if (strpos($value, 'activo') !== FALSE && $value != 'inactivo'){
				$this->db->or_like('active', 1);
			}
			if (strpos($value, 'inactivo') !== FALSE){
				$this->db->or_like('active', 0);
			}
			return $this->db->count_all_results('users');
		}
		
		/*Elimina los usuarios seleccionados*/
		public function delete($user)
		{
			$this->db->trans_begin();
			foreach ($user as $keyUser => $user_id) {
				$this->db->where('user_id', $user_id);
				$this->db->delete('users_groups');
				$this->db->where('id', $user_id);
				$this->db->delete('users');
			}
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
			$this->db->trans_commit();
			return true;
		}
		
		/*Cambia el estado de uno o varios usuarios a activo*/
		public function activate($user)
		{
			$this->db->trans_begin();
			foreach ($user as $keyForm => $user_id) {
				$this->db->set('active', TRUE);
				$this->db->where('id', $user_id);
				$this->db->update('users');
			}
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
			$this->db->trans_commit();
			return true;
		}
		
		/*Cambia el estado de uno o varios usuarios a inactivo */
		public function deactivate($user)
		{
			$this->db->trans_begin();
			foreach ($user as $keyForm => $user_id) {
				$this->db->set('active', FALSE);
				$this->db->where('id', $user_id);
				$this->db->update('users');
			}
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
			$this->db->trans_commit();
			return true;
		}
		
		/*Obtiene los permisos del usuario*/
		public function get_authorizations()
		{
			$id = $this->ion_auth->user()->row()->id;
			$this->db->select('description');
			$this->db->where('user_id', $id);
			$this->db->join('users_groups', 'groups.id = users_groups.group_id');
			$query = $this->db->get('groups')->result_array();
			$result = array_column($query, 'description');
			return $result;
		}
		
		/*Obtiene la list de Clientes*/
		public function getCustomerDropdow()
		{
			$customer = array(32, 21, 28, 25, 4, 33, 53);
			
			$result = [];
			$this->db->select('id_empresa as id , nombre_empresa as name');
			$this->db->where_in('id_empresa', $customer);
			$this->db->order_by(smw_tre_cl_smartway . '.TRAZER_DATA_EMPRESA.nombre_empresa');
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_DATA_EMPRESA');
			$result = $result + array_column($query->result_array(), 'name', 'id');
			
			return $result;
		}
		
		/*Cambia la contraseña*/
		public function change_password($user)
		{
			$id = $this->ion_auth->user()->row()->id;
			$this->db->trans_begin();
			$salt = $this->store_salt ? $this->salt() : FALSE;
			$password = $this->hash_password($user['password'], $salt);
			
			$data = array(
				'password' => $password
			);
			$this->db->set($data);
			$this->db->where('id', $id);
			$this->db->update('users');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return FALSE;
			}
			$this->db->trans_commit();
			return true;
		}
		
			
		
		/**
		 * Misc functions
		 *
		 * Hash password : Hashes the password to be stored in the database.
		 * Hash password db : This function takes a password and validates it
		 * against an entry in the users table.
		 * Salt : Generates a random salt value.
		 *
		 * @author Mathew
		 */
		
		/**
		 * Hashes the password to be stored in the database.
		 *
		 * @return void
		 * @author Mathew
		 **/
		public function hash_password($password, $salt = false, $use_sha1_override = FALSE)
		{
			if (empty($password)){
				return FALSE;
			}
			
			// bcrypt
			if ($use_sha1_override === FALSE && $this->hash_method == 'bcrypt'){
				return $this->bcrypt->hash($password);
			}
			
			if ($this->store_salt && $salt){
				return sha1($password . $salt);
			} else {
				$salt = $this->salt();
				return $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
			}
		}
		
		//Retorna IP en string para compatibilidad
		protected function _prepare_ip($ip_address)
		{
			return $ip_address;
		}
		
	}

?>