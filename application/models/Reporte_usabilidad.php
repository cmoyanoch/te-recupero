<?php
	
	class Reporte_usabilidad extends CI_Model
	{
		
		public function __construct()
		{
			$this->load->database();
		}
		
		public function getZoneFilter($filter = "")
		{
			$result[''] = 'Seleccione una Zona';
			$this->db->select(' id_zona, name ');
			if ($filter != ""){
				$this->db->where("id", $filter);
			}
			$this->db->order_by('name ASC');
			$query = $this->db->get("" . smw_tre_cl_smartway . ".TRAZER_MAS_ZONA ");
			
			$result = $query->result_array();
			
			return $result;
		}
				
		public function getZone($filter = "")
		{
			
			$this->db->select(' id_zona, name ');
			if ($filter != ""){
				$this->db->where("id", $filter);
			}
			$this->db->order_by('name ASC');
			$query = $this->db->get("" . smw_tre_cl_smartway . ".TRAZER_MAS_ZONA ");
			$result = $query->result_array();
			
			return $result;
		}
		
		public function getClient($_CONFIG_USER)
		{
			$sql = " SELECT id_empresa, nombre_empresa
					   FROM  ". smw_tre_cl_smartway .".TRAZER_DATA_EMPRESA
					   WHERE
					   id_empresa IN ( ".$_CONFIG_USER['CUST']." )
						ORDER BY nombre_empresa ";
					$query  = $this->db->query($sql);
			$result = $query->result_array();

			return $result;
		}
	}

?>