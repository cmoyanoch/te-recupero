<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Capsulas_model extends CI_Model {

private $bd_;

  function __construct()
  {
    // Call the Model constructor
    parent::__construct();
    
  }


public function CapsuleData($sumador){

	$num1 = 0;
	$num2 = PAGINADO_CANT;
	$num1 = $num1 + $sumador;
	$num2 = $num2 + $sumador;

	try{
	
		$this->db->select('Id, NameCapsule, Description, DateTime, Link, State, Type');
		$this->db->from('' . smw_tre_cl_smartway . '.DATA_CAPSULES');
		$this->db->where('State = "1" ');	
		$this->db->order_by('NameCapsule','asc');

	if ($sumador==0) {
		$this->db->limit($num2);
	}else{
		$this->db->limit(PAGINADO_CANT,$num1);
	}

		$query = $this->db->get()->result_array();
		 // print_r($this->db->last_query());
		// return print_r($query);
		return $query;
	}catch(Exception $e){

	throw new Exception($e->getMessage(), 500);
		

	}

}


public function ContCapsule(){									

	try{
	$this->db->select('count(Id) as cantReg');
	$this->db->from('' . smw_tre_cl_smartway . '.DATA_CAPSULES');
	$this->db->where('State = "1" ');	

	$query = $this->db->get()->row_array();
	
	// return print_r($query);
	// print_r($this->db->last_query());
	return $query;

	}catch(Exception $e){

	throw new Exception($e->getMessage(), 500);
		

	}

}


	public function StateDocumentCap($id){
		$res = "NOK";
		$state = 0;

	    try{

  			$data= array(
  				'State' => $state
  				);

          	$this->db->where('Id = "'.$id.'"');

          	if($this->db->update('' . smw_tre_cl_smartway . '.DATA_CAPSULES', $data)){
          		return $res= 'OK';
     		}else{
     			return $res;
     		}

	         // echo $this->db->last_query();

	      // return print_r($querys);

	    }catch(Exception $e){
	      
	      throw new Exception($e->getMessage(), 500);

	    }

	}


	public function existFullNameCap($FullName){									

		try{
		$this->db->select('count(Id) as cant');
		$this->db->from('' . smw_tre_cl_smartway . '.DATA_CAPSULES');
		$this->db->where('State = "1" ');	
		$this->db->where('NameCapsule = "'.$FullName.'" ');	

		$query = $this->db->get()->row_array();
		
		// return print_r($query);
		// print_r($this->db->last_query());
		return $query;

		}catch(Exception $e){

		throw new Exception($e->getMessage(), 500);
			

		}

	}



	public function AddCapsule($description, $FullName, $dateNow, $type){
	  $res ='NOK';

// return print_r(URL_CAPSULA.$FullName);
		try{

			$data= array(
	                 'Description' => $description,
	                 'NameCapsule' => $FullName,
	                 'DateTime' => $dateNow,
	                 'Type' => $type,
	                 'Link' => URL_CAPSULA.$FullName
	         ); 

	         if($this->db->insert('' . smw_tre_cl_smartway . '.DATA_CAPSULES', $data)){
	             return $res= 'OK';
	         }else{
	        	 return $res;
	         } 


		}catch(Exception $e){	      
	      throw new Exception($e->getMessage(), 500);
		}

	}




	public function BuscarCap($buscar, $sumador){

		$num1 = 0;
		$num2 = PAGINADO_CANT;
		$num1 = $num1 + $sumador;
		$num2 = $num2 + $sumador; 

		try{
		
                    $this->db->select('Id, NameCapsule, Description, DateTime, Link, State, Type');
                    $this->db->from('' . smw_tre_cl_smartway . '.DATA_CAPSULES');
                    if($buscar!=""){
                        $this->db->where('NameCapsule like "%'.$buscar.'%" ');	
                    }
                    $this->db->where('State = "1" ');	

		if ($sumador==0) {
                    $this->db->limit($num2);
		}else{
                    $this->db->limit(PAGINADO_CANT,$num1);
		}


		$query = $this->db->get()->result_array();
		 // print_r($this->db->last_query());
		// return print_r($query);
		return $query;

		}catch(Exception $e){

		throw new Exception($e->getMessage(), 500);
			

		}

	}


	public function BuscarCap_cont($buscar){

            try{

                $this->db->select('count(Id) as cantReg');
                $this->db->from('' . smw_tre_cl_smartway . '.DATA_CAPSULES');
                if($buscar!=""){
                    $this->db->where('NameCapsule like "%'.$buscar.'%" ');	
                }
                $this->db->where('State = "1" ');	

            $query = $this->db->get()->row_array();
             // print_r($this->db->last_query());
            // return print_r($query);
            return $query;

            }catch(Exception $e){

            throw new Exception($e->getMessage(), 500);	

            }

	}



	public function actDescCapsule($id,$desc){
            $res = 'NOK';

            try{

                $data= array(
                    'Description' => $desc						  
                );
                $this->db->where('id = "'.$id.'"');

                if($this->db->update('' . smw_tre_cl_smartway . '.DATA_CAPSULES', $data)){
                        // print_r($this->db->last_query());
                        return $res= 'OK';
                }else{
                        return $res;
                }

            }catch(Exception $e){

                throw new Exception($e->getMessage(), 500);			

            }
	}






}