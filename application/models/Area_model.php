<?php
	/**
	 * Created by PhpStorm.
	 * User: rene
	 * Date: 02-07-18
	 * Time: 18:57
	 */
	
	class Area_model extends CI_Model
	{
	
		public function __construct()
		{
			
			$this->load->database();
		}
		
		public function find($value ="", $limit = 0, $page = NULL)
		{
			$this->db->select('id, name');
			$this->db->like('name', $value);
			$this->db->order_by('name');
			$this->db->limit($limit, (($page-1)*$limit));
			
			$query  = $this->db->get(smw_tre_cl_smartway.'.TRAZER_MAS_AREA');
			$result =  $query->result_array();
			
			return $result;
		}
		
		public function count($value = "")
		{
			$this->db->like('name', $value);
			return $this->db->count_all_results(smw_tre_cl_smartway.'.TRAZER_MAS_AREA');
		}
		
		public function get($id){
			
			$this->db->select('id, name');
			$this->db->where('id', $id);
			
			$query  = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_AREA');
			$result = $query->row_array();
			
			return $result;
			
		}
	}