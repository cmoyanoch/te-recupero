<?php
require_once INTERFACE_HOME_PATH . '/application/libraries/nusoap.php';

class Calendar_model extends CI_Model {

	public function __construct(){
		$this->load->database();

	}

	/*Guarda en la base de datos la información del formulario creado */
		public function create($ticket, $fechaactual){	

		
			print_r($ticket);
			//return;

		$row = $this->getLatLnG($ticket['sucursal']);
		$result =  $row->row();

		if($ticket['sla']=='14 dias'){
		 	$sla = '336';
		 }else{
			$sla = explode(" ",$ticket['sla'])[0];
		}

		

		// $status = (($ticket['tecnico'] !== '')?'Asignado':'Abierto');
		// $tecnico = (($ticket['tecnico'] == '')?NULL:$ticket['tecnico']);


		//////////////////////////////////////////////////////////////////

		

		// $this->db->trans_begin();			
		// $ticket_data = array(
		// 	'incident' => $ticket['incident'],
		// 	'idstatus' => 'Abierto',
		// 	'title' => $ticket['title'],
		// 	'address' => $ticket['dir'],
		// 	'id_form' => $ticket['form'],
		// 	'iduser' => NULL,
		// 	'datecreation' => $fechaactual,
		// 	'coordX' => "$result->lat",
		// 	'coordY' => "$result->lng",
		// 	'sla' => $sla,
		// 	'id_group' => $ticket['grupo'],
		// 	'description' => $ticket['descripcion'],
		// 	'id_customer' => $ticket['cliente'],
		// 	'service' => $ticket['servicio'],
		// 	'category' => $ticket['categoria'],
		// 	'type' => $ticket['tipo'],
		// 	'piso' => $ticket['piso'],
		// 	'sector' => $ticket['sector'],
		// 	'area' => $ticket['area'],
		// 	'id_sector' => $ticket['area']
			
		// );


		$this->db->trans_begin();			
		$ticket_data = array(
			// 'incident' => $ticket['incident'],
			'idstatus' => 'Abierto',
			'title' => $ticket['title'],
			'address' => $ticket['dir'],
			'id_form' => $ticket['form'],
			'iduser' => $tecnico,
			'datecreation' => $fechaactual,
			'coordX' => "$result->lat",
			'coordY' => "$result->lng",
			'sla' => $sla,
			'id_group' => $ticket['grupo'],
			'description' => $ticket['descripcion'],
			'id_customer' => $ticket['cliente'],
			'service' => $ticket['servicio'],
			'category' => $ticket['categoria'],
			'type' => $ticket['tipo'],
			'piso' => $ticket['piso'],
			'sector' => $ticket['sector'],
			'area' => $ticket['area'],
			'id_sector' => $ticket['area']
			
		);
		
		$this->db->insert(smw_tre_cl_smartway.'.TRAZER_DATA_INCIDENT' ,$ticket_data);
		$lastId = $this->db->insert_id();
		
			$this->db->set('incident', $lastId);
			$this->db->where('id', $lastId);
			$this->db->update(smw_tre_cl_smartway.'.TRAZER_DATA_INCIDENT');

		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			
			return FALSE;
		}
		$this->db->trans_commit();
		
		return TRUE;
	}

    public function find($id_group){	

        $start = '2014-01-01';
        $end   = '2030-12-31';		

        $sql = "SELECT T1.id, T1.start ,T1.end ,T1.title, T1.color  FROM " . smw_tre_cl_smartway . ".TRAZER_CALENDAR T1
                LEFT OUTER JOIN " . smw_tre_cl_smartway . ".TRAZER_DATA_INCIDENT T2 ON T1.id_incident = T2.incident
                WHERE 
                (DATE(T1.start) >= '$start' AND DATE( T1.start) <= '$end') ";
        if((int)$id_group)
            $sql .= " AND T2.id = ".$id_group;
        
        $query = $this->db->query($sql); 
        $result =  $query->result_array();	

        return ($result); 
    }
    
    public function getGroup( ){
     
        $sql = " SELECT id, name FROM ".smw_tre_cl_smartway.".`TRAZER_MAS_GRUOP` ";
        $sql .= "  Order by name ";
        $query  = $this->db->query($sql); 
        $result = $query->result_array();
        
        return ($result);
    }

	public function updateTicket($title,$start,$end,$id){
		try{
		    $query = $this->db->query("UPDATE ".smw_tre_cl_smartway.".TRAZER_CALENDAR
                    SET start = '$start', 
                    end = '$end'
                    WHERE id = $id"); 
                    return "OK";
		    } 
		    catch (Exception $e)
                    {
		    	return "ERROR";
		      // throw new Exception($e->getMessage(), 500);
		    } 			
	}

	public function getTecnicosDisponiblesAgenda($id,$start,$end){


		try{
			 /*$query = $this->db->query("SELECT tmu.id, tmu.name 
				FROM CBRE_TRAZER.TRAZER_MAS_USER tmu 
					LEFT JOIN CBRE_TRAZER.TRAZER_AGENDA ta ON tmu.id = ta.id_user
					LEFT JOIN CBRE_TRAZER.TRAZER_CALENDAR tc ON ta.id_calendar = tc.id
				WHERE	(tc.start NOT BETWEEN '$start' AND '$end'
					AND tc.end NOT BETWEEN '$start' AND '$end')
					OR (tc.start IS NULL AND tc.end IS NULL);");*/

			 $query = $this->db->query(" 
			 SELECT * FROM  ".smw_tre_cl_smartway.".TRAZER_MAS_USER tmu 
			 WHERE tmu.id NOT IN( 
			 SELECT ta.id_user
			 FROM ".smw_tre_cl_smartway.".TRAZER_AGENDA ta INNER JOIN ".smw_tre_cl_smartway.".TRAZER_CALENDAR tc ON ta.id_calendar = tc.id
			WHERE (tc.start BETWEEN '$start' AND '$end'
			 AND tc.end  BETWEEN '$start' AND '$end'))");

	        $querys = $query->result_array(); 
	      return $querys;
		    } 
		    catch (Exception $e) {
		    	return "ERROR";
		      // throw new Exception($e->getMessage(), 500);
		    } 			
	}
	public function getTecnicoSelected($idEvent){
		try{
			 $query = $this->db->query("SELECT tmu.id, tmu.name 
		FROM ".smw_tre_cl_smartway.".TRAZER_MAS_USER tmu 
		LEFT JOIN ".smw_tre_cl_smartway.".TRAZER_AGENDA ta ON tmu.id = ta.id_user
		WHERE ta.id_calendar = '$idEvent'");

	        $querys = $query->result_array(); 
	      return $querys;
		    } 
		    catch (Exception $e) {
		    	return "ERROR";
		      // throw new Exception($e->getMessage(), 500);
		    } 			
	}

	public function count($value = ""){
		$this->db->like('id_calendar', $value);	
		//$this->db->where("type is not null");			
		// return $this->db->count_all_results('sla');	
		return $this->db->count_all_results(smw_tre_cl_smartway.'.TRAZER_CALENDAR');
	}


	public function setTiketTech($idUser,$idCalendar,$fechaactual){

		$this->db->trans_begin();			
		$agenda_data = array(
			'id_calendar' => $idCalendar, 
			'id_user' => $idUser,
			'date_creation' => $fechaactual						
		);		
		$this->db->insert(smw_tre_cl_smartway.'.TRAZER_AGENDA' ,$agenda_data);

		$this->db->set('color', '#257e4a');
		$this->db->where('id', $idCalendar);
		$this->db->update(smw_tre_cl_smartway.'.TRAZER_CALENDAR');

		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	} 

	public function setUpdateTiketTech($idUser,$idCalendar,$fechaactual)
{
		/*$this->db->trans_begin();			
		$agenda_data = array(
			'id_calendar' => $idCalendar, 
			'id_user' => $idUser,
			'date_creation' => $fechaactual						
		);		
		$this->db->insert('CBRE_TRAZER.TRAZER_AGENDA' ,$agenda_data);*/

		$this->db->set('id_user', $idUser);
		$this->db->where('id_calendar', $idCalendar);
		$this->db->update(smw_tre_cl_smartway.'.TRAZER_AGENDA');

		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	} 
}

?>