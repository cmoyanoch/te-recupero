<?php
date_default_timezone_set('America/Santiago');
require_once INTERFACE_HOME_PATH . '/application/libraries/JSON.php';
require_once INTERFACE_HOME_PATH . '/application/libraries/nusoap.php';

class Notificaciones_model extends CI_Model
{
    private $bd_;

    function __construct()
    {
        parent::__construct();
    }

    public function buscarEmpresas()
    {
        try {
            $this->db->select('id as idemp, name as Empresa');
            $this->db->from('' . smw_tre_cl_smartway . '.TRAZER_MAS_GRUOP');
            $this->db->order_by('name');

            $query = $this->db->get()->result_array();
            return $query;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }
    }

    public function buscarTecnicosEmpresa($idemp = null, $word = null)
    {
        try {
            $query = "SELECT 
                      u.id, u.name as Nombre, tmg.name as Empresa
                      FROM " . smw_tre_cl_smartway . ".TRAZER_MAS_USER u
                      join " . smw_tre_cl_smartway . ".TRAZER_MAS_GRUOP tmg on u.id_mas_group=tmg.id";

            $where = "";
            if ($idemp != null) {
                $where .= " u.id_mas_group ='{$idemp}' ";
            }
            if ($word != null) {
                $where .= (strlen($where) > 0 ? " and " : "") . " u.name like '%{$word}%'  or u.rut like '%{$word}%'";
            }
            $query .= (strlen($where) > 0 ? " where " . $where : "");
            $query = $this->db->query($query);
            $resp = $query->result_array();

            return $resp;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }
    }

    public function saveNotifications($text, $todos, $id_user, $usuariosTemp, $usuariosTemp2)
    {
        $res = '0';
        $fecha = date('Y-m-d H:i:s');
        try {
            $data = array(
                'MESSAGE' => $text,  //notificación
                'STATE_ALL' => $todos, //0 ó 1
                'ID_USER_SENDING' => $id_user, //id del usuario que envía el notificación
                'CREATE_DATE' => $fecha  //fecha de envío de notificación
            );

            if ($this->db->insert('' . smw_tre_cl_smartway . '.DATA_NOTIFICATION', $data)) {
                $ultimo_idDN = $this->db->insert_id();

                $data2 = array(
                    'ID_NOTIFICATION' => $ultimo_idDN, //notificación
                    'ID_USER_GET' => $usuariosTemp,
                    'FIELD' => $usuariosTemp2
                );

                $this->db->insert('' . smw_tre_cl_smartway . '.DATA_REL_NOTIFICATION_USER', $data2);

                return $res = $ultimo_idDN;

            } else {
                return $res;
            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }
    }

    public function notificacionTicket($imei,$mensaje)
    {
        try {

            $i=0;
            $data[$i]["Name"] = "Imei";
            $data[$i]["Value"] = $imei;
            $i++;
            $data[$i]["Name"] = "Msg";
            $data[$i]["Value"] = $mensaje;

            $requestFinal = array(
                "OperationRequest" => array(
                    "Header" => array(
                        "DateTime" => date("Y-m-d"),
                        "Operation" => "enviarNotificacionTicket",
                        "Destination" => ""
                    ),
                    "Data" => array(
                        "Property" => array(
                            "ListValue" => array(
                                "Property" => $data
                            )
                        )
                    )
                )
            );

            $client = new nusoap_client(URL_IP, true);

            $result = $client->call('OutputRequest', $requestFinal);

            if ($client->fault) {
                $code = 2;
                $description = 'Error al consultar el servicio';
            } else {        // Chequea errores
                $err = $client->getError();
                if ($err) {             // Muestra el error
                    $code = 3;
                    $description = $err;
                } else {                // Muestra el resultado
                    $code = 0;
                    $description = 'Consulta exitosa';
                }
            }

            if ($code == 2 || $code == 3) {
                $var = "NOK";
                return $var;
            } else if ($code == 0) {
                return $result;
            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }

    }

    public function notificaciones($idNotifications)
    {
        try {

            $requestFinal = array(
                "OperationRequest" => array(
                    "Header" => array(
                        "DateTime" => date("Y-m-d"),
                        "Operation" => "enviarNotificacion",
                        "Destination" => ""
                    ),
                    "Data" => array(
                        "Property" => array(
                            array(
                                "Name" => "IdNoti",
                                "Value" => $idNotifications,
                            ),
                        )
                    )
                )
            );

            $client = new nusoap_client(URL_IP, true);

            $result = $client->call('OutputRequest', $requestFinal);

            if ($client->fault) {
                $code = 2;
                $description = 'Error al consultar el servicio';
            } else {        // Chequea errores
                $err = $client->getError();
                if ($err) {             // Muestra el error
                    $code = 3;
                    $description = $err;
                } else {                // Muestra el resultado
                    $code = 0;
                    $description = 'Consulta exitosa';
                }
            }

            if ($code == 2 || $code == 3) {
                $var = "NOK";
                return $var;
            } else if ($code == 0) {
                return $result;
            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }

    }

    public function nameUserWeb($id_user)
    {
        try {
            $query = "SELECT 
                      u.first_name, u.last_name
                      FROM smw_tre_cl_dynamicForms.users u
                      where u.id=$id_user";
            $query = $this->db->query($query);
            $resp = $query->row_array();

            return $resp;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }

    }

    public function sendTicketNotification($id_tech, $incident)
    {
        $query = "SELECT 
                    u.imei
                    FROM " . smw_tre_cl_smartway . ".TRAZER_MAS_USER u                    
                    WHERE 
                    u.id = " . $id_tech;

        $query = $this->db->query($query);
        $user = $query->result_array();

        $imei = $user[0]['imei'];
        $mensaje = "Ticket Asignado: ".$incident;

        $this->notificacionTicket($imei,$mensaje);
    }

    public function GuardarNotificaciones($text, $empresa, $todos, $id_user, $usuariosTemp, $usuariosTemp2)
    {
        $res = '0';
        $fecha = date('Y-m-d H:i:s');
        try {
            $data = array(
                'MESSAGE' => $text,  //notificación
                'BUSSINES' => $empresa,  //empresa
                'STATE_ALL' => $todos, //0 ó 1
                'ID_USER_SENDING' => $id_user, //id del usuario que envía el notificación
                'CREATE_DATE' => $fecha  //fecha de envío de notificación
            );

            if ($this->db->insert('' . smw_tre_cl_smartway . '.DATA_NOTIFICATION', $data)) {
                $ultimo_idDN = $this->db->insert_id();
                $data2 = array(
                    'ID_NOTIFICATION' => $ultimo_idDN, //notificación
                    'ID_USER_GET' => $usuariosTemp,
                    'FIELD' => $usuariosTemp2
                );
                $this->db->insert('' . smw_tre_cl_smartway . '.DATA_REL_NOTIFICATION_USER', $data2);
                return $res = $ultimo_idDN;
            } else {
                return $res;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }
    }

}