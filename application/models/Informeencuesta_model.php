<?php

class Informeencuesta_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function getTotalTareas() {
//id_status 1 = abierto y 11 asignado
        $sql = 'SELECT count(i.id) as total, z.name as nombrezona, sum(i.idstatus = 1) as noasignado, z.id_zona FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT i
            inner join ' . smw_tre_cl_smartway . '.TRAZER_MAS_GRUOP g on i.id_group = g.id
            inner join ' . smw_tre_cl_smartway . '.TRAZER_MAS_ZONA z on g.id_zona = z.id_zona
            group by id_zona ;';
    }

    public function getIncIdenciasRepo($value = "", $limit = 0, $page = NULL, $gruposUser) {
        //echo 'en getIncident';
        //echo 'en el modelox';
        //echo $gruposUser;
        //return;
        $this->db->select('I.id, I.incident, I.idstatus, I.title,I.address,I.response,I.datecreation,I.persistent_id,
                            I.updatetime,I.dateclose,I.iduser,I.coordX,I.coordY,I.sla,id_form,I.datestandby, (SELECT STS.name FROM ' . smw_tre_cl_smartway . '.TRAZER_MAS_STATUS STS WHERE STS.id = I.idstatus LIMIT 1) nameStatus, (SELECT T.headId FROM ' . smw_tre_cl_smartway . '.TRAZER_DATA_TRAZABILITY T WHERE I.incident = T.idticket
                        ORDER BY T.UpdateTime DESC LIMIT 1) HeadId,(SELECT USR.imei FROM ' . smw_tre_cl_smartway . '.TRAZER_MAS_USER USR WHERE USR.id = I.iduser LIMIT 1) imei,(SELECT USR.name FROM ' . smw_tre_cl_smartway . '.TRAZER_MAS_USER USR WHERE USR.id = I.iduser LIMIT 1) name');

        // $this->db->where("id_group in (" . $gruposUser . ") and  preventivo = 0");
        $this->db->where("I.id_form = 22");

        $this->db->order_by('I.updatetime DESC');
        // $this->db->where("preventivo is null or preventivo = 0");
        $query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT I');
        $result = $query->result_array();

        return $result;
    }

    public function getRespuestaFormulario($headId) {

        // Definimos las preguntas a evaluar FORM 22 Revisar a futuros formularios
        define("QUESTION_ID_1", 375);
        define("QUESTION_ID_2", 376);
        define("QUESTION_ID_3", 377);
        define("QUESTION_ID_4", 378);

        // SQL para obtener los valores de las preguntas
        $sql = "SELECT TBFDA.QuestionId, TBFDA.value
                  FROM TOOLBOX_FORMS.TBF_DATA_ANSWERS TBFDA
                 WHERE TBFDA.HeaderId = " . $headId . " AND TBFDA.QuestionId in (" . QUESTION_ID_1 . "," . QUESTION_ID_2 . "," . QUESTION_ID_3 . "," . QUESTION_ID_4 . ");
            ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function getTotales($zona, $tecnico, $grupo, $fecha) {
        $sql = " CALL smw_tre_cl_smartway.sp_get_datos_encuesta ('" . $tecnico . "', '" . $zona . "', '" . $grupo . "', '" . (int) $fecha . "', '674, 675, 676, 677,  666, 667, 668, 669, 670, 671')";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function getZonas() {
        $zonas = [];
        $sql = 'SELECT z.name as nombre, z.id_zona as id FROM ' . smw_tre_cl_smartway . '.TRAZER_MAS_ZONA z;';
        $result = $this->db->query($sql);

        if (!empty($result)) {
            $zonas = $result->result_array();
        }

        $this->db->close();

        return $zonas;
    }

    public function getGrupoResolutores($group) {
        $groups = [];
        $sql = " SELECT id, name as nombre FROM smw_tre_cl_smartway.TRAZER_MAS_GRUOP WHERE id IN ( " . $group . ")";
        $result = $this->db->query($sql);

        if (!empty($result)) {
            $groups = $result->result_array();
            $result->free_result();
        }
        $this->db->close();

        return $groups;
    }

    public function getClientes($customer) {
        $clientes = [];

        $sql = "SELECT nombre_empresa as 'nombre', id_empresa as 'id'
 				  FROM " . smw_tre_cl_smartway . ".TRAZER_DATA_EMPRESA
 				  WHERE
 				  id_empresa IN ( $customer ) ";

        $result = $this->db->query($sql);

        if (!empty($result)) {
            $clientes = $result->result_array();
        }
        $this->db->close();

        return $clientes;
    }

    public function getQuestions($question) {
        $questions = [];

        $sql = "SELECT question_id as 'id', text as 'nombre' 
                FROM smw_tre_cl_dynamicForms.question 
                WHERE question_id IN ( 674, 675, 676, 677,  666, 667, 668, 669, 670, 671)";

        $result = $this->db->query($sql);

        if (!empty($result)) {
            $questions = $result->result_array();
        }
        $this->db->close();

        return $questions;
    }

    public function getQuestionsByCustomer($zona, $cliente, $fecha, $grupo, $pregunta) {
        $customers = [];
        $sql = "CALL smw_tre_cl_smartway.sp_get_totales(" . $zona . ", " . $cliente . ", " . $fecha . ", " . $grupo . "," . $pregunta . ")";

        /* SELECT
          x.ApptNumber, CAST(SUBSTRING(x.Value, 1, 1) AS UNSIGNED) AS value, d.position, d.text AS nombre, e.nombre_empresa AS cliente1, e.id_empresa AS id, g.name, z.name, u.name

          FROM TOOLBOX_FORMS.TBF_DATA_ANSWERS x

          INNER JOIN smw_tre_cl_dynamicForms.question             d ON x.QuestionId  = d.question_id
          INNER JOIN smw_tre_cl_smartway.TRAZER_DATA_INCIDENT i ON x.ApptNumber  = i.incident
          INNER JOIN smw_tre_cl_smartway.TRAZER_DATA_EMPRESA  e ON i.id_customer = e.id_empresa
          INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_GRUOP     g ON i.id_group    = g.id
          INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_ZONA      z ON g.id_zona     = z.id_zona
          INNER JOIN smw_tre_cl_smartway.TRAZER_MAS_USER      u ON i.idTec       = u.id

          WHERE
          d.question_id IN ( 674, 675, 676, 677,  666, 667, 668, 669, 670, 671 ) AND i.id_customer =

          ORDER BY x.ApptNumber, d.position ASC */
        $result = $this->db->query($sql);

        if (!empty($result)) {
            $customers = $result->result_array();
        }
        $this->db->close();

        return $customers;
    }

}
