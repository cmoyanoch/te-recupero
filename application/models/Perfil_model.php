<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Perfil_model extends CI_Model
	{
		
		private $bd_;
		
		function __construct(){
			parent::__construct();
			
		}
		
		
		public function name_profile($id_perfil){
			
			try {
				$this->db->select('nombre_perfil');
				$this->db->from('smw_tre_cl_dynamicForms.perfil');
				$this->db->where('id_perfil = "' . $id_perfil . '" ');
				
				$query = $this->db->get()->result_array();
				return $query;
			} catch (Exception $e) {
				throw new Exception($e->getMessage(), 500);
			}
		}
	}
