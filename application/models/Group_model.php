<?php

class group_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	/*Guarda una nueva categoría*/
	public function create($group){

		$this->db->trans_begin();			
		$group = array(
			'nom_documento' => $group['name'],
			'avr_documento' => $group['description']
		);
		
    $this->db->insert(smw_tre_cl_smartway.'.TRAZER_MAS_TIPO_DOCUMENTO' ,$group);
    
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Edita una categoría*/
	public function edit($group){
		
		$this->db->trans_begin();			
		$group_data = array(
			'nom_documento' => $group['name'],
			'avr_documento' => $group['description']

		);
		$this->db->set($group_data);
		$this->db->where('id', $group['id']);
		$this->db->update(smw_tre_cl_smartway.'.TRAZER_MAS_TIPO_DOCUMENTO');
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo "update false";
			return FALSE;
		}
		$this->db->trans_commit();
		//echo "update true";
		return TRUE;
	}

	/*Obtiene una categoría por el ID*/
	public function get($id){
		//echo 'modelo con '.$id;
		$this->db->where('id', $id);
		$query = $this->db->get(smw_tre_cl_smartway.'.TRAZER_MAS_TIPO_DOCUMENTO');
		return $query->row_array();
	}

	/*Lista las categorías creadas. Si $value tiene un valor filtra el listado considerando el nombre de la categoría*/
	public function find($value ="", $limit = 0, $page = NULL){	

        
		$this->db->select('id, nom_documento,avr_documento');
		$this->db->limit($limit, (($page-1)*$limit));	
		$this->db->like('nom_documento', $value);			
		$this->db->order_by('nom_documento');
        $query = $this->db->get(smw_tre_cl_smartway.'.TRAZER_MAS_TIPO_DOCUMENTO');
		$result =  $query->result_array();
		return $result;

	}

	/*Cuenta el total de categorías creadas en el sistema */
	public function count($value = ""){
		$this->db->like('nom_documento', $value);				
		return $this->db->count_all_results(smw_tre_cl_smartway.'.TRAZER_MAS_TIPO_DOCUMENTO');
	}

	/*Elimina la categoría seleccionada*/
	public function delete($group){
		$this->db->trans_begin();
		foreach ($group as $keyCategory => $group_id) {
			if ($this->canDelete($group_id)){
				$this->db->where('group_id', $group_id);
				$this->db->delete('group');	
			}			
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}


}

?>