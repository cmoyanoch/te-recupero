<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manualesdoc_model extends CI_Model {

private $bd_;

  function __construct()
  {
    // Call the Model constructor
    parent::__construct();
    
  }



    public function DataMod($sumador){

        $num1 = 0;
        $num2 = PAGINADO_CANT;
        $num1 = $num1 + $sumador;
        $num2 = $num2 + $sumador;

        try{

            $this->db->select('id, Nombre, DateTime, NombreArchivo');
            $this->db->from('' . smw_tre_cl_smartway . '.data_manual');
            $this->db->where('State = "1" ');	
            $this->db->order_by('Nombre','asc');

        if ($sumador==0) {
                $this->db->limit($num2);
        }else{
                $this->db->limit(PAGINADO_CANT,$num1);
        }


        $query = $this->db->get()->result_array();
         // print_r($this->db->last_query());
        // return print_r($query);
        return $query;

        }catch(Exception $e){

        throw new Exception($e->getMessage(), 500);


        }

    }
  
  
  

    public function CantDataMod(){									

        try{
        $this->db->select('count(id) as cantReg');
        $this->db->from('' . smw_tre_cl_smartway . '.data_manual');
        $this->db->where('State = "1" ');	

        $query = $this->db->get()->row_array();

        // return print_r($query);
        // print_r($this->db->last_query());
        return $query;

        }catch(Exception $e){

        throw new Exception($e->getMessage(), 500);


        }

    }  
  
  
  

    public function StateDocument($id){
        
        $res = "NOK";
        $state = 0;

        try{

            $data= array(
                'State' => $state
            );

            $this->db->where('id = "'.$id.'"');

            if($this->db->update('' . smw_tre_cl_smartway . '.data_manual', $data)){
                    return $res= 'OK';
            }else{
                    return $res;
            }

             // echo $this->db->last_query();

          // return print_r($querys);

        }catch(Exception $e){
          throw new Exception($e->getMessage(), 500);
        }

    }

  

    public function actDescMan($id,$desc){
            $res = 'NOK';

            try{

            $data= array(
                'Nombre' => $desc						  
              );

            $this->db->where('id = "'.$id.'"');

            if($this->db->update('' . smw_tre_cl_smartway . '.data_manual', $data)){
                    // print_r($this->db->last_query());
                    return $res= 'OK';
            }else{
                    return $res;
            }

            }catch(Exception $e){

                    throw new Exception($e->getMessage(), 500);			

            }
    }  


        

    public function routeLoad(){									

            try{
            $this->db->select('URL');
            $this->db->from('' . smw_tre_cl_smartway . '.DATA_MANUAL_URL');
            $this->db->where('Id = "1" ');	

            $query = $this->db->get()->row_array();

            // return print_r($query);
            // print_r($this->db->last_query());
            return $query;

            }catch(Exception $e){

            throw new Exception($e->getMessage(), 500);


            }

    }        
    
    

    public function existFullName($FullName){									

            try{
            $this->db->select('count(id) as cant');
            $this->db->from('' . smw_tre_cl_smartway . '.data_manual');
            $this->db->where('State = "1" ');	
            $this->db->where('NombreArchivo = "'.$FullName.'" ');	

            $query = $this->db->get()->row_array();

            // return print_r($query);
            // print_r($this->db->last_query());
            return $query;

            }catch(Exception $e){

            throw new Exception($e->getMessage(), 500);


            }

    }
    
    
    

	public function existName($OnlyName){									

		try{
		$this->db->select('count(id) as cant');
		$this->db->from('' . smw_tre_cl_smartway . '.data_manual');
		$this->db->where('State = "1" ');	
		$this->db->where('Nombre = "'.$OnlyName.'" ');	

		$query = $this->db->get()->row_array();
		
		// return print_r($query);
		// print_r($this->db->last_query());
		return $query;

		}catch(Exception $e){

		throw new Exception($e->getMessage(), 500);
			

		}

	}    
    
    

	public function AddManual($OnlyName, $FullName, $dateNow){
	  $res ='NOK';

		try{

			$data= array(
	                 'Nombre' => $OnlyName,
	                 'NombreArchivo' => $FullName,
	                 'Datetime' => $dateNow
	         ); 

	         if($this->db->insert('' . smw_tre_cl_smartway . '.data_manual', $data)){
	             return $res= 'OK';
	         }else{
	        	 return $res;
	         } 


		}catch(Exception $e){
	      
	      throw new Exception($e->getMessage(), 500);

		}

	}    

        
        

	public function BuscarManu($buscar, $sumador){

		$num1 = 0;
		$num2 = PAGINADO_CANT;
		$num1 = $num1 + $sumador;
		$num2 = $num2 + $sumador; 

		try{

                    $this->db->select('id, Nombre, DateTime, NombreArchivo');
                    $this->db->from('' . smw_tre_cl_smartway . '.data_manual');
                    $this->db->where('State = "1" ');	
                    $this->db->where('NombreArchivo like "%'.$buscar.'%" ');

		if ($sumador==0) {
                    $this->db->limit($num2);
		}else{
                    $this->db->limit(PAGINADO_CANT,$num1);
		}


		$query = $this->db->get()->result_array();
		 // print_r($this->db->last_query());
		// return print_r($query);
		return $query;

		}catch(Exception $e){

			throw new Exception($e->getMessage(), 500);
		}
	}

	public function BuscarManu_cont($buscar){

            try{

                $this->db->select('count(Id) as cantReg');
                $this->db->from('' . smw_tre_cl_smartway . '.data_manual');
                $this->db->where('Nombre like "%'.$buscar.'%" ');	

                $query = $this->db->get()->row_array();
                 // print_r($this->db->last_query());
                // return print_r($query);
                return $query;

            }catch(Exception $e){
                throw new Exception($e->getMessage(), 500);	
            }

	}
        
        
        
        
        
        
}