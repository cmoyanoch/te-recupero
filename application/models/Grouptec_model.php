<?php

class Grouptec_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	/*Guarda una nueva categoría*/
	public function create($grouptec){
		$this->db->trans_begin();			
		$grouptec_data = array(
			'name' => $grouptec['name']
		);
		$this->db->insert('grouptec' ,$grouptec_data);
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Edita una grupo*/
	public function edit($grouptec){
		$this->db->trans_begin();			
		$grouptec_data = array(
			'name' => $grouptec['name']
		);
		$this->db->set($grouptec_data);
		$this->db->where('grouptec_id', $grouptec['grouptec_id']);
		$this->db->update('grouptec');
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Obtiene una categoría por el ID*/
	public function get($id){
		$this->db->where('grouptec_id', $id);
		$query = $this->db->get('grouptec');
		return $query->row_array();
	}

	/*Lista las categorías creadas. Si $value tiene un valor filtra el listado considerando el nombre de la categoría*/
	public function find($value ="", $limit = 0, $page = NULL){		
		$this->db->select('grouptecA.grouptec_id, grouptecA.name');		
		$this->db->limit($limit, (($page-1)*$limit));		
		$this->db->like('grouptecA.name', $value);
		$this->db->order_by('grouptecA.name');
		$query = $this->db->get('grouptec grouptecA');
		$result =  $query->result_array();		
		foreach ($result as $key => $value) {			
			$result[$key]['can_delete'] = $this->canDelete($value['grouptec_id']);
		}
		return $result;
	}

	/*Cuenta el total de categorías creadas en el sistema */
	public function count($value = ""){
		$this->db->like('name', $value);				
		return $this->db->count_all_results('grouptec');
	}

	/*Elimina la categoría seleccionada*/
	public function delete($grouptec){
		$this->db->trans_begin();
		foreach ($grouptec as $keyCategory => $grouptec_id) {
			if ($this->canDelete($grouptec_id)){
				$this->db->where('grouptec_id', $grouptec_id);
				$this->db->delete('grouptec');	
			}			
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}

	/*Se determina si la categoría se puede eliminar en base a su relación con otras categorías y otras preguntas */
	public function canDelete($id){
                //TMP
                return true;
                //TMP
		$this->db->where('cat_grouptec_id', $id);
		$this->db->or_where('question.grouptec_id', $id);
		$this->db->join('question', 'grouptec.grouptec_id = question.grouptec_id', 'LEFT');
		$result = $this->db->count_all_results('grouptec');
		if($result == 0){
			return true;
		}else{
			return false;
		}
	}
	
}

?>