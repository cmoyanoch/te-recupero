<?php
	require_once INTERFACE_HOME_PATH . '/application/libraries/nusoap.php';
	
	class Ticket_model extends CI_Model
	{
		
		public function __construct()
		{
			$this->load->database();
		}
		
		public function create($ticket, $fechaactual)
		{
			$preventivo = (($ticket['agenda'] == '') ? 0 : $ticket['agenda']);
			
			$this->db->trans_begin();
			$ticket_data = array(
				'incident'        => $ticket['incident'],
				'idstatus'        => STATUSABIERTO,
				'title'           => $ticket['title'],
				'address'         => $ticket['dir'],
				'id_form'         => $ticket['form'],
				'iduser'          => $tecnico,
				'datecreation'    => $fechaactual,
				'id_group'        => $ticket['grupo'],
				'description'     => $ticket['descripcion'],
				'id_customer'     => 3,
				'service'         => $ticket['servicio'],
				'category'        => $ticket['categoria'],
				'type'            => $ticket['tipo'],
				'preventivo'      => $preventivo,
				'type_incident'   => $ticket['type_incident'],
				'id_area'         => $ticket['area'],
				'id_bahia'        => $ticket['bahia']
			);
		
			$this->db->insert(smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT', $ticket_data);
			
			if ($ticket['agenda'] == true){
				
				$arrayFecha      = (explode("-", $ticket['fecha']));
				$ticket['fecha'] = $arrayFecha[2] . '-' . $arrayFecha[1] . '-' . $arrayFecha[0];
				
				$ticket_data_preventivo = array(
					'id_incident'  => $ticket['incident'],
					'title'        => $ticket['title'],
					'start'        => $ticket['fecha'] . ' ' . $ticket['hora'],
					'end'          => $ticket['fecha'] . ' ' . date('H:i:s', strtotime($ticket['hora']) + 3600),
					'color'        => '#23527c',
				);
				$this->db->insert(smw_tre_cl_smartway . '.TRAZER_CALENDAR', $ticket_data_preventivo);
			}
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				
				return FALSE;
			}

			$client = new nusoap_client(URL_IP_FORM_ENPOINT2, 'wsdl');
			
			$err = $client->getError();
			if ($err){
				echo 'Error en Constructor' . $err;
			}
			
			$address   = 'Av. Miraflores 8953, Renca, Chile';
			$latGlobal = '-33.3956337';
			$lngGlobal = '-70.7617082';

			$requestFinal = array();
			$requestFinal['OperationRequest']['Header']['Company'] = 'Simpledata';
			$requestFinal['OperationRequest']['Header']['Login']  = '1234567hdfghjk';
			$requestFinal['OperationRequest']['Header']['PasswordHash'] = 'qwertyuio';
			$requestFinal['OperationRequest']['Header']['DateTime'] = date('c');
			$requestFinal['OperationRequest']['Header']['Destination'] = '';
			$requestFinal['OperationRequest']['Header']['Operation'] = 'UpdateStatusTicket';
			$i = 0;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'estado';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = STATUSASIGN;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'id_tech';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $ticket['conductor'];
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'id_user';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $tecnico;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'descripcion';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $ticket['descripcion'];
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'incident';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $ticket['incident'];
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'id_form';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = 21;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'sla';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = '0';
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'coordX';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $latGlobal;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'coordY';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $lngGlobal;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'address';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $address;
						
			$result = $client->call('OutputRequest', $requestFinal);
			
			if ($client->fault){
				$this->db->trans_rollback();
				return FALSE;

			} else {
				$err = $client->getError();
				if ($err){
					$this->db->trans_rollback();
					return FALSE;
				}
			}


			$this->db->trans_commit();
			
			return TRUE;
		}
		
		public function getLatLnG($idSucursal)
		{
			$query = $this->db->query("SELECT lat,lng FROM " . smw_tre_cl_smartway . ".TRAZER_MAS_SITE   WHERE id = $idSucursal");
			$querys = $query->result_array();
			return $querys;
		}
		
		public function actualizaState($ticket, $estado, $fechaactual, $id_user)
		{
			$this->db->trans_begin();
			
			$ticket_data2 = array(
				'id_incident' => $ticket['incident'],
				'updatetime'  => $fechaactual,
				'estado'      => $estado,
				'id_user'     => $id_user,
				'user_origen' => 'web',
			);
			$this->db->insert(smw_tre_cl_smartway . '.TRAZER_HIS_TICKET', $ticket_data2);
			
			if ($this->db->trans_status() === FALSE){
				 $this->db->trans_rollback();
				 return FALSE;
			}
			$this->db->trans_commit();
			return TRUE;
		}
		

		public function getCreatedDate($id)
		{
			$this->db->select("created_date");
			$this->db->where('form_id', $id);
			$queryForm = $this->db->get('form');
			return $queryForm->row_array();
		}
		
		/*Obtiene las categorías padres posibles*/
		public function get_direction_dropdown()
		{
			
			$result[''] = 'Seleccione una dirección';
			//SELECT id, NAME, address FROM TRAZER_MAS_SITE WHERE region = 'Región Metropolitana';
			$this->db->select('id, name, address');
			$this->db->where('region', 'Región Metropolitana');
			// $this->db->order_by(smw_tre_cl_smartway.'.TRAZER_MAS_GRUOP.name');
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_SITE');
			//$result = $result + array_column($query->result_array(), 'name', 'id', 'address');
			$result = $query->result_array();
			//print_r($query);
			return $result;
		}
		
		public function get_directionFilter_dropdown($idzona)
		{
			
			$result[''] = 'Seleccione una dirección';
			//SELECT id, NAME, address FROM TRAZER_MAS_SITE WHERE region = 'Región Metropolitana';
			$this->db->select('id, name');
			$this->db->where('region', $idzona);
			// $this->db->order_by(smw_tre_cl_smartway.'.TRAZER_MAS_GRUOP.name');
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_SITE');
			//$result = $result + array_column($query->result_array(), 'name', 'id', 'address');
			$result = $query->result_array();
			//print_r($query);
			return $result;
		}
		
		public function get_grupos_dropdown($gruposUser, $idzona)
		{
			$result[''] = 'Seleccione una dirección';
			$this->db->select('id, name');
			
			$gruposUser = explode(',', $gruposUser);
			
			if ($idzona){
				$this->db->where('id_zona', $idzona);
			}
			//$this->db->where_in('id', $gruposUser);
			
			$query  = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_GRUOP');
			$result = $query->result_array();
			
			return $result;
		}
		
		public function getTechAvailableForGroup($idGroup, $id_customer)
		{
			$sql = " SELECT  U.id, U.name, U.phone, U.imei, U.coordx, U.coordy, U.updatetimecoord, I.incident, I.idstatus, MAX(I.updatetime)
					   FROM " . smw_tre_cl_smartway . ".TRAZER_MAS_USER U
					   LEFT JOIN " . smw_tre_cl_smartway . ".TRAZER_DATA_INCIDENT I ON U.id = I.idTec
					   WHERE U.id_grupo = $idGroup and  U.is_delete = 0 AND U.inactiveType = 0
					   GROUP BY U.id
					   ORDER BY U.name ASC ";
			
			$query  = $this->db->query($sql);
			$querys = $query->result_array();
			
			$tmpquerys = $querys;
			$_RES =  array();
			foreach($tmpquerys as  $tec){
				
				$sql = " SELECT I.idTec
					   	FROM " . smw_tre_cl_smartway . ".TRAZER_DATA_INCIDENT I
					      WHERE
					         I.id_group = $idGroup AND
					         I.idTec    = '".$tec['id']."' AND
					         I.idstatus = 11 ";
				$query2  = $this->db->query($sql);
				$querys2 = $query2->result_array();
				
				$_RES[] = $querys2[0]['idTec'];
			}
			
			$position = array();
			foreach ($_RES as $key1 => $not){
				$x = 0;
				foreach ($tmpquerys as $key2 => $tecnico){
					
					
					if( (int)$tecnico['id'] == (int)$not ){
						
						$position[$x] = $key2;
					}
					$x++;
				}
			}
			
			foreach($position as $pos){
				unset($querys[$pos]);
			}
			
			$array = array();
			foreach ($querys as $pos){
				$array[] = $pos;
			}

         return $array;
		}
		
		public function get_zona_dropdown()
		{
			$result[''] = 'Seleccione una dirección';
			//SELECT id, NAME, address FROM TRAZER_MAS_SITE WHERE region = 'Región Metropolitana';
			$this->db->select('id_zona, name');
			//	$this->db->where('region','Región Metropolitana');
			// $this->db->order_by(smw_tre_cl_smartway.'.TRAZER_MAS_GRUOP.name');
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_ZONA');
			//$result = $result + array_column($query->result_array(), 'name', 'id', 'address');
			$result = $query->result_array();
			//print_r($query);
			return $result;
		}
		
		public function get_cliente_dropdown($_CONFIG_USER)
		{
			
			$result[''] = 'Seleccione una Empresa';
			$this->db->select('id_empresa, nombre_empresa');
			
			$gruposUser = explode(',', $_CONFIG_USER);
			
			
			$this->db->where_in('id_empresa', $gruposUser);
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_DATA_EMPRESA');
			$result = $result + array_column($query->result_array(), 'nombre_empresa', 'id_empresa');
			
			return $result;
		}
		
		public function get_service_dropdown($idcliente)
		{
			
			$query = $this->db->query("SELECT DISTINCT servicio FROM " . smw_tre_cl_smartway . ".TRAZER_MAS_SLA WHERE id_customer= $idcliente");
			$querys = $query->result_array();
			
			return $querys;
		}
		
		public function getcategoriasforService($txtServicio)
		{
			
			$query = $this->db->query("SELECT DISTINCT categoria FROM " . smw_tre_cl_smartway . ".TRAZER_MAS_SLA WHERE servicio = '$txtServicio'");
			$querys = $query->result_array();
			return $querys;
		}
		
		public function getSla($txtServicio, $txtCategoria)
		{
			$query = $this->db->query("SELECT id, tipo,tds,sla FROM " . smw_tre_cl_smartway . ".TRAZER_MAS_SLA WHERE servicio = '$txtServicio' AND categoria = '$txtCategoria';");
			$querys = $query->result_array();
			return $querys;
		}
		
		public function getDirectionForZona($idZona)
		{
			
			$result[''] = 'Seleccione una dirección';
			$this->db->select('id, name,address');
			$this->db->where('id_zona', $idZona);
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_SITE');
			$result = $query->result_array();
			return $result;
		}
		
		public function get_formAct_dropdown()
		{
			$result[''] = 'Seleccione un Formulario';
			$this->db->select('form_id, title');
			$this->db->where('active', 1);
			$this->db->order_by('form.title');
			$query = $this->db->get('form');
			$result = $result + array_column($query->result_array(), 'title', 'form_id');
			return $result;
		}
		
		public function getLatLngForDirection($latitud, $longitud, $cliente, $long_name, $region, $formatted_address, $idZona)
		{
			
			$this->db->select('id');
			$this->db->where('lat', $latitud);
			$this->db->where('lng', $longitud);
			$this->db->order_by('id');
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_SITE');
			$result = $query->result_array();
			
			if (isset($result[0]['id'])){
				$id = $result[0]['id'];
			} else {
				$this->db->trans_begin();
				$site_data = array(
					'name' => $long_name,
					'address' => $formatted_address,
					'region' => $region,
					'lat' => $latitud,
					'lng' => $longitud,
					'customer' => $cliente,
					'id_zona' => $idZona,
				);
				$this->db->insert(smw_tre_cl_smartway . '.TRAZER_MAS_SITE', $site_data);
				$id = $this->db->insert_id();
				
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					
					return FALSE;
				}
				$this->db->trans_commit();
				
			}
			
			return $id;
		}
		
		public function validateStatus($incident)
		{
			$client = new nusoap_client(URL_IP_FORM_ENPOINT2, 'wsdl');
			
			$err = $client->getError();
			if ($err){
				echo 'Error en Constructor' . $err;
			}
			
			$requestFinal = array();
			$requestFinal['OperationRequest']['Header']['Company'] = 'Simpledata';
			$requestFinal['OperationRequest']['Header']['Login'] = '1234567hdfghjk';
			$requestFinal['OperationRequest']['Header']['PasswordHash'] = 'qwertyuio';
			$requestFinal['OperationRequest']['Header']['DateTime'] = date('c');
			$requestFinal['OperationRequest']['Header']['Destination'] = '';
			$requestFinal['OperationRequest']['Header']['Operation'] = 'ValidateStatusTicket';
			$i = 0;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'incident';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $incident;
			
			$result = $client->call('OutputRequest', $requestFinal);
			
			if ($client->fault){
				return "0";
				//print_r($result);
			} else {
				// Chequea errores
				$err = $client->getError();
				if ($err){
					// Muestra el error
					return "0";
				} else {
					// Muestra el resultado
					return $result['Return']['Code'];
					// print_r ($result);
				}
			}
		}
		
		// MODIFICA ESTADO DEL TICKET
		public function changeStatus($incident, $estado, $id_tech, $id_user, $descripcion, $id_form, $sla, $coordX, $coordY, $address)
		{
			$client = new nusoap_client(URL_IP_FORM_ENPOINT2, 'wsdl');
			
			$err = $client->getError();
			if ($err){
				echo 'Error en Constructor' . $err;
			}
			
			$requestFinal = array();
			$requestFinal['OperationRequest']['Header']['Company'] = 'Simpledata';
			$requestFinal['OperationRequest']['Header']['Login']  = '1234567hdfghjk';
			$requestFinal['OperationRequest']['Header']['PasswordHash'] = 'qwertyuio';
			$requestFinal['OperationRequest']['Header']['DateTime'] = date('c');
			$requestFinal['OperationRequest']['Header']['Destination'] = '';
			$requestFinal['OperationRequest']['Header']['Operation'] = 'UpdateStatusTicket';
			$i = 0;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'estado';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $estado;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'id_tech';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $id_tech;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'id_user';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $id_user;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'descripcion';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $descripcion;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'incident';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $incident;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'id_form';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $id_form;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'sla';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $sla;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'coordX';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $coordX;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'coordY';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $coordY;
			$i++;
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Name'] = 'address';
			$requestFinal['OperationRequest']['Data']['Property'][$i]['Value'] = $address;
			
			//print_r($requestFinal);
			
			$result = $client->call('OutputRequest', $requestFinal);
			
			if ($client->fault){
				echo 'Fallo';
				//print_r($result);
			} else {
				// Chequea errores
				$err = $client->getError();
				if ($err){
					// Muestra el error
					echo 'Error' . $err;
				} else {
					// Muestra el resultado
					echo 'OK';
					// print_r ($result);
				}
			}
		}
		
		public function cerrarStatus($ticket, $status, $obs)
		{
			
			try {
				
				$requestFinal = array('idIncident' => $ticket,
					'id_head' => '',
					'accion' => 0);
				$client = new nusoap_client(URL_IP_FORM_ENPOINT2, true);
				$result = $client->call('RequestCerrarTicket', $requestFinal);
				if ($client->fault){
					$code = 2;
					$description = 'Error al consultar el servicio';
				} else {
					$err = $client->getError();
					if ($err){
						$code = 3;
						$description = $err;
					} else {
						$code = 0;
						$description = 'Consulta exitosa';
					}
				}
				if ($code == 2 || $code == 3){
					$var = "NOK";
					return $var;
				} else if ($code == 0){
					return $result;
				}
			} catch (Exception $e) {
				throw new Exception($e->getMessage(), 500);
			}
		}
		
		public function getId()
		{
			$sql = " SELECT MAX(id) as id
 					FROM " . smw_tre_cl_smartway . ".TRAZER_DATA_INCIDENT
 					ORDER BY id DESC ";
			$query = $this->db->query($sql);
			$querys = $query->result_array();
			return $querys;
		}
		
		public function deleteTicket($ticket)
		{
			$sql = " UPDATE " . smw_tre_cl_smartway . ".TRAZER_DATA_INCIDENT
						SET
						idstatus     = 99,
						batch_status = 3
						WHERE
						incident = $ticket ";
			$query = $this->db->query($sql);
			
			return $query;
		}
		
		public function validChangeTech($idTicket, $idTech)
		{
			$sql = " SELECT idTec
	               FROM " . smw_tre_cl_smartway . ".TRAZER_DATA_INCIDENT
						WHERE
					   id    = $idTicket AND
					   idTec = $idTech ";
			$query = $this->db->query($sql);
			$querys = $query->result_array();
			
			return (int)$querys[0]['idTec'];
		}
		
		public function itTicketCa($idTicket, $incident){
			
			$sql = " SELECT persistent_id
	               FROM " . smw_tre_cl_smartway . ".TRAZER_DATA_INCIDENT
						WHERE
					  ( id       = '".$idTicket."' OR
					    incident = '".$incident."' ) ";
			
			$query = $this->db->query($sql);
			$querys = $query->result_array();
			
			return $querys[0]['persistent_id'];
			
		}
		
		public function validateChangeGroup($incident){
			
			$sql = " SELECT id_group FROM  ".smw_tre_cl_smartway.".TRAZER_DATA_INCIDENT WHERE  incident = ".$incident."; ";
			
			$query = $this->db->query($sql);
			$querys = $query->result_array();
			
			return $querys[0]['id_group'];
			
		}
		
		public function getGroupIdCa($group){
			$sql = " SELECT external_id FROM ".smw_tre_cl_smartway.".TRAZER_MAS_GRUOP WHERE id = ".$group."; ";
			
			$query = $this->db->query($sql);
			$querys = $query->result_array();
			
			return $querys[0]['external_id'];
		}
		
		public function getArea(){
			
			$result[''] = 'Seleccione un Area';
			
			$this->db->select('id, name');
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_AREA');
			$result = $result + array_column($query->result_array(), 'name', 'id');
			
			return $result;
			
		}
		
		public function getBahia($id_area){
			$this->db->select('id, name');
			$this->db->where('id_area', $id_area);
			$query  = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_BAHIA');
			$result = $query->result_array();
			
			return $result;
		}
		
		public function getConductor($idZona, $idGroup) {
			
			$this->db->select('id, CONCAT(numberTruck, " - ", name) as name');
			$this->db->where('id_zona', $idZona);
			$this->db->where('id_grupo', $idGroup);
			$this->db->where('is_delete', '0');
			$query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_USER');
			$result = $query->result_array();
			
			return $result;
		}

		 /* Devuelve el estado de un Ticket */
        public function getEstado($id)
        {
         try{
            $sql = "SELECT t1.idstatus, t1.type_incident, t2.name FROM ".smw_tre_cl_smartway.".TRAZER_DATA_INCIDENT t1
                    INNER JOIN  ".smw_tre_cl_smartway.".TRAZER_MAS_STATUS t2 ON t2.id = t1.idstatus 
                    WHERE t1.incident = '".$id."'";
            $query  = $this->db->query($sql);
            return $query->result_array();
         }catch(Exception $e){
            throw new Exception($e->getMessage(), 500);
         }
        }


          /* Lista los proximos estados posibles, al estado actual del Ticket */
        public function get_estados_dropdown($sig_edos)
        {
         try{
            $sql = "SELECT id, name FROM ".smw_tre_cl_smartway.".TRAZER_MAS_STATUS  
                    WHERE id IN (".$sig_edos.") ORDER BY id ASC";
            $query  = $this->db->query($sql);
            $result[0]['id'] = '0';
            $result[0]['name'] = 'Selecione um estado';
            $result1 = $query->result_array();
            $result2 = array_merge($result, $result1);
            return $result2;
         }catch(Exception $e){
            throw new Exception($e->getMessage(), 500);
         }
        }


          /* Lista la informacion general del Ticket */
        public function getInfo($id)
        {
         try{
         	
            $sql = "  SELECT `t1`.`id`, `t1`.`incident`, `t1`.`title`, `t1`.`address`, DATE_FORMAT(`t1`.`datecreation`, '%d/%m/%Y %H:%i:%s') AS fcreacion,
										DATE_FORMAT(`t1`.`updated_at`, '%d/%m/%Y %H:%i:%s') AS fact, DATE_FORMAT(`t1`.`dateclose`, '%d/%m/%Y %H:%i:%s') AS fcierre,
										`t1`.`description`, `t1`.`coordX`, `t1`.`coordY`, `t1`.`idstatus`, `t1`.`type_incident`, `t2`.`name` AS grupo, `t2`.`id_zona`, `t5`.`name` AS estado,
										`t3`.`name`,`t1`.`persistent_id`
							FROM smw_tre_cl_smartway.TRAZER_DATA_INCIDENT `t1`
								INNER JOIN      smw_tre_cl_smartway.`TRAZER_MAS_GRUOP`     `t2` ON `t2`.`id`       = `t1`.`id_group`
								LEFT OUTER JOIN smw_tre_cl_smartway.`TRAZER_MAS_USER`      `t3` ON `t3`.`id`       = `t1`.`idTec`
								INNER JOIN      smw_tre_cl_smartway.`TRAZER_MAS_STATUS`    `t5` ON `t5`.`id`       = `t1`.`idstatus`

							WHERE `t1`.`incident`='".$id."'";
            $query  = $this->db->query($sql);
            return $query->result_array();
         }
            
         catch(Exception $e){
            throw new Exception($e->getMessage(), 500);
         }
        }


            /* Actualiza el estado del Ticket */
        public function actEstado($ticket, $id)
        {
         $res = 'NOK';
         try{
            $this->db->where('incident ', $id);
            if($this->db->update('' . smw_tre_cl_smartway . '.TRAZER_DATA_INCIDENT', $ticket)){
               //var_dump($this->db->last_query());
               return $res= 'OK';
            }else{
               //var_dump($this->db->last_query());
               return $res;
            }
         }catch(Exception $e){
            throw new Exception($e->getMessage(), 500);
         }
        }


            /* Registra el histórico del ticket */
        public function add_historico($data){
         $this->db->trans_begin();
         $this->db->insert(smw_tre_cl_smartway.'.TRAZER_HIS_TICKET' ,$data);
         if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return FALSE;
         }
         $this->db->trans_commit();
         return TRUE;
        }


         /* Registra log del Ticket */
        public function registrarLog($log){
         try{
            $this->db->trans_begin();
            $this->db->insert(smw_tre_cl_smartway.'.TRAZER_LOG_TICKET', $log);
            if ($this->db->trans_status() === FALSE){
               $this->db->trans_rollback();
               return FALSE;
            }
            $this->db->trans_commit();
            return TRUE;
         }catch(Exception $e){
            throw new Exception($e->getMessage(), 500);
         }
        }
		
	}

?>