<?php
	
	class Dataperfil_model extends CI_Model
	{
		
		public function __construct()	{
			
			$this->load->database();
		
		}
		
		public function getDataProfile($userId, $perfilId, $module = null){

			$module    = '';
			$cust_idx  = '';
			$group_idx = '';
			$_RES      = array();
			
			# filtra los valores antiguos #
			$group    = '608, 609, 610, 89, 606, 607, 3,5,1,2,4,9,10,4,6,7,8,11,12,13,313,314,576,580,581,584, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605 ';
			$sql = " SELECT id_empresa  FROM smw_tre_cl_smartway.TRAZER_DATA_EMPRESA ";
			$_customer  = $this->db->query($sql);
			$customers  = $_customer->result_array();
			
			
			$sql = " SELECT id_group FROM ".smw_tre_cl_smartway . ".TRAZER_GRUPO_CORDINADOR
                  WHERE id_cordinador = $userId AND
                  id_group IN ( ".$group." ) ";
			$_group = $this->db->query($sql);
			$groups = $_group->result_array();
			
			
			foreach($customers AS $cust){
				$cust_idx .= $cust['id_empresa'].",";
			}
			
			foreach($groups AS $gr){
				$group_idx .= $gr['id_group'].",";
			}
			
			if($perfilId == 4){
				
				$sql = " SELECT customer_id  FROM smw_tre_cl_dynamicForms.users_customers
	                  WHERE
	                  user_id      = $userId ";
				$_customer  = $this->db->query($sql);
				$customers  = $_customer->result_array();
				
				foreach($customers AS $cust){
					$cust_idx .= $cust['customer_id'].",";
				}
			}
			
/*			if($perfilId == 5 or $perfilId == 1){
				$cust_idx  = $customer;
				$group_idx = $group;
			}
	*/

			
			$_RES['USER_ID']  = $userId;
			$_RES['PERFIL']   = $perfilId;
			$_RES['GROUP']    = substr($group_idx, 0, -1);
			$_RES['CUST']     = substr($cust_idx , 0, -1);
			
			return $_RES;
			
		}
	}