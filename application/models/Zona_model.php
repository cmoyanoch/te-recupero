<?php

class Zona_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	/*Guarda una nueva categoría*/
	public function create($zona){
		$this->db->trans_begin();			
		$zona_data = array(
			'nombre_empresa' => $zona['name'],
			'rut_empresa' => $zona['rut']
		);
		$this->db->insert(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA' ,$zona_data);
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Edita una categoría*/
	public function edit($zona){

		$this->db->trans_begin();			
		$zona_data = array(
			'nombre_empresa' => $zona['name'],
			'rut_empresa' => $zona['rut']
		);

		$this->db->set($zona_data);
		$this->db->where('id_empresa', $zona['id_zona']);
		$this->db->update(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA');

		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Obtiene una categoría por el ID*/
	public function get($id){
		$this->db->where('id_empresa', $id);
		$query = $this->db->get(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA');

	}

	/*Lista las categorías creadas. Si $value tiene un valor filtra el listado considerando el nombre de la categoría*/
	public function find($value ="", $limit = 0, $page = NULL){	

        try {

        $this->db->select('id_empresa, nombre_empresa,rut_empresa');
        $this->db->from('' . smw_tre_cl_smartway . '.TRAZER_DATA_EMPRESA');
        $this->db->order_by('nombre_empresa', 'asc');
		$this->db->where('id_status = "0" ');
		$query = $this->db->get()->result_array();
				
		return $query;
				
			} catch (Exception $e) {
				throw new Exception($e->getMessage(), 500);
			}

	}

	/*Cuenta el total de categorías creadas en el sistema */
	public function count($value = ""){
		$this->db->like('nombre_empresa', $value);				
		return $this->db->count_all_results(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA');
	}

	/*Elimina la categoría seleccionada*/
	public function delete($zona){

		$this->db->trans_begin();			
        $this->db->set('id_status', 1);
		$this->db->where('id_empresa', $zona['id_zona']);
		$this->db->update(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA');

		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	
} 

} ?>