<?php

class Question_type_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	/*Guarda un nuevo type*/
	public function create($question_type){
		$this->db->trans_begin();			
		$question_type_data = array(
			'name' => $question_type['name']
		);
		$this->db->insert('question_type' ,$question_type_data);
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Edita un type*/
	public function edit($question_type){
		$this->db->trans_begin();			
		$question_type_data = array(
			'name' => $question_type['name']
		);
		$this->db->set($question_type_data);
		$this->db->where('question_type_id', $question_type['question_type_id']);
		$this->db->update('question_type');
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Obtiene un type por el ID*/
	public function get($question_type_id){
		$this->db->where('question_type_id', $question_type_id);
		$query = $this->db->get('question_type');
		return $query->row_array();
	}

	/*Lista los tipos creados. Si $value tiene un valor filtra el listado considerando el nombre de los tipos*/
        
	public function find($value ="", $limit = 0, $page = NULL){		
		$this->db->select('question_type.question_type_id, question_type.name');		
		$this->db->limit($limit, (($page-1)*$limit));		
		$this->db->like('question_type.name', $value);
		$this->db->order_by('question_type.question_type_id');
		$query = $this->db->get('question_type');
		$result =  $query->result_array();		
		//foreach ($result as $key => $value) {			
		//	$result[$key]['can_delete'] = $this->canDelete($value['question_type_id']);
		//}
		return $result;
	}
        
	/*Cuenta el total de tipos creados en el sistema */
	public function count($value = ""){
		$this->db->like('name', $value);				
		return $this->db->count_all_results('question_type');
	}

	/*Elimina el tipo seleccionado*/
        
	public function delete($question_type_id){
		$this->db->trans_begin();
		foreach ($question_type as $keyType => $question_type_id) {
			//if ($this->canDelete($question_type_id)){
				$this->db->where('question_type_id', $question_type_id);
				$this->db->delete('question_type');	
			//}			
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}
        
        
	/*Se determina si la categoría se puede eliminar en base a su relación con otras categorías y otras preguntas */
        
        /*
	public function canDelete($question_type_id){
                //TMP
                return true;
                //TMP
		$this->db->where('question_type_id', $question_type_id);
		//$this->db->or_where('question.type_question_type_id', $question_type_id);
		//$this->db->join('question', 'type.type_question_type_id = question.type_question_type_id', 'LEFT');
		$result = $this->db->count_all_results('type');
		if($result == 0){
			return true;
		}else{
			return false;
		}
	}
         * */
         
	
        
}

?>