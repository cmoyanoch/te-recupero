<?php

class Empresa_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	/*Guarda una nueva categoría*/
	public function create($empresa){
		$this->db->trans_begin();			
		$empresa_data = array(
			'nombre_empresa' => $empresa['nombre_empresa'],
			'radio_cerco'    => $empresa['radio_cerco'],
			'radio_cerco_2'  => $empresa['radio_cerco_2'],
			'radio_cerco_3'  => $empresa['radio_cerco_3'],
			'radio_cerco_4'  => $empresa['radio_cerco_4']

		);
		$this->db->insert(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA' ,$empresa_data);
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Edita una categoría*/
	public function edit($empresa){
		$this->db->trans_begin();			
		$empresa_data = array(
			'radio_cerco'    => $empresa['radio_cerco'],
			'radio_cerco_2'  => $empresa['radio_cerco_2'],
			'radio_cerco_3'  => $empresa['radio_cerco_3'],
			'radio_cerco_4'  => $empresa['radio_cerco_4']
		);
		$this->db->set($empresa_data);
		$this->db->where('id_empresa', $empresa['id_empresa']);
		$this->db->update(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA');
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		//new dBug($empresa['nombre_empresa']);
		return TRUE;
	}

	/*Obtiene una categoría por el ID*/
	public function get($id){
		$this->db->where('id_empresa', $id);
		$query = $this->db->get(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA');
		return $query->row_array();
	}

	/*Lista las categorías creadas. Si $value tiene un valor filtra el listado considerando el nombre de la categoría*/
	public function find($value ="", $limit = 0, $page = NULL){			
		$this->db->limit($limit, (($page-1)*$limit));		
		$this->db->like('nombre_empresa', $value);
		$this->db->order_by('nombre_empresa');
		$query = $this->db->get(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA');
		$result =  $query->result_array();		
    

		foreach ($result as $key => $value) {			
			//$result[$key]['can_delete'] = $this->canDelete($value['id_empresa']);
		}

		return $result;
	}

	/*Cuenta el total de categorías creadas en el sistema */
	public function count($value = ""){
		$this->db->like('nombre_empresa', $value);	
		//new dBug('a');exit();				
		return $this->db->count_all_results(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA');
	}

	/*Elimina la categoría seleccionada*/
	public function delete($empresa){
		$this->db->trans_begin();
		foreach ($empresa as $keyCategory => $id_empresa) {
			if ($this->canDelete($id_empresa)){
				$this->db->where('id_empresa', $id_empresa);
				$this->db->delete(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA');	
			}			
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}

	/*Se determina si la categoría se puede eliminar en base a su relación con otras categorías y otras preguntas */
	public function canDelete($id){
                //TMP
                return true;
                //TMP
		$this->db->where('cat_id_empresa', $id);
		$this->db->or_where('question.id_empresa', $id);
		$this->db->join('question', 'empresa.id_empresa = question.id_empresa', 'LEFT');
		$result = $this->db->count_all_results(smw_tre_cl_smartway.'.TRAZER_DATA_EMPRESA');
		if($result == 0){
			return true;
		}else{
			return false;
		}
	}
	
}

?>