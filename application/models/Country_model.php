<?php

class Country_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	/*Guarda una nueva categoría*/
	public function create($country){
		$this->db->trans_begin();			
		$country_data = array(
			'name' => $country['name']
		);
		$this->db->insert('country' ,$country_data);
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Edita una categoría*/
	public function edit($country){
		$this->db->trans_begin();			
		$country_data = array(
			'name' => $country['name']
		);
		$this->db->set($country_data);
		$this->db->where('country_id', $country['country_id']);
		$this->db->update('country');
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return TRUE;
	}

	/*Obtiene una categoría por el ID*/
	public function get($id){
		$this->db->where('country_id', $id);
		$query = $this->db->get('country');
		return $query->row_array();
	}

	/*Lista las categorías creadas. Si $value tiene un valor filtra el listado considerando el nombre de la categoría*/
	public function find($value ="", $limit = 0, $page = NULL){		
		$this->db->select('countryA.country_id, countryA.name');		
		$this->db->limit($limit, (($page-1)*$limit));		
		$this->db->like('countryA.name', $value);
		$this->db->order_by('countryA.name');
		$query = $this->db->get('country countryA');
		$result =  $query->result_array();		
		foreach ($result as $key => $value) {			
			$result[$key]['can_delete'] = $this->canDelete($value['country_id']);
		}
		return $result;
	}

	/*Cuenta el total de categorías creadas en el sistema */
	public function count($value = ""){
		$this->db->like('name', $value);				
		return $this->db->count_all_results('country');
	}

	/*Elimina la categoría seleccionada*/
	public function delete($country){
		$this->db->trans_begin();
		foreach ($country as $keyCategory => $country_id) {
			if ($this->canDelete($country_id)){
				$this->db->where('country_id', $country_id);
				$this->db->delete('country');	
			}			
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		$this->db->trans_commit();
		return true;
	}

	/*Se determina si la categoría se puede eliminar en base a su relación con otras categorías y otras preguntas */
	public function canDelete($id){
                //TMP
                return true;
                //TMP
		$this->db->where('cat_country_id', $id);
		$this->db->or_where('question.country_id', $id);
		$this->db->join('question', 'country.country_id = question.country_id', 'LEFT');
		$result = $this->db->count_all_results('country');
		if($result == 0){
			return true;
		}else{
			return false;
		}
	}
	
}

?>