<?php

	class Tracking_model extends CI_Model
	{
		


	public function __construct(){

			$this->load->database();
	}
		
	public function insertdoc($ticket,$n,$tipo){

                 if($tipo== 2 ){ 

                       $tabla=".TRAZER_DATA_FACTURASS";  

                          }else{ 

                            $tabla=".TRAZER_DATA_FACTURAS";   

                           }

		                $this->db->trans_begin();   	
                       
                  for($i=0;$i<count($ticket);$i++){ 
                     $ticket_data = array(

                                'nro_opera' => $n,
                                'deudor' => $ticket[$i][0],
                                'rut_deudor' => $ticket[$i][1],
                                'date_vto' => $ticket[$i][2],
                                'nro_doc' => $ticket[$i][3],
                                'cod_bco' => $ticket[$i][4],
                                'monto_doc' => $ticket[$i][5],
                                'monto_defp' => $ticket[$i][6],
                                'date_ingreso' => date('Y-m-d H:i:s'),

                        );            

                     
                        $this->db->insert(smw_tre_cl_smartway .$tabla, $ticket_data);
		              }
                 	if ($this->db->trans_status() === FALSE){
				         $this->db->trans_rollback();
				         return FALSE;
		             }
             
	 	           	  $this->db->trans_commit();  
	                        return TRUE;                  
	}
				
  public function insertOperacion($ticket,$tipo){
		

               if($tipo == 2 ){ 

                    $tabla=".TRAZER_MAS_OPERACIONS";  

                 }else{ 

                    $tabla=".TRAZER_MAS_OPERACION";  

                 }

	
		                     $this->db->trans_begin();   	       
                         $ticket_data = array(
                              
                                'nro_opera' => $ticket[16],
                                'date_opera' => $ticket[1],
                                'tasa_dif' => $ticket[9],
                                'id_empresa' => $ticket[0],
                                'id_cliente' => $ticket[5],
                                'rut_cliente' => $ticket[11],
                                'tipo_doc' => $ticket[6],
                                'comi_total' => $ticket[14],
                                'monto_gasto' => $ticket[20],
                                'porc_ant' => $ticket[16],
                                'date_ingreso' => date('Y-m-d'),
                        );            
                     
                    $this->db->insert(smw_tre_cl_smartway .$tabla, $ticket_data);	
                    $id = $this->db->insert_id();

                    if ($this->db->trans_status() === FALSE){

				        $this->db->trans_rollback();

				        return FALSE;
			           }
             
	 	            $this->db->trans_commit(); 

                    

	                      return $id;                  
	}

	public function count($value = NULL, $tabla){

                 if($tipo== 2 ){ $tabla=".TRAZER_MAS_OPERACIONS";     }else{ $tabla=".TRAZER_MAS_OPERACION";     }

			                   if ($value['factoriFilter'])
			 	                 $this->db->where('id_empresa',$value['factoriFilter']);           

                         if ($value['filterOpera'])
                         	$this->db->where('nro_opera',$value['filterOpera']);      

                         if ($value['clienteFilter'])
             	                $this->db->where('id_cliente',$value['clienteFilter']);
           
		               return $this->db->count_all_results(smw_tre_cl_smartway .$tabla);
	}
	
	public function getIncIdencias($value , $limit = 0, $page = NULL ){
			

                 	if ($page == 0){
				              $page = 0;
			              } else {
				              $page = $page * 10;
			              }


                       
                       $q="";

                 if ($value['factoriFilter'])              
                          $q .= "A.`id_empresa` = ".$value['factoriFilter']." AND ";
                 if ($value['filterOpera'])                 
                          $q .= "A.`nro_opera` = ".$value['filterOpera']." AND ";
                  if ($value['clienteFilter'])
                          $q .= "A.`id_cliente` = ".$value['clienteFilter']." AND ";
                  if ($value['docFilter'])
                          $q .= "A.`tipo_doc` = ".$value['clienteFilter']." AND ";
                  if ($value['fechaFilter'])
                          $q .= "A.`id_cliente` = ".$value['clienteFilter']." AND ";

              


                  $q .=" A.estado != 2 AND 1 = 1 ORDER BY A.id LIMIT $page, $limit ";
                     


                 $sql = "SELECT DISTINCT A.nro_opera, 
                 A.id, 
                 C.nombre_empresa, 
                 C.rut_empresa, 
                 A.porc_ant,
                 DATE_FORMAT(A.`date_opera`,'%d-%m-%Y') AS date_opera ,D.avr_documento, 
                 A.comi_total, 
                 (A.comi_total * 0.19) AS iva_comi, 
                 ((SELECT SUM(monto_doc) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURAS WHERE estado = 0  AND nro_opera = A.id) * A.porc_ant) AS monto_adelan,
                 '' AS monto_aplica, 
                 '' AS total_girado,
                 A.monto_gasto,   
                 (SELECT SUM(monto_doc) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURAS WHERE estado = 0  AND nro_opera = A.id) AS monto_doc,
                 '' AS mto_nomant,
                 '' AS mto_noant,
                 '' AS mto_difpre,
                 '' AS precio_cesion,
                 '' AS mto_antsdctos,
                 (SELECT COUNT(*) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURAS WHERE estado = 0  AND nro_opera = A.id) AS total_doc
                 FROM smw_tre_cl_smartway.TRAZER_MAS_OPERACION  A
                 LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_DATA_FACTURAS B ON A.`id` = B.`nro_opera`
                 LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_DATA_EMPRESA  C ON A.`id_empresa` = C.`id_empresa`
                 LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_MAS_TIPO_DOCUMENTO D ON A.tipo_doc = D.id
                 WHERE ".$q." "; 




	                 $query = $this->db->query($sql);
 	                 $result = $query->result_array();
			
                     return $result;
	}

	public function getIncIdenciass($value , $limit = 0, $page = NULL){
			
                 if ($page == 0){
			    	          $page = 0;
		        	        } else {
				               $page = $page * 10;
		                	}

                       $q="";

                    if ($value['factoriFilter'])              
                          $q .= "A.`id_empresa` = ".$value['factoriFilter']." AND ";
                    if ($value['filterOpera'])                 
                          $q .= "A.`nro_opera` = ".$value['filterOpera']." AND ";
                    if ($value['clienteFilter'])
                          $q .= "A.`id_cliente` = ".$value['clienteFilter']." AND ";
                    if ($value['docFilter'])
                          $q .= "A.`tipo_doc` = ".$value['clienteFilter']." AND ";
                    if ($value['fechaFilter'])
                          $q .= "A.`id_cliente` = ".$value['clienteFilter']." AND ";


                          $q .=" A.estado != 2 AND 1 = 1 ORDER BY A.id LIMIT $page, $limit ";
                     

                  $sql = "SELECT DISTINCT A.nro_opera, A.id, C.nombre_empresa, C.rut_empresa, DATE_FORMAT(A.`date_opera`,'%d-%m-%Y') AS date_opera ,D.avr_documento, 
                    A.comi_total, A.iva_comi, A.monto_adelan,A.monto_aplica, A.total_girado,A.monto_gasto,   

                    (SELECT SUM(monto_doc) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURASS WHERE estado = 0  AND nro_opera = A.nro_opera) AS monto_doc,
          
                 '' AS mto_nomant,
                 '' AS mto_noant,
                    (SELECT SUM(mto_difpre) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURASS WHERE estado = 0  AND nro_opera = A.nro_opera) AS mto_difpre,
                    (SELECT SUM(precio_cesion) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURASS WHERE estado = 0  AND nro_opera = A.nro_opera) AS precio_cesion,
                    (SELECT SUM(mto_antsdctos) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURASS WHERE estado = 0  AND nro_opera = A.nro_opera) AS mto_antsdctos,
                    (SELECT COUNT(*) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURASS WHERE estado = 0  AND nro_opera = A.nro_opera) AS total_doc
                     FROM smw_tre_cl_smartway.TRAZER_MAS_OPERACIONS  A
                     LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_DATA_FACTURASS B ON A.`nro_opera` = B.`nro_opera`
                     LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_DATA_EMPRESA  C ON A.`id_empresa` = C.`id_empresa`
                     LEFT OUTER JOIN smw_tre_cl_smartway.TRAZER_MAS_TIPO_DOCUMENTO D ON A.tipo_doc = D.id
                     WHERE ".$q." ";
		                 $query = $this->db->query($sql);
 		                 $result = $query->result_array();
			
                       return $result;
	}

	public function deleteDoc($doc,$tipo){

                        if($tipo == 2 ){ $tabla=".TRAZER_DATA_FACTURASS"; }else{ $tabla=".TRAZER_DATA_FACTURAS";  }

			                  $this->db->trans_begin();
                        $data = array(
                                'estado' => '1',
                                'updatetime' => date('Y-m-d h:i:s'),
                        );

                        $this->db->where('id', $doc);
                        $this->db->update(smw_tre_cl_smartway.$tabla,$data);
		
               
			                  if ($this->db->trans_status() === FALSE){
				                $this->db->trans_rollback();
				                return FALSE;
			                  }
			                  $this->db->trans_commit();
			                  return TRUE;
	}
	
  public function deleteOpe($ope,$tipo){


                         if($tipo== 2 ){ $tabla=".TRAZER_MAS_OPERACIONS";     }else{ $tabla=".TRAZER_MAS_OPERACION";     }


                          $this->db->trans_begin();
                           $data = array(
                                'estado' => '2',
                                'updatetime' => date('Y-m-d h:i:s'),
                            );

                          $this->db->where('id', $ope);
                          $this->db->update(smw_tre_cl_smartway.$tabla,$data);

                          if ($this->db->trans_status() === FALSE){
                                $this->db->trans_rollback();
                                return FALSE;
                            }
                          $this->db->trans_commit();
                          return TRUE;
  }

	public function getDocumento($opera,$tipo){




	                     if($tipo== 2 ){ 
	                       $tabA=".TRAZER_DATA_FACTURASS A"; 
	                       $tabB=".TRAZER_MAS_OPERACIONS B"; 
	                      }else{ 
	                        $tabA=".TRAZER_DATA_FACTURAS A";  
	                        $tabB=".TRAZER_MAS_OPERACION B"; 
	                      }


                          $this->db->select("
                         A.`id`, A.`deudor`,A.`rut_deudor`,
                          DATE_FORMAT(A.`date_emisor`,'%d-%m-%Y') AS date_emisor,
                          DATE_FORMAT(A.`date_vto`,'%d-%m-%Y') AS date_vto, 
                          '' AS dias,
                          A.`nro_doc`, A.`cod_bco`,A.`monto_doc`,A.`porc_ant`,           
                          (A.monto_doc * A.porc_ant/100) AS mto_nomant ,           
                          A.monto_doc - (A.monto_doc * A.porc_ant/100) AS`mto_noant`, 
                          '' AS mto_difpre ,
                          '' AS precio_cesion,
                          '' AS mto_antsdctos");

                          $this->db->from(smw_tre_cl_smartway.$tabA);
                          $this->db->join(smw_tre_cl_smartway.$tabB, 'A.nro_opera = B.id');
                          $this->db->where('A.nro_opera',$opera );
                          $this->db->where('A.estado','0');
                          $this->db->order_by('A.id', 'DESC');
                          $querys = $this->db->get()->result_array();

                         return $querys;
	}
       
  public function getOperacion($opera,$tipo){

                      if($tipo== 2 ){ 
                        
                            $sql = "SELECT c.nombre_cliente, c.rut_cliente, c.direccion_cliente, a.nro_opera,a.date_ingreso,d.name,a.date_otorga,b.nom_documento,(SELECT COUNT(*) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURASS WHERE estado IS NULL AND nro_opera = a.nro_opera ) AS cant_doc, (SELECT SUM(monto_doc) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURASS WHERE estado =0 AND nro_opera = a.nro_opera ) AS monto_doc, (SELECT SUM(mto_difpre) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURASS WHERE estado =0  AND nro_opera = a.nro_opera ) AS monto_dif, (SELECT SUM(precio_cesion) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURASS WHERE estado =0  AND nro_opera = a.nro_opera ) AS preci_ces, (SELECT SUM(mto_noant) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURASS WHERE estado =0 AND nro_opera = a.nro_opera ) AS monto_nof, a.monto_adelan , a.comi_total,a.monto_gasto,a.`iva_comi`,'' AS monto ,a.total_girado FROM smw_tre_cl_smartway.TRAZER_MAS_OPERACIONS a JOIN smw_tre_cl_smartway.TRAZER_MAS_CLIENTES c ON a.id_cliente = c.id_cliente JOIN smw_tre_cl_smartway.TRAZER_MAS_TIPO_DOCUMENTO b ON a.tipo_doc = b.id JOIN smw_tre_cl_smartway.TRAZER_MAS_STATUS d ON a.estado = d.id WHERE a.nro_opera ='" . $opera . "'";
                      }else{ 

                            $sql = "SELECT DISTINCT a.nro_opera,c.nombre_cliente,c.rut_cliente,c.direccion_cliente,a.date_ingreso,d.name,a.date_opera,b.nom_documento,(SELECT COUNT(*) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURAS WHERE estado = 0 AND nro_opera = a.id ) AS cant_doc,(SELECT SUM(monto_doc) FROM smw_tre_cl_smartway.TRAZER_DATA_FACTURAS WHERE estado = 0 AND nro_opera = a.id ) AS monto_doc,a.comi_total,a.monto_gasto
                              FROM smw_tre_cl_smartway.TRAZER_MAS_OPERACION a 
                              JOIN smw_tre_cl_smartway.TRAZER_MAS_CLIENTES c ON a.id_cliente = c.id_cliente 
                              JOIN smw_tre_cl_smartway.TRAZER_MAS_TIPO_DOCUMENTO b ON a.tipo_doc = b.id 
                              JOIN smw_tre_cl_smartway.TRAZER_MAS_STATUS d ON a.estado = d.id 
                              WHERE a.estado != 2 AND a.nro_opera ='" . $opera . "'";  
                         }
                          $query = $this->db->query($sql);
                          $result = $query->result_array();

                          return $result;
  }
	
	public function getFactoFilter(){
			
			$result[''] = 'Seleccione un Factoring';
			$sql = " SELECT `id_empresa`,`nombre_empresa` FROM smw_tre_cl_smartway.TRAZER_DATA_EMPRESA WHERE id_status = '0' ORDER BY `id_empresa` ASC; ";
			$_facto = $this->db->query($sql);
			
			$result = $result + array_column($_facto->result_array(), 'nombre_empresa', 'id_empresa');
			
			return $result;
	}
		
  public function getFactoCliente(){

                        $result[''] = 'Seleccione un Cliente';
                        $sql = " SELECT `id_cliente`,`nombre_cliente` FROM smw_tre_cl_smartway.TRAZER_MAS_CLIENTES WHERE id_status = '0' ORDER BY `nombre_cliente` ASC; ";
                        $_zonas = $this->db->query($sql);

                        $result = $result + array_column($_zonas->result_array(), 'nombre_cliente', 'id_cliente');

                        return $result;
  }

  public function getRut($id){

      $this->db->select('rut_cliente AS rut');
      $this->db->where('id_cliente', $id);
      $query = $this->db->get(smw_tre_cl_smartway . '.TRAZER_MAS_CLIENTES');
      $result = $query->result_array();
      
      return $result;
  }

  public function getTipoDoc(){
			
			$result[''] = 'Seleccione Documento';
			$sql = " SELECT `id`,`nom_documento` FROM smw_tre_cl_smartway.TRAZER_MAS_TIPO_DOCUMENTO ORDER BY `nom_documento`; ";
			$doc = $this->db->query($sql);
			$result = $result+array_column($doc->result_array(), 'nom_documento', 'id');
			return $result;	
	}

  public function get_cliente_dropdown($clie){

   
      			$result[''] = 'Seleccione un Cliente';
			      $this->db->select('id_cliente AS id ,nombre_cliente AS name ');
			      if ($clie){
			      $this->db->where('id_empresa', $clie);
			               }
			     $query  = $this->db->get('smw_tre_cl_smartway.TRAZER_MAS_CLIENTES');
			     $result = $query->result_array();
			
	        	return $result;            
  }

} ?>
