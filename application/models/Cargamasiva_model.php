<?php
require_once INTERFACE_HOME_PATH . '/application/libraries/nusoap.php';

class Cargamasiva_model extends CI_Model {

	public function __construct(){
		$this->load->database();

	}

	/*Guarda en la base de datos la información del formulario creado */
	public function cargamasivatickets($arraydatos){
		$arr = array();
		$this->db->trans_begin();

        foreach ($arraydatos as $row) {
        	//new dBug($row);
			$ticket_data = array(
				'incident' => $row['incident'],
				'idstatus' => STATUSABIERTO,
				'title' => $row['title'],
				'address' => $row['address'],
				'response' => $row['response'],
				'datecreation' => $row['datecreation'],
				'updatetime' => $row['updatetime'],
				'dateclose' => $row['dateclose'],
				'iduser' => $row['iduser'],
				'coordX' => $row['coordX'],
				'coordY' => $row['coordY'],
				'id_form' => $row['id_form'],
				'sla' => $row['sla'],
				'id_group' => $row['id_group'],
				'description' => $row['descripcion'],
				'datestandby' => $row['datestandby'],
				'id_customer' => $row['id_customer'],
				'service' => $row['service'],
				'category' => $row['category'],
				'type' => $row['type'],
				'preventivo' => $row['preventivo']
			);
			$arre = array();
			if($this->db->insert(smw_tre_cl_smartway.'.TRAZER_DATA_INCIDENT' ,$ticket_data)){
				$arre['error'] = false;
				$arre['id']['message'] = $this->db->insert_id()." /  incident N°: ".$row['incident'];
				array_push($arr, $arre) ;
			}else{
				$arre['error'] = true;
				$arre['id'] = $row['incident'];
				array_push($arr, $arre) ;
			}
        }
//		  print_r($this->db->last_query());

		if($this->db->trans_status() === FALSE){
//			$this->db->trans_rollback();
        	//new dBug('rollback');
        	//new dBug($this->db->trans_status());exit();			
		}
		$this->db->trans_commit();
        return $arr;
	}
}

		