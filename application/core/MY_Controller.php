<?php defined('BASEPATH') OR exit('No direct script access allowed');
 require_once INTERFACE_HOME_PATH . '/application/libraries/dBug.php';
 
class MY_Controller extends CI_Controller {
	protected $data = array();
	

	function __construct() {
		parent::__construct();
	}

	protected function render($view = NULL, $template = 'master'){
		if($template == 'json' || $this->input->is_ajax_request()){
			header('Content-Type: application/json');
			//echo json_encode($this->data);
		}else {				
			$this->data['content'] = (is_null($view)) ? '' : $this->load->view($view , $this->data, TRUE);
			$this->data['menu'] = $this->load->view('templates/menu/admin_menu', NULL, TRUE);
			$this->data['flashMessage'] = $this->load->view('templates/message/flash_message', $this->data, TRUE);
			$this->data['modalMessage'] = $this->load->view('templates/message/modal_message', $this->data, TRUE);
			$this->load->view('templates/'.$template.'_view', $this->data);
		}
	}
	
	protected function not_authorized(){
		show_404();
	}
}