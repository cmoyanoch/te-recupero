<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['error_prefix'] = '<div class="form-alert-message"><svg class="icon"><use xlink:href="'.site_url('assets/images/icons.svg').'#icon-warning"></use></svg>';
$config['error_suffix'] = '</div>';

?>