<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

// defined('smw_tre_cl_smartway')      OR define('smw_tre_cl_smartway_DESA', 'smw_tre_cl_smartway_DESA');
// defined('DYNAMIC_FORMS')      OR define('DYNAMIC_FORMS', 'dynamicFormsDesa');
defined('DYNAMIC_FORMS') OR define('DYNAMIC_FORMS', 'smw_tre_cl_dynamicForms');
defined('smw_tre_cl_dynamicForms') OR define('smw_tre_cl_dynamicForms', 'smw_tre_cl_dynamicForms');
define('smw_tre_cl_smartway', 'smw_tre_cl_smartway');
define('INTERFACE_HOME_PATH', '/var/www/html/smartway/cl/terecupero');
define('DIRECTORIO_UPLOAD', '/var/www/html/smartway/cl/terecupero');

define('DOMINIO_DINAMICO', 'https://terecupero-dev.simpledatacorp.com');
define('DOMINIO_RUTA', 'https://terecupero-dev.simpledatacorp.com/');



define('USER_DB', 'desa_trz');
define('PASS_DB', '#tr4z_5.D.g');
//define('HOST_DB', 'localhost');
define('HOST_DB', 'localhost');
define('PORT_DB', '3306');
//define('SCHEMA_DB_SDM', 'sct_smartphone_pe');
define('SCHEMA_DB_SDM', 'smw_tre_cl_smartway');
define('URL_IP_FORM_ENPOINT', DOMINIO_DINAMICO.'/soap/TDC.php?wsdl');
define('URL_IP_FORM_ENPOINT2', DOMINIO_DINAMICO.'/soap/TDC.php?wsdl');
//define('URL_IP_FORM_ENPOINT2', INTERFACE_HOME_PATH.'/soap/TDC.php?wsdl');

define('STATUSABIERTO', '1'); //Abierto
define('STATUSRECEIVED', '2'); //Recibido
define('STATUSINPROGRESS', '3'); //En Progeso
define('STATUSONSITE', '4'); //En el lugar
define('STATUSRESOLVED', '5'); //Resuelto
define('STATUSCLOSE', '6'); //Cerrado
define('STATUSPOSTPONE', '7'); //Pospuesto
define('STATUSSUSPENDED', '8'); //Suspendido
define('STATUSCANCEL', '9'); //Cancelado
define('STATUSCLOSEPARTIAL', '10'); //Cerrado Parcial
define('STATUSASIGN', '11'); //Asignado
define('STATUSVISUALIZED', '12'); //Visualizado
define('STATUSONROUTE', '13'); //En Camino al cliente
define('STATUSREASIGN', '14'); //Reasignado
define('STATUSREOPEN', '15'); //reopen




////////////////////////////////////////////////////////////////////////////////
define('URL_CAPSULA', DOMINIO_DINAMICO.'/CARPETA_CONTENEDORA/CARPETA_CAPSULAS/'); // URL DE LAS CÁPSULAS
define('URL_MANUAL', DOMINIO_DINAMICO.'/CARPETA_CONTENEDORA/CARPETA_MANUALES/'); // URL DE LAS CÁPSULAS
define('LOAD_GIF', DOMINIO_DINAMICO.'/toolbox/pe/portal/v0.01/loadGif/loading.gif');
define('DIRECTORIO_CAPSULA', '/var/www/html/smartway/cl/terecupero/CARPETA_CONTENEDORA/CARPETA_CAPSULAS/'); // URL DE LAS CAPSULAS
define('DIRECTORIO_MANUAL', '/var/www/html/smartway/cl/terecupero/CARPETA_CONTENEDORA/CARPETA_MANUALES/'); // URL DE LOS MANUALES
define('PAGINADO_CANT', '10');
define('CSV_DIR', DOMINIO_DINAMICO.'/files/usability/');
define('URL_IP', DOMINIO_DINAMICO . '/soap/TDC.php?wsdl'); // IP de servicios para Bandeja Comercial
//define('smw_tre_cl_smartway', 'smw_tre_cl_smartway');
//define('DYNAMIC_FORMS', 'dynamicForms');

define('SESSION_LOST_CODE', 5);
define('SESSION_LOST_DESCRIPTION', 'Sesion Caducada');
