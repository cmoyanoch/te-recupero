<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

require_once INTERFACE_HOME_PATH . '/application/libraries/Classes/PHPExcel.php';
	
	class Tecnicos extends MY_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('tecnicos_model');
			$this->load->model('ticket_model');
			$this->session->keep_flashdata('message');
			if (!$this->ion_auth->logged_in()){
				redirect('auth/login');
			}
		}
		
		public function account()
		{
			$user = $this->ion_auth->user()->row();
			$data['title'] = "Mis Datos";
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			$data['authorizations'] = $this->user_model->get_authorizations();
			$data['user'] = array(
				'name' => $user->first_name . " " . $user->last_name,
				'email' => $user->email
			);
			$this->data = $data;
			$this->render('user/account');
			
		}
		
		public function password()
		{
			$data['title'] = "Cambiar Contraseña";
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			$this->form_validation->set_rules('user[password]', 'contraseña', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[user[password_confirm]]');
			$this->form_validation->set_rules('user[password_confirm]', 'repita contraseña', 'required');
			
			if ($this->form_validation->run() == FALSE){
				$this->data = $data;
				$this->render('user/password');
			} else {
				$user = $this->input->post('user');
				if ($this->user_model->change_password($user) == FALSE){
					$this->data = $data;
					$this->session->set_flashdata('flashMessage', -1);
					$this->render('user/password');
				} else {
					$this->session->set_flashdata('flashMessage', 2);
					redirect('user/account');
				}
			}
		}
		
		public function index()
		{
			$id_perfil = $this->session->userdata('id_perfil');
			if (!$this->ion_auth->is_grupo($id_perfil, '19')){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('user/account');
			}
			
			$data['search_value'] = "";
			$config['per_page'] = 7;
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			$page = $this->input->get('page') != null ? $this->input->get('page') : 1;
			
			if (!is_null($this->input->get('value'))){
				$config['base_url'] = site_url('tecnicos/index?value=' . $this->input->get('value'));
				$config['total_rows'] = $this->tecnicos_model->count($this->input->get('value'));
				$data['tecnicos'] = $this->tecnicos_model->find($this->input->get('value'), $config['per_page'], $page);
				$data['search_value'] = $this->input->get('value');
			} else {
				$config['base_url'] = site_url('tecnicos/index');
				$config['total_rows'] = $this->tecnicos_model->count();
				$data['tecnicos'] = $this->tecnicos_model->find(null, $config['per_page'], $page);
			}
			
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$this->data = $data;
			$this->render('tecnicos/index');
		}
		
		public function create()
		{
			$this->session->mark_as_temp('message', 5);
			
			$id_perfil = $this->session->userdata('id_perfil');
			if (!$this->ion_auth->is_grupo($id_perfil, '16')){
				
				$this->session->set_flashdata('flashMessage', 7);
				redirect('user/account');
			}

			$data['title']  = "Crear Técnico";
			$data['groups'] = $this->tecnicos_model->get_group_dropdown();
			$data['zona']   = $this->ticket_model->get_zona_dropdown();
			
			$this->form_validation->set_rules('tec[name]', 'Nombre Cliente', 'required');
			$this->form_validation->set_rules('tec[rut]', 'RUT', 'required');
			$this->form_validation->set_rules('tec[direc]',  'Direccion', 'required');

			/*
			$iValidEmail = $this->tecnicos_model->validEmail($this->input->post('tec'));
			$iValidPhone = $this->tecnicos_model->validPhone($this->input->post('tec'));
			$iValidImei  = $this->tecnicos_model->validImei($this->input->post('tec'));
			
			if (count($iValidEmail) > 0){
				$this->session->set_flashdata('message', 'Ya existe Email en Base de Datos');
			}
			
			if (count($iValidPhone) > 0){
				$this->session->set_flashdata('message', 'Ya existe Telefono en Base de Datos');
			}
			
			if (count($iValidImei) > 0){
				$this->session->set_flashdata('message', 'Ya existe IMEI en Base de Datos');
			}

            */
			
			if ($this->form_validation->run() == FALSE){
				
				$tmpSD = $this->input->post('tec');
				
				if (!empty($tmpSD)){

					$data['tec'] = $this->input->post('tec');
				}
				
				$this->data = $data;
				$this->render('tecnicos/create');
				
			} else {
				
				if ($this->tecnicos_model->create($this->input->post('tec')) == FALSE){
					
					$data['tec'] = $this->input->post('tec');
					$this->data = $data;
					$this->session->set_flashdata('flashMessage', -1);
					$this->render('tecnicos/create');
				} else {
					
					$this->session->set_flashdata('flashMessage', 1);
					redirect('tecnicos/index');
				}
			}
		}
		
		public function delete()
		{
			$id_perfil = $this->session->userdata('id_perfil');
			if (!$this->ion_auth->is_grupo($id_perfil, '18')){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('tecnicos/index');
			}
			
			$result = $this->tecnicos_model->delete($this->uri->segment(3, 0));
			if ($result){
				$this->session->set_flashdata('flashMessage', 0);
			} else {
				$this->session->set_flashdata('flashMessage', -1);
			}
			redirect('tecnicos/index');
		}
		
		public function change_state()
		{
			if (!$this->ion_auth->in_group('9') && !$this->ion_auth->is_admin()){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('tecnicos/index');
			}
			if ($user = $this->input->post('user')){
				$result = FALSE;
				$this->session->set_flashdata('flashMessage', -1);
				
				if ($user['action'] == 0){
					$result = $this->user_model->deactivate($user['user_id']);
					$this->session->set_flashdata('flashMessage', 6);
				}
				else if ($user['action'] == 1){
					$result = $this->user_model->activate($user['user_id']);
					$this->session->set_flashdata('flashMessage', 5);
				}
			}
			redirect('tecnicos/index');
		}
		
		public function edit($id)
		{
			$id_perfil = $this->session->userdata('id_perfil');
			if (!$this->ion_auth->is_grupo($id_perfil, '17')){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('tecnicos/index');
			}
			
			$data['title'] = "Editar Técnico";
			$data['edit'] = TRUE;
			
			$data['zona'] = $this->ticket_model->get_zona_dropdown();
			
			
			$auxTec = $this->tecnicos_model->get($id);
			if ($this->input->post('tec')){
				$tecnicos = $this->input->post('tec');
			} else {
				$tecnicos = $auxTec;
			}
			$data['groups'] = $this->tecnicos_model->get_group_dropdown($tecnicos['id_zona']);
			$data['tec'] = $tecnicos;
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			
			$this->form_validation->set_rules('tec[name]', 'name', 'required');
			
			if ($this->form_validation->run() == FALSE){
				$this->data = $data;
				$this->render('tecnicos/create');
			} else {
				if ($this->tecnicos_model->edit($tecnicos) == FALSE){
					$this->data = $data;
					$this->session->set_flashdata('flashMessage', -1);
					$this->render('tecnicos/create');
				} else {
					//Envía aviso de creación exitosa
					$this->session->set_flashdata('flashMessage', 1);
					redirect('tecnicos/index');
				}
			}
		}
		
		public function setInactive(){
			
			$type       = (int)$this->input->post('type');
			$commentary = addslashes(trim($this->input->post('commentary')));
			$idTec      = (int)$this->input->post('idTec');
			
			if($type && $commentary && $idTec){
				$response = $this->tecnicos_model->setInactive($idTec, $type, $commentary);
			}

			return print_r($response);
		}
		
		public function setActive(){
			
			$idTec      = (int)$this->input->post('idTec');
			
			if($idTec){
				$response = $this->tecnicos_model->setActive($idTec);
			}
			
			return print_r($response);
		}


		public function getExcelTecnicos()
		{
			$objPHPExcel    =   new PHPExcel();
			 
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Nombre Cliente');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Rut');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Direccion');
	
			$objPHPExcel->getActiveSheet()->getStyle("A1:C1")->getFont()->setBold(true);

			$i=2;


			$data['tecnicos'] = $this->tecnicos_model->find();

			$fecha = date('dmYhis');


			foreach ($data['tecnicos'] as $tecnico) 
			{

		   $objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $tecnico['nomb']);
		   $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $tecnico['rut']);
		   $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $tecnico['direc']);

				$i++;

			}
			

			 
			$objWriter  =   new PHPExcel_Writer_Excel2007($objPHPExcel);
			 
			 
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="listado-clientes-'.$fecha.'.xlsx"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
			$objWriter->save('php://output');

		}
	
		
	}
