<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_type extends MY_Controller {

	function __construct() {
		echo "acaaaaaa";
		parent::__construct();		
		$this->load->model('question_type_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}	
	}

	/* Listado de tipos */
	public function index(){

		echo 'xxxxxxxxxx';
		// $data['search_value'] = "";
		// $config['per_page'] = 7;
		// $data['flashMessage'] = $this->session->flashdata('flashMessage');	
		// $page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
		// if(!is_null($this->input->get('value'))){
		// 	$config['base_url'] = site_url('question_type/index?value='.$this->input->get('value'));						
		// 	$config['total_rows'] = $this->question_type_model->count($this->input->get('value'));			
		// 	$data['tipos'] = $this->question_type_model->find($this->input->get('value'), $config['per_page'], $page);
		// 	$data['search_value'] = $this->input->get('value');
		// }else{
		// 	$config['base_url'] = site_url('question_type/index');
		// 	$config['total_rows'] = $this->question_type_model->count();			
		// 	$data['tipos'] = $this->question_type_model->find(null, $config['per_page'], $page);			
		// }
		// $this->pagination->initialize($config);		
		// $data['pagination'] = $this->pagination->create_links();
		// $this->data = $data;
		// $this->render('question_type/index');
	}

	/*Formulario de creación de type */
	public function create(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('question_type/index');
		}
		$data['title'] = "Crear Tipo";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');

		$this->form_validation->set_rules('question_type[name]', 'nombre', 'required|is_unique[question_type.name]');		
		if($this->form_validation->run() == FALSE){
                        $tmpSD = $this->input->post('question_type');
			if(!empty($tmpSD)){
				$data['question_type'] = $this->input->post('question_type');
			}
			$this->data = $data;
			$this->render('question_type/create');
		}else{
			if($this->question_type_model->create($this->input->post('question_type')) == FALSE){				
				$data['question_type'] = $this->input->post('question_type');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('question_type/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('question_type/index');
			}		
		}	
	}

	/* Formulario de edición de type */
	public function edit($question_type_id){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('question_type/index');
		}
		$data['title'] = "Editar Tipo";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$auxType = $this->question_type_model->get($question_type_id);
		if($this->input->post('question_type')){
			$question_type = $this->input->post('question_type');
		}else{
			$question_type = $auxType;
		}
		$data['question_type'] = $question_type;

		if($question_type['name'] != $auxType['name']){
			$this->form_validation->set_rules('question_type[name]', 'nombre', 'required');			
		}else{
			$this->form_validation->set_rules('question_type[name]', 'nombre', 'required');			
		}
		
		$this->form_validation->set_rules('question_type[question_type_id]', 'index de los question_type', 'required');	
		if($this->form_validation->run() == FALSE){						
			$this->data = $data;
			$this->render('question_type/create');
		}else{
			if($this->question_type_model->edit($question_type) == FALSE){								
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('question_type/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('question_type/index');
			}		
		}
	}

	/* Función Eliminar question_type */
	public function delete(){
        //Se comprueban las credenciales                        
            if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
                    $this->session->set_flashdata('flashMessage', 7);
                    redirect('question_type/index');
            }		
            if($question_type = $this->input->post('question_type')){	                     
                    $result = $this->question_type_model->delete($question_type['question_type_id']);									
                    if($result){
                            $this->session->set_flashdata('flashMessage', 0);
                    }else{
                            $this->session->set_flashdata('flashMessage', -1);
                    }			
            }		
            redirect('question_type/index');                                 
	}

}
