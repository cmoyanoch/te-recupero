<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	date_default_timezone_set("Chile/Continental");
	
	ini_set('upload_max_filesize', '45M');
	ini_set('post_max_size', '45M');
	ini_set('max_input_time', 3000);
	ini_set('max_execution_time', 3000);
	
	
	class Foros extends MY_Controller
	{
		
		
		public function __construct()
		{
			parent::__construct();
			$this->load->model('Foros_model');
			
			if (!$this->ion_auth->logged_in()){
				redirect('auth/login');
			}
		}
		
		public function index()
		{
			
			$fechaInicio = "0";
			$fechaFinal = "0";
			$titulo = "0";
			$sumador = "0";
			
			$data['template'] = 'foros/tablaListForos';
			$data['datos'] = $this->Foros_model->ObtieneListaForos($fechaInicio, $fechaFinal, $sumador, $titulo);
			$data['CReg'] = $this->Foros_model->ObtieneCantListaForos($fechaInicio, $fechaFinal, $titulo);
			$data['contadorPag'] = 1;
			
			$data['data'] = $data;
			$this->data = $data;
			$this->render('foros/tablaListForos');
			
		}
		
		
		public function PaginarForos()
		{
			
			$fechaInicio = "0";
			$fechaFinal = "0";
			$titulo = "0";
			$sumador = PAGINADO_CANT;
			
			if (isset($_REQUEST['paginador'])){
				$paginador = $this->input->post('paginador', TRUE);
			}
			
			$mult = $paginador - 1;
			$sumador = $mult * $sumador;
			
			$data['template'] = 'foros/tablaListForos';
			$data['datos'] = $this->Foros_model->ObtieneListaForos($fechaInicio, $fechaFinal, $sumador, $titulo);
			$data['CReg'] = $this->Foros_model->ObtieneCantListaForos($fechaInicio, $fechaFinal, $titulo);
			$data['contadorPag'] = $paginador;
			$this->load->view('foros/tablaListForos', array('data' => $data));
		}
		
		
		public function PaginarForos2()
		{
			
			$datepickerInicio = "";
			$datepickerFinal = "";
			$titulo = "0";
			$sumador = PAGINADO_CANT;
			
			if (isset($_REQUEST['paginador'])){
				$paginador = $this->input->post('paginador', TRUE);
			}
			if (isset($_REQUEST['datepickerInicio'])){
				$datepickerInicio = $this->input->post('datepickerInicio', TRUE);
				$datepickerInicio = $this->invertirFecha($datepickerInicio);
				
			}
			if (isset($_REQUEST['datepickerFinal'])){
				$datepickerFinal = $this->input->post('datepickerFinal', TRUE);
				$datepickerFinal = $this->invertirFecha($datepickerFinal);
			}
			
			$mult = $paginador - 1;
			$sumador = $mult * $sumador;
			
			$data['template'] = 'foros/tablaListForos';
			$data['datos'] = $this->Foros_model->ObtieneListaForos($datepickerInicio, $datepickerFinal, $sumador, $titulo);
			$data['CReg'] = $this->Foros_model->ObtieneCantListaForos($datepickerInicio, $datepickerFinal, $titulo);
			$data['contadorPag'] = $paginador;
			$this->load->view('foros/tablaListForos', array('data' => $data));
		}
		
		
		public function PaginarForos3()
		{
			
			$datepickerInicio = "0";
			$datepickerFinal = "0";
			$titulo = "0";
			$sumador = PAGINADO_CANT;
			
			if (isset($_REQUEST['paginador'])){
				$paginador = $this->input->post('paginador', TRUE);
			}
			if (isset($_REQUEST['titulo'])){
				$titulo = $this->input->post('titulo', TRUE);
			}
			
			$mult = $paginador - 1;
			$sumador = $mult * $sumador;
			
			$data['template'] = 'foros/tablaListForos';
			$data['datos'] = $this->Foros_model->ObtieneListaForos($datepickerInicio, $datepickerFinal, $sumador, $titulo);
			$data['CReg'] = $this->Foros_model->ObtieneCantListaForos($datepickerInicio, $datepickerFinal, $titulo);
			$data['contadorPag'] = $paginador;
			$this->load->view('foros/tablaListForos', array('data' => $data));
		}
		
		
		public function buscarLFfecha()
		{
			
			$fechaInicio = "0";
			$fechaFinal = "0";
			$titulo = "0";
			$sumador = "0";
			$fecha1 = date('d/m/Y');
			$aux = "";
			
			if (isset($_REQUEST['datepickerInicio'])){
				$fechaInicio = $this->input->post('datepickerInicio', TRUE);
				
				if ($fechaInicio == 0){
					$fechaInicio = $fecha1;
				}
				$fechaInicio = $this->invertirFecha($fechaInicio);
			}
			
			
			if (isset($_REQUEST['datepickerFinal'])){
				$fechaFinal = $this->input->post('datepickerFinal', TRUE);
				
				if ($fechaFinal == 0){
					$fechaFinal = $fecha1;
				}
				$fechaFinal = $this->invertirFecha($fechaFinal);
			}
			
			
			if ($fechaInicio > $fechaFinal){
				$aux = $fechaInicio;
				$fechaInicio = $fechaFinal;
				$fechaFinal = $aux;
			}
			
			
			$data['template'] = 'foros/tablaListForos';
			$data['datos'] = $this->Foros_model->ObtieneListaForos($fechaInicio, $fechaFinal, $sumador, $titulo);
			$data['CReg'] = $this->Foros_model->ObtieneCantListaForos($fechaInicio, $fechaFinal, $titulo);
			$data['contadorPag'] = 1;
			
			$this->load->view('foros/tablaListForos', array('data' => $data));
		}
		
		
		public function buscarLFtitulo()
		{
			
			$fechaInicio = "0";
			$fechaFinal = "0";
			$titulo = "0";
			$sumador = "0";
			
			if (isset($_REQUEST['titulo'])){
				$titulo = $this->input->post('titulo', TRUE);
			}
			
			
			$data['template'] = 'foros/tablaListForos';
			$data['datos'] = $this->Foros_model->ObtieneListaForos($fechaInicio, $fechaFinal, $sumador, $titulo);
			$data['CReg'] = $this->Foros_model->ObtieneCantListaForos($fechaInicio, $fechaFinal, $titulo);
			$data['contadorPag'] = 1;
			
			
			if ($data['datos']){
				$this->load->view('foros/tablaListForos', array('data' => $data));
			} else {
				$val = 'NOK';
				return print_r($val);
			}
		}
		
		public function EliminarForo()
		{
			
			$fechaInicio = "0";
			$fechaFinal = "0";
			$titulo = "0";
			$sumador = "0";
			
			
			$id = "";
			$val = "NOK";
			
			if (isset($_REQUEST['id'])){
				// $id   = $_REQUEST['id'];
				$id = $this->input->post('id', TRUE);
			}
			
			
			$r1 = $this->Foros_model->EliminaRespForos_model($id);
			
			if ($r1 == 1){
				
				$data['template'] = 'foros/tablaListForos';
				$data['datos'] = $this->Foros_model->ObtieneListaForos($fechaInicio, $fechaFinal, $sumador, $titulo);
				$data['CReg'] = $this->Foros_model->ObtieneCantListaForos($fechaInicio, $fechaFinal, $titulo);
				$data['contadorPag'] = 1;
				
				$this->load->view('foros/tablaListForos', array('data' => $data));
				
			} else {
				$r1 = 100;
				return $r1;
			}
			
			return $val;
		}
		
		
		public function EditarForo()
		{
			
			$id = "";
			
			if (isset($_REQUEST['id'])){
				
				$id = $this->input->post('id', TRUE);
			}
			
			
			$data['foro'] = $this->Foros_model->ObtieneForo($id);
			$this->load->view('foros/popup_editar', array('data' => $data));
			
		}
		
		
		public function EditarForo2()
		{
			
			$fechaInicio = "0";
			$fechaFinal = "0";
			$titulo = "0";
			$sumador = "0";
			$fecha = date('d/m/Y');
			$hora = date('H:m:i');
			
			
			$id = "";
			
			if (isset($_REQUEST['id'])){
				$id = $this->input->post('id', TRUE);
			}
			
			if (isset($_REQUEST['titulo'])){
				$titulo = $this->input->post('titulo', TRUE);
			}
			
			if (isset($_REQUEST['campoObs'])){
				$campoObs = $this->input->post('campoObs', TRUE);
			}
			
			
			$user_id = $_SESSION['user_id'];
			$fecha = $this->invertirFecha($fecha);
			$fecha = $fecha . ' ' . $hora;
			
			$data['foro'] = $this->Foros_model->EditarForo($id, $titulo, $campoObs, $user_id, $fecha);
			
		}
		
		
		public function VerForo()
		{
			
			$id = "";
			
			if (isset($_REQUEST['id'])){
				
				$id = $this->input->post('id', TRUE);
			}
			
			$data['foro'] = $this->Foros_model->ObtieneForo($id);
			$data['com'] = $this->Foros_model->ObtieneComentarios($id);
			$this->load->view('foros/popup_ver', array('data' => $data));
			
		}
		
		
		public function BorrarComentario()
		{
			
			$fechaInicio = "0";
			$fechaFinal = "0";
			$titulo = "0";
			$sumador = "0";
			
			if (isset($_REQUEST['id'])){
				$id = $this->input->post('id', TRUE);
			}
			if (isset($_REQUEST['countAnswer'])){
				$countAnswer = $this->input->post('countAnswer', TRUE);
			}
			if (isset($_REQUEST['id_foro'])){
				$id_foro = $this->input->post('id_foro', TRUE);
			}
			
			$countAnswer = $countAnswer - 1;
			
			$r1 = $this->Foros_model->BorrarComentario_model($id);
			$r2 = $this->Foros_model->RestarResp($countAnswer, $id_foro);
			
			$data['template'] = 'foros/tablaListForos';
			$data['datos'] = $this->Foros_model->ObtieneListaForos($fechaInicio, $fechaFinal, $sumador, $titulo);
			$data['CReg'] = $this->Foros_model->ObtieneCantListaForos($fechaInicio, $fechaFinal, $titulo);
			$data['contadorPag'] = 1;
			
			$this->load->view('foros/tablaListForos', array('data' => $data));
			
		}
		
		
		public function LevantaPopup()
		{
			
			if (isset($_REQUEST['ok'])){
				
				// $ok = $_REQUEST['ok'];
				$ok = $this->input->post('ok', TRUE);
			}
			
			$this->load->view('foros/popup_crear');
			
		}
		
		
		public function guardarNUevoForo()
		{
			
			$fechaInicio = "0";
			$fechaFinal = "0";
			$sumador = "0";
			$id_userGuarda = $_SESSION['user_id'];
			
			$fecha = date('d/m/Y');
			$hora = date('H:m:i');
			
			$fecha = $this->invertirFecha($fecha);
			$fecha = $fecha . ' ' . $hora;
			
			
			if (isset($_REQUEST['titulo'])){
				$titulo = $this->input->post('titulo', TRUE);
			}
			if (isset($_REQUEST['descripcion'])){
				$descripcion = $this->input->post('descripcion', TRUE);
			}
			
			$resul = $this->Foros_model->GuardarForo($titulo, $descripcion, $fecha, $id_userGuarda);
			
		}
		
		
		public function invertirFecha($fecha)
		{
			
			$f = explode("/", $fecha);
			$fechaF = $f["2"] . "/" . $f["1"] . "/" . $f["0"];
			
			return $fechaF;
		}
		
		
	}
