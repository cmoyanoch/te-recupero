<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Priority extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('priority_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	public function index(){
		$this->render('priority/index');
	}

	public function getDataInit() {
		echo json_encode($this->priority_model->getData());
	}

	public function updatePriority(){
		$idPriority = rawurlencode($this->input->post('idPriority'));
		$namePriority = rawurlencode($this->input->post('namePriority'));

		echo json_encode($this->priority_model->updateData($idPriority, $namePriority));
	}

	public function daletePriority() {
		$idPriority = rawurlencode($this->input->post('idPriority'));

		echo json_encode($this->priority_model->deleteData($idPriority));
	}

	public function createPriority() {
		$namePriority = rawurlencode($this->input->post('namePriority'));

		echo json_encode($this->priority_model->createData($namePriority));
	}
}
