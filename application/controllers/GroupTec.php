<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GroupTec extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('grouptec_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){
		$data['search_value'] = "";
		$config['per_page'] = 7;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('grouptec/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->grouptec_model->count($this->input->get('value'));			
			$data['grouptecs'] = $this->grouptec_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
		}else{
			$config['base_url'] = site_url('grouptec/index');
			$config['total_rows'] = $this->grouptec_model->count();			
			$data['grouptecs'] = $this->grouptec_model->find(null, $config['per_page'], $page);			
		}
		$this->pagination->initialize($config);		
		$data['pagination'] = $this->pagination->create_links();
		$this->data = $data;
		$this->render('grouptec/index');
	}

	/*Formulario de creación de categoría */
	public function create(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('groupTec/index');
		}
		$data['title'] = "Crear cuadrilla";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');

		$this->form_validation->set_rules('grouptec[name]', 'nombre', 'required|is_unique[grouptec.name]');		
		if($this->form_validation->run() == FALSE){
                        $tmpSD = $this->input->post('grouptec');
			if(!empty($tmpSD)){
				$data['grouptec'] = $this->input->post('grouptec');
			}
			$this->data = $data;
			$this->render('grouptec/create');
		}else{
			if($this->grouptec_model->create($this->input->post('grouptec')) == FALSE){				
				$data['grouptec'] = $this->input->post('grouptec');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('grouptec/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('groupTec/index');
			}		
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){

		
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('groupTec/index');
		}
		$data['title'] = "Editar cuadrilla";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$auxgrouptec = $this->grouptec_model->get($id);
		if($this->input->post('grouptec')){
			$grouptec = $this->input->post('grouptec');  
		}else{
			$grouptec = $auxgrouptec;
		}
		$data['grouptec'] = $grouptec;

		// if($grouptec['name'] != $auxgrouptec['name']){
		// 	$this->form_validation->set_rules('grouptec[name]', 'nombre', 'required');			
		// }else{
			$this->form_validation->set_rules('grouptec[name]', 'nombre', 'required');			
		// }
		
		$this->form_validation->set_rules('grouptec[grouptec_id]', 'index de la cuadrilla', 'required');	
		if($this->form_validation->run() == FALSE){						
			$this->data = $data;
			$this->render('grouptec/create');
		}else{

		//echo $this->grouptec_model->edit($grouptec);
			if($this->grouptec_model->edit($grouptec) == FALSE){							
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('grouptec/create');
			}else{ 
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('groupTec/index');
			}		
		}
	}

	/* Función Eliminar categoría */
	public function delete(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('groupTec/index');
		}		
		if($grouptec = $this->input->post('grouptec')){												
			$result = $this->grouptec_model->delete($grouptec['grouptec_id']);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('groupTec/index');
	}

}
