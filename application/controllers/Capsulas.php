<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Chile/Continental");

ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '100M');
ini_set('max_input_time', 3000);
ini_set('max_execution_time', 3000);
// ini_set('MAX_FILE_SIZE', '100M');


 
// require_once APPPATH . '/libraries/Clases/PHPExcel.php';

class Capsulas extends MY_Controller {


    public function __construct()
        {
            parent::__construct();  
            $this->load->model('Capsulas_model');
//            session_start();

            if (!$this->ion_auth->logged_in()){
                redirect('auth/login');
            }

        }	

	public function index()	{   	    

            $sumador = 0;
            $data['listado'] = $this->Capsulas_model->CapsuleData($sumador);
            $data['CReg'] = $this->Capsulas_model->ContCapsule(); 
            $data['contadorPag'] = 1;
            
            $data['data']=$data;
            $this->data = $data;
            $this->render('capsulas/index');
            
	}


	public function PaginarCapsulas(){   	    

            $sumador = PAGINADO_CANT;

            if (isset($_REQUEST['paginador'])) {
                    $paginador = $this->input->post('paginador', TRUE);
            }

            $mult = $paginador -1;
            $sumador = $mult*$sumador;

                    $data['listado'] = $this->Capsulas_model->CapsuleData($sumador);
                    $data['CReg'] = $this->Capsulas_model->ContCapsule();
                    $data['contadorPag'] = $paginador;

            $this->load->view('capsulas/index', array('data' => $data));

	}


	public function PaginarCapsulas2(){   	    

        $sumador = PAGINADO_CANT;

        if (isset($_REQUEST['paginador'])) {
        	$paginador = $this->input->post('paginador', TRUE);
        }

        if (isset($_REQUEST['buscar'])) {
        	$buscar = $this->input->post('buscar', TRUE);
        }

       	$mult = $paginador -1;
     	$sumador = $mult*$sumador;

        $data['listado'] = $this->Capsulas_model->BuscarCap($buscar,$sumador); 
        $data['CReg'] = $this->Capsulas_model->BuscarCap_cont($buscar);    
		$data['contadorPag'] = $paginador;

        $this->load->view('capsulas/tableCapsule', array('data' => $data));

	}



	public function StateChangeCap(){   

            $id = $this->input->post('id', TRUE);
            $data['state'] = $this->Capsulas_model->StateDocumentCap($id);

            
            $buscar = "";
            $sumador = 0;

              $data['listado'] = $this->Capsulas_model->BuscarCap($buscar,$sumador); 
              $data['CReg'] = $this->Capsulas_model->BuscarCap_cont($buscar);    
              $data['contadorPag'] = 1;

            if ($data['listado']) {     
              $this->load->view('capsulas/index', array('data' => $data));      
             }else{
               $val = 'NOK';
               return print_r($val);
             }        
            
	}



	public function uploadCap(){

            $resp = "";
		$type = "";
		$description = $this->input->post('descriptionCap', TRUE);
		$FullName = $_FILES['userfile']['name'];
		$rutaWeb = DIRECTORIO_CAPSULA;

                
		$tipo_archivo = $_FILES['userfile']['type'];

		$FullName = str_replace(' ', '_', $FullName);
		$FullName = str_replace('á', 'a', $FullName);
		$FullName = str_replace('é', 'e', $FullName);
		$FullName = str_replace('í', 'i', $FullName);
		$FullName = str_replace('ó', 'o', $FullName);
		$FullName = str_replace('ú', 'u', $FullName);
		$FullName = str_replace('Á', 'A', $FullName);
		$FullName = str_replace('É', 'E', $FullName);
		$FullName = str_replace('Í', 'I', $FullName);
		$FullName = str_replace('Ó', 'O', $FullName);
		$FullName = str_replace('Ú', 'U', $FullName);
		$FullName = str_replace('Ñ', 'N', $FullName);
		$FullName = str_replace('ñ', 'n', $FullName);

		$FullName = str_replace('!', '', $FullName);
		$FullName = str_replace('¡', '', $FullName);
		$FullName = str_replace('¿', '', $FullName);
		$FullName = str_replace('(', '', $FullName);
		$FullName = str_replace(')', '', $FullName);
		$FullName = str_replace('$', '', $FullName);
		$FullName = str_replace('&', '', $FullName);
		$FullName = str_replace('#', '', $FullName);
		$FullName = str_replace('=', '', $FullName);
		$FullName = str_replace('+', '', $FullName);
		$FullName = str_replace('{', '', $FullName);
		$FullName = str_replace('}', '', $FullName);
		$FullName = str_replace('[', '', $FullName);
		$FullName = str_replace(']', '', $FullName);

		$FullName = str_replace('ä', 'a', $FullName);
		$FullName = str_replace('ë', 'e', $FullName);
		$FullName = str_replace('ï', 'i', $FullName);
		$FullName = str_replace('ö', 'o', $FullName);
		$FullName = str_replace('ü', 'u', $FullName);
		$FullName = str_replace('Ä', 'A', $FullName);
		$FullName = str_replace('Ë', 'E', $FullName);
		$FullName = str_replace('Ï', 'I', $FullName);
		$FullName = str_replace('Ö', 'O', $FullName);
		$FullName = str_replace('Ü', 'U', $FullName);

		$FullName = str_replace('â', 'a', $FullName);
		$FullName = str_replace('à', 'a', $FullName);
		$FullName = str_replace('ê', 'e', $FullName);
		$FullName = str_replace('è', 'e', $FullName);
		$FullName = str_replace('î', 'i', $FullName);
		$FullName = str_replace('ì', 'i', $FullName);
		$FullName = str_replace('ô', 'o', $FullName);
		$FullName = str_replace('ò', 'o', $FullName);
		$FullName = str_replace('û', 'u', $FullName);
		$FullName = str_replace('ù', 'u', $FullName);

		$FullName = str_replace('Â', 'a', $FullName);
		$FullName = str_replace('À', 'a', $FullName);
		$FullName = str_replace('Ê', 'e', $FullName);
		$FullName = str_replace('È', 'e', $FullName);
		$FullName = str_replace('Î', 'i', $FullName);
		$FullName = str_replace('Ì', 'i', $FullName);
		$FullName = str_replace('Ô', 'o', $FullName);
		$FullName = str_replace('Ò', 'o', $FullName);
		$FullName = str_replace('Û', 'u', $FullName);
		$FullName = str_replace('Ù', 'u', $FullName);




	    $existFullName = $this->Capsulas_model->existFullNameCap($FullName); 
            if ($existFullName['cant'] > 0) {
                $resp = "9";
                return print_r($resp);
            }


		$nom = explode( '.', $FullName );
		$ext = $nom[1];
                

		if ($ext=='m4a' || $ext=='M4A' || $ext=='mp3' || $ext=='MP3' || $ext=='wma' || $ext=='WMA' || $ext=='wav' || $ext=='WAV') {
                    $type = "2";
		}else if ($ext=='mp4' || $ext=='MP4' || $ext=='avi' || $ext=='AVI' || $ext=='wmv' || $ext=='WMV' || $ext=='flv' || $ext=='FLV') {
                    $type = "1";
		}

		if( $ext!='mp4' && $ext!='MP4' && $ext!='avi' && $ext!='AVI' && $ext!='wmv' && $ext!='WMV' && $ext!='flv' && $ext!='FLV' && 
                    $ext!='m4a' && $ext!='M4A' && $ext!='m4a' && $ext!='M4A' && $ext!='mp3' && $ext!='MP3' && $ext!='wma' && $ext!='WMA' && 
                    $ext!='wav' && $ext!='WAV')
		{	
		    $resp = "1";
		    return print_r($resp);
		}
		// Vamos a verificar si hay errores. 
                
                
		if($_FILES['userfile']['error'] > 0){
                    // echo 'Hemos encontrado un problema: ';
                    switch($_FILES['userfile']['error'])
                    {	
                        // Archivo excedio la propiedad de PHP upload_max_filesize
                        case 1:     $resp = "2";	return print_r($resp);

                        case 2: 	$resp = "3";	return print_r($resp);
                        // El archivo se proceso parcialmente
                        case 3: 	$resp = "4";	return print_r($resp);
                        // Archivo no se pudo procesarse
                        case 4: 	$resp = "5";	return print_r($resp);  

                        // default:  	$resp = "6";	return print_r($resp);  
                    }
                    // exit;
		}

		$up_folder = $rutaWeb.$FullName;  
		if(is_uploaded_file($_FILES['userfile']['tmp_name'])){
			if(!move_uploaded_file($_FILES['userfile']['tmp_name'], $up_folder)){
			    $resp = "7";
			    return print_r($resp);
			}
		}
		else{
		    $resp = "8";
		    return print_r($resp);
		}

		$dateNow = date('Y-m-d H:m:s'); 
	    $BDName = $this->Capsulas_model->AddCapsule($description, $FullName, $dateNow, $type); 

            
	    $resp = "OK";
	    return print_r($resp);


	}



    public function BuscarCapsula(){
        
        $buscar = "";
        $sumador = 0;

        if (isset($_REQUEST['buscar'])) {
          $buscar = $this->input->post('buscar', TRUE);
        }  

            $data['listado'] = $this->Capsulas_model->BuscarCap($buscar,$sumador); 
            $data['CReg'] = $this->Capsulas_model->BuscarCap_cont($buscar);    
            $data['contadorPag'] = 1;
          
        if ($data['listado']) {     
            $this->load->view('capsulas/index', array('data' => $data));   
         }else{
            $val = 'NOK';
            return print_r($val);
         }        

   }


	public function actDescCap(){

            $id = "";      
            $desc = "";      

            if (isset($_REQUEST['id'])) {
                    $id = $this->input->post('id', TRUE);
            }     
            if (isset($_REQUEST['desc'])) {
                    $desc = $this->input->post('desc', TRUE);
            }       

            $resp = $this->Capsulas_model->actDescCapsule($id,$desc); 
            return print_r($resp);

	}







	public function invertirFecha($fecha)	{   	    

		$f = explode("/", $fecha);
		$fechaF = $f["2"]."/".$f["1"]."/".$f["0"];
		
		return $fechaF;
	}




}
