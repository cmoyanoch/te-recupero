<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('user_model');
        $this->load->model('tecnicos_model');

		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	/* Listado de datos del usuario */
	public function account(){
		$user = $this->ion_auth->user()->row();
		$data['title'] = "Mis Datos";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');		
		$data['authorizations'] = $this->user_model->get_authorizations();
		$data['user'] = array(
				'name' => $user->first_name. " " . $user->last_name,
				'email' => $user->email
			);
		$this->data = $data;
		$this->render('user/account');

	}

	/* Función que cambia la contraseña de un usuario */
    public function password(){			
        $data['title'] = "Cambiar Contraseña";		
        $data['flashMessage'] = $this->session->flashdata('flashMessage');
        $this->form_validation->set_rules('user[password]', 'contraseña' , 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[user[password_confirm]]');
        $this->form_validation->set_rules('user[password_confirm]', 'repita contraseña', 'required');		
		
        if($this->form_validation->run() == FALSE){						
            $this->data = $data;
            $this->render('user/password');
        }else{
            $user = $this->input->post('user');
            if($this->user_model->change_password($user) == FALSE){								
                $this->data = $data;
                $this->session->set_flashdata('flashMessage', -1);
                $this->render('user/password');
            }else{
                $this->session->set_flashdata('flashMessage', 2);
                redirect('user/account');
            }		
        }
    }

    /* Listado de usuarios */
    public function index(){

        if(!$this->ion_auth->in_group('7') && !$this->ion_auth->in_group('8') && !$this->ion_auth->in_group('9') && !$this->ion_auth->in_group('10') && !$this->ion_auth->is_admin()){
            $this->session->set_flashdata('flashMessage', 7);
            redirect('form/index');
        }
        
        $data['search_value'] = "";
        $per_page   = $this->user_model->countList();
        

        $config['per_page'] = 7 ;

        
        $data['flashMessage'] = $this->session->flashdata('flashMessage');	
        $page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
        
        if(!is_null($this->input->get('value'))){
            $config['base_url']   = site_url('user/index?value='.$this->input->get('value'));						
            $config['total_rows'] = $this->user_model->count($this->input->get('value'));
            $data['users']        = $this->user_model->find($this->input->get('value'), $config['per_page'], $page);
            $data['search_value'] = $this->input->get('value');
        }else{	
            $config['base_url']   = site_url('user/index');
            $config['total_rows'] = $this->user_model->count();	
            $data['users']        = $this->user_model->find(null, $config['per_page'], $page);			
        }
        $this->pagination->initialize($config);		
        
        $data['pagination'] = $this->pagination->create_links();
        $this->data         = $data;
        
        $this->render('user/index');
    }

    public function create(){
        
        if(!$this->ion_auth->in_group('7') && !$this->ion_auth->is_admin()){
            $this->session->set_flashdata('flashMessage', 7);
            redirect('user/index');
        }	
        $data['title'] = "Crear usuario";
        $data['flashMessage'] = $this->session->flashdata('flashMessage');
        $this->form_validation->set_rules('user[first_name]', 'nombre', 'required');
        $this->form_validation->set_rules('user[id_grupo]', 'grupo', 'required');
        $this->form_validation->set_rules('user[last_name]', 'apellidos', 'required');        
        $this->form_validation->set_rules('user[email]', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('user[password]', 'contraseña' , 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[user[password_confirm]]');
        $this->form_validation->set_rules('user[password_confirm]', 'repita contraseña', 'required');
        
        $data['groups']    = $this->tecnicos_model->get_group_dropdown();
        $data['perfil']    = $this->user_model->get_perfil_array();
        $data['customers'] = $this->user_model->getCustomerDropdow();

        if($this->form_validation->run() == FALSE){
            $tmpSD = $this->input->post('user');
            
            if(!empty($tmpSD)){
                $data['user'] = $this->input->post('user');
            }
            $this->data = $data;
            $this->render('user/create');
            
        }else{		
            if($this->user_model->create($this->input->post('user')) == FALSE){				
                $data['user'] = $this->input->post('user');				
                $this->data = $data;
                $this->session->set_flashdata('flashMessage', -1);
                $this->render('user/create');
            }else{
                $this->session->set_flashdata('flashMessage', 1);
                redirect('user/index');
            }	
        }	
    }

    public function delete(){
        
        if(!$this->ion_auth->in_group('10')&& !$this->ion_auth->is_admin()){
            $this->session->set_flashdata('flashMessage', 7);
            redirect('user/index');
        }			
        if($user = $this->input->post('user')){												
            $result = $this->user_model->delete($user['user_id']);									
            if($result){
                $this->session->set_flashdata('flashMessage', 0);
            }else{
                $this->session->set_flashdata('flashMessage', -1);
            }			
        }		
        redirect('user/index');
    }

    public function change_state(){
        
        if(!$this->ion_auth->in_group('9') && !$this->ion_auth->is_admin()){
        $this->session->set_flashdata('flashMessage', 7);
            redirect('user/index');
        }	
        if($user = $this->input->post('user')){
            $result = FALSE;
            $this->session->set_flashdata('flashMessage', -1);
            if($user['action'] == 0){				
                $result = $this->user_model->deactivate($user['user_id']);					
                $this->session->set_flashdata('flashMessage', 6);
            }else if($user['action'] == 1){
                $result = $this->user_model->activate($user['user_id']);	
                $this->session->set_flashdata('flashMessage', 5);
            }			
        }		
        redirect('user/index');
    }

    public function edit($id){
    		if(!$this->ion_auth->in_group('8') && !$this->ion_auth->is_admin()){
            $this->session->set_flashdata('flashMessage', 7);
            redirect('user/index');
        }	

        $data['title'] = "Editar usuario";
        $data['edit'] = TRUE;
        $auxUser = $this->user_model->get($id);
        //print_r( $auxUser);
        if($this->input->post('user')){
            $user = $this->input->post('user');
        }else{ 
            $user = $auxUser;
        }
        
        
        //print_r($user);
        $data['user'] = $user;
        $data['flashMessage'] = $this->session->flashdata('flashMessage');

        $this->form_validation->set_rules('user[first_name]', 'nombre', 'required');
        $this->form_validation->set_rules('user[last_name]', 'apellidos', 'required');
        $this->form_validation->set_rules('user[id_grupo]', 'grupo', 'required');
        
        if($user['email'] != $auxUser['email']){
            $this->form_validation->set_rules('user[email]', 'email', 'required|valid_email|is_unique[users.email]');
        }else{
            $this->form_validation->set_rules('user[email]', 'email', 'required|valid_email');
        }
        if(isset($user['password']) || isset($user['password_confirm'])){
            if($user['password'] != '' || $user['password_confirm'] != ''){				
                $this->form_validation->set_rules('user[password]', 'contraseña' , 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[user[password_confirm]]');
                $this->form_validation->set_rules('user[password_confirm]', 'repita contraseña', 'required');	
            }
        }
        $data['groups']             = $this->tecnicos_model->get_group_dropdown();
        $data['groups_selected']    = $this->user_model->get_groups_by_user_id($id);
        $data['customers']          = $this->user_model->getCustomerDropdow();
        $data['customers_selected'] = $this->user_model->get_customers_by_user_id($id);

        $data['perfil'] = $this->user_model->get_perfil_array();

        if($this->form_validation->run() == FALSE){ 				
            $this->data = $data;
            $this->render('user/create');
        }else{	
            if($this->user_model->edit($this->input->post('user')) == FALSE){	
                $this->data = $data;
                $this->session->set_flashdata('flashMessage', -1);
                $this->render('user/create');
            }else{  
                $this->session->set_flashdata('flashMessage', 1);				
                redirect('user/index');
            }	
        }		
    }

}
