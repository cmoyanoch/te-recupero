<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('category_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){

		$id_perfil = $this->session->userdata('id_perfil');
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}

		$data['search_value'] = "";
		$config['per_page'] = 7;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('category/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->category_model->count($this->input->get('value'));			
			$data['categories'] = $this->category_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
		}else{
			$config['base_url'] = site_url('category/index');
			$config['total_rows'] = $this->category_model->count();			
			$data['categories'] = $this->category_model->find(null, $config['per_page'], $page);	
				
		}
		$this->pagination->initialize($config);		
		$data['pagination'] = $this->pagination->create_links();
		$this->data = $data;
		$this->render('category/index');
	}

	/*Formulario de creación de categoría */
	public function create(){
		//Se comprueban las credenciales
		/*if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('category/index');
		}*/

		$id_perfil = $this->session->userdata('id_perfil');
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}
		
		$data['title'] = "Crear categoría";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');			
		$data['categories'] = $this->category_model->get_categories_dropdown();

		$this->form_validation->set_rules('category[name]', 'nombre', 'required|is_unique[category.name]');		
		if($this->form_validation->run() == FALSE){
                        $tmpSD = $this->input->post('category');
			if(!empty($tmpSD)){
				$data['category'] = $this->input->post('category');
			}
			$this->data = $data;
			$this->render('category/create');
		}else{
			if($this->category_model->create($this->input->post('category')) == FALSE){				
				$data['category'] = $this->input->post('category');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('category/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('category/index');
			}		
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('category/index');
		}
		$data['title'] = "Editar categoría";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');			
		$data['categories'] = $this->category_model->get_categories_dropdown();
		$auxCategory = $this->category_model->get($id);
		if($this->input->post('category')){
			$category = $this->input->post('category');
		}else{
			$category = $auxCategory;
		}
		$data['category'] = $category;

		if($category['name'] != $auxCategory['name']){
			$this->form_validation->set_rules('category[name]', 'nombre', 'required|is_unique[category.name]');			
		}else{
			$this->form_validation->set_rules('category[name]', 'nombre', 'required');			
		}
		
		$this->form_validation->set_rules('category[category_id]', 'index de la categoría', 'required');	
		if($this->form_validation->run() == FALSE){						
			$this->data = $data;
			$this->render('category/create');
		}else{
			if($this->category_model->edit($category) == FALSE){								
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('category/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('category/index');
			}		
		}
	}

	/* Función Eliminar categoría */
	public function delete(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('category/index');
		}		
		if($category = $this->input->post('category')){												
			$result = $this->category_model->delete($category['category_id']);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('category/index');
	}

}
