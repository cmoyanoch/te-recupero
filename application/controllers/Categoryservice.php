<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoryservice extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('Categoryservice_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){
		$data['search_value'] = "";
		$config['per_page'] = 7;
		$this->data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$this->data['category_servicees'] = $this->Categoryservice_model->get_all();
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;
		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('category/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->Categoryservice_model->count($this->input->get('value'));			
			$this->data['categories'] = $this->Categoryservice_model->find($this->input->get('value'), $config['per_page'], $page);
			$this->data['search_value'] = $this->input->get('value');
		}else{
			$config['base_url'] = site_url('categoryservice/index');
			$config['total_rows'] = $this->Categoryservice_model->count();
			$this->data['category_servicees'] = $this->Categoryservice_model->find(null, $config['per_page'], $page);					
		}
		$this->pagination->initialize($config);		
		$this->data['pagination'] = $this->pagination->create_links();
		//new dBug($this->data);exit();
		$this->render('categoryservice/index');
	}

	/*Formulario de creación de categoría */
	public function create(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('categoryservice/index');
		}
		$this->data['title'] = "Crear category service";
		$this->data['flashMessage'] = $this->session->flashdata('flashMessage');
		$this->data['category_service'] = $this->input->post('category_service');
		//new dBug($this->input->post('category_service[nombre]') );
		$this->form_validation->set_rules('category_service[nombre]', 'nombre', 'required');	
		//new dBug($this->form_validation->run());exit();
		if($this->form_validation->run() == FALSE){
                        $tmpSD = $this->input->post('category_service');
			if(!empty($tmpSD)){
				$this->data['categoryservice'] = $this->input->post('category_service');
			}
			$this->render('categoryservice/create');
		}else{	
			if($this->Categoryservice_model->create($this->input->post('category_service')) == FALSE){	
				$this->data['categoryservice'] = $this->input->post('category_service');
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('categoryservice/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('categoryservice/index');
			}		
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('categoryservice/index');
		}
		$data['title'] = "Editar categoria padre";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$auxcategory_service = $this->Categoryservice_model->get($id);
		if($this->input->post('category_service')){
			$category_service = $this->input->post('category_service');
		}else{
			$category_service = $auxcategory_service;
		}
		$data['category_service'] = $category_service;
		//new dBug($data['category_service']);exit();
		
		//new dbug($category_service['id']);exit();
		if($category_service['nombre'] != $auxcategory_service['nombre']){
			$this->form_validation->set_rules('category_service[nombre]', 'category_service[nombre]', 'required');			
		}else{
			$this->form_validation->set_rules('category_service[nombre]', 'category_service[nombre]', 'required');			
		}
		
		//$this->form_validation->set_rules('category_service[id]', 'index de la category_service', 'required');	
		if($this->form_validation->run() == FALSE){			
				//new dbug('c');exit();
				$this->data = $data;			
			$this->data = $data;
			$this->render('categoryservice/create');
		}else{
			if($this->Categoryservice_model->edit($category_service) == FALSE){
				//new dbug('a');exit();
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('categoryservice/create');
			}else{
				//new dbug('b');exit();
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('categoryservice/index');
			}		
		}
	}

	/* Función Eliminar categoría */
	public function delete(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('categoryservice/index');
		}		
		if($category_service = $this->input->post('category_service')){												
			$result = $this->Categoryservice_model->delete($category_service['category_service_id']);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('categoryservice/index');
	}

}
