<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once INTERFACE_HOME_PATH . '/application/libraries/Classes/PHPExcel.php';

class Grilla extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('grilla_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	public function index() {		
		$this->render('grilla/index');
	}

	public function hercules() {		
		$this->render('grilla/hercules');
	}

	public function getDataInit() {
		echo json_encode($this->grilla_model->get());
	}

	public function getDataInitHercules() {


		echo json_encode($this->grilla_model->getHercules($_GET['search'],$_GET['truck']));
	}

	public function getDailyCharges()
	{
		$id= $_POST["id"];
		echo json_encode($this->grilla_model->getDailyCharges($id));
	}


	public function cerrarTicket()
	{
		$id= $_POST["id"];
		echo json_encode($this->grilla_model->cerrarTicket($id));
		
	}


	public function getExcelGrilla()
		{
			$objPHPExcel    =   new PHPExcel();
			 
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Carga');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Conductor');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Camión');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Empresa');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Bahía');
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Estado');
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Vuelta');
			$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Última actualización');
			 
			$objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);

			$i=2;


			$data['grillas'] = $this->grilla_model->get();

			$fecha = date('dmYhis');


			foreach ($data['grillas'] as $grilla) 
			{
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $grilla['incident']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $grilla['name']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $grilla['numberTruck']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $grilla['empresa']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $grilla['id_bahia']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $grilla['estado']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $grilla['id_area']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, $grilla['updatetimecoord']);

				$i++;

			}
			

			 
			$objWriter  =   new PHPExcel_Writer_Excel2007($objPHPExcel);
			 
			 
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="listado-grilla-'.$fecha.'.xlsx"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
			$objWriter->save('php://output');
		}


		public function getExcelGrillaHercules()
		{
			$objPHPExcel    =   new PHPExcel();
			 
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Carga');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Bahía');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Fecha de creación');
			 
			$objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);

			$i=2;


			$data['grillas'] = $this->grilla_model->getHercules();

			$fecha = date('dmYhis');


			foreach ($data['grillas'] as $grilla) 
			{
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $grilla['incident']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $grilla['id_bahia']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $grilla['datecreation']);

				$i++;

			}
			

			 
			$objWriter  =   new PHPExcel_Writer_Excel2007($objPHPExcel);
			 
			 
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="listado-grilla-'.$fecha.'.xlsx"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
			$objWriter->save('php://output');
		}
}
