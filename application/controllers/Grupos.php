<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos extends MY_Controller {

	function __construct() {
		parent::__construct();		
		//$this->load->model('country_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){
		echo "xxxxxxxxxxxxxxxxxxxxxxxxxa";
		return;
		$data['search_value'] = "";
		$config['per_page'] = 7;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('country/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->country_model->count($this->input->get('value'));			
			$data['countries'] = $this->country_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
		}else{
			$config['base_url'] = site_url('country/index');
			$config['total_rows'] = $this->country_model->count();			
			$data['countries'] = $this->country_model->find(null, $config['per_page'], $page);			
		}
		$this->pagination->initialize($config);		
		$data['pagination'] = $this->pagination->create_links();
		$this->data = $data;
		$this->render('country/index');
	}

	/*Formulario de creación de categoría */
	public function create(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('country/index');
		}
		$data['title'] = "Crear empresa";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');

		$this->form_validation->set_rules('country[name]', 'nombre', 'required|is_unique[country.name]');		
		if($this->form_validation->run() == FALSE){
                        $tmpSD = $this->input->post('country');
			if(!empty($tmpSD)){
				$data['country'] = $this->input->post('country');
			}
			$this->data = $data;
			$this->render('country/create');
		}else{
			if($this->country_model->create($this->input->post('country')) == FALSE){				
				$data['country'] = $this->input->post('country');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('country/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('country/index');
			}		
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('country/index');
		}
		$data['title'] = "Editar empresa";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$auxCountry = $this->country_model->get($id);
		if($this->input->post('country')){
			$country = $this->input->post('country');
		}else{
			$country = $auxCountry;
		}
		$data['country'] = $country;

		if($country['name'] != $auxCountry['name']){
			$this->form_validation->set_rules('country[name]', 'nombre', 'required');			
		}else{
			$this->form_validation->set_rules('country[name]', 'nombre', 'required');			
		}
		
		$this->form_validation->set_rules('country[country_id]', 'index de la empresa', 'required');	
		if($this->form_validation->run() == FALSE){						
			$this->data = $data;
			$this->render('country/create');
		}else{
			if($this->country_model->edit($country) == FALSE){								
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('country/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('country/index');
			}		
		}
	}

	/* Función Eliminar categoría */
	public function delete(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('country/index');
		}		
		if($country = $this->input->post('country')){												
			$result = $this->country_model->delete($country['country_id']);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('country/index');
	}

}
