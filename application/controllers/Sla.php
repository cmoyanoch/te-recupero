<?php
date_default_timezone_set('America/Santiago');
defined('BASEPATH') OR exit('No direct script access allowed');

class Sla extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('sla_model');
		$this->load->model('tracking_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){
		$data['search_value'] = "";
		$config['per_page'] = 7;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('sla/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->sla_model->count($this->input->get('value'));				
			$data['slas'] = $this->sla_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
		}else{
			$config['base_url'] = site_url('sla/index');
			$config['total_rows'] = $this->sla_model->count();			
			$data['slas'] = $this->sla_model->find(null, $config['per_page'], $page);			
		}
		$this->pagination->initialize($config);		
		$data['pagination'] = $this->pagination->create_links();
		$this->data = $data;
		$this->render('sla/index');
	}

	/*Formulario de creación de categoría */
	/* Formulario de creación de usuarios */
	public function create(){
		//Se verifican las credenciales
		if(!$this->ion_auth->in_group('7') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('sla/index');
		}	
		$data['title'] = "Crear SLA";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$this->form_validation->set_rules('sla[name]', 'nombre', 'required');
        // $this->form_validation->set_rules('sla[last_name]', 'apellidos', 'required');        
        // $this->form_validation->set_rules('sla[email]', 'email', 'required|valid_email|is_unique[users.email]');
        // $this->form_validation->set_rules('sla[password]', 'contraseña' , 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[sla[password_confirm]]');
        // $this->form_validation->set_rules('sla[password_confirm]', 'repita contraseña', 'required');

		if($this->form_validation->run() == FALSE){ 
                        $tmpSD = $this->input->post('sla');
                       
			if(!empty($tmpSD)){
				$data['sla'] = $this->input->post('sla');
			}

		
			$this->data = $data;
			
			$this->render('sla/create');
		}else{	
			if($this->sla_model->create($this->input->post('sla')) == FALSE){	
		
				$data['sla'] = $this->input->post('sla');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('sla/create');
			}else{ 
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('sla/index');
			}	
		}	
	}public function createTicket(){
		//Se verifican las credenciales
		if(!$this->ion_auth->in_group('7') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('tracking/index');
		}	
		$data['title'] = "Crear Ticket";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$this->form_validation->set_rules('ticket[incident]', 'nombre', 'required');
        

		if($this->form_validation->run() == FALSE){ 
                        $tmpSD = $this->input->post('ticket');
                       
			if(!empty($tmpSD)){
				$data['ticket'] = $this->input->post('ticket');
			}

			// echo "return 1";
			$this->data = $data;
			//return;
			$this->render('tracking/createTicket');  // primera vez entra aca !!!!
		}else{	
			// echo '0';
			// print_r($data['ticket']);
			 $fechaactual= date("Y-m-d H:i:s");
			if($this->tracking_model->create($this->input->post('ticket'),$fechaactual) == FALSE){	
				

		
			// print_r($data['ticket']);
		
				$data['ticket'] = $this->input->post('ticket');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				// echo "return 2";
				//return;
				$this->render('tracking/createTicket');
			 }else{ 
			 $dataHistorica = $this->tracking_model->actualizaState($this->input->post('ticket'),'Abierto',$fechaactual);
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				//return;
				
				redirect('tracking/tickets');
			}	
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('sla/index');
		}
		$data['title'] = "Editar SLA";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');			
		// $data['categories'] = $this->sla_model->get_categories_dropdown();
		$auxsla = $this->sla_model->get($id);
		if($this->input->post('sla')){
			$sla = $this->input->post('sla');
		}else{
			$sla = $auxsla;
		}
		$data['sla'] = $sla;

		if($sla['name'] != $auxsla['name']){
			$this->form_validation->set_rules('sla[name]', 'nombre', 'required|is_unique[sla.name]');			
		}else{
			$this->form_validation->set_rules('sla[name]', 'nombre', 'required');			
		}
		
		//$this->form_validation->set_rules('sla[id]', 'index de la categoría', 'required');	
		if($this->form_validation->run() == FALSE){						
			$this->data = $data;
			$this->render('sla/create');
		}else{
			if($this->sla_model->edit($sla) == FALSE){								
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('sla/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('sla/index');
			}		
		}
	}

	/* Función Eliminar categoría */
	public function delete(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('sla/index');
		}		
		if($sla = $this->input->post('sla')){												
			$result = $this->sla_model->delete($sla['sla_id']);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('sla/index');
	}

}
