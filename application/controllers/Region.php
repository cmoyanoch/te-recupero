<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Region extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('region_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){


		$id_perfil = $this->session->userdata('id_perfil');
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}

		$data['search_value'] = "";
		$config['per_page'] = 7;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('region/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->region_model->count($this->input->get('value'));			
			$data['regiones'] = $this->region_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
		}else{
			$config['base_url'] = site_url('region/index');
			$config['total_rows'] = $this->region_model->count();			
			$data['regiones'] = $this->region_model->find(null, $config['per_page'], $page);			
		}
		$this->pagination->initialize($config);		
		$data['pagination'] = $this->pagination->create_links();
		$this->data = $data;
		$this->render('region/index');
	}

	/*Formulario de creación de categoría */
	public function create(){
		//Se comprueban las credenciales
		/*if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('region/index');
		}*/

		$id_perfil = $this->session->userdata('id_perfil');
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}

		$data['title'] = "Crear Region";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');

		$this->form_validation->set_rules('region[name]', 'nombre', 'required|is_unique[region.name]');		
		if($this->form_validation->run() == FALSE){
                        $tmpSD = $this->input->post('region');
			if(!empty($tmpSD)){
				$data['region'] = $this->input->post('region');
			}
			$this->data = $data;
			$this->render('region/create');
		}else{
			if($this->region_model->create($this->input->post('region')) == FALSE){				
				$data['region'] = $this->input->post('region');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('region/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('region/index');
			}		
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('region/index');
		}
		$data['title'] = "Editar empresa";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$auxregion = $this->region_model->get($id);
		if($this->input->post('region')){
			$region = $this->input->post('region');
		}else{
			$region = $auxregion;
		}
		$data['region'] = $region;

		if($region['name'] != $auxregion['name']){
			$this->form_validation->set_rules('region[name]', 'nombre', 'required');			
		}else{
			$this->form_validation->set_rules('region[name]', 'nombre', 'required');			
		}
		
		$this->form_validation->set_rules('region[region_id]', 'index de la region', 'required');	
		if($this->form_validation->run() == FALSE){						
			$this->data = $data;
			$this->render('region/create');
		}else{
			if($this->region_model->edit($region) == FALSE){								
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('region/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('region/index');
			}		
		}
	}

	/* Función Eliminar categoría */
	public function delete(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('region/index');
		}		
		if($region = $this->input->post('region')){												
			$result = $this->region_model->delete($region['region_id']);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('region/index');
	}

}
