<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('empresa_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){

		$id_perfil = $this->session->userdata('id_perfil');
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}

		$this->data['search_value'] = "";
		$config['per_page'] = 7;
		$this->data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;	
    
		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('empresa/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->empresa_model->count($this->input->get('value'));			
			$this->data['empresaes'] = $this->empresa_model->find($this->input->get('value'), $config['per_page'], $page);
			$this->data['search_value'] = $this->input->get('value');
		}else{	
			$config['base_url'] = site_url('empresa/index');
			$config['total_rows'] = $this->empresa_model->count();	
			$this->data['empresaes'] = $this->empresa_model->find(null, $config['per_page'], $page);		
      
		}	
		$this->pagination->initialize($config);		
		$this->data['pagination'] = $this->pagination->create_links();
		$this->render('empresa/index');
	}

	/*Formulario de creación de categoría */
	public function create(){

		//Se comprueban las credenciales
		/*if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('empresa/index');
		}*/

		$id_perfil = $this->session->userdata('id_perfil');
		
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}
		
		$data['title'] = "Crear empresa";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$this->form_validation->set_rules('empresa[nombre_empresa]', 'nombre_empresa', 'required');
#    $this->form_validation->set_rules('empresa[sla_1]', 'sla_1', 'required');
#    $this->form_validation->set_rules('empresa[sla_2]', 'sla_2', 'required');
#    $this->form_validation->set_rules('empresa[sla_3]', 'sla_3', 'required');
    
		//new dBug($this->form_validation->run());exit();
		if($this->form_validation->run() == FALSE){
        $tmpSD = $this->input->post('empresa');
			
        if(!empty($tmpSD)){
				$data['empresa'] = $this->input->post('empresa');
			}
			$this->data = $data;
			$this->render('empresa/create');
		}else{
			if($this->empresa_model->create($this->input->post('empresa')) == FALSE){				
				$data['empresa'] = $this->input->post('empresa');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('empresa/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('empresa/index');
			}		
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){
		//Se comprueban las credenciales
		
		$id_perfil = $this->session->userdata('id_perfil');
		
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}
		$data['title'] = "Editar empresa";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$auxempresa = $this->empresa_model->get($id);
		if($this->input->post('empresa')){
			$empresa = $this->input->post('empresa');
		}else{
			$empresa = $auxempresa;
		}
		$data['empresa'] = $empresa;
	
		$this->form_validation->set_rules('empresa[radio_cerco]',   'radio_cerco',  'required');
		$this->form_validation->set_rules('empresa[radio_cerco_2]', 'radio_cerco_2', 'required');
		$this->form_validation->set_rules('empresa[radio_cerco_3]', 'radio_cerco_3', 'required');
		$this->form_validation->set_rules('empresa[radio_cerco_4]', 'radio_cerco_4', 'required');
		
		if($this->form_validation->run() == FALSE){	
			$this->data = $data;
			$this->render('empresa/create');
		}else{
			if($this->empresa_model->edit($empresa) == FALSE){	
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('empresa/create');
			}else{
				$this->session->set_flashdata('flashMessage', 1);
				redirect('empresa/index');
			}		
		}
	}

	/* Función Eliminar categoría */
	public function delete(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('empresa/index');
		}		
		if($empresa = $this->input->post('empresa')){												
			$result = $this->empresa_model->delete($empresa['id_empresa']);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('empresa/index');
	}

}
