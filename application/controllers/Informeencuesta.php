<?php

date_default_timezone_set('America/Santiago');
defined('BASEPATH') OR exit('No direct script access allowed');

class Informeencuesta extends MY_Controller {

    private $_CONFIG_USER;

    public function __construct() {
        parent::__construct();

        $this->load->model('Informeencuesta_model');
        $this->load->model('dataperfil_model');
        $this->load->model('auris_model');
        $this->load->model('formdynamic_model');
        $this->load->model('tracking_model');

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }

        $user_id = $this->session->userdata('user_id');
        $id_perfil = $this->session->userdata('id_perfil');
        $this->_CONFIG_USER = $this->dataperfil_model->getDataProfile($user_id, $id_perfil, 'dashboard');
    }

    public function index($idempresa = null) {
        $id_perfil = $this->session->userdata('id_perfil');
        if (!$this->ion_auth->is_grupo($id_perfil, '24')) {
            $this->session->set_flashdata('flashMessage', 7);
            redirect('user/account');
        }
        $this->data['title'] = 'Informe encuesta de satisfacción';
        $this->render('monitoring/informeencuesta');
    }

    public function getData($idempresa = null) {
        $data['tickets'] = $this->Informeencuesta_model->getIncIdenciasRepo();
        $data['encuesta'] = $this->encoleramiento($data['tickets']);

        $this->data = $data;
        $this->data['title'] = 'Informe encuesta de satisfacción';

        $this->render('monitoring/informeencuesta');
    }

    public function encoleramiento($tickets) {
        $resultEncuesta['D'] = 0;
        $resultEncuesta['R'] = 0;
        $resultEncuesta['B'] = 0;
        $resultEncuesta['N'] = 0;

        foreach ($tickets as $keytracking => $ticket) {

            if ($ticket['HeadId'] != '' and $ticket['id_form'] == '22') {
                $incident = $ticket['IncidentController'];
                $row = $this->tracking_model->getPdfAccGeneric($incident);
                $encuesta = $this->getResultEncuestas($ticket['HeadId']);

                if ($encuesta == 'D') {
                    $resultEncuesta['D'] += 1;
                } else if ($encuesta == 'R') {
                    $resultEncuesta['R'] += 1;
                } else if ($encuesta == 'B') {
                    $resultEncuesta['B'] += 1;
                } else {
                    $resultEncuesta['N'] += 1;
                }
                //$tickets[$keytracking]['idstatus']=$encoleramiewnto;
            }
        }

        return $resultEncuesta;
    }

    public function getResultEncuestas($headId) {
        $respuestas = [];
        if ($headId != null) {
            $respuestas = $this->Informeencuesta_model->getRespuestaFormulario($headId);
        }
        $positivo = 0;
        $encuesta = 'N';
        if (!empty($respuestas)) {
            foreach ($respuestas as $key => $value) {
                if ($value['value'] == 'SI') {
                    $positivo++;
                }
            }
            if ($positivo <= 1) {
                $encuesta = 'D';
            } else if ($positivo > 1 && $positivo <= 3) {
                $encuesta = 'R';
            } else if ($positivo > 3) {
                $encuesta = 'B';
            }
        }
        return $encuesta;
    }

    public function showEncoleramiento($headId, $rowGenericc) {

        $header = $this->formdynamic_model->getFormHead_model($headId);
        $fid = $header["Data"]["Property"]["ListValue"]["Property"]["ListValue"]["Property"][0]["Value"];
        $imei = $header["Data"]["Property"]["ListValue"]["Property"]["ListValue"]["Property"][1]["Value"];
        $numpet = $header["Data"]["Property"]["ListValue"]["Property"]["ListValue"]["Property"][2]["Value"];
        $fecha = $header["Data"]["Property"]["ListValue"]["Property"]["ListValue"]["Property"][3]["Value"];
        $title = $header["Data"]["Property"]["ListValue"]["Property"]["ListValue"]["Property"][4]["Value"];
        $IdFatc = $header["Data"]["Property"]["ListValue"]["Property"]["ListValue"]["Property"][5]["Value"];
        $Lng = $header["Data"]["Property"]["ListValue"]["Property"]["ListValue"]["Property"][6]["Value"];
        $Lat = $header["Data"]["Property"]["ListValue"]["Property"]["ListValue"]["Property"][7]["Value"];

        // RECIBIR CONFIGURACION DEL FORMULARIO
        $formConfig = $this->formdynamic_model->getFormConfig_model($fid, $imei, $numpet, $empty);
        $check = (int) $formConfig["Return"]["Code"];
        $formConfig = $formConfig["Data"]["Property"]["Value"];

        $hist = $this->formdynamic_model->getFormTicketHist_model($headId);

        $idIncident = $hist["Data"]["Property"]["ListValue"]["Property"][0]["ListValue"]["Property"][1]["Value"];
        $fecha1 = "";
        $fecha2 = "";
        $fecha3 = "";
        foreach ($hist["Data"]["Property"]["ListValue"]["Property"] AS $row) {
            if (strtolower($row["ListValue"]["Property"][3]["Value"]) == "abierto") {
                $fecha1 = $row["ListValue"]["Property"][2]["Value"];
            }
            if (strtolower($row["ListValue"]["Property"][3]["Value"]) == "en el lugar" || strtolower($row["ListValue"]["Property"][3]["Value"]) == "asignado") {
                $fecha2 = $row["ListValue"]["Property"][2]["Value"];
            }
            if (strtolower($row["ListValue"]["Property"][3]["Value"]) == "cerrado" || strtolower($row["ListValue"]["Property"][3]["Value"]) == "cerrado parcial") {
                $fecha3 = $row["ListValue"]["Property"][2]["Value"];
            }
        }

        if ($check != 101) {

            // RECIBIR DATOS INGRESADOS O PRELLENADOS DEL FORMULARIO
            $dataFormEntry = $this->formdynamic_model->getFormData_model($fid, $imei, $fecha, $numpet, $headId);

            if (isset($dataFormEntry["Data"]["Property"]["ListValue"]["Property"][0])) {
                $dataFormEntry = $dataFormEntry["Data"]["Property"]["ListValue"]["Property"];
            }

            if (isset($dataFormEntry["Data"]["Property"]["ListValue"]["Property"]["ListValue"])) {
                $tmpArr = $dataFormEntry["Data"]["Property"]["ListValue"]["Property"]["ListValue"];
                unset($dataFormEntry);
                $dataFormEntry[0]["ListValue"] = $tmpArr;
            }
            $formArr = json_decode($formConfig, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            //new dBug($rowGenericc['rut']);
            // RELACIONAR LOS ANSWERS CON LOS QUESTIONS
            foreach ($dataFormEntry AS $answer) {
                $rowData = $answer["ListValue"]["Property"];
                $value = $rowData[0]["Value"];
                $questId = $rowData[1]["Value"];
                $inputType = $rowData[2]["Value"];

                foreach ($formArr["section"] AS $sectionKey => $section) {
                    foreach ($section["question"] AS $questionKey => $question) {
                        if ($question["question_id"] == $questId) {
                            $formArr["section"][$sectionKey]["question"][$questionKey]["value"][] = $value;
                            $formArr["section"][$sectionKey]["question"][$questionKey]["inputType"] = $inputType;
                        }
                    }
                }
            }

            $formArr["numpet"] = $numpet;
            $formArr["user"] = $imei;
            $formArr["fecha"] = $fecha;

            // CONTAR LOS PREGUNTAS
            $cc = 0;
            foreach ($formArr["section"] AS $sectionKey => $section) {
                foreach ($section["question"] AS $questionKey => $question) {
                    $cc++;
                }
            }
            $formArr["cc"] = $cc;

            $data["formConfig"] = $formArr;
        }

        $questArr = array();
        foreach (array_keys($data["formConfig"]["section"]) AS $secKey) {
            foreach ($data["formConfig"]["section"][$secKey]["question"] AS $quest) {
                if ($quest["typeName"] == "") {
                    $quest["typeName"] = "DATOS";
                }

                $questArr[$quest["typeName"]][] = $quest;
            }
        }

        foreach ($section["question"] AS $questionKey => $question) {
            $cc++;
        }

        $countOK = 0;
        for ($i = 0; $i <= 4; $i++) {

            if ($questArr["DATOS"][$i]["value"][0] == 'SI') {
                $countOK++;
            };
        }

        if ($countOK <= 1) {
            return 'D';
        } else if ($countOK <= 3) {
            return 'R';
        } else {
            return 'B';
        }
    }

    public function totalesFunction() {
        $zona = addslashes(trim($_REQUEST['zona']));
        $tecnico = addslashes(trim($_REQUEST['tecnico']));
        $grupo = addslashes(trim($_REQUEST['grupo']));
        $periodo = addslashes(trim($_REQUEST['periodo']));

        $LevelOne = $this->Informeencuesta_model->getTotales($zona, $tecnico, $grupo, $periodo);
        print_r($LevelOne);
        $result = [];

        foreach ($LevelOne as $item) {
            array_push($result, $item);
        }

        switch ($periodo) {
            case 1:
                $iniDate = date('Y-m-01 00:00:00');
                $finDate = date('Y-m-t 23:59:59');
                $interval = $periodo;
                break;

            default:
                $finDate = date('Y-m-d 00:00:00');
                $interval = '-' . $periodo . ' day';
                $timestamp = strtotime($interval, strtotime($finDate));
                $iniDate = date('Y-m-d 23:59:59', $timestamp);
                break;
        }

        $data = array(
            'clientes' => $LevelOne,
            'titulo' => 'Satisfacción por Cliente',
            'head_title' => 'Satisfacción por Cliente',
            'tickets' => array(),
            'periodo' => array(
                'inicio' => $iniDate,
                'fin' => $finDate,
                'interval' => $interval,
            ),
        );

        $response['data'] = $data;

        echo json_encode($response);
    }

    public function filtros() {

        $id_perfil = $this->session->userdata('id_perfil');
        $response = ['data' => [], 'error' => []];

        if (!$this->ion_auth->is_grupo($id_perfil, '23')) {
            $this->session->set_flashdata('flashMessage', 7);

            $response['error'] = array(
                'code' => SESSION_LOST_CODE,
                'detail' => SESSION_LOST_DESCRIPTION,
                'redirect' => true,
                'url' => site_url('user/account'),
            );

            return json_encode($response);
        }

        // Grupos resolutores que tiene asignado
        $user = $this->session->userdata();
        $grupos = $this->Informeencuesta_model->getGrupoResolutores($this->_CONFIG_USER['GROUP']);
        $clientes = $this->Informeencuesta_model->getClientes($this->_CONFIG_USER['CUST']);
        $zonas = $this->Informeencuesta_model->getZonas();
        $preguntas = $this->Informeencuesta_model->getQuestions();

        $response['data'] = array(
            'user' => $user,
            'grupos' => $grupos,
            'zonas' => $zonas,
            'clientes' => $clientes,
            'preguntas' => $preguntas,
        );

        echo json_encode($response);
    }

    public function getQuestionByCustomer() {
        $cliente = addslashes(trim($_REQUEST['cliente']));
        $periodo = addslashes(trim($_REQUEST['periodo']));
        $grupo = addslashes(trim($_REQUEST['grupo']));
        $pregunta = addslashes(trim($_REQUEST['pregunta']));
        $zona = addslashes(trim($_REQUEST['zona']));
        $LevelOne = $this->Informeencuesta_model->getQuestionsByCustomer($zona, $cliente, $periodo, $grupo, $pregunta);

        $result = [];
        foreach ($LevelOne as $item) {
            array_push($result, $item);
        }

        switch ($periodo) {
            case 1:
                $iniDate = date('Y-m-01 00:00:00');
                $finDate = date('Y-m-t 23:59:59');
                $interval = $periodo;
                break;

            default:
                $finDate = date('Y-m-d 00:00:00');
                $interval = '-' . $periodo . ' day';
                $timestamp = strtotime($interval, strtotime($finDate));
                $iniDate = date('Y-m-d 23:59:59', $timestamp);
                break;
        }



        $data = array(
            'clientes' => $LevelOne,
            'titulo' => 'Satisfacción por Cliente',
            'head_title' => 'Cliente',
            'tickets' => array(),
            'periodo' => array(
                'inicio' => $iniDate,
                'fin' => $finDate,
                'interval' => $interval,
            )
        );

        $response['data'] = $data;



        echo json_encode($response);
    }

}
