<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryCA extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('categoryCA_model');
        if (!$this->ion_auth->logged_in()){
            redirect('auth/login');
        }
    }

    public function index(){
        $data['search_value'] = "";
        $data['ListCategory'] = $this->categoryCA_model->getListCategoryCA();
        $this->data = $data;

        $this->render('categoryCA/index');
    }

    public function create(){

    }
}