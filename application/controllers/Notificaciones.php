<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Chile/Continental");


class Notificaciones extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Notificaciones_model');

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }


    public function index()
    {

        $data['empresas'] = $this->Notificaciones_model->buscarEmpresas();
        $data['data'] = $data;
        $this->data = $data;
        $this->render('notificaciones/selecUsuarios');
    }


    public function buscarTecnicosEmpresa()
    {

        $idemp = $this->input->post('idemp', TRUE);
        $word = $this->input->post('setec', TRUE);
        $word = $word == "" || $word == null ? null : $word;
        $data = $this->Notificaciones_model->buscarTecnicosEmpresa(($idemp == "all" ? null : $idemp), $word);

        if ($idemp != all) {
            $tecnicos = $data;
        } else {

            foreach ($data as $tec) {
                $tecnicos[$tec["Empresa"]][] = $tec;
            }
        }
        return print_r(json_encode($tecnicos));

    }


    public function sendNotifications()
    {

        $resp = '';
        $text = $_REQUEST['text'];
        $id_usuarios = '';

        if (isset($_REQUEST['id_usuarios'])) {
            $id_usuarios = $_REQUEST['id_usuarios'];
        }

        $todos = $_REQUEST['todos'];
        $id_user = $_SESSION['user_id'];

        $fields = array(
            'registration_ids' => "",
            'data' => array('message' => $text)
        );

        if ($todos == 1) {
            $data = $this->Notificaciones_model->buscarTecnicosEmpresa();
            $id_usuarios = $this->array_column($data, "id");
        }

        $data['fields'] = json_encode($fields, true);
        $data['user'] = json_encode($id_usuarios, true);


        $idNotifications = $this->Notificaciones_model->saveNotifications($text, $todos, $id_user, $data['user'], $data['fields']);

        $resp = $this->Notificaciones_model->notificaciones($idNotifications);

        return print_r($resp['Data']["Property"][1]["Value"]);

    }

    public function buscarTecnicos()
    {

        // $text = $_REQUEST['text'];  // nombre de tecnico
        $text = $this->input->post('text', TRUE);
        // $empresaTecnicos = $_REQUEST['empresaTecnicos'];
        $empresaTecnicos = $this->input->post('empresaTecnicos', TRUE);

        if (isset($_REQUEST['usuarios'])) {
            // $usuarios = $_REQUEST['usuarios'];
            $usuarios = $this->input->post('usuarios', TRUE);
        } else {
            $usuarios = '0';
        }

        $tecnicos = $this->Notificaciones_model->buscarTecnicosText($text, $empresaTecnicos, $usuarios);
        // return print_r($tecnicos);
        $tabla = '<script src="' . base_url() . '/assets/js/notificaciones/notificaciones2.js"></script>';

        $tabla .= '<table style="margin-top: 7px;margin-right: 0px;width: 110% !important;margin-left: -13px;font-size: 10px;"> 
                    <tr>
                        <th> </th>
                        <th>Nombre </th>
                        <th>Empresa</th>
                    </tr>';

        $i = 0;
        foreach ($tecnicos as $value) {

            $Nombre = $value['Nombre'];
            $Empresa = $value['Empresa'];
            $idNotification = $value['idNotification'];
            $id = $value['id'];

            $tabla .= '
                    <tr id="' . $i . '-' . $id . '">
                        <td><input type="checkbox" id="' . $i . '-' . $id . '" idUsuario="' . $id . '" class="selectTecnico" value="' . $idNotification . '" Nombre="' . $Nombre . '" Empresa="' . $Empresa . '"></td>
                        <td>' . $Nombre . ' </td>
                        <td>' . $Empresa . '</td>
                    </tr>
                    ';
            $i++;
        }

        $tabla .= '</table>';
        echo $tabla;
    }


    public function enviarNotificaciones()
    {

        $resp = '';
        $text = $_REQUEST['text'];
        $empresa = $_REQUEST['empresa'];
        if (isset($_REQUEST['usuarios']) && isset($_REQUEST['id_usuarios'])) {
            $usuarios = $_REQUEST['usuarios'];
            $id_usuarios = $_REQUEST['id_usuarios'];
        } else {
            $usuarios = '';
            $id_usuarios = '';
        }


        $todos = $_REQUEST['todos'];
        $id_user = $_SESSION['user_id'];

        $idNotifications = $this->Notificaciones_model->GuardarNotificaciones($text, $empresa, $todos, $id_user, $data['user'], $data['fields']);

        $resp = $this->Notificaciones_model->notificaciones($idNotifications);

        return print_r($resp['Data']["Property"][1]["Value"]);

    }


    public function uploadData()
    {

        $cc = '';
        $resultado = '';
        $count = 0;
        $correctos = 0;
        $incorrectos = 0;
        $total = 0;
        $campos = array();

        $fp = fopen($_FILES['userfile']['tmp_name'], 'r') or die(redirect('csv_service'));
        while ($csv_line = fgetcsv($fp, 1024)) {
            $count++;
            if ($count == 1) {
                continue;
            }//keep this if condition if you want to remove the first row
            for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                $total++;
                $sw = 0;
                $datos = explode(";", $csv_line[0]);
                $insert_csv = array();

                if (isset($datos[0])) {
                    $datos[0] = trim($datos[0]);

                    if ($datos[0] == '') {
                        $sw = 1;
                        $insert_csv['Msisdn'] = 'SIN DATOS DESDE CARGA MASIVA';
                    } else {
                        $insert_csv['Msisdn'] = $datos[0];
                        $r = $this->Notificaciones_model->validaExiste($datos[0]);
                        if ($r == 'NOK') {
                            $sw = 1;
                            $insert_csv['Msisdn'] = $insert_csv['Msisdn'] . ' (ESTE TELÉFONO NO SE ENCUENTRA REGISTRADO O NO POSEE KEY EN B.D.)';
                        }
                    }
                } else {
                    $insert_csv['Msisdn'] = 'FILA NO DEFINIDA DESDE EL ARCHIVO CARGA MASIVA';
                    $sw = 1;
                }

            }

            $i++;

            if ($sw != 1) {


                $resu = $this->Notificaciones_model->traeDetalleUsu($datos[0]);

                foreach ($resu as $value) {
                    array_push($campos, $value);
                }
                $correctos++;

            } else {
                $resultado .= "\r\nERROR AL CARGAR ESTA FILA:  ";
                $resultado .= $insert_csv['Msisdn'];
                $incorrectos++;
            }
        }

        $fin = "TOTAL: " . $total . "\r\nBUENOS: " . $correctos . "\r\nMALOS: " . $incorrectos;
        $fin .= $resultado;
        fclose($fp) or die(redirect('notificaciones_service'));
        $data['total'] = $total;
        $data['correctos'] = $correctos;
        $data['incorrectos'] = $incorrectos;
        // $data['success'] = "OK";
        $data['resultado'] = $fin;

        if ($total == $incorrectos) {
            $op = 'NOK';
            return print_r($op);
        }


        $tabla = '<script src="' . base_url() . '/assets/js/notificaciones/notificaciones2.js"></script>';

        $tabla .= '<table id="tabCargarDat" style="margin-left: 0px;margin-top: 0px;margin-bottom: 0px;width: 330px;" >
                      <thead>
                          <tr>
                            <th> </th>
                            <th>Nombre</th>
                            <th>Empresa</th>
                          </tr>
                      </thead>
                      <tbody id="cargarSeleccionados">';

        $i = 0;
        foreach ($campos as $value) {

            $Nombre = $value['Nombre'];
            $Empresa = $value['Empresa'];
            $idNotification = $value['idNotification'];
            $id = $value['id'];

            $tabla .= '
                  <tr>
                    <td><input type="checkbox" checked class="tecnicoSeleccionado" value="' . $idNotification . '" idusuario="' . $id . '" nombre="' . $Nombre . '" empresa="' . $Empresa . '"></td>
                    <td>' . $Nombre . ' </td>
                    <td>' . $Empresa . '</td>
                  </tr>
                  ';
            $i++;
        }

        $tabla .= '</tbody></table>';
        echo $tabla;


    }

    function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();
        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }
        if (!is_array($params[0])) {
            trigger_error(
                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
                E_USER_WARNING
            );
            return null;
        }
        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }
        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }
        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string)$params[1] : null;
        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int)$params[2];
            } else {
                $paramsIndexKey = (string)$params[2];
            }
        }
        $resultArray = array();
        foreach ($paramsInput as $row) {
            $key = $value = null;
            $keySet = $valueSet = false;
            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string)$row[$paramsIndexKey];
            }
            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }
            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }
        }
        return $resultArray;
    }


}
