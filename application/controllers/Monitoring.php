<?php
	date_default_timezone_set('America/Santiago');
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Monitoring extends MY_Controller
	{
		
		function __construct()
		{

			parent::__construct();
			
			$this->load->model('monitoring_model');
			$this->load->model('dataperfil_model');
			
			if (!$this->ion_auth->logged_in()){
				redirect('auth/login');
			}

			$id_perfil = $this->session->userdata('id_perfil');
			
			if (!$this->ion_auth->is_grupo($id_perfil, '20')){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('user/account');
			}
		}
		
		/* Listado de categorías por Zona */
		
		public function custDashInc(){

			if ($idzone == null){
				$customer = (int)$_REQUEST['idcustomer'];
			}else{
				$customer = (int)$_REQUEST['idcustomer'];
			}
			
			if ($cliente == null){
				$cliente = (int)$_REQUEST['idzone'];
			}else{
				$cliente = (int)$_REQUEST['idzone'];
			}
			
			
			if ((int)$_REQUEST['periodo'] > 1){
				$periodo = $_REQUEST['periodo'];
			} else {
				$periodo = date('d', time());
			}

			$id_perfil = $this->session->userdata('id_perfil');
			$user_id   = $this->session->userdata('user_id');
			$_CONFIG_USER = $this->dataperfil_model->getDataProfile($user_id, $id_perfil, 'dashboard' );
			
			$data['dashInc'] = $this->monitoring_model->getClientDashInc(isset($cliente) ? $cliente : '', $periodo, $_CONFIG_USER);
			$data['idempresa'] = $customer;
			
			$data['paramEmp'] = $idempresa;
			$data['idzone']   = $idzone;
			
			$this->data = $data;
			
			$this->load->view('monitoring/custDashInc', array('data' => $data));
		}
		
		public function index($idempresa = null)
		{

			$id_perfil = $this->session->userdata('id_perfil');
			$user_id   = $this->session->userdata('user_id');
			
			if (!$this->ion_auth->is_grupo($id_perfil, '20')){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('user/account');
			}

			$_CONFIG_USER = $this->dataperfil_model->getDataProfile($user_id, $id_perfil, 'dashboard' );

			if (isset($_REQUEST['cliente'])){
				$cliente = (int)$_REQUEST['cliente'];
			}
			
			if (isset($_REQUEST['zona'])){
				$zona    = (int)$_REQUEST['zona'];
			}
			
			if ($_REQUEST['periodo'] > 1){
				$periodo = (int)$_REQUEST['periodo'];
			} else {
				$periodo = (int)date('d', time());
			}

			$data['clients']  = $this->monitoring_model->getClient($_CONFIG_USER);
			$data['zones']    = $this->monitoring_model->getZone();
			$data['paramEmp'] = $idempresa;

			# Gerentes o Jefe de Zona :: Zonas -> Clientes -> Grupos   -> Técnicos
			if($_CONFIG_USER['PERFIL']  == 3  || $_CONFIG_USER['PERFIL'] == 4 ){

				$data['dashInc'] = $this->monitoring_model->getZoneDashInc(isset($zona) ? $zona : '', $periodo, $_CONFIG_USER);
				$data['modo']    = 'bigBoss';
				
				$_RES = array();
				$cc   = 0;
				foreach ($data['zones'] as $zone) {
					
					$_RES['zonas'][$cc]['id_zona'] = $zone['id_zona'];
					$_RES['zonas'][$cc]['name']    = $zone['name'];
					
					foreach ($data['dashInc'] as $dato) {
						if ($zone['id_zona'] == $dato['id_zona'])
							$_RES['zonas'][$cc]['result'] = $dato;
					}
					$cc++;
				}
				
				$data['response'] = $_RES;
				$this->data = $data;
				$this->render('monitoring/index');
			}

			# Supervisor y Coordinador :: Clientes -> Grupos -> Técnicos
			if ($_CONFIG_USER['PERFIL'] == 2  || $_CONFIG_USER['PERFIL'] == 5 ){
				
				# t1.id_customer as id_cust, t6.nombre_empresa as empresa,
				$data['dashInc'] = $this->monitoring_model->getClientDashInc(isset($cliente) ? $cliente : '', $periodo, $_CONFIG_USER);

				$_RES = array();
				foreach ($data['dashInc'] as $key => $dato) {

					$_RES['cust'][$key]['id_cust'] = $dato['id_cust'];
					$_RES['cust'][$key]['name']    = $dato['empresa'];
					$_RES['cust'][$key]['result']  = $dato;
				}
				
				$data['response'] = $_RES;
				$this->data = $data;
				$this->render('monitoring/indexCust');
			}
			
			# Administrador :: Zonas -> Grupos -> Técnicos
			if($_CONFIG_USER['PERFIL'] == 1 ){
				
				$data['dashInc'] = $this->monitoring_model->getZoneDashInc(isset($zona) ? $zona : '', $periodo, $_CONFIG_USER);
				
				$_RES = array();
				$cc   = 0;
				foreach ($data['zones'] as $zone) {
					
					$_RES['zonas'][$cc]['id_zona'] = $zone['id_zona'];
					$_RES['zonas'][$cc]['name']    = $zone['name'];
					
					foreach ($data['dashInc'] as $dato) {
						if($zone['id_zona'] != 6 || $zone['id_zona'] != 7 )
							if ($zone['id_zona'] == $dato['id_zona'])
								$_RES['zonas'][$cc]['result'] = $dato;
					}
					$cc++;
				}
				
				$data['response'] = $_RES;
				$this->data = $data;
				$this->render('monitoring/index');
			}
			
		}
		
		public function returnForNav($filterZona = null, $idempresa = null)
		{
			
			$id_perfil = $this->session->userdata('id_perfil');
			$user_id   = $this->session->userdata('user_id');
			$_CONFIG_USER = $this->dataperfil_model->getDataProfile($user_id, $id_perfil, 'dashboard' );
			
			
			/* Get filter
			if (isset($_REQUEST['idempresa'])){
				$cliente = (int)$_REQUEST['idempresa'];
			}
			*/
			if (isset($_REQUEST['zona'])){
				$zona = (int)$_REQUEST['zona'];
			}
			

			if ((int)$_REQUEST['periodo'] > 1){
				$periodo = $_REQUEST['periodo'];
			} else {
				$periodo = (int)date('d', time());
				
			}

			$data['filterGroup'] = $this->monitoring_model->getGrupoResolutor();
			$data['dashInc']     = $this->monitoring_model->getZoneDashInc(isset($zona) ? $zona : '', $periodo, $_CONFIG_USER);
			$data['zones']       = $this->monitoring_model->getZone();
			$data['paramEmp']    = $idempresa;
			
			$_RES = array();
			$cc = 0;
			foreach ($data['zones'] as $zone) {
				
				$_RES['zonas'][$cc]['id_zona'] = $zone['id_zona'];
				$_RES['zonas'][$cc]['name'] = $zone['name'];
				
				foreach ($data['dashInc'] as $dato) {
					if($zone['id_zona'] != 6 || $zone['id_zona'] != 7 )
						if ($zone['id_zona'] == $dato['id_zona'])
							$_RES['zonas'][$cc]['result'] = $dato;
				}
				$cc++;
			}
			
			$data['response'] = $_RES;
			$this->data = $data;
			
			$this->load->view('monitoring/indexReturn', array('data' => $data));
			
		}
		
		/* Listado de categorías por Grupo Resolutor */
		public function groupDashInc($idzone = null, $idempresa = null, $periodo = null)
		{
			$modo =  addslashes(trim($_REQUEST['modo']));
			$id_perfil  = $this->session->userdata('id_perfil');
			$user_id    = $this->session->userdata('user_id');
			$_CONFIG_USER = $this->dataperfil_model->getDataProfile($user_id, $id_perfil, 'dashboard' );
			
			if ($idzone == null){
				$customer = (int)$_REQUEST['idcustomer'];
			}else{
				$customer = (int)$_REQUEST['idcustomer'];
			}
			
			if ($idzone == null){
				$idzone = (int)$_REQUEST['idzone'];
			}else{
				$idzone = (int)$_REQUEST['idzone'];
			}
			
			
			if ((int)$_REQUEST['periodo'] > 1){
				$periodo = $_REQUEST['periodo'];
			} else {
				$periodo = date('d', time());
			}
			

			$data['dashInc'] = $this->monitoring_model->getGroupDashInc($idzone, $idempresa, $periodo, $customer, $modo, $_CONFIG_USER);
			$data['idempresa'] = $customer;

			
			$data['paramEmp'] = $idempresa;
			$data['idzone'] = $idzone;
			
			$this->data = $data;
			
			//$this->render('monitoring/groupDashInc');
			$this->load->view('monitoring/groupDashInc', array('data' => $data));
		}
		
		/* Listado de categorías por Grupo Resolutor */
		public function tecnicoDashInc($idgroup = null, $idempresa = null){
			
			$modo       =  addslashes(trim($_REQUEST['modo']));
			$id_perfil  = $this->session->userdata('id_perfil');
			$user_id    = $this->session->userdata('user_id');
			$_CONFIG_USER = $this->dataperfil_model->getDataProfile($user_id, $id_perfil, 'dashboard' );
			$idcustomer = (int)$_REQUEST['idempresa'];

			if ($_REQUEST['periodo'] > 1){
				$periodo = (int)$_REQUEST['periodo'];
			} else {
				$periodo = (int)date('d', time());
			}
			
			if ($idgroup == null)
				$idgroup = (int)$_REQUEST['idgroup'];
			
			$data['idzone'] = (int)$_REQUEST['idzone'];
			
			$data['dashInc']  = $this->monitoring_model->getTecnicoDashInc($idgroup, $idempresa, $periodo, $modo, $idcustomer, $_CONFIG_USER);
			$data['paramEmp'] = $idempresa;
			$this->data = $data;
			

			$this->load->view('monitoring/tecnicoDashInc', array('data' => $data));
			
		}
		
		
		public function tiempoSolucion()
		{
			
			// $data['estados'] = $this->monitoring_model->getStates();
			// $data['sinAsig'] = $this->monitoring_model->getSinAsignar();
			// $data['slaAux'] = $this->monitoring_model->getSla();
			// $data['sla'] = $this->getslaAtrsados($data['slaAux']);
			//print_r($data['estados']);
			// $this->data = $data;
			$this->render('monitoring/tiempoSolucion');
		}
		
		public function prodResolutor()
		{
			
			// $data['estados'] = $this->monitoring_model->getStates();
			// $data['sinAsig'] = $this->monitoring_model->getSinAsignar();
			// $data['slaAux'] = $this->monitoring_model->getSla();
			// $data['sla'] = $this->getslaAtrsados($data['slaAux']);
			//print_r($data['estados']);
			// $this->data = $data;
			$this->render('monitoring/prodResolutor');
		}
		
		/* Formulario de creación de categoría */
		public function create()
		{
			//Se comprueban las credenciales
			if (!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('monitoring/index');
			}
			$data['title'] = "Crear empresa";
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			
			$this->form_validation->set_rules('monitoring[name]', 'nombre', 'required|is_unique[monitoring.name]');
			if ($this->form_validation->run() == FALSE){
				$tmpSD = $this->input->post('monitoring');
				if (!empty($tmpSD)){
					$data['monitoring'] = $this->input->post('monitoring');
				}
				$this->data = $data;
				$this->render('monitoring/create');
			} else {
				if ($this->monitoring_model->create($this->input->post('monitoring')) == FALSE){
					$data['monitoring'] = $this->input->post('monitoring');
					$this->data = $data;
					$this->session->set_flashdata('flashMessage', -1);
					$this->render('monitoring/create');
				} else {
					//Envía aviso de creación exitosa
					$this->session->set_flashdata('flashMessage', 1);
					redirect('monitoring/index');
				}
			}
		}
		
		/* Formulario de edición de categoría */
		public function edit($id)
		{
			//Se comprueban las credenciales
			if (!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('monitoring/index');
			}
			$data['title'] = "Editar empresa";
			$data['edit'] = TRUE;
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			$auxmonitoring = $this->monitoring_model->get($id);
			if ($this->input->post('monitoring')){
				$monitoring = $this->input->post('monitoring');
			} else {
				$monitoring = $auxmonitoring;
			}
			$data['monitoring'] = $monitoring;
			
			if ($monitoring['name'] != $auxmonitoring['name']){
				$this->form_validation->set_rules('monitoring[name]', 'nombre', 'required');
			} else {
				$this->form_validation->set_rules('monitoring[name]', 'nombre', 'required');
			}
			
			$this->form_validation->set_rules('monitoring[monitoring_id]', 'index de la empresa', 'required');
			if ($this->form_validation->run() == FALSE){
				$this->data = $data;
				$this->render('monitoring/create');
			} else {
				if ($this->monitoring_model->edit($monitoring) == FALSE){
					$this->data = $data;
					$this->session->set_flashdata('flashMessage', -1);
					$this->render('monitoring/create');
				} else {
					//Envía aviso de creación exitosa
					$this->session->set_flashdata('flashMessage', 1);
					redirect('monitoring/index');
				}
			}
		}
		
		/* Función Eliminar categoría */
		public function delete()
		{
			//Se comprueban las credenciales
			if (!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('monitoring/index');
			}
			if ($monitoring = $this->input->post('monitoring')){
				$result = $this->monitoring_model->delete($monitoring['monitoring_id']);
				if ($result){
					$this->session->set_flashdata('flashMessage', 0);
				} else {
					$this->session->set_flashdata('flashMessage', -1);
				}
			}
			redirect('monitoring/index');
		}
		
		public function getslaAtrsados($arr)
		{
			
			$contCerradoAtiempo = 0;
			$constCerradoTrde = 0;
			$contAbiertosOk = 0;
			$contAbiertosNOK = 0;
			
			$contAbiertos = 0;
			$contCerrados = 0;
			
			foreach ($arr as $keytracking => $ticket) {
				if ($ticket['idstatus'] != 'Cerrado'){
					
					$fechaTecnico = $ticket['datecreation'];
					$fechaactual = date("Y-m-d h:i:s");
					$restrictionDate = strtotime($fechaTecnico);
					$now = strtotime(date('Y-m-d H:i:s'));
					$diferencia = $now - $restrictionDate;
					$diferencia = ($diferencia / 60) / 60;
					// echo '***************';
					// echo $diferencia;
					//  echo '-';
					// echo $ticket['sla'];
					//   echo '***************';
					if ($diferencia > $ticket['sla']){
						$contAbiertosNOK = $contAbiertosNOK + 1;
					} else {
						$contAbiertosOk = $contAbiertosOk + 1;
					}
					
					
					// $arrTime =  (explode(".",$diferencia));
					// $arrTime[1]= substr($arrTime[1],0,2);
					// $arrTime[1]= intval($arrTime[1]*0.6);
				} else {
					
					$fechaTecnico = $ticket['datecreation'];
					$fechaactual = $ticket['updatetime'];
					$restrictionDate = strtotime($fechaTecnico);
					$now = strtotime($fechaactual);
					$diferencia = $now - $restrictionDate;
					$diferencia = ($diferencia / 60) / 60;
					if ($diferencia > $ticket['sla']){
						// $color = 'background-color:red';
						$constCerradoTrde = $constCerradoTrde + 1;
					} else {
						// $color = 'background-color:green';
						$contCerradoAtiempo = $contCerradoAtiempo + 1;
					}
					
					//       	$arrTime =  (explode(".",$diferencia));
					// $arrTime[1]= substr($arrTime[1],0,2);
					// $arrTime[1]= intval($arrTime[1]*0.6);
				}
			}
			
			$sla['contCerradoAtiempo'] = $contCerradoAtiempo;
			$sla['constCerradoTrde'] = $constCerradoTrde;
			$sla['contAbiertosOk'] = $contAbiertosOk;
			$sla['contAbiertosNOK'] = $contAbiertosNOK;
			$sla['contAbiertos'] = $contAbiertos;
			$sla['contCerrados'] = $contCerrados;
			
			
			return $sla;
		}
		
	}
