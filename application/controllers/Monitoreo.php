<?php
	date_default_timezone_set('America/Santiago');
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Monitoreo extends MY_Controller
	{
		
		function __construct()
		{

			parent::__construct();
			
			$this->load->model('reporte_usabilidad');
			$this->load->model('dataperfil_model');
			$this->load->model('tracking_model');
			$this->load->model('monitoring_model');

			if (!$this->ion_auth->logged_in()){
				redirect('auth/login');
			}

			$id_perfil = $this->session->userdata('id_perfil');
			
			if (!$this->ion_auth->is_grupo($id_perfil, '20')){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('monitoreo/reporte');
			}
		}


		public function reporte($busqueda = null)
		{

			$id_perfil = $this->session->userdata('id_perfil');
			$user_id   = $this->session->userdata('user_id');
			
			if (!$this->ion_auth->is_grupo($id_perfil, '20')){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('user/account');
			}

			$buscar =  $this->input->get('busqueda');

			if($buscar!=1)
			{	

				$hoy = date('Y-m-d');
				$nuevafecha = strtotime ( '-6 day' , strtotime ( $hoy ) ) ;
				$nuevafecha = date ( 'Y-m-d' , $nuevafecha );


				$tecnicos = $this->monitoring_model->getTecnicos();

				foreach($tecnicos as $tecnico)
				{	

					$info = $this->monitoring_model->getInitialReport($nuevafecha,$hoy,$tecnico["id"]);

					$fechav = $hoy;
					$sentinela = 0;

					for($i=0;$i<=6;$i++)
					{	

						if($sentinela != 0)
						{
							$fechav = strtotime ( '-1 day' , strtotime ( $fechav ) ) ;
							$fechav = date ( 'Y-m-d' , $fechav );
						}
						else
						{
							$sentinela = 1;
						}
						
						
						$key = '';

						$key = array_search($fechav, array_column($info, 'fecha'));

						if(false === $key)
						{
							$info[] = array('fecha' => $fechav , 'cantidad' => 0 );
						}


							
					}

					$fechav = $hoy;
					$sentinela = 0;

					usort($info, array($this, "date_compare"));

					$reporte[] = array('name' => $tecnico["name"] , 
										'numberTruck' => $tecnico["numberTruck"] , 
										'info' => $info );
				}
				
				
				$data["reporte"] = $reporte;
				
				$this->data = $data;
				$this->render('monitoreo/reporte');


			}
			
		
		}


		public function date_compare($a, $b)
		{
		    $t1 = strtotime($a['fecha']);
		    $t2 = strtotime($b['fecha']);
		    return $t1 - $t2;
		}
	}
