<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('company_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){
		$data['search_value'] = "";
		$config['per_page'] = 7;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('company/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->company_model->count($this->input->get('value'));			
			$data['companies'] = $this->company_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
		}else{
			$config['base_url'] = site_url('company/index');
			$config['total_rows'] = $this->company_model->count();			
			$data['companies'] = $this->company_model->find(null, $config['per_page'], $page);			
		}
		$this->pagination->initialize($config);		
		$data['pagination'] = $this->pagination->create_links();
		$this->data = $data;
		$this->render('company/index');
	}

	/*Formulario de creación de categoría */
	public function create(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('company/index');
		}
		$data['title'] = "Crear empresa";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');

		$this->form_validation->set_rules('company[name]', 'nombre', 'required|is_unique[company.name]');		
		if($this->form_validation->run() == FALSE){
                        $tmpSD = $this->input->post('company');
			if(!empty($tmpSD)){
				$data['company'] = $this->input->post('company');
			}
			$this->data = $data;
			$this->render('company/create');
		}else{
			if($this->company_model->create($this->input->post('company')) == FALSE){				
				$data['company'] = $this->input->post('company');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('company/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('company/index');
			}		
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('company/index');
		}
		$data['title'] = "Editar empresa";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$auxCompany = $this->company_model->get($id);
		if($this->input->post('company')){
			$company = $this->input->post('company');
		}else{
			$company = $auxCompany;
		}
		$data['company'] = $company;

		if($company['name'] != $auxCompany['name']){
			$this->form_validation->set_rules('company[name]', 'nombre', 'required');			
		}else{
			$this->form_validation->set_rules('company[name]', 'nombre', 'required');			
		}
		
		$this->form_validation->set_rules('company[company_id]', 'index de la empresa', 'required');	
		if($this->form_validation->run() == FALSE){						
			$this->data = $data;
			$this->render('company/create');
		}else{
			if($this->company_model->edit($company) == FALSE){								
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('company/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('company/index');
			}		
		}
	}

	/* Función Eliminar categoría */
	public function delete(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('company/index');
		}		
		if($company = $this->input->post('company')){												
			$result = $this->company_model->delete($company['company_id']);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('company/index');
	}

}
