<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//date_default_timezone_set("Chile/Continental");

ini_set('upload_max_filesize', '45M');
ini_set('post_max_size', '45M');
ini_set('max_input_time', 3000);
ini_set('max_execution_time', 3000);



// require_once APPPATH . '/libraries/Clases/PHPExcel.php';

class ManualesDoc extends MY_Controller {


	public function __construct()
	{
            parent::__construct();  
            $this->load->model('Manualesdoc_model');
//            session_start();

            if (!$this->ion_auth->logged_in()){
                redirect('auth/login');
            }
    		
	}	

       
        
        
	public function index()	{
                
            $sumador = 0;
            $data['listado'] = $this->Manualesdoc_model->DataMod($sumador);
            $data['CReg'] = $this->Manualesdoc_model->CantDataMod(); 
            $data['contadorPag'] = 1;
                
            $data['data']=$data;
            $this->data = $data;
            $this->render('manualesDoc/subirManuales');

	}	
        
        

	public function StateChange(){   

            $id = $this->input->post('id', TRUE);

            $data['state'] = $this->Manualesdoc_model->StateDocument($id);

            if($data['state']=="OK"){

                $data['template'] = 'manualesDoc/subirManuales'; 
                $sumador = 0;
                $data['listado'] = $this->Manualesdoc_model->DataMod($sumador);
                $data['CReg'] = $this->Manualesdoc_model->CantDataMod(); 
                $data['contadorPag'] = 1;

                $this->load->view('manualesDoc/subirManuales', array('data' => $data));
            }else{
                return print_r("NOK");
            }

	}
        
        

	public function actDescManuales(){

            $id = "";      
            $desc = "";      

            if (isset($_REQUEST['id'])) {
                    $id = $this->input->post('id', TRUE);
            }     
            if (isset($_REQUEST['desc'])) {
                    $desc = $this->input->post('desc', TRUE);
            }       


            $resp = $this->Manualesdoc_model->actDescMan($id,$desc); 
            return print_r($resp);

	}
        
        
        
        

	public function upload(){

		$resp = "";
		// $OnlyName = $_POST['fileName'];
		$OnlyName = $this->input->post('fileName', TRUE);
		$FullName = $_FILES['userfile']['name'];
		// $rutaWeb = "/var/www/html/toolbox/portal/v0.08.desa/assets/manuales_temp/";
		$rutaWeb = DIRECTORIO_MANUAL;

//		$data['carga'] = $this->Manualesdoc_model->routeLoad(); 
//		$carga = $data['carga'];
		$tipo_archivo = $_FILES['userfile']['type'];

		$FullName = str_replace(' ', '_', $FullName);
		$FullName = str_replace('á', 'a', $FullName);
		$FullName = str_replace('é', 'e', $FullName);
		$FullName = str_replace('í', 'i', $FullName);
		$FullName = str_replace('ó', 'o', $FullName);
		$FullName = str_replace('ú', 'u', $FullName);
		$FullName = str_replace('Á', 'A', $FullName);
		$FullName = str_replace('É', 'E', $FullName);
		$FullName = str_replace('Í', 'I', $FullName);
		$FullName = str_replace('Ó', 'O', $FullName);
		$FullName = str_replace('Ú', 'U', $FullName);
		$FullName = str_replace('Ñ', 'N', $FullName);
		$FullName = str_replace('ñ', 'n', $FullName);

		$FullName = str_replace('!', '', $FullName);
		$FullName = str_replace('¡', '', $FullName);
		$FullName = str_replace('¿', '', $FullName);
		$FullName = str_replace('(', '', $FullName);
		$FullName = str_replace(')', '', $FullName);
		$FullName = str_replace('$', '', $FullName);
		$FullName = str_replace('&', '', $FullName);
		$FullName = str_replace('#', '', $FullName);
		$FullName = str_replace('=', '', $FullName);
		$FullName = str_replace('+', '', $FullName);
		$FullName = str_replace('{', '', $FullName);
		$FullName = str_replace('}', '', $FullName);
		$FullName = str_replace('[', '', $FullName);
		$FullName = str_replace(']', '', $FullName);

		$FullName = str_replace('ä', 'a', $FullName);
		$FullName = str_replace('ë', 'e', $FullName);
		$FullName = str_replace('ï', 'i', $FullName);
		$FullName = str_replace('ö', 'o', $FullName);
		$FullName = str_replace('ü', 'u', $FullName);
		$FullName = str_replace('Ä', 'A', $FullName);
		$FullName = str_replace('Ë', 'E', $FullName);
		$FullName = str_replace('Ï', 'I', $FullName);
		$FullName = str_replace('Ö', 'O', $FullName);
		$FullName = str_replace('Ü', 'U', $FullName);

		$FullName = str_replace('â', 'a', $FullName);
		$FullName = str_replace('à', 'a', $FullName);
		$FullName = str_replace('ê', 'e', $FullName);
		$FullName = str_replace('è', 'e', $FullName);
		$FullName = str_replace('î', 'i', $FullName);
		$FullName = str_replace('ì', 'i', $FullName);
		$FullName = str_replace('ô', 'o', $FullName);
		$FullName = str_replace('ò', 'o', $FullName);
		$FullName = str_replace('û', 'u', $FullName);
		$FullName = str_replace('ù', 'u', $FullName);

		$FullName = str_replace('Â', 'a', $FullName);
		$FullName = str_replace('À', 'a', $FullName);
		$FullName = str_replace('Ê', 'e', $FullName);
		$FullName = str_replace('È', 'e', $FullName);
		$FullName = str_replace('Î', 'i', $FullName);
		$FullName = str_replace('Ì', 'i', $FullName);
		$FullName = str_replace('Ô', 'o', $FullName);
		$FullName = str_replace('Ò', 'o', $FullName);
		$FullName = str_replace('Û', 'u', $FullName);
		$FullName = str_replace('Ù', 'u', $FullName);




		$existFullName = $this->Manualesdoc_model->existFullName($FullName); 
		if ($existFullName['cant'] > 0) {
			$resp = "9";
			return print_r($resp);
		}

		$existName = $this->Manualesdoc_model->existName($OnlyName); 
		if ($existFullName['cant'] > 0) {
			$resp = "10";
			return print_r($resp);
		}

		$nom = explode( '.', $FullName );
		$ext = $nom[1];

	// if($_FILES['userfile']['type'] != 'text/plain' )
	// if (!(strpos($tipo_archivo, "pdf") || strpos($tipo_archivo, "doc") || strpos($tipo_archivo, "csv") || strpos($tipo_archivo, "docx") ) ) 
		if( $ext!= 'pdf' && $ext!= 'PDF' && $ext!= 'doc' && $ext!= 'DOC' && $ext!= 'docx' && $ext!= 'DOCX' && $ext!= 'csv' && $ext!= 'CSV' )
		{	
			$resp = "1";
			return print_r($resp);
		}
                
	// Vamos a verificar si hay errores. 
		if($_FILES['userfile']['error'] > 0){
		// echo 'Hemos encontrado un problema: ';
			switch($_FILES['userfile']['error'])
			{	// Errores de PHP
				// Archivo excedio la propiedad de PHP upload_max_filesize
				// case 1:     $resp = "2";	return print_r($resp);
				// Su tamaño es muy grande (El tamaño dado en el campo invisible)
				case 2: 	$resp = "3";	return print_r($resp);
				// El archivo se proceso parcialmente
				case 3: 	$resp = "4";	return print_r($resp);
				// Archivo no se pudo procesarse
				case 4: 	$resp = "5";	return print_r($resp);  

				// default:  	$resp = "6";	return print_r($resp);  
			}
			// exit;	
		}
		//  Verificamos si es un archivo de texto 

		// Se puede procesar el archivo
		// Poniendolo en una carpeta llamada 'upload'
		// $up_folder = $rutaWeb.$_FILES['userfile']['name'];
		$up_folder = $rutaWeb.$FullName;
		if(is_uploaded_file($_FILES['userfile']['tmp_name'])){
			if(!move_uploaded_file($_FILES['userfile']['tmp_name'], $up_folder)){
				// No se puede mover el archivo de carpeta
				$resp = "7";
				return print_r($resp);
			}
		}
		else{
			// Si se llega a este punto, cabe la 
			// posibilidad de que sea un archivo malicioso
			$resp = "8";
			return print_r($resp);
		}
		// Si todas las pruebas anteriores pasan, 
		// desplegamos un mensaje de exito.
		// echo 'Archivo procesado correctamente.<br />';

		$dateNow = date('Y-m-d H:m:s'); 
		$BDName = $this->Manualesdoc_model->AddManual($OnlyName, $FullName, $dateNow); 



		$resp = "OK";
		return print_r($resp);


	}
        
        
        
        
        
        
        
        

    function Trace1()  {     


      ///////////////////////7TRACE//////////////////////////////7
      $op = $this->input->post('op', TRUE);
      $valor = $this->input->post('valor', TRUE);

      if ( $op==1) {
          $accion = "31";
      }

          $date = $dateNow = date('Y-m-d H:i:s'); 
          $fono = "";
          $nPeticion = $valor;
          $idElement = "";

          $this->trace_model->trace($_SESSION['user_id'], $date, $accion, $fono, $nPeticion, $idElement);

      ///////////////////////////////////////////////////////////


  }



    public function PaginarManuales(){   
            // return print_r("43343423424");

        $sumador = PAGINADO_CANT;

        if (isset($_REQUEST['paginador'])) {
                // $paginador   = $_REQUEST['paginador'];
                $paginador = $this->input->post('paginador', TRUE);
        }

        $mult = $paginador -1;
        $sumador = $mult*$sumador;

        $data['template'] = 'manualesDoc/subirManuales'; 
        $data['listado'] = $this->Manualesdoc_model->DataMod($sumador);
        $data['CReg'] = $this->Manualesdoc_model->CantDataMod(); 
        $data['contadorPag'] = $paginador;
//        $data['descarga'] = $this->foros_model->routeDownload(); 

        $this->load->view('manualesDoc/subirManuales', array('data' => $data));        


    }
  
  

	public function buscarManual(){
            
            $buscar = "";
            $sumador = 0;

            if (isset($_REQUEST['buscar'])) {
                $buscar = $this->input->post('buscar', TRUE);
            }  

            $data['listado'] = $this->Manualesdoc_model->BuscarManu($buscar,$sumador); 
            $data['CReg'] = $this->Manualesdoc_model->BuscarManu_cont($buscar);    
            $data['contadorPag'] = 1;

            if ($data['listado']) {     
                $this->load->view('manualesDoc/subirManuales', array('data' => $data));      

            }else{
                $val = 'NOK';
                return print_r($val);
            }        

	}

//  



}
