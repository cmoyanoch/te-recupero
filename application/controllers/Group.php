<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('group_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){
		
		//return;

		$id_perfil = $this->session->userdata('id_perfil');
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}

		$data['search_value'] = "";
		$config['per_page'] = 7;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('group/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->group_model->count($this->input->get('value'));			
			$data['groups'] = $this->group_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
		}else{
			$config['base_url'] = site_url('group/index');
			$config['total_rows'] = $this->group_model->count();			
			$data['groups'] = $this->group_model->find(null, $config['per_page'], $page);
			//print_r($data['groups']);			
		}
		$this->pagination->initialize($config);		
		$data['pagination'] = $this->pagination->create_links();
		$this->data = $data;
		$this->render('group/index');
	}

	

	/*Formulario de creación de categoría */
	public function create(){
     

     $id_perfil = $this->session->userdata('id_perfil');
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		} 

        // $data['zone'] = $this->group_model->getZone();
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('group/index');
		}
		$data['title'] = "Crear Documento";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');

	$this->form_validation->set_rules('group[name]', 'nombre', 'required');
    $this->form_validation->set_rules('group[description]', 'Description', 'required');

    
		if($this->form_validation->run() == FALSE){
		
        $tmpSD = $this->input->post('group');
        
			if(!empty($tmpSD)){
        $tmpSD = $this->input->post('group');
        
			}
			$this->data = $data;
			$this->render('group/create');
      
		}else{ 
       $tmpSD = $this->input->post('group');
        
			if($this->group_model->create($tmpSD) == FALSE){		
				$data['group'] = $this->input->post('group');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('group/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('group/index');
			}		
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){
		

		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('group/index');
		}
		$data['title'] = "Editar Documento";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');			
		$auxgroup = $this->group_model->get($id);

		if($this->input->post('group')){
			$group = $this->input->post('group');
		}else{  
			$group = $auxgroup;
		}
		$data['group'] = $group;

		$this->form_validation->set_rules('group[name]', 'nombre', 'required');			
		
		
		$this->form_validation->set_rules('group[description]', 'Description', 'required');

		if($this->form_validation->run() == FALSE){					
			$this->data = $data;			
			$this->render('group/create');
		}else{ 
			if($this->group_model->edit($group) == FALSE){							
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('group/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('group/index');
			}		
		}
	}

	/* Función Eliminar categoría */
	public function delete(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('group/index');
		}		
		if($group = $this->input->post('group')){												
			$result = $this->group_model->delete($group['group_id']);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('group/index');
	}

}
