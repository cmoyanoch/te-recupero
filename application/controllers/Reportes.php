<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//date_default_timezone_set("Chile/Continental");

ini_set('upload_max_filesize', '45M');
ini_set('post_max_size', '45M');
ini_set('max_input_time', 3000);
ini_set('max_execution_time', 3000);



// require_once APPPATH . '/libraries/Clases/PHPExcel.php';

class Reportes extends MY_Controller {


	public function __construct()
	{
            parent::__construct();  
            $this->load->model('Reportes_model');
//            session_start();

            if (!$this->ion_auth->logged_in()){
                redirect('auth/login');
            }
    		
	}	

       
        
        
    
    public function acumulativos(){   

        $id  = trim(addslashes( $_GET['id'] ));
        $id2 = $id;
        
        if (!empty($id)) { 

            if($id == 1){
                $data['tituloPag']  = "Acumulativo Cambio de Deco";
                $id =   14; // ID DEL TIPO DE REPORTE
            }else if($id == 2){
                $data['tituloPag']  = "Certificación Código QR";
                $id =   15; // ID DEL TIPO DE REPORTE
            }else if($id == 3){
                $data['tituloPag']  = "Cambio de Par";
                $id =   16; // ID DEL TIPO DE REPORTE
            }else if($id == 4){
                $data['tituloPag']  = "Reportes Formularios Dinámicos";
                $id =   17; // ID DEL TIPO DE REPORTE
            }else if($id == 5){
                $data['tituloPag']  = "Reportes Test de Velocidad";
                $id =   18; // ID DEL TIPO DE REPORTE
            }else if($id == 6){
                $data['tituloPag']  = "Reportes Certificación Cobre/Fibra";
                $id =   19; // ID DEL TIPO DE REPORTE
            }else if($id == 7){
                $data['tituloPag']  = "Reportes Wifi Design";
                $id =   20; // ID DEL TIPO DE REPORTE
            }else if($id == 2018){
                $data['tituloPag']  = "Reportes CSV Descargables";
                $id =   2018; // ID DEL TIPO DE REPORTE
            }
       
        
            $data['idTipoRep']  = $id2;
            $sumador = "0";
            $fIni = "";
            $fFin = ""; 
            $seleccionados = array();
            $data['datos'] = $this->Reportes_model->acumulativo_model($sumador,$fIni,$fFin,$seleccionados,$id);    
            $data['CReg'] = $this->Reportes_model->acumulativo_model_cont($fIni,$fFin,$seleccionados,$id);
            $data['contadorPag'] = 1;

            $data['data']=$data;
            $this->data = $data;
            $this->render('csv_acumulativos/contenedor_acumulativos');
            
        }else{
            session_destroy();
            header('Location: '.base_url());
        }
    }
    
    
    
    public function acumulativos_pag(){    
        $sumador = PAGINADO_CANT;
        $paginador  = "";
        $fIni = "";
        $fFin = "";

        $fIni = trim(addslashes($this->input->post('fIni', TRUE)));
        $fFin = trim(addslashes($this->input->post('fFin', TRUE)));
        $seleccionados = trim(addslashes($this->input->post('seleccionados', TRUE)));

        if (isset($_REQUEST['paginador'])) {
            $paginador = trim(addslashes($this->input->post('paginador', TRUE)));
        }
        if (isset($_REQUEST['id'])) {
            $id = trim(addslashes($this->input->post('id', TRUE)));
            $id2 = $id;
        }
        
        if($id == 1){
            $data['tituloPag']  = "Acumulativo Cambio de Deco";
            $id =   14; // ID DEL TIPO DE REPORTE
        }else if($id == 2){
            $data['tituloPag']  = "Certificación Código QR";
            $id =   15; // ID DEL TIPO DE REPORTE
        }else if($id == 3){
            $data['tituloPag']  = "Cambio de Par";
            $id =   16; // ID DEL TIPO DE REPORTE
        }else if($id == 4){
            $data['tituloPag']  = "Reportes Formularios Dinámicos";
            $id =   17; // ID DEL TIPO DE REPORTE
        }else if($id == 5){
            $data['tituloPag']  = "Reportes Test de Velocidad";
            $id =   18; // ID DEL TIPO DE REPORTE
        }else if($id == 6){
            $data['tituloPag']  = "Reportes Certificación Cobre/Fibra";
            $id =   19; // ID DEL TIPO DE REPORTE
        }else if($id == 2018){
            $data['tituloPag']  = "Reportes CSV Descargables";
            $id =   2018; // ID DEL TIPO DE REPORTE
        }

        $data['idTipoRep']  = $id2;


        if ($fIni!="" && $fFin!="") {
            $fIni = $this->invertirFecha2($fIni);
            $fFin = $this->invertirFecha2($fFin);      
            
            if ($fIni > $fFin) {
                $aux  = $fIni;
                $fIni = $fFin;
                $fFin = $aux;
            }                
            
        }else{
            $fIni = "";
            $fFin = "";      
        }


        $mult = $paginador -1;
        $sumador = $mult*$sumador;

        $data['template'] ='csv_acumulativos/acumulativos'; 
        $data['datos'] = $this->Reportes_model->acumulativo_model($sumador,$fIni,$fFin,$seleccionados,$id);    
        $data['CReg'] = $this->Reportes_model->acumulativo_model_cont($fIni,$fFin,$seleccionados,$id);        
        $data['contadorPag'] = $paginador;
        $this->load->view('csv_acumulativos/acumulativos', array('data' => $data));
    }

    
    
    public function acumulativos_fecha(){    
        $sumador = "0";
        $fIni = "";
        $fFin = "";
        $fIni = trim(addslashes($this->input->post('fIni', TRUE)));
        $fFin = trim(addslashes($this->input->post('fFin', TRUE)));
        $seleccionados = trim(addslashes($this->input->post('seleccionados', TRUE)));

        if ($fIni!="" && $fFin!="") {
            $fIni = $this->invertirFecha2($fIni);
            $fFin = $this->invertirFecha2($fFin);  
            
            if ($fIni > $fFin) {
                $aux  = $fIni;
                $fIni = $fFin;
                $fFin = $aux;
            }        
            
        }else{
            $fIni = "";
            $fFin = "";      
        }

        
        if (isset($_REQUEST['id'])) {
            $id = trim(addslashes($this->input->post('id', TRUE)));
            $id2 = $id;
        }
        
        
        $arrF1 = explode("-", $fIni);
        $arrF2 = explode("-", $fFin);
        $arrF3 = explode("-", date('Y-m-d'));
        $fIni = $arrF1[0]."-".$arrF1[1]."-01";
        $fFin = $arrF2[0]."-".$arrF2[1]."-".$arrF3[2];
        
        
        if($id == 1){
            $data['tituloPag']  = "Acumulativo Cambio de Deco";
            $id =   14; // ID DEL TIPO DE REPORTE
        }else if($id == 2){
            $data['tituloPag']  = "Certificación Código QR";
            $id =   15; // ID DEL TIPO DE REPORTE
        }else if($id == 3){
            $data['tituloPag']  = "Cambio de Par";
            $id =   16; // ID DEL TIPO DE REPORTE
        }else if($id == 4){
            $data['tituloPag']  = "Reportes Formularios Dinámicos";
            $id =   17; // ID DEL TIPO DE REPORTE
        }else if($id == 5){
            $data['tituloPag']  = "Reportes Test de Velocidad";
            $id =   18; // ID DEL TIPO DE REPORTE
        }else if($id == 6){
            $data['tituloPag']  = "Reportes Certificación Cobre/Fibra";
            $id =   19; // ID DEL TIPO DE REPORTE
        }else if($id == 2018){
            $data['tituloPag']  = "Reportes CSV Descargables";
            $id =   2018; // ID DEL TIPO DE REPORTE
        }

        $data['idTipoRep']  = $id2;       
        $data['template'] ='csv_acumulativos/acumulativos'; 
        $data['datos'] = $this->Reportes_model->acumulativo_model($sumador,$fIni,$fFin,$seleccionados,$id);    
        $data['CReg'] = $this->Reportes_model->acumulativo_model_cont($fIni,$fFin,$seleccionados,$id);    
        $data['contadorPag'] = 1;
        $this->load->view('csv_acumulativos/acumulativos', array('data' => $data));
    }

    
    
   function acumulativos_comprimir()  {     

        global $argv;

        $op = trim(addslashes($this->input->post('op', TRUE)));
        
        if (isset($_REQUEST['id'])) {
            $id = trim(addslashes($this->input->post('id', TRUE)));
            $id2 = $id;
        }
        
        
        if($id == 1){
            $nomAcum    =   "acumulativoCambioDeco_";
            $id =   14; 
        }else if($id == 2){
            $nomAcum    =   "certificacionQR_";
            $id =   15; 
        }else if($id == 3){
            $nomAcum    =   "cambioDePar_";
            $id =   16; 
        }else if($id == 4){
            $nomAcum    =   "formularioDinamico_";
            $id =   17; 
        }else if($id == 4){
            $nomAcum    =   "testVelocidad_";
            $id =   18; 
        }else if($id == 4){
            $nomAcum    =   "certCobreFibra_";
            $id =   19; 
        }else if($id == 2018){
            $nomAcum    =   "csvDescargable_";
            $id =   2018; 
        }

        if ($op=='1') {
           $post2 = $this->Reportes_model->acumulativo_mode_zip($id);   
           $post = array(); 

           foreach ($post2 as $value) {
               array_push($post, $value['ReportName']);
           }

        }else{  

          if (isset($_REQUEST['archivos']) ) {
              $post = $_REQUEST['archivos'];
//            $post = trim(addslashes($this->input->post('archivos', TRUE)));
          }else{
            $post = 'error';
            return '0';
          }

        }

        $dir = realpath($argv[0]).'/descarga_zip/'; 
        $handle = opendir($dir); 

        while ($file = readdir($handle))  {   
          if (is_file($dir.$file)) { unlink($dir.$file); }
        } 

      $zip = new ZipArchive(); 
      $nombreArchivo = $nomAcum.date('Ymd_His').'.zip';

      $filename = realpath($argv[0]).'/descarga_zip/'.$nomAcum.date('Ymd_His').'.zip';

      
      if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
          return '0';
      }else{

          for ($i=0; $i < count($post); $i++) { 
            
              $url = realpath($argv[0]).'/ReportFiles/';   
              $exists = is_file( $url.$post[$i] );
                            
              if ($exists == true) {
                  $zip->addFile($url.$post[$i], $post[$i]);
              }
              
          }

        $zip->close();

        return print_r($nombreArchivo);
      }
    }
    

   public function invertirFecha2($fecha) {         

    $f = explode("/", $fecha);
    $fechaF = $f["2"]."-".$f["1"]."-".$f["0"];
    
    return $fechaF;
  }


}