<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once INTERFACE_HOME_PATH . '/application/libraries/Classes/PHPExcel.php';
require_once INTERFACE_HOME_PATH . '/application/libraries/Classes/fpdf.php';
require_once INTERFACE_HOME_PATH . '/application/libraries/dBug.php';


date_default_timezone_set('America/Santiago');
ini_set('upload_max_filesize', '400M');
ini_set('post_max_size', '400M');
ini_set('max_input_time', 500);
ini_set('max_execution_time', 500);
ini_set('memory_limit', '512M');

class Tracking extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('tracking_model');
		$this->load->model('ticket_model');
		$this->load->model('dataperfil_model');
                $this->load->model('formdynamic_model');

	
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login');
		}

	}
	

    public function createTicket(){



        $config['base_url'] = site_url('tracking/createTicket');

        $data['fact'] = $this->tracking_model->getFactoFilter();
        $data['clie'] = $this->tracking_model->getFactoCliente();
        $data['tdoc'] = $this->tracking_model->getTipoDoc();
        $data['tipo'] = $this->input->post('tipo');
       
        $this->data = $data;
  
      
        $this->load->view('tracking/createTicket', array('data' => $data,));

      //  $this->render('tracking/createTicket');
      /*

		$this->form_validation->set_rules('opera[cliente]', 'Cliente', 'required');
		$this->form_validation->set_rules('opera[rut]', 'Rut', 'required');
		$this->form_validation->set_rules('opera[noper]', 'Nº Operación', 'required');
		$this->form_validation->set_rules('opera[tdoc]', 'Tipo de Documentos', 'required');

		$this->form_validation->set_rules('opera[ctotal]', 'Comisión Total', 'required');
		$this->form_validation->set_rules('opera[icomi]', 'IVA Comisión', 'required');
		$this->form_validation->set_rules('opera[mgasto]', 'Monto Gastos', 'required');
		$this->form_validation->set_rules('opera[madela]', 'Monto Adelanto', 'required');
		$this->form_validation->set_rules('opera[mapli]', 'Monto Aplicado', 'required');
                $this->form_validation->set_rules('opera[tgira]', 'Monto Aplicado', 'required');       

		if($this->form_validation->run() == FALSE){ 
                        $tmpSD = $this->input->post('opera');                       
			if(!empty($tmpSD)){
				$data['ticket'] = $this->input->post('opera');
			}
		    $data['opera'] = $this->input->post('opera');
			$this->data = $data;			
			$this->render('tracking/createTicket');  // primera vez entra aca 
    	}else{	


		//	$data['ticket'] = $this->input->post('opera');				
			$this->data = $data;	
                 	$this->render('tracking/createTicket'); 
	
		}	*/
	}


    public function getRut() {

              $idRut = $_POST["data"];
              $data['Rut'] = $this->tracking_model->getRut($idRut);
            
              return print_r(json_encode($data['Rut']));
	}


    public function getCliente() {

              $idFact = $_POST["idFact"];
              $data['Cliente'] = $this->tracking_model->get_cliente_dropdown($idFact);
            
              return print_r(json_encode($data['Cliente']));
	}


	public function getDocumento() {

		$oper = $_POST["oper"];            
        $tipo = $this->input->post('tipo');

		$config['base_url'] = site_url('tracking/historicoTicked');
		$data['historicoTickets'] = $this->tracking_model->getDocumento($oper,$tipo);
		$this->data = $data;
		$this->load->view('tracking/historicoTicked', array('data' => $data, 'oper' =>$oper,'tipo' =>$tipo ));
	}


	public function getOperacion() {

		$oper = $_POST["oper"];
        $tipo = $_POST["tipo"];

		$config['base_url'] = site_url('tracking/detailOperacion');
		$data['DetalleOpperacion'] = $this->tracking_model->getOperacion($oper,$tipo);
		$this->data = $data;
        $this->load->view('tracking/detailOperacion', array('data' => $data));      
	}

	public function tickets() {

		$id_perfil = $this->session->userdata('id_perfil');
		
		if (!$this->ion_auth->is_grupo($id_perfil, '14')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}
		
		$user_id      = $this->session->userdata('user_id');
	        $data['tipo'] =1;		
		$data['search_value'] = "";
		$config['per_page']   = 10;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		
		$page       = $this->input->get('page') != null ? $this->input->get('page') : 1;
		$user_id    = $this->session->userdata('user_id');
		
		$data['factoriFilter']    = $this->tracking_model->getFactoFilter();
	        $data['clienteFilter']    = $this->tracking_model->getFactoCliente();	
	        $data['docFilter']        = $this->tracking_model->getTipoDoc();


        	if (!is_null($this->input->get('value'))) {



                $config['base_url']   = site_url('tracking/tickets?value='.$this->input->get('value').'&factoriFilter='.$this->input->get('factoriFilter').'&filterCliente='.$this->input->get('clienteFilter').'&filterOpera='.$this->input->get('filterOpera').'&docFilter='.$this->input->get('docFilter').'&dateFilter='.$this->input->get('filterdate'));
			
            
             $config['total_rows'] = $this->tracking_model->count($this->input->get('value'));  
             $data['tickets']      = $this->tracking_model->getIncIdencias($this->input, $config['per_page'], $page);

		

                 echo "no vacio";


		} else {


        //        echo "vacio";


      //         $config['base_url']   = site_url('tracking/tickets?factoriFilter='.$this->input->get('factoriFilter').'&filterCliente='.$this->input->get('clienteFilter').'&filterOpera='.$this->input->get('filterOpera'));	



       if (!is_null($this->input->get())) { 


        // print_r($this->input->get()); 

         $config['total_rows'] = $this->tracking_model->count($this->input->get());
        
  
         $data['tickets']      = $this->tracking_model->getIncIdencias($this->input->get(), $config['per_page']);
         

         }
	
        	}
		
	
		$data['pagination'] = $this->pagination->create_links();
	    $this->data = $data;
		$this->render('tracking/tickets');
	}


    public function simulado() {

		$id_perfil = $this->session->userdata('id_perfil');
		
		if (!$this->ion_auth->is_grupo($id_perfil, '14')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}
		
		$user_id      = $this->session->userdata('user_id');
	

          
		$data['search_value'] = "";
		$config['per_page']   = 10;

		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		
		$page       = $this->input->get('page') != null ? $this->input->get('page') : 1;
		$user_id    = $this->session->userdata('user_id');
		

                $data['factoriFilter']    = $this->tracking_model->getFactoFilter();
                $data['clienteFilter']    = 'Seleccione un Cliente';
                $data['docFilter']        = $this->tracking_model->getTipoDoc();
	

        if (!is_null($this->input->get('value'))) {


		
                $config['base_url']   = site_url('tracking/tickets?value='.$this->input->get('value').'&factoriFilter='.$this->input->get('factoriFilter').'&filterCliente='.$this->input->get('clienteFilter').'&filterOpera='.$this->input->get('filterOpera').'&docFilter='.$this->input->get('filterDoc').'&dateFilter='.$this->input->get('filterdate'));
	
            
        $config['total_rows'] = $this->tracking_model->count($this->input->get('value'));  
        $data['tickets']      = $this->tracking_model->getIncIdenciass($this->input, $config['per_page'], $page);

		


		} else {


        //        echo "vacio";


      //         $config['base_url']   = site_url('tracking/simulado?factoriFilter='.$this->input->get('factoriFilter').'&filterCliente='.$this->input->get('clienteFilter').'&filterOpera='.$this->input->get('filterOpera'));	



       if (!is_null($this->input->get())) { 


        // print_r($this->input->get()); 

         $config['total_rows'] = $this->tracking_model->count($this->input->get());
        
  
         $data['tickets']      = $this->tracking_model->getIncIdenciass($this->input->get(), $config['per_page']);
         


         }}
		
	
	      $data['pagination'] = $this->pagination->create_links();
	      $this->data = $data;
   	      $this->render('tracking/simulado');
	}


    public function operacion() {

          $ope = $this->input->post('data');
          $doc = $this->input->post('dato');
          $tipo = $this->input->post('tipo');


          $res  = $this->tracking_model->insertOperacion($ope,$tipo);         
          $id = $res;         
          $resp  = $this->tracking_model->insertdoc($doc,$id,$tipo);

          if($resp){

              $res = 'OK';

          }else{

              $res = 'NOK'; 

               } 
  

             return print_r($res);
    }
      

    public function documento() {
       
  
          $var = $this->input->post('datos');
          $nop = $this->input->post('ndoc'); 
          $tipo = $this->input->post('tipo');

          $resp  = $this->tracking_model->insertdoc($var,$nop,$tipo);

          if($resp){
	              $res = 'OK';
	      }else{
	              $res = 'NOK';
			}
			
	         return print_r($res);          
	}


    public function deleteOpe(){

                $ope   = addslashes(trim($_POST["ope"]));
                $tipo = $this->input->post('tipo'); 
                $response = $this->tracking_model->deleteOpe($ope,$tipo);

                if($response){
                                $response = 'OK';
                }else{
                                $response = 'NOK';
                }

                return print_r($response);
    }
    
    public function deleteDoc(){
			
	        $doc   = addslashes(trim($_POST["doc"]));
                $tipo = $this->input->post('tipo');
           
		  $response = $this->tracking_model->deleteDoc($doc,$tipo);

		  if($response){
				$response = 'OK';
		  }else{
				$response = 'NOK';
		  }  
			
		  return print_r($response);
	}
	
	public function showModal() {
		
		$incident = $_POST["incident"];
		$idTech = $_POST["idTech"];
		
		$data['ticket']       = $this->tracking_model->getIncident($incident);
		$data['positionTech'] = $this->tracking_model->getPositionTech($idTech);

		$this->data = $data;
		$this->load->view('tracking/modalDirections', array('data' => $data));
	}

	public function getExcelTickets(){

			$objPHPExcel    =   new PHPExcel();
			 
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Carga');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Título');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Fecha Creación');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Estado');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'N° Camión');
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Conductor');
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Empresa');
			 
			$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);

			$i=2;

			$fecha = date('dmYhis');

			$ticketsExcel= $this->tracking_model->getIncIdenciasXML();

			//var_dump($ticketsExcel);

			foreach ($ticketsExcel as $ticket) 
			{
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $ticket['incident']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $ticket['title']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $ticket['datecreation']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $ticket['idstatus']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $ticket['numberTruck']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $ticket['name']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $ticket['empresa']);

				$i++;

			}
	
			$objWriter  =   new PHPExcel_Writer_Excel2007($objPHPExcel);
			 
			 
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="listado-conductores-'.$fecha.'.xlsx"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
			$objWriter->save('php://output');
	}	

}
