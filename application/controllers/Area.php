<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Area extends MY_Controller
	{
		public function __construct()
		{
			
			parent::__construct();
			
			$this->load->model('area_model');
			
			if (!$this->ion_auth->logged_in())
			{
				redirect('auth/login');
			}
		}
		
		public function index()
		{
			$data['search_value'] = "";
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			$page                 = $this->input->get('page') != null ? $this->input->get('page') : 1;
			
			$config['per_page']   = 10;
			$config['base_url']   = site_url('area/index?value='.$this->input->get('value'));
			$config['total_rows'] = $this->area_model->count($this->input->get('value'));
			$data['areas']        = $this->area_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
			
			$this->pagination->initialize($config);
			
			$data['pagination'] = $this->pagination->create_links();
			
			$this->data = $data;
			$this->render('area/index');
			
		}
		
		public function edit($id)
		{
			
			$data['title'] = "Editar Area";
			$data['edit']  = TRUE;
			
			$site                 = $this->area_model->get($id);
			$data['site']         = $site[0];
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			
			$this->form_validation->set_rules('area[name]', 'nombre', 'required');
			
			if($this->form_validation->run() == FALSE){
				$this->data = $data;
				$this->render('area/edit');
			}else{
				
				if($this->area_model->edit($this->input->post('site')) == FALSE){
					$this->data = $data;
					$this->session->set_flashdata('flashMessage', -1);
					$this->render('area/edit');
				}else{
					$this->session->set_flashdata('flashMessage', 1);
					redirect('area/index');
				}
			}
		}
		
		public function create()
		{
			$data['title'] = "Crear Area";
			$data['edit']  = FALSE;
			
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			
			$this->form_validation->set_rules('area[name]', 'nombre', 'required');
			
			if($this->form_validation->run() == FALSE){
				
				$this->data = $data;
				$this->render('area/edit');
				
			}else{
				
				if($this->site_model->create($this->input->post('site')) == FALSE){
					$this->data = $data;
					$this->session->set_flashdata('flashMessage', -1);
					$this->render('area/edit');
				}else{
					$this->session->set_flashdata('flashMessage', 1);
					redirect('area/index');
				}
			}
		
		}
		
		public function delete()
		{
		
		}
		
	}