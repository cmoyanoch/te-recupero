<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cargamasiva extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cargamasiva_model');
        if (!$this->ion_auth->logged_in()){
            redirect('auth/login');
        }

    }

    /* Listado de categorías */
    public function index(){

        $id_perfil =  $this->session->userdata('id_perfil');
         
        if(!$this->ion_auth->is_grupo($id_perfil,'11')){               
             $this->session->set_flashdata('flashMessage', 7);
             redirect('user/account');
        }
        $this->data['flashMessage'] = $this->session->flashdata('flashMessage');
        $this->render('ticket/cargamasiva');
    }

    public function cvs($file){
        //new dBug($file['upload_data']['full_path']);
        //new dBug(array_map('str_getcsv', file($file['upload_data']['full_path'])));

        $lines = file($file['upload_data']['full_path']);

        $header = array_shift($lines);
        //new dBug($header);
        $states = array_map(function ($line) use ($header) {
            $state = array_combine(
                str_getcsv($header),
                str_getcsv($line)
            );
            return $state;
        }, $lines);
        //new dBug($states);


        return $states;
    }
    public function do_upload()
    {
//        $config['upload_path']          = INTERFACE_HOME_PATH.'/upload';
        $config['upload_path']          = DIRECTORIO_UPLOAD.'/upload';
        $config['allowed_types']        = 'csv';
        $config['max_size']             = 10000;
        $config['file_name']            = 'shit.csv';
        $config['max_height']           = 768;
        $this->load->library('upload', $config);
        if ( !$this->upload->do_upload('userfile'))
        {
            $this->data['error'] = array('error' => $this->upload->display_errors());
            //new dBug($error); exit();
            $this->render('ticket/cargamasiva');
        }
        else
        {
            $this->data = array('upload_data' => $this->upload->data());
            $this->data['resultado'] = $this->cargamasiva_model->cargamasivatickets($this->cvs($this->data));
            $this->render('ticket/cargamasiva');
        }
    }
}