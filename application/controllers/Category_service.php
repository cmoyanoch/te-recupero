<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoryservice extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('category_service_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){
		$data['search_value'] = "";
		$config['per_page'] = 7;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('category_service/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->category_service_model->count($this->input->get('value'));			
			$data['category_servicees'] = $this->category_service_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
		}else{
			$config['base_url'] = site_url('category_service/index');
			$config['total_rows'] = $this->category_service_model->count();			
			$data['category_servicees'] = $this->category_service_model->find(null, $config['per_page'], $page);			
		}
		$this->pagination->initialize($config);		
		$data['pagination'] = $this->pagination->create_links();
		$this->data = $data;
		$this->render('category_service/index');
	}

	/*Formulario de creación de categoría */
	public function create(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('category_service/index');
		}
		$data['title'] = "Crear category_service";
		$data['flashMessage'] = $this->session->flashdata('flashMessage');

		$this->form_validation->set_rules('category_service[name]', 'nombre', 'required|is_unique[category_service.name]');		
		if($this->form_validation->run() == FALSE){
                        $tmpSD = $this->input->post('category_service');
			if(!empty($tmpSD)){
				$data['category_service'] = $this->input->post('category_service');
			}
			$this->data = $data;
			$this->render('category_service/create');
		}else{
			if($this->category_service_model->create($this->input->post('category_service')) == FALSE){				
				$data['category_service'] = $this->input->post('category_service');				
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('category_service/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('category_service/index');
			}		
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('category_service/index');
		}
		$data['title'] = "Editar empresa";
		$data['edit'] = TRUE;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');
		$auxcategory_service = $this->category_service_model->get($id);
		if($this->input->post('category_service')){
			$category_service = $this->input->post('category_service');
		}else{
			$category_service = $auxcategory_service;
		}
		$data['category_service'] = $category_service;

		if($category_service['name'] != $auxcategory_service['name']){
			$this->form_validation->set_rules('category_service[name]', 'nombre', 'required');			
		}else{
			$this->form_validation->set_rules('category_service[name]', 'nombre', 'required');			
		}
		
		$this->form_validation->set_rules('category_service[category_service_id]', 'index de la category_service', 'required');	
		if($this->form_validation->run() == FALSE){						
			$this->data = $data;
			$this->render('category_service/create');
		}else{
			if($this->category_service_model->edit($category_service) == FALSE){								
				$this->data = $data;
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('category_service/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('category_service/index');
			}		
		}
	}

	/* Función Eliminar categoría */
	public function delete(){
		//Se comprueban las credenciales
		if(!$this->ion_auth->in_group('6') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('category_service/index');
		}		
		if($category_service = $this->input->post('category_service')){												
			$result = $this->category_service_model->delete($category_service['category_service_id']);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('category_service/index');
	}

}
