<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');
class Calendar extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('calendar_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}
	/* Listado de usuarios */
	public function index(){
		//Se verifican las credenciales
		if(!$this->ion_auth->in_group('14') && !$this->ion_auth->in_group('8') && !$this->ion_auth->in_group('9') && !$this->ion_auth->in_group('10') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}	

    //$this->calendar_model->count($this->input->get('value'));
    
		$data['search_value'] = $this->input->get('g');
    $data['groups']       = $this->calendar_model->getGroup((int)$data['search_value']);

    
		$config['per_page'] = 7;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');	

		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;	

		if(!is_null($this->input->get('value'))){
			$config['base_url'] = site_url('user/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->calendar_model->count($this->input->get('value'));	
			// echo $config['total_rows'];		
			$data['tecnicos'] = $this->calendar_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
		}else{
			/*$config['base_url'] = site_url('calendar/index');
			$config['total_rows'] = $this->calendar_model->count();	
      // echo $config['total_rows'];	*/		
			$data['calendar'] = ($this->calendar_model->find((int)$data['search_value']));
                        
                        for ($i=0; $i < count($data['calendar']); $i++) { 
                                $nodo = $data['calendar'][$i]['title'];
                                
                              // end' => $ticket['fecha'].' '.date('H:i:s',strtotime($ticket['hora'])+3600),
                                $data['calendar'][$i]['title'] =  utf8_decode($nodo);
                               // $data['calendar'][$i]['end'] = date('Y-m-d H:i:s',strtotime($data['calendar'][$i]['end'])+3600);
                               // 
                               // $query1[$i]['response'] =  ($nodo);
                                
                                
                               

                          }
			//echo '*****************';

			//json_encode($events);  
			 //echo '<pre>';
                        //print_r($data['calendar']);
                         //echo '</pre>';

		}
		$this->pagination->initialize($config);		
		$data['pagination'] = $this->pagination->create_links();
		$this->data = $data;
		$this->render('calendar/index');
	}
	
	public function update(){
		 $title =$_POST["title"];
		 $start = date('Y-m-d H:i:s',strtotime($_POST["start"]));
		 $end =date('Y-m-d H:i:s',strtotime($_POST["end"]));		
		 $id =$_POST["id"];
		 $data['update'] = $this->calendar_model->updateTicket($title,$start,$end,$id);
		 //print_r(json_encode($data['direcction']));
		 return print_r(json_encode($data['update']));
	}

	public function getTecnicosDisponiblesAgenda(){

		 $idEvent =$_POST["idEvent"];
		 $start = date('Y-m-d H:i:s',strtotime($_POST["starttime"]));
		 $end =date('Y-m-d H:i:s',strtotime($_POST["endtime"]));		
		
		 $data['tecnicos'] = $this->calendar_model->getTecnicosDisponiblesAgenda($idEvent,$start,$end);
		 $data['tecnico'] = $this->calendar_model->getTecnicoSelected($idEvent);
		 //print_r(json_encode($data['tecnicos']));
		 //return print_r(json_encode($data['tecnicos']));
		 return print_r(json_encode($data));
	}

	

	public function getTechAvailableForGroup(){
		 //echo $idzona;
		 $idGrupo = $_POST["idGrupo"];
		 //echo $idGrupo;
		 $data['TechAvailable'] = $this->calendar_model->getTechAvailableForGroup($idGrupo);

		 //print_r(json_encode($data['direcction']));
		 return print_r(json_encode($data['TechAvailable']));

	}

	public function asignarTicket(){

		 $idUser =$_POST["idUser"];
		 $idCalendar =$_POST["idCalendar"];		
		 $fechaactual= date("Y-m-d H:i:s");

		
		 $config['total_rows'] = $this->calendar_model->count($idCalendar);	

		 if($config['total_rows'] == 0){
		 	 $data['asignar'] = $this->calendar_model->setTiketTech($idUser,$idCalendar,$fechaactual);
		 }else{
		 	 $data['asignar'] = $this->calendar_model->setUpdateTiketTech($idUser,$idCalendar,$fechaactual);
		 }

		  //$idUser = $this->session->userdata('user_id');
		  //$dataHistorica = $this->tracking_model->actualizaState($incident,'Asignado',$fechaactual,$idUser);
		 return print_r(($data['asignar']));
		 
	}
}
