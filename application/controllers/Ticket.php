<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Ticket extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			
			$this->load->model('ticket_model');
			$this->load->model('tracking_model');
			$this->load->model('dataperfil_model');
			
			if (!$this->ion_auth->logged_in()){
				redirect('auth/login');
			}
		}
		
		public function account()
		{
			$user = $this->ion_auth->user()->row();
			$data['title'] = "Mis Datos";
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			$data['authorizations'] = $this->user_model->get_authorizations();
			$data['user'] = array(
				'name' => $user->first_name . " " . $user->last_name,
				'email' => $user->email
			);
			$this->data = $data;
			$this->render('user/account');
			
		}
		
		public function password()
		{
			$data['title'] = "Cambiar Contraseña";
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			$this->form_validation->set_rules('user[password]', 'contraseña', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[user[password_confirm]]');
			$this->form_validation->set_rules('user[password_confirm]', 'repita contraseña', 'required');
			
			if ($this->form_validation->run() == FALSE){
				$this->data = $data;
				$this->render('user/password');
			} else {
				$user = $this->input->post('user');
				if ($this->user_model->change_password($user) == FALSE){
					$this->data = $data;
					$this->session->set_flashdata('flashMessage', -1);
					$this->render('user/password');
				} else {
					$this->session->set_flashdata('flashMessage', 2);
					redirect('user/account');
				}
			}
		}
		
		public function index()
		{
			if (!$this->ion_auth->in_group('7') && !$this->ion_auth->in_group('8') && !$this->ion_auth->in_group('9') && !$this->ion_auth->in_group('10') && !$this->ion_auth->is_admin()){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('form/index');
			}
			
			$data['search_value'] = "";
			$config['per_page'] = 7;
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			$page = $this->input->get('page') != null ? $this->input->get('page') : 1;
			
			if (!is_null($this->input->get('value'))){
				$config['base_url']   = site_url('user/index?value=' . $this->input->get('value'));
				$config['total_rows'] = $this->ticket_model->count($this->input->get('value'));

				$data['tecnicos']     = $this->ticket_model->find($this->input->get('value'), $config['per_page'], $page);
				$data['search_value'] = $this->input->get('value');
			} else {
				$config['base_url']   = site_url('tecnicos/index');
				$config['total_rows'] = $this->ticket_model->count();

				$data['tecnicos'] = $this->ticket_model->find(null, $config['per_page'], $page);
			}

			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$this->data = $data;
			$this->render('tecnicos/index');
		}
		
		public function create()
		{
			if (!$this->ion_auth->in_group('7') && !$this->ion_auth->is_admin()){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('tecnicos/index');
			}
			$data['title'] = "Crear Técnico";
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			$data['groups'] = $this->ticket_model->get_group_dropdown();
			
			$this->form_validation->set_rules('tec[name]', 'nombre', 'required');
	
			if ($this->form_validation->run() == FALSE){
				$tmpSD = $this->input->post('tec');
				
				if (!empty($tmpSD)){ //echo "if validate run2";
					$data['tec'] = $this->input->post('tec');
				}
				
				$this->data = $data;
				$this->render('tecnicos/create');
			} else {
				if ($this->ticket_model->create($this->input->post('tec')) == FALSE){
					
					$data['tec'] = $this->input->post('tec');
					$this->data = $data;
					$this->session->set_flashdata('flashMessage', -1);
					$this->render('tecnicos/create');
				} else {
					$this->session->set_flashdata('flashMessage', 1);
					redirect('tecnicos/index');
				}
			}
		}
		
		public function createTicket()
		{
			$data['id'] = $this->ticket_model->getId();
			
			if (!$this->ion_auth->in_group('11') && !$this->ion_auth->is_admin()){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('tracking/index');
			}
			
			$user_id       = $this->session->userdata('user_id');
			$id_perfil     = $this->session->userdata('id_perfil');
			$_CONFIG_USER  = $this->dataperfil_model->getDataProfile($user_id, $id_perfil, 'asignar' );
			
			$data['title']          = "Crear Ticket";
			$data['flashMes 2sage'] = $this->session->flashdata('flashMessage');
			$data['direcction']     = $this->ticket_model->get_direction_dropdown();
			$data['zona']           = $this->ticket_model->get_zona_dropdown();
			$data['form']           = $this->ticket_model->get_formAct_dropdown();
			$data['area']           = $this->ticket_model->getArea();

			$this->form_validation->set_rules('ticket[incident]',  'incidente', 'required');
			$this->form_validation->set_rules('ticket[area]',      'area',      'required');
			$this->form_validation->set_rules('ticket[bahia]',     'bahia',     'required');
			$this->form_validation->set_rules('ticket[zona]',      'zona',      'required');
			$this->form_validation->set_rules('ticket[grupo]',     'grupo',     'required');
			$this->form_validation->set_rules('ticket[conductor]', 'conductor', 'required');

			
			if ($this->form_validation->run() == FALSE){
				$tmpSD = $this->input->post('ticket');
				if (!empty($tmpSD)){
					$data['ticket'] = $this->input->post('ticket');
				}
				$this->data = $data;
				$this->render('ticket/create');  // primera vez entra aca
			} else {
				$fechaactual = date("Y-m-d H:i:s");
				if ($this->ticket_model->create($this->input->post('ticket'), $fechaactual) == FALSE){
					
					$data['ticket'] = $this->input->post('ticket');
					$this->data     = $data;
					$this->session->set_flashdata('flashMessage', -1);
					
					$this->render('ticket/create');
				} else {
					
					$idUser        = $this->session->userdata('user_id');
					$dataHistorica = $this->ticket_model->actualizaState($this->input->post('ticket'), STATUSABIERTO, $fechaactual, $idUser);

					$this->session->set_flashdata('flashMessage', 1);
					redirect('tracking/tickets');
				}
			}
		}
		
		public function getBahia(){
			
			$idArea = $this->input->post('id_area');
			$response = $this->ticket_model->getBahia($idArea);
			
			return print_r(json_encode($response));
			
		}
		
		public function autocompletar()
		{
			$this->render('ticket/autocompletar');
		}
		
		public function delete()
		{
			if (!$this->ion_auth->in_group('10') && !$this->ion_auth->is_admin()){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('user/index');
			}
			if ($user = $this->input->post('user')){
				$result = $this->user_model->delete($user['user_id']);
				if ($result){
					$this->session->set_flashdata('flashMessage', 0);
				} else {
					$this->session->set_flashdata('flashMessage', -1);
				}
			}
			redirect('user/index');
		}
		
		public function change_state()
		{
			if (!$this->ion_auth->in_group('9') && !$this->ion_auth->is_admin()){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('user/index');
			}
			if ($user = $this->input->post('user')){
				$result = FALSE;
				$this->session->set_flashdata('flashMessage', -1);

				if ($user['action'] == 0){
					$result = $this->user_model->deactivate($user['user_id']);
					$this->session->set_flashdata('flashMessage', 6);
				}else if ($user['action'] == 1){
					$result = $this->user_model->activate($user['user_id']);
					$this->session->set_flashdata('flashMessage', 5);
				}
			}
			redirect('user/index');
		}
		
		public function edit($id)
		{
			if (!$this->ion_auth->in_group('8') && !$this->ion_auth->is_admin()){
				$this->session->set_flashdata('flashMessage', 7);
				redirect('tecnicos/index');
			}
			
			if ($id == 1){
				redirect('tecnicos/index');
			}
			
			$data['title']    = "Editar Técnico";
			$data['edit']     = TRUE;
			$data['groups']   = $this->ticket_model->get_group_dropdown();
			
			$auxTec = $this->ticket_model->get($id);
			if ($this->input->post('tec')){
				$tecnicos = $this->input->post('tec');
			} else {
				$tecnicos = $auxTec;
			}
			$data['tec'] = $tecnicos;
			$data['flashMessage'] = $this->session->flashdata('flashMessage');
			
			$this->form_validation->set_rules('tec[name]', 'nombre', 'required');
			$this->form_validation->set_rules('tec[alias]', 'apellidos', 'required');
			
			if ($this->form_validation->run() == FALSE){
				$this->data = $data;
				$this->render('tecnicos/create');
			} else {
				if ($this->ticket_model->edit($this->input->post('tec')) == FALSE){
					$this->data = $data;
					$this->session->set_flashdata('flashMessage', -1);
					$this->render('tecnicos/create');
				} else {
					//Envía aviso de creación exitosa
					$this->session->set_flashdata('flashMessage', 1);
					redirect('tecnicos/index');
				}
			}
		}
		
		public function getZonas()
		{
			$idzona = $_POST["idzona2"];
			$data['direcction'] = $this->ticket_model->get_directionFilter_dropdown($idzona);
			
			return print_r(json_encode($data['direcction']));
			
		}
		
		public function getGrupos()
		{
			$idzona       = $_POST["idzona"];
			$user_id      = $this->session->userdata('user_id');
			$id_perfil    = $this->session->userdata('id_perfil');
			$_CONFIG_USER = $this->dataperfil_model->getDataProfile($user_id, $id_perfil, 'asignar' );

			$data['Grupos'] = $this->ticket_model->get_grupos_dropdown( $_CONFIG_USER['GROUP'],  $idzona);

			return print_r(json_encode($data['Grupos']));
		}
		
		public function getTecnicos()
		{
			$idzona     = $_POST["idzona"];
			$idgrupo    = $_POST["idgrupo"];
			
			$user_id       = $this->session->userdata('user_id');
			$id_perfil     = $this->session->userdata('id_perfil');
			$_CONFIG_USER  = $this->dataperfil_model->getDataProfile($user_id, $id_perfil, 'asignar' );
			
			$data = $this->tracking_model->getTech( $_CONFIG_USER['GROUP'],  $idzona, $idgrupo);

			return print_r(json_encode($data));
		}
		
		public function services()
		{
			$idCliente        = $_POST["idCliente"];
			$data['service']  = $this->ticket_model->get_service_dropdown($idCliente);

			return print_r(json_encode($data['service']));
		}
		
		public function getValidarEstadoTicket()
		{
			$incident = $_POST["incident"];
			$response = $this->ticket_model->validateStatus($incident);
			if ($response == "0"){
				$data['asignar'] = "OK";
			} else {
				$data['asignar'] = "NOK";
			}

			return print_r(json_encode($data['asignar']));

		}
		
		public function getTechAvailableForGroup()
		{
			$idGrupo       = $_POST["idGrupo"];
			$id_customer   = $_POST["id_customer"];
			$data['TechAvailable'] = $this->ticket_model->getTechAvailableForGroup($idGrupo, $id_customer);

			return print_r(json_encode($data['TechAvailable']));
		}
		
		public function getDirectionForZona()
		{
			$idZona             = $_POST["idZona"];
			$data['directions'] = $this->ticket_model->getDirectionForZona($idZona);
		
			return print_r(json_encode($data['directions']));
		}
		
		public function getLatLng()
		{
			$cliente   = $_POST["cliente"];
			$direccion = $_POST["direction"];
			$idZona    = $_POST["idZona"];
			$geo       = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($direccion) . '&sensor=false');
			
			$geo = json_decode($geo, true);
			
			if ($geo['status'] == 'OK'){
			
				$latitud   = $geo['results'][0]['geometry']['location']['lat'];
				$longitud  = $geo['results'][0]['geometry']['location']['lng'];
				$long_name = $geo['results'][0]['address_components'][1]['long_name'];
				$region    = $geo['results'][0]['address_components'][5]['long_name'];
				$formatted_address = $geo['results'][0]['formatted_address'];
				$data['LatLng'] = $this->ticket_model->getLatLngForDirection($latitud, $longitud, $cliente, $long_name, $region, $formatted_address, $idZona);

				return print_r(json_encode($data['LatLng']));
			} else {
				echo $geo['status']; // ZERO_RESULTS
			}
		}
		
		public function getCategoria()
		{
			$txtServicio        = $_POST["txtServicio"];
			$data['categorias'] = $this->ticket_model->getcategoriasforService($txtServicio);
			
			return print_r(json_encode($data['categorias']));
		}
		
		public function getSla()
		{
			$txtServicio  = $_POST["txtServicio"];
			$txtCategoria = $_POST["txtCategoria"];
			$data['slas'] = $this->ticket_model->getSla($txtServicio, $txtCategoria);
			
			return print_r(json_encode($data['slas']));
			
		}
		
		public function changeStatus()
		{
			$incident    = $_POST["incident"];
			$estado      = $_POST["status"];
			$descripcion = $_POST["observacion"];
			
			$idTicket = ($_POST["idTicket"] != '') ? $_POST["idTicket"] : '';
			$id_tech  = ($_POST["idTech"] != '') ? $_POST["idTicket"] : '';
			$id_form  = ($_POST["idForm"] != '') ? $_POST["idTicket"] : '';
			$sla      = ($_POST["sla"] != '') ? $_POST["idTicket"] : '';
			$coordX   = ($_POST["lat"] != '') ? $_POST["idTicket"] : '';
			$coordY   = ($_POST["lng"] != '') ? $_POST["idTicket"] : '';
			
			$id_user = $this->session->userdata('user_id');
			
			$data['asignar'] = $this->ticket_model->changeStatus($incident, $estado, $id_tech, $id_user, $descripcion, $id_form, $sla, $coordX, $coordY);
			
			return print_r(($data['asignar']));
		}
	
		public function deleteTicket(){
			
			$ticket   = addslashes(trim($_POST["ticket"]));
			$response = $this->ticket_model->deleteTicket($ticket);

			if($response){
				$response = 'OK';
			}else{
				$response = 'NOK';
			}
			
			return print_r($response);
			
		}

		public function getConductor(){
			
			$idZona = $this->input->post('idZona');
			$idGroup = $this->input->post('idGroup');
			$response = $this->ticket_model->getConductor($idZona, $idGroup);
			
			return print_r(json_encode($response));
			
		}


		/* Despliega nuevas opciones del Ticket */
		public function detail_Ticket($id)
      {
         $data['search_value'] = $id;
         $this->data = $data;
         $this->render('ticket/index');
      }

      /* Lista los siguientes estados del ticket, de acuerdo al estado actual y a su tipo (Requerimiento o Incidente) */
      public function getEstado()
      {

         if($this->input->post('id_ticket'))
         {
            $result = $this->ticket_model->getEstado($this->input->post('id_ticket'));
            if(!is_null($result[0]['idstatus']))
            {
               $estados = array();
               if( (int)$result[0]['type_incident'] == 2 ) /* TIPO REQUERIMIENTO */
               {
                  switch ( (int)$result[0]['idstatus'] ) {
   
                     default:
                        $estados = [3,17,5];
                        break;
                     /*case 1: // ABIERTO
                        $estados = [3,8,9];
                        break;
                     case 3: // EN PROGRESO
                        $estados = [8];
                        break;
                     case 5: // RESUELTO
                        $estados = [6,15];
                        break;
                     case 7: // POSPUESTO
                        $estados = [3,15];
                        break;
                     case 8: // SUSPENDIDO
                        $estados = [3];
                        break;
                     case 10: // CERRADO PARCIAL
                        $estados = [15];
                        break;
                     case 15: // REABIERTO
                        $estados = [3,8,9];
                        break;
                     default:
                        $estados = NULL;
                        break;*/
                  }
               }else if( (int)$result[0]['type_incident'] == 1 ) /* TIPO INCIDENTE */
               {
                  switch ( (int)$result[0]['idstatus'] ) {
                     default:
                        $estados = [3,17,5];
                        break;
                     /*case 1: // ABIERTO
                        $estados = [3,8,9];
                        break;
                     case 3: // EN PROGRESO
                        $estados = [8,9,10];
                        break;
                     case 5: // RESUELTO
                        $estados = [6,9,15];
                        break;
                     case 7: // POSPUESTO
                        $estados = [3,8,9,15];
                        break;
                     case 8: // SUSPENDIDO
                        $estados = [3,9,15];
                        break;
                     case 10: // CERRADO PARCIAL
                        $estados = [15];
                        break;
                     case 15: // REABIERTO
                        $estados = [3,8,9];
                        break;
                     default:
                        $estados = NULL;
                        break;*/
                  }
               }
               if( !is_null($estados) )
               {
                  $str_estados = implode( ", ", $estados );
                  $estados = $this->ticket_model->get_estados_dropdown($str_estados);
               }
               $res = array(
                  'estado_actual' => $result[0]['name'],
                  'estados' => $estados
               );
            }

            return print_r(json_encode($res));
         }

      }

            /* Lista la informcion general del ticket */
      public function getInfo()
      {
         if($this->input->post('id_ticket'))
         {
            $result = $this->ticket_model->getInfo($this->input->post('id_ticket'));
            return print_r(json_encode($result[0]));
      }
	 }


	  /* Actualiza estado del Ticket */
      public function actEstado()
      {
         $res = 'NOK';
         $persistent_id = addslashes(trim($this->input->post('persistent_id')));
         $estado_actual = addslashes(trim($this->input->post('estado_actual')));
         $estado_nuevo = addslashes(trim($this->input->post('estado_nuevo')));
         if($this->input->post('sig_estados') && $this->input->post('txt_com') && $this->input->post('id_ticket'))
         {
            /* Consumir servicio para actualizar CA */
            if($persistent_id){
               $resChangeStatusCA = $this->ticket_model->sendDataChangeStatusCA($persistent_id, addslashes(trim($this->input->post('txt_com'))), addslashes(trim($this->input->post('id_ticket'))), (int)$this->input->post('sig_estados'));
               $res       = $resChangeStatusCA;
            }
            //print_r(empty($persistent_id));
            if( ($persistent_id && $res[0]['Value']=='OK') || !$persistent_id )
            {
               $nvo_estado = array(
                  'idstatus' => (int)$this->input->post('sig_estados')
               );
               $result = $this->ticket_model->actEstado($nvo_estado, $this->input->post('id_ticket'));
               if($result == "OK")
               {
                  $user_id = $this->session->userdata('user_id');
                  $dateNow = date('Y-m-d H:i:s');
                  $his_ticket = array(
                     'id_incident' => $this->input->post('id_ticket'),
                     'updatetime' => $dateNow,
                     'estado' => (int)$this->input->post('sig_estados'),
                     'lat' => NULL,
                     'lng' => NULL,
                     'description' => addslashes(trim($this->input->post('txt_com'))),
                     'id_user' => $user_id,
                     'user_origen' => 'web'
                  );
                  if($this->ticket_model->add_historico($his_ticket)){
                     $log = array(
                        'type_register'        =>  "Actualizar Estado",
                        'incident'             =>  addslashes(trim($this->input->post('id_ticket'))),
                        'create_user'          =>  (int)$user_id,
                        'create_date'          =>  $dateNow,
                        'description'          =>  addslashes(trim($this->input->post('txt_com'))),
                        'description_system'   =>  "Estado cambia de '".$estado_actual."' a '".$estado_nuevo."'"
                     );
                     if($this->ticket_model->registrarLog($log)){
                           $res = 'OK';
                     }

                     $res = 'OK';
                  }

               }
            }



            return print_r(json_encode($res));
         }
      }


	}