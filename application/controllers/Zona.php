<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Zona extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('zona_model');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	
	}

	/* Listado de categorías */
	public function index(){

		$id_perfil = $this->session->userdata('id_perfil');
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}

		$data['search_value'] = "";
		$config['per_page'] = 7;
		$data['flashMessage'] = $this->session->flashdata('flashMessage');	
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;		
		if(!is_null($this->input->get('value'))){
		$config['base_url'] = site_url('zona/index?value='.$this->input->get('value'));						
			$config['total_rows'] = $this->zona_model->count($this->input->get('value'));			
			$data['zonaes'] = $this->zona_model->find($this->input->get('value'), $config['per_page'], $page);
			$data['search_value'] = $this->input->get('value');
		}else{
			$config['base_url'] = site_url('zona/index');
			$config['total_rows'] = $this->zona_model->count();			
			$data['zonaes'] = $this->zona_model->find(null, $config['per_page'], $page);			
		}
		$this->pagination->initialize($config);		
		$data['pagination'] = $this->pagination->create_links();
		$this->data = $data;
		$this->render('zona/index');
	}

	/*Formulario de creación de categoría */
	public function create(){
		//Se comprueban las credenciales
		/*if(!$this->ion_auth->in_group('4') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('zona/index');
		}*/
		$id_perfil = $this->session->userdata('id_perfil');
		if (!$this->ion_auth->is_grupo($id_perfil, '22')) {
			$this->session->set_flashdata('flashMessage', 7);
			redirect('user/account');
		}

		$this->data['title'] = "Crear Factoring";
		$this->data['flashMessage'] = $this->session->flashdata('flashMessage');
		//new dBug($this->input->post('zona'));

	   $this->form_validation->set_rules('zona[name]', 'nombre', 'required');	
	   $this->form_validation->set_rules('zona[rut]', 'Rut', 'required');	



		if($this->form_validation->run() == FALSE){
                $tmpSD = $this->input->post('zona');
			if(!empty($tmpSD)){
				$this->data['zona'] = $this->input->post('zona');
			}
			$this->render('zona/create');
		}else{
			if($this->zona_model->create($this->input->post('zona')) == FALSE){				
				$this->data['zona'] = $this->input->post('zona');
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('zona/create');
			}else{
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('zona/index');
			}		
		}	
	}

	/* Formulario de edición de categoría */
	public function edit($id){
		//Se comprueban las credenciales
	  
		if(!$this->ion_auth->in_group('5') && !$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('zona/index');
		}
		$this->data['title'] = "Editar Factoring";
		$this->data['edit'] = TRUE;
		$this->data['flashMessage'] = $this->session->flashdata('flashMessage');

	    $this->form_validation->set_rules('zona[name]', 'nombre', 'required');			
		$this->form_validation->set_rules('zona[rut]', 'Rut', 'required');		


		if($this->form_validation->run() == FALSE){		
			$this->render('zona/create');
		//new dBug($this->data.'A');exit();
		}else{
		//new dBug($this->data.'b');exit();
			if($this->zona_model->edit($this->data['zona']) == FALSE){	
				$this->session->set_flashdata('flashMessage', -1);
				$this->render('zona/create');
			}else{
		//new dBug($this->data.'c');exit();
				//Envía aviso de creación exitosa
				$this->session->set_flashdata('flashMessage', 1);
				redirect('zona/index');
			}		
		}

	}

	/* Función Eliminar categoría */
	public function delete(){

		/*if(!$this-> $this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('zona/index');
		}	*/


		if($zona = $this->input->get('id')){												
			$result = $this->zona_model->delete($zona);									
			if($result){
				$this->session->set_flashdata('flashMessage', 0);
			}else{
				$this->session->set_flashdata('flashMessage', -1);
			}			
		}		
		redirect('zona/index');
	



	}

}
