<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfiles extends MY_Controller {

	function __construct() {
		parent::__construct();		
		$this->load->model('user_model');  
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	public function index(){
		/*if(!$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('auth/login');
		}*/
		//new dBug('a');exit();
		$id_perfil =  $this->session->userdata('id_perfil');          
        if($id_perfil != 1){               
             $this->session->set_flashdata('flashMessage', 7);
             redirect('user/account');
        }
		$this->data['title'] = "Perfiles de usuario";
		$this->data['perfil'] = $this->user_model->get_perfil();
		$this->session->set_flashdata('flashMessage', -1);
		$this->render('perfiles/index');
	}

	public function create(){
		/*if(!$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('auth/login');
		}*/

		 $id_perfil =  $this->session->userdata('id_perfil');
              
        if(!$this->ion_auth->is_grupo($id_perfil,'1')){               
             $this->session->set_flashdata('flashMessage', 7);
             redirect('user/account');
        }
		$this->data['title'] = "Perfiles de usuario";
		$this->data['group'] = $this->user_model->get_groups();
		$this->data['permisos'] = array();
		//new dBug($this->data);exit();
		$this->session->set_flashdata('flashMessage', -1);
		$this->render('perfiles/create');
	}

	public function edit(){
		if(!$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('auth/login');
		}
		$this->data['title'] = "Perfiles de usuario";
		$this->data['perfil'] = $this->user_model->get_perfil_by_id($this->uri->segment(3, 0));
		$this->data['group'] = $this->user_model->get_groups();
		$this->data['permisos'] = $this->user_model->getpermisosperfil($this->uri->segment(3, 0));		
		$this->session->set_flashdata('flashMessage', -1);
		$this->render('perfiles/create');
	}

	public function save(){
		if(!$this->ion_auth->is_admin()){
			$this->session->set_flashdata('flashMessage', 7);
			redirect('auth/login');
		}
		//new dBug($this->input->post());exit();

		$this->user_model->save_perfil();
		$this->session->set_flashdata('flashMessage', -1);
		redirect('perfiles/index');
	}
}