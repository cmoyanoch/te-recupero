<!--script src="<?php echo base_url();?>assets/js/capsulas/paginacion.js"></script -->
<!-- <script src="<?php echo base_url();?>assets/js/manuales/manuales.js"></script>   -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/dataTables.css">
<script src="<?php echo base_url();?>assets/js/dataTables.js"></script>


<?php

      $Creg = $data['CReg'];             //CANTIDAD TOTAL DE REGISTROS QUE CONTENGA LA CONSULTA. ARRAY
      $registrosTot = $Creg['cantReg'];  //CANTIDAD TOTAL DE REGISTROS QUE CONTENGA LA CONSULTA NUMERO
      $contreg = $data['contadorPag'];   //TRAE COMO ARRAY EL NUMERO DE LA PAGINA EN EL QUE VAMOS
      $contadorPag = $contreg;           //RECIBE COMO NUMERO LA PAGINA EN EL QUE VAMOS
      $pag = $registrosTot/PAGINADO_CANT;
      $pag = floor($pag);
      $resto = $registrosTot%PAGINADO_CANT;
      if ($resto>0 || $pag==0) {
        $pag = $pag+1;
      }

      
      $palabra = ' Página de ';

//    echo '<pre>';
//     return print_r($data);

       $cont = 0;
          
?>

  <script type="text/javascript">
  $(document).ready(function() {
      $('#TablaCapsulas').DataTable(
        {
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": false,
            "bInfo": false,
            "bAutoWidth": false }
        );
  } );

  </script>

  <style type="text/css">
  #TablaCapsulas{
    margin: auto;
  }
  .div1{
    margin: auto;
  }
  </style>


<div id="container">

  <div id="tabCapsulas" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  

    <table id="TablaCapsulas" class="table table-striped tablaA" style="width: 100%;">

    <thead>
        <tr class="listCap">
            <th style="width: 18px;">N°</th>
            <th>ARCHIVO</th>
            <th>DESCRIPCIÓN</th>
            <th style="width: 125px;">FECHA DE CARGA</th>
            <th>QUITAR</th>
        </tr>
    </thead>

     <tbody id="CargarCapsulas">

            <?php
              foreach ($data['listado'] as $val) {
                $cont ++;
                $fechaIni = explode(" ", $val['DateTime']);
                $fechaDiv = explode("-", $fechaIni[0]);
                $fechafin = $fechaDiv[2].'-'.$fechaDiv[1].'-'.$fechaDiv[0].' '.$fechaIni[1];

            ?>                    
              <tr class="trManuales" id="<?=$val['Id'];?>" style="cursor: pointer;">
                  <td><?=$cont;?></td>
                  <td><?=strtoupper($val['NameCapsule']);?></td>
                  <td><input type="" style="border-style: hidden;color: #3385CF; width: 600px;" maxlength="70" class="form-control descCap" id="<? echo $val['Id'] ?>" value="<? echo strtoupper($val['Description']); ?>"> </td>
                  <td><?=$fechafin;?></td>
                  <td align="center">
                  <a id="<?=$val['Id'];?>" class="btn btn-danger fa fa-times fa-2x quitarDoc" aria-hidden="true">  </a>
                  </td>       
<!--                   <td align="center">
                  <a href="<?  ?>" download="something.zip" class="btn btn-success fa fa-cloud-download fa-2x" >  </a>
                  </td>       -->             
              </tr>
            <?php
              }
            ?>

    </table>

    <div class="col-sm-12 col-md-12 col-xs-12" style="text-align: right;">
    <?php if($contadorPag > 1){ ?>
     <i type="button" id="atras" class="btn btn-success fa fa-arrow-left fa-2x" aria-hidden="true"></i>
    <?php } ?>

     <span class="label" id="cp" style="font-size: 12px;color: black;" ><?php echo $contadorPag ?></span>
     <span class="label" id="pal" style="font-size: 12px;color: black;" ><?php echo $palabra ?></span>
     <span class="label" id="pg" style="font-size: 12px;color: black;" ><?php echo $pag ?></span>   
     
     <?php if($contadorPag < $pag){ ?>
      <i type="button" id="adelante" class="btn btn-success fa fa-arrow-right fa-2x" aria-hidden="true" ></i>
     <?php } ?>
    </div>


  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br><br></div>

  </div>
     
</div>


<!--div class="">
<iframe  src="https://www.youtube.com/embed/sOnqjkJTMaA" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div-->