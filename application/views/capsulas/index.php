<script src="<?php echo base_url();?>assets/js/jquery-bootpag.js"></script>
<script src="<?php echo base_url();?>assets/js/capsulas/paginacion.js"></script>


<div id="contenedor_Cap">

        <div class="list">
            <ol class="breadcrumb" style="margin-top: 0px;">
                <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
                <li><a href="#"><strong>Administración</strong></a></li>
                <li class="active"><strong> Cápsulas </strong></li>
            </ol>            
        </div>
    
	<div class="col-sm-12 col-md-12 col-xs-12 " >
	    <?php echo $this->load->view('capsulas/upload'); ?>
	</div> 

	<div class="col-sm-12 col-md-12 col-xs-12 " align="center" id="carouselDiv" >
	    <?php echo $this->load->view('capsulas/carousel'); ?>
	</div> 

	<div class="col-sm-12 col-md-12 col-xs-12 " align="center" >
	    <?php echo $this->load->view('capsulas/search'); ?>
	</div> 

	<div class="col-sm-12 col-md-12 col-xs-12 "><br></div> 
        
	<div class="col-sm-12 col-md-12 col-xs-12 " id="cargarTabla">
	    <?php echo $this->load->view('capsulas/tableCapsule');  ?>
	</div> 

</div>