  <script src="<?php echo base_url();?>assets/js/carousel/jquery-1.7.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/carousel/jquery.featureCarousel.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/carousel/feature-carousel.css">

<script type="text/javascript">
  $(document).ready(function() {
    var carousel = $("#carousel").featureCarousel({
          // include options like this:
          // (use quotes only for string values, and no trailing comma after last option)
          // option: value,
          // option: value
          autoPlay: 0
        });

    $("#but_prev").click(function () {
      carousel.prev();
    });
    $("#but_pause").click(function () {
      carousel.pause();
    });
    $("#but_start").click(function () {
      carousel.start();
    });
    $("#but_next").click(function () {
      carousel.next();
    });
  });
</script>


<?php

//   print_r($data);

?>



<div class="carousel-container">
  <div id="carousel">

 <?php foreach ($data['listado'] as $res) { 
      
      if ($res['Type'] == '1') { ?>
        <div class="carousel-feature" title="<?php echo $res['NameCapsule'].' '.$res['Description']; ?>" >     
          <a href="#">
            <img width="380" height="240" src="#" hidden>
            <video class="carousel-image" alt="Image Caption" width="380" height="240" controls>
              <source src="<?php echo $res['Link']; ?>" type="video/mp4">
            </video>
          </a>
        </div>
    <?php } else if ($res['Type'] == '2') { ?>
         <div class="carousel-feature" title="<?php echo $res['NameCapsule'].' '.$res['Description']; ?>" >
          <a href="#">
            <img width="380" height="240" src="#" hidden>
            <video class="carousel-image" alt="Image Caption" width="320" height="240" controls>
              <source src="<?php echo $res['Link']; ?>" type="audio/mp4">
            </video>
          </a>
        </div>
  <?php  }  } ?>



  </div>
  <div id="carousel-left"><img src="<?php echo base_url();?>assets/img/carousel/arrow-left.png" /></div>
  <div id="carousel-right"><img src="<?php echo base_url();?>assets/img/carousel/arrow-right.png" /></div>  
</div>        	