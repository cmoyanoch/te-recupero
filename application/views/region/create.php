<?php 
	$hasData = false;
	if(isset($region)){
		$hasData = true;
	}

?>
<div class="list">
    <ol class="breadcrumb" style="margin-top: 4px;">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Mantenedores</strong></a></li>
        <li><a href="#"><strong>Región</strong></a></li>
        <li class="active"><strong> Crear </strong></li>
    </ol>               
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('region/edit/'.$region['region_id'], array('id'=>'edit-form'));
		echo form_hidden('region[region_id]', $region['region_id']);
	}else{
		echo form_open('region/create'); 	
	}	
	?>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-group">
				<label for="region[name]">Nombre Región:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'region[name]','pattern'=>'[A-Za-z]+','title'=>'Ingrese solo caracteres alfabéticos sin acento ', 'class'=> 'form-control', 'value'=> set_value('region[name]', isset($region['name'])? $region['name'] :''))); ?>
				<?php echo form_error('region[name]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-default blue">Guardar</button>	
			</div>	
		</div>
	</div>
