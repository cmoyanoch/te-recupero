<style>
    .ms-container {
        background: transparent url('../../assets/img/switch.png') no-repeat 50% 50%;
        width: 100%;
    }
</style>


        <div class="list">
            <ol class="breadcrumb" style="margin-top: 0px;">
                <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
                <li><a href="#"><strong>Administración</strong></a></li>
                <li class="active"><strong> Notificaciones </strong></li>
            </ol>            
        </div>

    <div class="content" >
        <div class="col-md-8">

            <div class="panel panel-primary">
                <div class="panel-heading">Destinatarios</div>
                <div class="panel-body">
                    <div class="col-sm-12 col-md-2" >
                        <input type="checkbox" id="allUserSend" ><strong> Todos</strong>
                    </div>
                    <label class="col-md-2 col-sm-12">Grupos: </label>
                    <div class="col-md-4 col-sm-12" >
                        <select id="selectEmpresasTec" style="width: 100%;">
                            <option value="-1">Seleccione</option>
                            <option value="all">TODOS</option>
                           <!-- <option value="clist">CARGAR LISTA</option>-->
                            <?php foreach ($data['empresas'] as $value) { ?>
                                <option value="<?php echo $value['Empresa']; ?>"><?php echo $value['Empresa']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-12" >
                        <div class='input-group'>
                            <span class='input-group-addon'><i class='fa fa-search'></i></span>
                            <input type="text" class="form-control" id="searchTec" placeholder="Buscar Nombre" style="width: 100%;"/>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <select multiple="multiple" id="select-destinatario" name="my-select[]" style="width:100%">

                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">Mensaje</div>
                <div class="panel-body">
                    <textarea name="message" id="msjNoti" rows="12" maxlength="700" style="width:100%"></textarea><br/>
                    <small>Largo Mensaje: <span id="cantidadTexto">700</span></small>
                </div>
                <div class="panel-footer">
                    <button type="button" class="btn btn-success" id="sendMSJ" ><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Enviar Notificaci&oacute;n</button>
                </div>
            </div>
        </div>
    </div>

<div id="cargarEspera" > </div>


<script src="<?php echo base_url();?>assets/js/notificaciones/notificaciones.js"></script>
