<style>
	label{
		font-weight: bold !important;
		color: #2b2b2b;
		font-size: 15px;
	}
</style>
<div class="list">
	<ol class="breadcrumb" style="margin-top: 4px;">
		<li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
		<li><a href="#"><strong>Mantenedores</strong></a></li>
		<li><a href="#"><strong>Area</strong></a></li>
		<li class="active"><strong> <?php if ($site['id']){ echo "Editar"; } else { echo "Crear"; } ?></strong></li>
	</ol>
</div>
<div class="container">
	<?php
		if($edit){
			echo form_open('site/edit/'.$area['id'], array('id'=>'edit-form'));
			echo form_hidden('area[id]', $area['id']);
		}else{
			echo form_open('area/create');
		}
	
	?>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="area[name]">Nombre</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'area[name]','title'=>'Ingrese solo caracteres alfabéticos sin acento ', 'class'=> 'form-control', 'value'=> set_value('area[name]', isset($area['id'])? $area['name'] :''))); ?>
				<?php echo form_error('area[name]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-buttons" style="text-align: left;">
				<a class="btn btn-default" href="<?=base_url()?>area/index" role="button">Volver</a>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-buttons">
				<button type="submit" class="btn btn-success">Guardar</button>
			</div>
		</div>
	</div>
