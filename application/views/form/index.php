<div class="list">
    
        <ol class="breadcrumb" style="margin-top: 4px;">
            <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
            <li><a href="#"><strong>Formulario</strong></a></li>
            <li class="active"><strong> Buscar </strong></li>
        </ol>               
    
	<div class="container-fluid menu-content">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
				<button type="button"  onclick="history.back()" class="btn btn-success">atrás</button>
				</div>
				<div class="col-md-5">
					<?php echo form_open(site_url('form/index'), array('method'=>'get')); ?>
					<div class="input-group">
						<?php echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué formulario deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>					
						<span class="input-group-btn">
							<button class="btn btn-default blue" type="submit">Buscar</button>
						</span>
					</div>
					<?php echo form_close(); ?>
				</div>
				<div class="col-md-6 button-content">								
					<a href="<?php echo site_url('form/create'); ?>" class="btn btn-default blue">Crear</a>				
				</div>
			</div>			
		</div>		
	</div>
	<div class="container ">
		<div class="row">
			<div class="col-md-12">				
				<?php if (count($forms) > 0){ ?>
				<div class="table-responsive">
					<?php echo form_open(site_url('form/change_state'), array('id'=>'list-form')) ?>										
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
								<td class="cell-selection"><?php echo form_checkbox('select-all', '', FALSE, array('id'=>'select-all')); ?></td>
								<td>ID</td>
								<td>Título</td>
								<td>Descripcion</td>
								<td>Autor</td>							
								<td>F. Creación</td>
								<td>F. Modificación</td>
								<td>Estado</td>
								<td class="with-icon"></td>
								<td class="with-icon"></td>
							</tr>				
						</thead>
						<tbody>
							<?php foreach ($forms as $keyForm => $form): ?>							
								<tr data-id ="<?php echo $form['form_id']; ?>">								
									<td><?php echo form_checkbox('form[form_id][]', $form['form_id'], FALSE,array('class'=> 'checkbox-element'));?></td>
									<td><?php echo $form['form_id']; ?></td>
									<td><?php echo character_limiter($form['title'], 30, "...") ?></td>
									<td><?php echo character_limiter($form['description'], 50, "...") ?></td>
									<td><?php echo $form['user'] ?></td>								
									<td><?php echo $form['created'] ?></td>
									<td><?php echo $form['modificated'] ?></td>
									<td><?php echo $form['active'] ?></td>
									<td>
										<a title="<?php echo $form['active'] == 'Activo' ? 'Desactivar': 'Activar';?>" href="" data-title="<?php echo $form['active'] == 'Activo' ? 'Desactivar formulario': 'Activar formulario';?>" data-message="¿Estás seguro que deseas <?php echo $form['active'] == 'Activo' ? 'desactivar': 'activar';?> el formulario de título: <?php echo $form['title'] ?>? Recueda que dependiendo de la opción seleccionada quedará accesible o no para su descarga en los dispositivos móviles." data-function="changeState" data-param="<?php echo ($form['active'] == 'Activo' ? '0': '1') .','. $form['form_id'];?>" data-toggle="modal" data-target="#normal-modal">
											<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#'.($form['active'] == 'Activo' ? 'icon-stop': 'icon-play') ?>"></use></svg>
										</a>
									</td>
									<td>
										<?php if ($form['active'] != 'Activo'): ?>
											<a title="Editar" href="<?php echo site_url('form/edit/'.$form['form_id']) ?>">
												<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-edit' ?>"></use></svg>
											</a>
										<?php endif ?>										
									</td>								
								</tr>							
							<?php endforeach ?>						
						</tbody>
					</table>
				</div>
				<?php echo form_close(); ?>				
				<?php }else{ 
					if ($search_value != "") {
						echo "<p class='message-no-data'>No existen resultados con los criterios de búsqueda ingresados.</p>";
						echo "<p class='message-no-data link'><a href='".site_url('form/index')."'>Volver al inicio</a></p>";
					}else{
						echo "<p class='message-no-data'>El sistema aun no registra información.</p>";
					}	
				} ?>				
			</div>	
		</div>		
		<div class="row">
			<div class="col-md-12 pagination-content">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<?php echo $pagination; ?>					
					</ul>
				</nav>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		<?php if ($search_value): ?>			
			$("#search-value").focus();
			$("#search-value").val($("#search-value").val());
		<?php endif ?>

		/*Al cargar la web selecciona todos los elementos según el estado del principal*/
		if($('#select-all').prop('checked')){
			$('.checkbox-element').prop('checked', true);
			addDeactivateButton();
		}else{
			$('.checkbox-element').prop('checked', false);
			$('#deactivate-button').remove();
			$('#activate-button').remove();
		}
		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		if($('input.checkbox-element:checked').length > 0){
			addDeactivateButton();
		}

		/* Funcionalidad del botón del modal "Aceptar" para activar/desactivar formulario */
		$(document).on('click', '#accept-button-modal.changeState', function(){
			$param = ($(this).data('param')).split(",");

			$form = $('#list-form');
			if($param.length == 2){
				$form.append($('<input/>').attr({"type":"hidden", "name":"form[form_id][]", "value":$param[1]}));
				$('.checkbox-element').prop('checked', false);				
			}
			$form.append($('<input/>').attr({"type":"hidden", "name":"form[action]", "value":$param[0]}));				             
            $form.submit();
            $('#accept-button-modal').removeClass('changeState');
			$('#accept-button-modal').removeData('param');
			$('#normal-modal').modal('hide');            
		})

		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		$('#list-form').on('change', function(){			
			if($('input.checkbox-element:checked').length > 0){
				addDeactivateButton();
			}else{
				$('#deactivate-button').remove();
				$('#activate-button').remove();
			}
		});

		/*Selecciona todos los elementos según el estado del principal*/
		$('#select-all').on('change', function(){
			if($(this).prop('checked')){
				$('.checkbox-element').prop('checked', true);				
			}else{
				$('.checkbox-element').prop('checked', false);
				$('#deactivate-button').remove();
				$('#activate-button').remove();
			}
		});

		/*Crea el botón desactivar*/
		function addDeactivateButton(){
			if($('.menu-content .button-content').find('#deactivate-button').length == 0){
				$buttonDeactivate = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'deactivate-button',"data-title":"Desactivar formulario(s)" ,"data-message":"¿Estás seguro que deseas desactivar el(los) formulario(s) seleccionado(s)?", "data-function":"changeState", "data-toggle":"modal", "data-target":"#normal-modal", "data-param":"0"}).html('Desactivar');
				$buttonActivate = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'activate-button',"data-title":"Activar formulario(s)","data-message":"¿Estás seguro que deseas activar el(los) formulario(s) seleccionado(s)?", "data-function":"changeState", "data-toggle":"modal", "data-target":"#normal-modal", "data-param":"1"}).html('Activar');
				$('.menu-content .button-content').prepend($buttonActivate);	
				$('.menu-content .button-content').prepend($buttonDeactivate);	
			}			
		}
		

		
	});
</script>