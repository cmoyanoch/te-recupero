<?php 
	$hasData = false;
	if(isset($form)){
		$hasData = true;
	}
?>	

<style>
label{
	font-weight: bold !important;
    /*color: white*/;
    font-size: 15px;
}
</style>


<div class="list">
    
    <ol class="breadcrumb" style="margin-top: 4px;">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Mantenedores</strong></a></li>
        <li><a href="#"><strong>Empresa</strong></a></li>
        <li class="active"><strong> Crear </strong></li>
    </ol>               
    
</div>

<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('form/edit/'.$form['form_id'], array('id'=>'edit-form'));
		echo form_hidden('form[form_id]', $form['form_id']);
	}else{
		echo form_open('form/create', array('id'=>'create-form')); 	
	}	
	?>
	<div class="row">
		<div class="col-md-12">					
			<div class="form-group">
				<label for="form[title]">Título del formulario</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'form[title]', 'class'=> 'form-control', 'value'=> set_value('form[title]', isset($form['title'])? $form['title'] :''))); ?>
				<?php echo form_error('form[title]'); ?>
			</div>			
			<div class="form-group">
				<label for="form[description]">Descripción</label>
				<?php echo form_textarea(array('name'=>'form[description]', 'class'=> 'form-control', 'rows'=>2, 'value'=>set_value('form[description]', isset($form['description']) ? $form['description']:''))); ?>
				<?php echo form_error('form[description]'); ?>
			</div>
                    <!--
                        <div class="form-group">
                                <label for="form[country_id]">País:</label>
                                <?php// echo form_dropdown('form[country_id]', $countries, (int)$form["country_id"] = 0 ? '' : (int)$form["country_id"], array('class'=>'form-control')); ?>
                                <?php// echo form_error('form[country_id]'); ?>
                        </div>
                        <div class="form-group">
                                <label for="form[company_id]">Empresa:</label>
                                <?php// echo form_dropdown('form[company_id]', $companies, (int)$form["company_id"] = 0 ? '' : (int)$form["company_id"], array('class'=>'form-control')); ?>
                                <?php// echo form_error('form[company_id]'); ?>
                        </div>
                    -->
			<div class="general-content">				
				<div class="form-options" id="form-options">
					<a href="" title="Nueva pregunta" id="add-question">
						<div class="add-question">
							<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-add-question'; ?>"></use></svg>	
						</div>
					</a>
					<a href="" title="Nueva sección" id="add-section">
						<div class="add-section">
							<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-add-section'; ?>"></use></svg>	
						</div>
					</a>
				</div>
				<?php echo $form_validation_graph_cycle; ?>		
				<?php echo $form_validation_graph_alone; ?>
				<!--Carga Dinámica-->
				<?php 
					if($hasData){
				?>
				<ul class="sections">
					<?php $countSection = 0; ?>
					<?php foreach ($form['section'] as $keySection => $section): ?>
						<?php $countQuestion = 1; ?>
						<li id="section-<?php echo $keySection ?>" class="section" data-target="<?php echo $keySection ?>">
							<?php echo form_input(array('type'=> 'hidden', 'name'=>'form[section]['.$keySection.'][section_index]','class'=>'section-index','value'=>$keySection)); ?>
							<?php echo form_error('form[section]['.$keySection.'][section_index]'); ?>
							<p class="title section"><?php echo $countSection == 0 ? 'Formulario Raíz' : 'Sección '.$countSection ?></p>
							<ul class="questions">
								<?php foreach ($form['section'][$keySection]['question'] as $keyQuestion => $question): ?>
									<li id="question-<?php echo $keyQuestion ?>" class="question" data-target="<?php echo $keyQuestion ?>">
										<div class="mask"></div>
										<div class="row">
											<div class="col-md-12">
												<div class="question-number">
													<p><?php echo "Pregunta ".$countQuestion; ?></p>
												</div>
											</div>
										</div>										
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<?php echo form_input(
                                                                                                                array(
                                                                                                                    'type'=>'text', 
                                                                                                                    'name'=>'form[section]['.$keySection.'][question]['.$keyQuestion.'][text]', 
                                                                                                                    'class'=> 'form-control',
                                                                                                                    'placeholder'=>'Ingrese la pregunta', 
                                                                                                                    'value'=>htmlspecialchars_decode(set_value(
                                                                                                                                'form[section]['.$keySection.'][question]['.$keyQuestion.'][text]', 
                                                                                                                                $form['section'][$keySection]['question'][$keyQuestion]['text']
                                                                                                                            ))
                                                                                                                    )
                                                                                                                ); ?>
													<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][text]'); ?>	                                                                                                        
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">												
													<?php echo form_dropdown('form[section]['.$keySection.'][question]['.$keyQuestion.'][input_type_id]', $inputTypes, set_value('form[section]['.$keySection.'][question]['.$keyQuestion.'][input_type_id]',$form['section'][$keySection]['question'][$keyQuestion]['input_type_id']), array('class'=>'form-control type-question')); ?>
													<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][input_type_id]'); ?>                                                                                                        
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<?php echo form_dropdown('form[section]['.$keySection.'][question]['.$keyQuestion.'][category_id]', $categories, set_value('form[section]['.$keySection.'][question]['.$keyQuestion.'][category_id]',$form['section'][$keySection]['question'][$keyQuestion]['category_id']), array('class'=>'form-control')); ?>
													<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][category_id]'); ?>                                                                                                        
												</div>												
											</div>
                                                                                        <div class="col-md-2">
												<div class="form-group">
													<?php echo form_dropdown('form[section]['.$keySection.'][question]['.$keyQuestion.'][question_type_id]', $questiontypes, set_value('form[section]['.$keySection.'][question]['.$keyQuestion.'][question_type_id]',$form['section'][$keySection]['question'][$keyQuestion]['question_type_id']), array('class'=>'form-control')); ?>                                                                                                                                                                                                                
													<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][question_type_id]'); ?>                                                                                                        
                                                                                                        <?php //echo form_dropdown('form[section][0][question][0][question_type_id]', $question_type, '', array('class'=>'form-control')); ?>
                                                                                                        <?php //echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][question_type_id]'); ?>
												</div>												
											</div>
                                                                                        
										</div>
										<ul class="input-options">
											<?php if ($form['section'][$keySection]['question'][$keyQuestion]['input_type_id'] == '5' || $form['section'][$keySection]['question'][$keyQuestion]['input_type_id'] == '6' || $form['section'][$keySection]['question'][$keyQuestion]['input_type_id'] == '7'): ?>											
												<?php foreach ($form['section'][$keySection]['question'][$keyQuestion]['option'] as $keyOption => $option): ?>
													<li class="option">
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<div class="input-group">
																		<span class="input-group-addon <?php echo $form['section'][$keySection]['question'][$keyQuestion]['input_type_id'] == 7  ? 'select' : '' ?>">
																			<?php switch ($form['section'][$keySection]['question'][$keyQuestion]['input_type_id']) {
																				//Selección múltiple (radioButton)
																				case '5':
																					echo form_input(array('type'=>'radio', 'tabindex'=>"-1", 'readonly'=>'readonly','style'=>'cursor_auto', 'disabled'=>'disabled'));
																					break;
																				//Casilla de verificación (checkbox)
																				case '6':
																					echo form_input(array('type'=>'checkbox', 'tabindex'=>"-1", 'readonly'=>'readonly','style'=>'cursor_auto', 'disabled'=>'disabled'));
																					break;
																				//Despegable (select)
																				case '7':
																					echo '<svg class="icon" xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'.site_url('assets/images/icons.svg').'#icon-down"></use></svg>';
																					break;
																				default:
																					break;
																			} ?>																		
																		</span>
																		<?php echo form_input(array('type'=>'text', 'name'=>'form[section]['.$keySection.'][question]['.$keyQuestion.'][option]['.$keyOption.'][text]', 'placeholder'=>'Ingrese opción', 'class'=>'form-control', 'value'=> set_value('form[section]['.$keySection.'][question]['.$keyQuestion.'][option]['.$keyOption.'][text]',$form['section'][$keySection]['question'][$keyQuestion]['option'][$keyOption]['text']))) ?>
																		<div class="input-group-addon delete-button-content">
																			<?php if (count($form['section'][$keySection]['question'][$keyQuestion]['option']) > 1): ?>
																				<a href="" title="Eliminar opción" class="delete-option">
																					<svg class="icon" xmlns="http://www.w3.org/2000/svg">
																						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-close'; ?>"></use>
																					</svg>
																				</a>
																			<?php endif ?>																				
																		</div>																		
																	</div>
																	<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][option]['.$keyOption.'][text]'); ?>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<?php echo form_dropdown('form[section]['.$keySection.'][question]['.$keyQuestion.'][option]['.$keyOption.'][section]', array($form['section'][$keySection]['question'][$keyQuestion]['option'][$keyOption]['section']=>'Cargando sección...'), set_value('form[section]['.$keySection.'][question]['.$keyQuestion.'][option]['.$keyOption.'][section]', $form['section'][$keySection]['question'][$keyQuestion]['option'][$keyOption]['section']), array('class'=>'form-control section-select')); ?>
																	<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][option]['.$keyOption.'][section]'); ?>
																</div>
															</div>
														</div>
													</li>
												<?php endforeach ?>
												<li class="add-more-btn-content">
													<a class="btn btn-default" href="" role="button">Agregar otra opción</a>
												</li>
											<?php endif ?>
										</ul>
										<?php $restrictionTitle = false; ?>
										<ul class="input-restrictions">
											<?php if ($form['section'][$keySection]['question'][$keyQuestion]['input_type_id'] == '8'): ?>
												<?php $restrictionTitle = true ?>
												<hr>
												<li><p>Restricciones de la pregunta</p></li>
												<li>
													<div class="row">
														<div class="col-md-3">
															<div class="form-group">
																<?php echo form_input(array('type'=>'number', 'class'=>'form-control', 'name'=>'form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][1]','placeholder'=>'Cantidad mínima','value'=>set_value('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][1]',$form['section'][$keySection]['question'][$keyQuestion]['restriction']['1']))) ?>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<?php echo form_input(array('type'=>'number', 'class'=>'form-control', 'name'=>'form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][2]','placeholder'=>'Cantidad máxima', 'value'=>set_value('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][2]',$form['section'][$keySection]['question'][$keyQuestion]['restriction']['2']))) ?>																
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<?php echo form_dropdown('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][section]', array($form['section'][$keySection]['question'][$keyQuestion]['restriction']['section']=>'Cargando sección...'), set_value('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][section]', $form['section'][$keySection]['question'][$keyQuestion]['restriction']['section']), array('class'=>'form-control section-select')) ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][1]'); ?>
															<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][2]'); ?>
															<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][section]'); ?>
														</div>
													</div>
												</li>											
											<?php endif ?>
											
											<?php if ($form['section'][$keySection]['question'][$keyQuestion]['input_type_id'] == '11'): ?>
												<?php if (!$restrictionTitle): ?>
													<hr>
													<li><p>Restricciones de la pregunta</p></li>
												<?php endif ?>
												<li class="image-restriction">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">																
																<?php echo form_input(array('class'=>'form-control', 'type'=>'number', 'name'=>'form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][3]', 'placeholder'=>'Ingrese cantidad', 'value'=>set_value('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][3]', $form['section'][$keySection]['question'][$keyQuestion]['restriction']['3']))); ?>	
																<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][3]'); ?>				
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<div class="input-group">																	
																	<?php echo form_input(array('class'=>'form-control', 'type'=>'number', 'name'=>'form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][4]', 'placeholder'=>'Ingrese tamaño máximo total', 'value'=>set_value('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][4]',$form['section'][$keySection]['question'][$keyQuestion]['restriction']['4']))); ?>
																	<div class="input-group-addon">MB</div>	
																</div>
																<?php echo form_error('form[section]['.$keySection.'][question]['.$keyQuestion.'][restriction][4]') ?>
															</div>
														</div>
													</div>													
												</li>
											<?php endif ?>								
										</ul>										
										<div class="row question-options">
											<hr>
											<div class="col-md-6">
												<div class="form-inline">
													<div class="form-group">
														<?php echo form_checkbox(array('name'=>'form[section]['.$keySection.'][question]['.$keyQuestion.'][required]', 'value'=>'true', 'checked'=>set_checkbox('form[section]['.$keySection.'][question]['.$keyQuestion.'][required]','true', isset($form['section'][$keySection]['question'][$keyQuestion]['required']) ? (($form['section'][$keySection]['question'][$keyQuestion]['required']) == 1 ? true:false) :false ))); ?>
														<label for="form[section][<?php echo $keySection ?>][question][<?php echo $keyQuestion ?>][required]">Obligatoria</label>		
													</div>													
												</div>										
											</div>
											<div class="col-md-6 buttons">
												<?php if (count($section['question']) > 1): ?>
													<a href="" title="Eliminar pregunta" class="delete-question">
														<svg class="icon" xmlns="http://www.w3.org/2000/svg">
															<use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete'; ?>"></use>
														</svg>
													</a>
												<?php endif ?>												
											</div>
										</div>
									</li>
								<?php $countQuestion++; ?>
								<?php endforeach ?>
							</ul>							
						</li>
					<?php $countSection++; ?>						
					<?php endforeach ?>
				</ul>
				<?php 
					}else{
				 ?>
				<!--Carga inicial -->
				<ul class="sections">
					<li id="section-0" class="section selected" data-target="0">
						<?php echo form_input(array('type'=> 'hidden', 'name'=>'form[section][0][section_index]','class'=>'section-index','value'=>'0')) ?>
						<!-- <p class="title section">Formulario Raíz</p> -->
						<p class="title "><label>Formulario Raíz</label></p>
						<ul class="questions">
							<li id="question-0" class="question" data-target="0">
								<div class="mask"></div>
								<div class="row">
									<div class="col-md-12">
										<div class="question-number">
											<p>Pregunta 1</p>									
										</div>								
									</div>	
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<?php echo form_input(array('type'=>'text', 'name'=>'form[section][0][question][0][text]', 'class'=> 'form-control','placeholder'=>'Ingrese la pregunta')); ?>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<?php echo form_dropdown('form[section][0][question][0][input_type_id]', $inputTypes, '', array('class'=>'form-control type-question')); ?>			
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<?php echo form_dropdown('form[section][0][question][0][category_id]', $categories, '', array('class'=>'form-control')); ?>									
										</div>
									</div>
                                                                        <div class="col-md-2">
										<div class="form-group">
											<?php echo form_dropdown('form[section][0][question][0][question_type_id]', $questiontypes, '', array('class'=>'form-control')); ?>									
										</div>
									</div>
								</div>
								<ul class="input-options"></ul>
								<ul class="input-restrictions"></ul>							
								<div class="row question-options">
									<hr>
									<div class="col-md-6">
										<div class="form-inline">
											<div class="form-group">
												<?php echo form_checkbox(array('name'=>'form[section][0][question][0][required]', 'value'=>'true')); ?>
												<label for="form[section][0][question][0][required]">Obligatoria</label>		
											</div>											
										</div>										
									</div>
										<div class="col-md-6 buttons">										
									</div>
								</div>
							</li>							
						</ul>
					</li>
				</ul>
				<?php 
					}
				?>		
			</div>
			<div class="form-buttons">
				<button type="submit" class="btn btn-success">Guardar</button>	
			</div>					
		</div>	
	</div>
        <input type="hidden" name="delOpt" id="delOpt" value="">
        <input type="hidden" name="delSec" id="delSec" value="">
        <input type="hidden" name="delQue" id="delQue" value="">
	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
	$(function(){
		/*Variables globales*/
		$questionSelected = "";
		$sectionSelected = "#section-0";
		$principalSection = "#section-0";
		<?php if ($hasData){ ?>
		$sectionSelected = "#"+ $('li.section').first().attr('id');
		$principalSection = "#"+ $('li.section').first().attr('id');
		$sectionIndex = <?php echo $sectionIndex; ?>;
		$questionIndex = <?php echo $questionIndex; ?>;
		$optionIndex = <?php echo $optionIndex; ?>;
		
		<?php }else{ ?>
		$sectionIndex = 0;
		$questionIndex = 0;
		$optionIndex = 0;
		<?php } ?>
		refreshSectionSelect();
		/*Permite al usuario ordenar las preguntas de una sección de manera amigable */
		$('li.section ul.questions').sortable({
			handle: ".question-number",			
			placeholder: "ui-state-highlight",			
			stop: function(event, ui) {
				sortIndexQuestions();
			}
		});		

		/*Agrega una pregunta a continuación de la seleccionada*/
		$('#add-question').on('click' ,function(event){
			event.preventDefault();			
			$questionIndex++;
			$sectionTarget = $($sectionSelected).data('target');			
			$newQuestion = createQuestion($sectionTarget, $questionIndex);
			if($questionSelected != ''){
				$($questionSelected).after($newQuestion);				
			}else{				
				$($sectionSelected + ' ul.questions').append($newQuestion);
			}
			$($newQuestion).find('input:first').focus();			
			sortIndexQuestions();
			//Se genera el botón eliminar para la primera pregunta
			if($newQuestion.parent().find('li.question:first').find('a.delete-question').length == 0){
				$deleteIcon = '<svg class="icon" xmlns="http://www.w3.org/2000/svg"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete'; ?>"></use></svg>';
				$newQuestion.parent().find('li.question .question-options .buttons:first').append($('<a/>').attr({'href':'', 'title':'Eliminar pregunta','class':'delete-question'}).html($deleteIcon));
			}
		});

		/*Agrega una sección */
		$('#add-section').on('click',function(event){
			event.preventDefault();			
                        
			$sectionIndex ++;
			$questionIndex ++;
			$newSection = createSection($sectionIndex, $questionIndex);
			$('ul.sections').append($newSection);
			$($newSection).find('input[type="text"]:first').focus();						
			/*Permite al usuario ordenar las preguntas de una sección de manera amigable */	
			$('li.section ul.questions').sortable({
				handle: ".question-number",	
				placeholder: "ui-state-highlight",			
				stop: function(event, ui) {
					sortIndexQuestions();
				}
			});
					
			sortIndexQuestions();
			sortIndexSection();
			refreshSectionSelect();	
		});		

		/*Elimina una pregunta, Se utiliza el evento mouseup para que no se ejecute la función asociada al evento on click*/
		$(document).on('mouseup', 'a.delete-question', function(event){
			event.preventDefault();			
			$question = $(this).closest('li.question');
                        
                        var queStr = $($question).find('input[type="text"]').first().attr("name");
                        queArr = queStr.split("][");
                        
                        $("#delQue").val(queArr[3] + "#" + queArr[1] + ',' + $("#delQue").val());
                        
			$section = $($question).closest('li.section');
			$qty = $($section).find('li.question').length;
			if($qty > 1){				
				//Se deja la pregunta siguiente o anterior seleccionada
				if($($question).prev().length > 0){
					$aux = $($question).prev().find('input').first();
				}else{
					$aux = $($question).next().find('input').first();
				}
				$question.remove();
				$($aux).focus();
				$qty--;
					if($qty == 1){
					 $($section).find('li.question a.delete-question').remove();
				}
				sortIndexQuestions();					
			}
		});

		/*Elimina una opción*/
		$(document).on('mouseup', 'a.delete-option', function(event){
                        
			event.preventDefault();			
                            
                        $option = $(this).closest('li.option');
                        
                        var optStr = $($option).find('input[type="text"]').first().attr("name");
                        optArr = optStr.split("][");
                        
                        $("#delOpt").val(optArr[5] + "#" + optArr[3] + ',' + $("#delOpt").val());
                        
                        $options = $($option).parent();
                        $qty = $($options).find('li.option').length;
                        if($qty > 1){				
                                //Se deja la opción siguiente o anterior seleccionada
                                if($($option).prev().length > 0) {
                                        $aux = $($option).prev().find('input[type="text"]').first();
                                }else{
                                        $aux = $($option).next().find('input[type="text"]').first();
                                }
                                $option.remove();
                                $($aux).focus();
                                $qty--;
                                        if($qty == 1){
                                         $($options).find('li.option .delete-button-content a').remove();
                                }
                        }
		});

		/* Crea opciones según el input seleccionado*/
		$(document).on('click', '.add-more-btn-content a', function(event){
			event.preventDefault();			
			$question = $(this).closest('li.question');
			$questionTarget = $($question).data('target');
			$section = $(this).closest('li.section');
			$sectionTarget = $($section).data('target');
			$options = $(this).closest('ul.input-options');
			//Se incluyen las opciones según el elemento seleccionado antes del botón "agregar"
			switch ($($question).find('select.type-question').val()) {
				//Selección múltiple (Radio Button)
				case "5":
					$(this).parent().before(createRadioButtonField($sectionTarget, $questionTarget, $optionIndex));
					$optionIndex++;					
					break;
				//Casilla de verificación (Checkbox)
				case "6":
					$(this).parent().before(createCheckboxField($sectionTarget, $questionTarget, $optionIndex));
					$optionIndex++;					
					break;
				//Despegable (Select)
				case "7":
					$(this).parent().before(createSelectOptionField($sectionTarget, $questionTarget, $optionIndex));
					$optionIndex++;					
					break;					
			}
			//Se genera el botón eliminar para la primera opción
			if($($options).find('li.option:first').find('a.delete-option').length == 0){
				$deleteIcon = '<svg class="icon" xmlns="http://www.w3.org/2000/svg"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-close'; ?>"></use></svg>';
				$($options).find('li.option:first .delete-button-content').append($('<a/>').attr({'href':'', 'title':'Eliminar opción','class':'delete-option'}).html($deleteIcon));
			}
		});		
		
		/*Muestra los inputs adicionales según el tipo seleccionado*/
		$(document).on('change', '.type-question',function(){			
			$question = $(this).closest('li.question');
			$questionTarget = $($question).data('target');
			$section = $($question).closest('li.section');
			$sectionTarget = $($section).data('target');
			//Se elimina el contenido anterior
			$inputOptions = $question.find('ul.input-options').html('');
			$inputRestrictions = $question.find('ul.input-restrictions').html('');			
			//Se incluyen las opciones según el elemento seleccionado
			switch ($(this).val()) {
				//Selección múltiple (Radio Button)
				case "5":
					$option = createRadioButtonField($sectionTarget, $questionTarget, $optionIndex);
					$($option).find('a.delete-option').remove();					
					$inputOptions.append($option);
					$optionIndex++;
					$inputOptions.append(createAddMoreOptionButton());					
					break;
				//Casilla de verificación (Checkbox)
				case "6":
					$option = createCheckboxField($sectionTarget, $questionTarget, $optionIndex);
					$($option).find('a.delete-option').remove();					
					$inputOptions.append($option);
					$optionIndex++;
					$inputOptions.append(createAddMoreOptionButton());
					break;
				//Despegable (Select)
				case "7":
					$option = createSelectOptionField($sectionTarget, $questionTarget, $optionIndex);
					$($option).find('a.delete-option').remove();					
					$inputOptions.append($option);
					$optionIndex++;
					$inputOptions.append(createAddMoreOptionButton());
					break;
				//Campo personalizado
				case "8":
					$inputRestrictions.append($('<hr/>'));
					$inputRestrictions.append($('<li/>').append($('<p/>').html("Restricciones de la pregunta")));
					$inputRestrictions.append(createCustomFieldA($sectionTarget, $questionTarget));					
					break;
				//Imagen
				case "11":
					$inputRestrictions.append($('<hr/>'));
					$inputRestrictions.append($('<li/>').append($('<p>/').html("Restricciones de la pregunta")));
					$inputRestrictions.append(createRestrictionImageField($sectionTarget, $questionTarget));										
					$($question).find('.has-image-checkbox-content').remove();					
					break;
			}
		});	

		/*Se descata la sección actual */
		$(document).on('click', '.section p.title.section', function(){
			$('li.section').removeClass('selected');
			$(this).parent().addClass('selected');
			$sectionSelected = '#'+ $(this).attr('id');
			$(this).parent().find('ul.questions').find('li.question').first().find('input[type="text"]').first().focus();
		});

		/*Se destaca la pregunta en la que se trabaja al hacer clic*/
		$(document).on('click', '.question',function(){
			highlight($(this));
		});

		/*Se destaca la pregunta en la que se trabaja al hacer focus*/
		$(document).on('focus', '.question input, .question a, .question button, .question select' ,function(){
			highlight($(this).closest('li.question'));			

		});

		/*Elimina una Sección */
		$(document).on('click', '#accept-button-modal.deleteSection', function(){
			$('#accept-button-modal').removeClass('deleteSection');
			
			if($sectionSelected != $principalSection){
				$aux = $($sectionSelected).prev().find('input[type="text"]').first();
                                
                                var secStr = $($sectionSelected).find('input[type="text"]').first().attr("name");
                                secArr = secStr.split("][");
                        
                                $("#delSec").val(secArr[1] + ',' + $("#delSec").val());
                                
				$($sectionSelected).remove();	
                                // ASD
				sortIndexSection();
				refreshSectionSelect();				
				$($aux).focus();
			}
			$('#normal-modal').modal('hide');            
		});	

		/*Destaca la pregunta y la sección en la que se trabaja */
		function highlight(question){
			$deleteIcon = '<svg class="icon" xmlns="http://www.w3.org/2000/svg"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete'; ?>"></use></svg>'
			$('.question .mask').removeClass('selected');			
			$('.question').removeClass('selected');
			$('li.section').removeClass('selected');
			$(question).find('.mask').addClass('selected');
			question.addClass('selected');
			question.closest('li.section').addClass('selected');			
			$questionSelected = '#' + $(question).attr('id');
			$sectionSelected = 	'#' + $(question).closest('li.section').attr('id');	
			//se posiciona el menú de opciones en el lugar de la pregunta
			$('#form-options').stop().animate({"top": ($(question).position().top + 30 )}, "fast");  
			if($sectionSelected == $principalSection){
				$('#delete-section').remove();
			}else{
				if($('#delete-section').length == 0){
					$('#form-options').append(
					$('<a/>').attr({'title':'Eliminar sección', 'href':'', 'id':'delete-section', 'data-toggle':'modal', 'data-target':'#normal-modal', 'data-title':'Eliminar una sección' ,'data-message':'¿Estás seguro que quieres eliminar la sección? Se eliminarán todos los vínculos asociados a la sección.', 'data-function': 'deleteSection'}).append(
						$('<div/>').addClass('delete-section').html($deleteIcon)));
				}
			}
		}

		/*Cambia el número de la sección según el orden establecido */
		function sortIndexSection(){
			$sections = $('li.section').not(':first');			
			for (var i = 0; i < $sections.length; i++) {
				$($sections[i]).find('p.title.section').html('Sección '+ (i+1));				
				
			}
		}

		/*Cambia el número de la pregunta según orden establecido */
		function sortIndexQuestions(){			
			$sections = $('li.section');			
			for (var i = 0; i < $sections.length; i++) {
				$questions = $($sections[i]).find('li.question');				
				for (var j = 0; j < $questions.length; j++) {
					$($questions[j]).find('.question-number').html('Pregunta '+ ($($questions[j]).index() + 1));
				}
			}
		}		

		/*Actualiza las secciones presentes en los select*/
		function refreshSectionSelect(){		
			$newSelect = createSectionSelect();
			$select = $('select.section-select');

			for (var i = 0; i < $select.length; i++) {								
				$value = $($select[i]).val();				
				$target = $($select[i]).closest('li.section').data('target');				
				$($select[i]).html($($newSelect).html());
				if($($select[i]).find('option[value="'+$value+'"]').length){
					$($select[i]).val($value);
				}
				$($select[i]).find('option[value="'+$target+'"]').remove();
			}
		}
		
		//Obtiene todas las secciones creadas para su utilización en el select
		function createSectionSelect(sectionIndex = 0){			
			$select = $('<select/>').addClass('form-control section-select').append(
				$('<option/>').attr('value', '').html('Seleccione una sección'));
			$sections = $('ul.sections li.section:not(:first)');
			for (var i = 0; i < $sections.length; i++) {
				if($($sections[i]).find('input.section-index').val() != sectionIndex){
					$($select).append($('<option/>').attr('value', $($sections[i]).find('input.section-index').val()).html($($sections[i]).find('p.title.section').html()));				
				}
			}
			return $select;
		}

		/*Crea una nueva sección*/
		function createSection(sectionIndex, questionIndex){
			$question = createQuestion(sectionIndex, questionIndex);
			$question.find('a.delete-question').remove()
			$sectionName = 'form[section]['+sectionIndex+'][section_index]';
			
			$li = $('<li/>').attr({'id':'section-' + sectionIndex,'class':'section', 'data-target': sectionIndex}).append(
					$('<input/>').attr({'type':'hidden', 'name':$sectionName, 'value':sectionIndex, 'class':'section-index'})).append(
					$('<p/>').addClass('title section').html('Sección ' + sectionIndex)).append(
					$('<ul/>').addClass('questions').append(
						$($question)));			
			return($li);
		}

		/*Crea una nueva pregunta*/		
		function createQuestion(sectionIndex, questionIndex){	
			$inputTypes	= <?php echo json_encode($inputTypes); ?>;
			$inputCategories = <?php echo json_encode($categories); ?>;
                        $inputQuestionType = <?php echo json_encode($questiontypes); ?>;
			$questionName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][text]';
			$inputTypeName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][input_type_id]';
			$requiredName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][required]';			
			$inputCategoryName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][category_id]';
                        $inputQuestionTypeName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][question_type_id]';
                       
                        $deleteIcon = '<svg class="icon" xmlns="http://www.w3.org/2000/svg"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete'; ?>"></use></svg>'
			
			$select = $('<select/>').attr({'class':'form-control type-question', 'name':$inputTypeName});
			for(var i in $inputTypes){				
				if(i == ''){
					$select.prepend($('<option/>').attr({'value':i}).html($inputTypes[i]));			  
				}else{
					$select.append($('<option/>').attr({'value':i}).html($inputTypes[i]));			  
				}
			}
			$select.val('');
			$categorySelect = $('<select/>').attr({'class':'form-control', 'name':$inputCategoryName});
			for(var i in $inputCategories){	
				if(i == ''){
					$categorySelect.prepend($('<option/>').attr({'value':i}).html($inputCategories[i]));			  
				}else{
					$categorySelect.append($('<option/>').attr({'value':i}).html($inputCategories[i]));			  
				}
				
			}
			$categorySelect.val('');
                        
                        $question_typeSelect = $('<select/>').attr({'class':'form-control', 'name':$inputQuestionTypeName});
			for(var i in $inputQuestionType){	
				if(i == ''){
					$question_typeSelect.prepend($('<option/>').attr({'value':i}).html($inputQuestionType[i]));			  
				}else{
					$question_typeSelect.append($('<option/>').attr({'value':i}).html($inputQuestionType[i]));			  
				}
				
			}
			$question_typeSelect.val('');

			$li = $('<li/>').attr({'class':'question', 'id':'question-'+questionIndex, 'data-target':questionIndex}).append(
					$('<div/>').addClass('mask')).append(
					$('<div/>').addClass('row').append(
						$('<div/>').addClass('col-md-12').append(
							$('<div/>').addClass('question-number').append(
								$('<p/>').html('Pregunta ')
								)
							)
						)
					).append(
					$('<div/>').addClass('row').append(
						$('<div/>').addClass('col-md-4').append(
							$('<div/>').addClass('form-group').append(
								$('<input/>').attr({'type':'text', 'name':$questionName, 'class':'form-control', 'placeholder':'Ingrese la pregunta'})
								)
							)
						).append(
						$('<div/>').addClass('col-md-3').append(
							$('<div/>').addClass('form-group').append(
								$($select)
								)
							)
						).append(
						$('<div/>').addClass('col-md-3').append(
							$('<div/>').addClass('form-group').append(
								$($categorySelect)
								)
							)
						).append(
						$('<div/>').addClass('col-md-2').append(
							$('<div/>').addClass('form-group').append(
								$($question_typeSelect)
								)
							)
						)
					).append(
					$('<ul/>').addClass('input-options')).append(
					$('<ul/>').addClass('input-restrictions')).append(
					$('<div/>').addClass('row question-options').append(
						$('<hr/>')).append(
						$('<div/>').addClass('col-md-6').append(
							$('<div/>').addClass('form-inline').append(
								$('<div/>').addClass('form-group').append(
									$('<input/>').attr({'type':'checkbox', 'name':$requiredName, 'value':'true'})).append(
									$('<label/>').attr('for',$requiredName).html('&nbsp;Obligatoria')						
								)
							)
						)).append(
						$('<div/>').addClass('col-md-6 buttons').append(
							$('<a/>').attr({'href':'', 'title':'Eliminar pregunta','class':'delete-question'}).html($deleteIcon)
							)
						)
					);
			return $li;
		}

		/*Crea los campos de opciones para el input personalizado que generan secciones.*/
		function createCustomFieldA(sectionIndex, questionIndex){
			//Restriccion de cantidad mínima
			$minName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][restriction][1]';						
			//Restricción de cantidad máxima
			$maxName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][restriction][2]';			
			$sectionName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][restriction][section]';
			
			$select = createSectionSelect(sectionIndex); 
			$select.attr('name', $sectionName);

			$li = $('<li/>').append(
						$('<div/>').addClass('row').append(
							$('<div/>').addClass('col-md-3').append(
								$('<div/>').addClass('form-group').append(									
									$('<input/>').attr({'type':'number', 'class':'form-control', 'name': $minName, 'placeholder':'Cantidad mínima'})
									)
								)
							).append(
							$('<div/>').addClass('col-md-3').append(
								$('<div/>').addClass('form-group').append(									
									$('<input/>').attr({'type':'number', 'class':'form-control', 'name': $maxName, 'placeholder':'Cantidad máxima'})
									)
								)
							).append(
							$('<div/>').addClass('col-md-6').append(
								$('<div/>').addClass('form-group').append(
									$select)
								)
							)
						);
			return $li;
		}

		/*Crea las restricciones de una imagen */
		function createRestrictionImageField(sectionIndex, questionIndex){
			//Restricción de cantidad
			$qtyName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][restriction][3]';			
			//Restricción de tamaño
			$sizeName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][restriction][4]';			
			$li = $('<li>').addClass('image-restriction').append(
						$('<div/>').addClass('row').append(
							$('<div/>').addClass('col-md-6').append(
								$('<div/>').addClass('form-group').append(
									
									$('<input/>').attr({'class':'form-control', 'type':'number', 'name':$qtyName, 'placeholder':'Ingrese cantidad máxima de imágenes'})
									)
								)
							).append(
							$('<div/>').addClass('col-md-6').append(
								$('<div/>').addClass('form-group').append(
									$('<div/>').addClass('input-group').append(										
										$('<input/>').attr({'class':'form-control', 'type':'number', 'name':$sizeName, 'placeholder':'Ingrese tamaño máximo total'})).append(
										$('<div/>').addClass('input-group-addon').html("MB"))
										)
								)
							)
						);					
			return $li;
		}

		/*Crea una nueva opción checkbox*/
		function createCheckboxField(sectionIndex, questionIndex, optionIndex){
			$optionName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][option]['+optionIndex+'][text]';
			$sectionName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][option]['+optionIndex+'][section]';
			
			$select = createSectionSelect(sectionIndex); 
			$select.attr('name', $sectionName);

			$deleteIcon = '<svg class="icon" xmlns="http://www.w3.org/2000/svg"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-close'; ?>"></use></svg>'

			$li = $('<li/>').addClass('option').append(
					$('<div/>').addClass('row').append(
						$('<div/>').addClass('col-md-6').append(
							$('<div/>').addClass('form-group').append(
								$('<div/>').addClass('input-group').append(
									$('<span/>').addClass('input-group-addon').append(
										$('<input/>').attr({'type':'checkbox','tabindex':'-1', 'readonly':'true', 'style':'cursor:auto', 'disabled':'true'}))).append(
									$('<input/>').attr({'type':'text','name':$optionName,'placeholder':'Ingrese opción', 'class':'form-control'})).append(
									$('<div/>').addClass('input-group-addon delete-button-content').append(
										$('<a/>').attr({'href':'', 'title':'Eliminar opción', 'class':'delete-option'}).html($deleteIcon)
										)
									)
								)
							)
						).append(
						$('<div/>').addClass('col-md-6').append(
							$('<div/>').addClass('form-group').append(
								$select)
							)
						)
					);
			return $li;
		}
		
		/*Crea una nueva opción radiobutton*/
		function createRadioButtonField(sectionIndex, questionIndex, optionIndex){
			$optionName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][option]['+optionIndex+'][text]';
			$sectionName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][option]['+optionIndex+'][section]';
			
			$select = createSectionSelect(sectionIndex); 
			$select.attr('name', $sectionName);

			$deleteIcon = '<svg class="icon" xmlns="http://www.w3.org/2000/svg"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-close'; ?>"></use></svg>'

			$li = $('<li/>').addClass('option').append(
					$('<div/>').addClass('row').append(
						$('<div/>').addClass('col-md-6').append(
							$('<div/>').addClass('form-group').append(
								$('<div/>').addClass('input-group').append(
									$('<span/>').addClass('input-group-addon').append(
										$('<input/>').attr({'type':'radio','tabindex':'-1', 'readonly':'true', 'style':'cursor:auto','disabled':'true'}))).append(
									$('<input/>').attr({'type':'text','name':$optionName,'placeholder':'Ingrese opción', 'class':'form-control'})).append(
									$('<div/>').addClass('input-group-addon delete-button-content').append(
										$('<a/>').attr({'href':'', 'title':'Eliminar opción', 'class':'delete-option'}).html($deleteIcon)
										)
									)
								)
							)
						).append(
						$('<div/>').addClass('col-md-6').append(
							$('<div/>').addClass('form-group').append(
								$select)
							)
						)
					);
			return $li;
		}

		/*Crea una nueva opción select*/
		function createSelectOptionField(sectionIndex, questionIndex, optionIndex){
			$optionName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][option]['+optionIndex+'][text]';
			$sectionName = 'form[section]['+sectionIndex+'][question]['+questionIndex+'][option]['+optionIndex+'][section]';			
			
			$select = createSectionSelect(sectionIndex); 
			$select.attr('name', $sectionName);

			$deleteIcon = '<svg class="icon" xmlns="http://www.w3.org/2000/svg"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-close'; ?>"></use></svg>'
			$downIcon = '<svg class="icon" xmlns="http://www.w3.org/2000/svg"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-down'; ?>"></use></svg>'

			$li = $('<li/>').addClass('option').append(
					$('<div/>').addClass('row').append(
						$('<div/>').addClass('col-md-6').append(
							$('<div/>').addClass('form-group').append(
								$('<div/>').addClass('input-group').append(
									$('<span/>').addClass('input-group-addon select').html($downIcon)).append(												
									$('<input/>').attr({'type':'text','name':$optionName,'placeholder':'Ingrese opción', 'class':'form-control'})).append(
									$('<div/>').addClass('input-group-addon delete-button-content').append(
										$('<a/>').attr({'href':'', 'title':'Eliminar opción', 'class':'delete-option'}).html($deleteIcon)
										)
									)
								)
							)
						).append(
						$('<div/>').addClass('col-md-6').append(
							$('<div/>').addClass('form-group').append(
								$select)
							)
						)
					);
			return $li;
		}

		/*Crea el botón que permite la adición de opciones */ 
 		function createAddMoreOptionButton(){
 			$li = $('<li/>').addClass('add-more-btn-content').append(
 				$('<a/>').attr({'class':'btn btn-default','href':'', 'role':'button'}).html("Agregar otra opción"));
 			return $li;
 		}
	});
</script>