<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<?php echo link_tag('assets/css/bootstrap.css'); ?>
	<?php echo link_tag('assets/css/general.css'); ?>
 	<script src="<?php echo site_url('assets/js/jquery-3.2.1.min.js') ?>"></script> 
	<script src="<?php echo site_url('assets/js/jquery-ui.js') ?>"></script>  
	<script src="<?php echo site_url('assets/js/bootstrap.min.js') ?>"></script>

	<title>Te recupero</title>
</head>
<body>
	<div class="line login" style="background-color: #1E3652 !important;"></div>
	<div class="container-fluid " >
		<div class="row">
			<div class="col-md-4 col-sm-2 col-xs-1"></div>
			<div class="col-md-4 col-sm-8 col-xs-10 form-content">
				<div class="row">
				<div class="col-md-8 col-md-offset-2">

                                       <img class="img-responsive" src="<?php echo site_url('assets/images/elementos-03.png') ?>"> 
				</div>
				</div>
				<h1>Te recupero</h1>
				<?php echo form_open("auth/login");?>
				<div class="form-group">
					<?php echo lang('login_identity_label', 'identity');?>
					<?php echo form_input($identity);?>
					<?php echo form_error('identity'); ?>
				</div>
				<div class="form-group">
					<?php echo lang('login_password_label', 'password');?>
					<?php echo form_input($password);?>
					<?php echo form_error('password'); ?>
				</div>
				<div class="checkbox">
					<label>
						<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> <?php echo lang('login_remember_label', 'remember');?>
					</label>
				</div>
				<div class="message-login"><?php echo $message; ?></div>
				<div class="form-buttons">
					<button type="submit" style="background-color: #1E3652 !important;" class="btn btn-default blue"><?php echo lang('login_submit_btn') ?></button>
				</div>			
				<?php echo form_close();?>				
			</div>

			<div class="col-md-4 col-sm-2 col-xs-1"></div>
		</div>		
	</div>
</body>
</html>

