<div id="formDiv" style="padding:0xp !important;margin:0px !important;">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/dataTables.css">

<div id="divBloqueo2" style="position: absolute; width: 98%; height: 100%;  background-color: rgba(255, 255, 255, 0.22);  z-index: 1; text-align: center; display: none;"></div>
<div id="divBloqueo" style="position: absolute; margin-bottom: 20px; position: absolute; width: 98%; height: 100%;  background-color: rgba(255, 255, 255, 0.22);  z-index: 1; text-align: center; display: none;"></div>
<div><br></div>

<br><br>
<center>

<div class="list-group" style="background-color:white; width:600px;">
    <div class="form-group" align="center" style="font-family: calibri;font-size: 26px !important; margin-top: 23px;color:black;">
        <strong>Ver Formulario</strong>
    </div>
   
    <div><br></div>
    
    <div id="frmingreso"> 

        <input type="hidden" class="form-control" id="formId" value="<?=$data["formId"]?>">

        <div class="form-group">

            <div class="col-sm-12 col-md-12 col-xs-12" align="left" style="height: 33px;">
                <input style="width: 100%;" type="text" name="search_val" id="search_val" placeholder="Ingresa al menos 4 caracteres para buscar un tecnico" value="">
            </div>
            <br><br>

            <div class="col-sm-12 col-md-12 col-xs-12" align="left" style="height: 33px;"> 
                <select style="width: 100%;" name="imei" id="imei">
                    <option value="0">Seleccionar Tecnico</option>
                </select>
            </div>
            <br><br>

            <div class="col-sm-12 col-md-12 col-xs-12" align="left" style="height: 33px;">
                <input style="width: 100%;" type="text" name="fono" id="fono" placeholder="Ingresa fono" value="">
            </div>
            <br><br>
            
            <div class="col-sm-12 col-md-12 col-xs-12" align="left" style="height: 33px;"> 
                <select style="width: 100%;" name="numpet" id="numpet">
                    <option value="0">Seleccionar Numero Peticion</option>
                </select>
            </div>
            <br><br>
        </div>

        <div class="modal-footer">
            <div class="col-sm-12 col-md-12 col-xs-12" align="center">
                <button type="button" id="mostrar" class="btn btn-success"  style="width: 180px;">Mostrar</button>   
            </div>       
        </div>

    </div>
</div> 
</center>
<script>
    $(document).ready(function() {
        $("#mostrar").click(function(e){
            var numpet  = $("#numpet").val();
            var imei    = $("#imei").val();
            var formId  = $("#formId").val();
            var tecName = $("#imei").find(":selected").text();
            
            var err = 0;
            if(numpet == 0) {
                swal("Alerta ToolBox!", "Por favor selecciona una numero peticion!", "error");
                err++;
            }
            if(imei == 0) {
                swal("Alerta ToolBox!", "Por favor selecciona un tecnico!", "error");
                err++;
            }
            if(err != 0) {
                return false;
            } else {
                $("#divBloqueo").css("display", "block");
                $('#divBloqueo').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');
                
                data = {
                    'formId'    : formId,
                    'imei'      : imei,
                    'numpet'    : numpet,
                    'empty'     : 1,
                    'tecname'   : tecName
                   };
                
                postUrl = base_url + "index.php/form_service/showForm";
                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function(result) {
                        console.log(result);
                        $("#divBloqueo").css("display", "none");
                        $("#divBloqueo").html("");
                        if(result != "NOK") {
                            $("#divBloqueo").css("display", "none");

                            $("#formDiv").html(result);
                        } else {
                            swal("Alerta ToolBox!", "No hay datos!", "error");
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
            }
        });
        
        $("#fono").change(function(e){
            var imei = $("#imei").val();
            var fono = $("#fono").val();
            
            if(fono != "") {
                
                data = {'imei' : imei, 'fono' : fono};

                $("#divBloqueo").css("display", "block");
                $('#divBloqueo').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

                postUrl = base_url + "index.php/form_service/loadSelectNumpet";
                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function(result) {
                        console.log(result);
                        $("#numpet").html(result);
                        $("#divBloqueo").css("display", "none");
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
            }
        });
        
        $("#search_val").change(function(e){

            var search_val = $("#search_val").val();
            
            if(search_val.length > 3) {
                data = {'search_val' : search_val};

                $("#divBloqueo").css("display", "block");
                $('#divBloqueo').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

                postUrl = base_url + "index.php/form_service/loadSelectTecnicos";

                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function(result) {
                        $("#imei").html(result);
                        $("#divBloqueo").css("display", "none");
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
            }
            
        });
    });
</script>
</div>