<script src="<?php echo base_url();?>assets/js/bandejaComercial/temCom.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style>
  .dropdown-submenu {
      position: relative;
  }

  .dropdown-submenu .dropdown-menu {
      top: 0;
      left: 100%;
      margin-top: -1px;
  }

  .selectPequeno{
    font-size: 14px;
    font-weight: bold;
    padding: 0px 7px;
    padding-right: 0px;

  }
   label{
        font-weight: 300;
  }

  input[type="text"]{
    font-size: 13px;
    width:50%;
  }

</style>

<div class="modal fade" id="modalQ_OnLine" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document" style="width: 600px;">
        <div id="divBloqueoPopup" style="position: absolute; width: 98%; height: 100%;  background-color: rgba(255, 255, 255, 0.22);  z-index: 1; text-align: center; display: none;"></div>
        <div class="modal-content">
            <div class="modal-header " align="center" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel"> Ver formulario </h4>
            </div>
            <div class="modal-body" style="height: 244px;">
                <div id="frmingreso"> 

                    <div id="divBloqueo" style=" margin-bottom: 20px; position: absolute; width: 98%; height: 100%;  background-color: rgba(255, 255, 255, 0.22);  z-index: 1; text-align: center; display: none;"></div>

                    <input type="hidden" class="form-control" id="formId" value="<?=$data["formId"]?>">

                    <div class="form-group">
                        
                        <div class="col-sm-12 col-md-12 col-xs-12" align="left" style="height: 33px;">
                            <input style="width: 100%;" type="text" name="search_val" id="search_val" placeholder="Ingresa al menos 4 caracteres para buscar un tecnico" value="">
                        </div>
                        <br><br>
                        
                        <div class="col-sm-12 col-md-12 col-xs-12" align="left" style="height: 33px;"> 
                            <select style="width: 100%;" name="imei" id="imei">
                                <option value="0">Seleccionar Tecnico</option>
                            </select>
                        </div>
                        <br><br>
                        
                        <div class="col-sm-12 col-md-12 col-xs-12" align="left" style="height: 33px;"> 
                            <select style="width: 100%;" name="numpet" id="numpet">
                                <option value="0">Seleccionar Numero Peticion</option>
                            </select>
                        </div>
                        <!--
                        <div class="col-sm-12 col-md-12 col-xs-12" align="left" style="height: 33px;">
                            <input style="width: 100%;" type="text" name="numpet" id="numpet" placeholder="Ingresa numero peticion" value="">
                        </div>
                        -->
                        <br><br>
                    </div>

                    <div class="modal-footer">
                        <div class="col-sm-12 col-md-6 col-xs-12" align="left">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 180px;">Salir</button>   
                        </div>

                        <div class="col-sm-12 col-md-6 col-xs-12" align="right">
                            <button type="button" id="mostrar" class="btn btn-success"  style="width: 180px;">Mostrar</button>   
                        </div>       
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#mostrar").click(function(e){
            var numpet  = $("#numpet").val();
            var imei    = $("#imei").val();
            var formId  = $("#formId").val();
            var tecName = $("#imei").find(":selected").text();
            
            var err = 0;
            if(numpet == "") {
                swal("Alerta ToolBox!", "Por favor ingresa una numero peticion!", "error");
                err++;
            }
            if(imei == 0) {
                swal("Alerta ToolBox!", "Por favor selecciona un tecnico!", "error");
                err++;
            }
            if(err != 0) {
                return false;
            } else {
                $("#divBloqueoPopup").css("display", "block");
                $('#divBloqueoPopup').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');
                
                data = {
                    'formId'    : formId,
                    'imei'      : imei,
                    'numpet'    : numpet,
                    'empty'     : 1,
                    'tecname'   : tecName
                   };
                
                postUrl = base_url + "index.php/form_service/showForm";
                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function(result) {
                        $("#divBloqueoPopup").css("display", "none");
                        $("#divBloqueoPopup").html("");
                        if(result != "NOK") {
                            $("#divBloqueo2").css("display", "none");
                            $("#modalQ_OnLine").modal("hide");
                            $("#formDiv").html(result);
                        } else {
                            swal("Alerta ToolBox!", "No hay datos!", "error");
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
            }
        });
        
        $("#imei").change(function(e){
            var imei = $("#imei").val();
            
            data = {'imei' : imei};

            $("#divBloqueoPopup").css("display", "block");
            $('#divBloqueoPopup').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

            postUrl = base_url + "index.php/form_service/loadSelectTecnicos";
            $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                dataType: "text",
                success: function(result) {
                    $("#imei").html(result);
                    $("#divBloqueoPopup").css("display", "none");
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });
            
        });
        
        $("#search_val").change(function(e){
            var search_val = $("#search_val").val();
            
            if(search_val.length > 3) {
                data = {'search_val' : search_val};

                $("#divBloqueoPopup").css("display", "block");
                $('#divBloqueoPopup').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

                postUrl = base_url + "index.php/form_service/loadSelectNumpet";
                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function(result) {
                        $("#numpet").html(result);
                        $("#divBloqueoPopup").css("display", "none");
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
            }
            
        });
    });
</script>