<div id="formDiv">
<style type="text/css">
    #frmingreso{
        width: 500px;
        margin: auto;
    }    
</style>

<div id="divBloqueo2" style="position: absolute; width: 98%; height: 100%;  background-color: rgba(255, 255, 255, 0.22);  z-index: 1; text-align: center; display: none;"></div>
<div id="frmingreso">
    <div><br></div>
    <div class="list-group" >
        <div class="form-group" align="center" style="font-family: calibri;font-size: 26px !important; margin-top: 23px;color:white;">
            <strong>Formularios dinamicos</strong>
        </div>        

        <div><br></div>

        <div class="form-group" style="font-family: calibri;font-size: 26px !important; margin-top: 23px;color:white;">
            <div class="col-sm-6 col-md-6 col-xs-6"  align="center" style="width: 50% !important">
                Formulario
            </div>
            <div class="col-sm-6 col-md-6 col-xs-6"  align="center" style="width: 50% !important">
                Ingresos
            </div>
        </div>
        
        <?php
        foreach($data["data"] AS $form) {
            $formId    = $form["ListValue"]["Property"][0]["Value"];
            $formTitle = $form["ListValue"]["Property"][1]["Value"];
            ?>
            <div class="form-group">
                <div class="col-sm-6 col-md-6 col-xs-6"  align="center" style="width: 50% !important">
                    <button type="button" class="btn btn-success btn-form-empty" id="<?=$formId?>" style="width: 100% !important"><?=$formTitle?></button>
                    <br><br>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-6"  align="center" style="width: 50% !important">
                    <button type="button" class="btn btn-success btn-form" id="<?=$formId?>" style="width: 100% !important"><?=$formTitle?></button>
                    <br><br>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".btn-form").click(function(e){
            var formId = $(this).attr("id");
            
            $("#divBloqueo2").css("display", "block");
            $('#divBloqueo2').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

            
            data = {
                    'formId' : formId
                   };

            postUrl = base_url + "index.php/form_service/initDynamicForms";
            $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                dataType: "text",
                success: function(result) {
                    $("#divBloqueo2").css("display", "none");
                    $("#formDiv").html(result);
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });
        });
        
        $(".btn-form-empty").click(function(e){
            var formId  = $(this).attr("id");
            
            $("#divBloqueo2").css("display", "block");
            $('#divBloqueo2').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

            data = {
                    'formId'    : formId
                   };
            postUrl = base_url + "index.php/form_service/showPopupTecnicoNumpet";
            $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                dataType: "text",
                success: function(result) {
                    if(result != "NOK") {
                        $("#modalPrincipal").html(result);
                        $("#modalQ_OnLine").modal("show");
                        $("#divBloqueo2").css("display", "none");
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });
        });
    });
</script>
</div>

<div id="modalPrincipalCSUb"></div>
<div id="modalPrincipalCS"></div>
<div id="modalPrincipal"></div>