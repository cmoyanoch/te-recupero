<div id="divShowForm">
<script src="<?php echo base_url();?>assets/js/casa/popup_ubicacion.js"></script>
<script src="<?php echo base_url();?>assets/js/dynamicForms/paginacion.js"></script>
<script src="<?php echo base_url();?>assets/js/dataTables.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/dataTables.css">
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJc-O9rkJ6lxxGGKHXzukR519_4JGoPDA&signed_in=true"></script>
<?php
$registrosTot   = $data['cc'];          //CANTIDAD TOTAL DE REGISTROS QUE CONTENGA LA CONSULTA NUMERO
$contreg        = $data['currPage'];    //TRAE COMO ARRAY EL NUMERO DE LA PAGINA EN EL QUE VAMOS
$contadorPag    = $contreg;             //RECIBE COMO NUMERO LA PAGINA EN EL QUE VAMOS
$pag            = $registrosTot / 10;
$pag            = floor($pag);
$resto          = $registrosTot % 10;

if($resto > 0 || $pag == 0) {
    $pag = $pag+1;
}
$palabra = ' Página de ';

if(empty($data['header'])) {
    $data['header'] = "";
}
if(empty($data['data'])) {
    $data['data'] = "";
    $arrCount = 0;
} else {
    $arrCount = count($data['data']);
}

// FORM FIXED
if(!(int)$data['reloaded']) {
    ?>
    <div class="col-sm-12 col-md-8 col-xs-12" style=" font-family: calibri;font-size: 30px !important;margin-top: 23px;" >
        <label  style="color: white;" id="nombreBandeja">Bandeja de Formularios <?=$data["formTitle"]?></label><br>
    </div>
    <div class="col-sm-0 col-md-6 col-xs-0" style="font-family: calibri;font-size: 20px !important;"> </div>  

    <div class="col-sm-6 col-md-4 col-xs-6">
        <input type="text" style="height: 35px;margin-top: 0%;" name="search_txt" title="Busqueda por N° Petición, Fono, Nombres" 
        id="search_txt" placeholder="Buscar" value="<?=$data["search_txt"]?>">
    </div>
    
    <div class="col-sm-3 col-md-1 col-xs-3">
        <button type="button" id="filterBtn" class="fa fa-search fa-2x" title="Buscar" style="height: 37px; width:74px;"></button>
    </div>

    <div class="col-sm-3 col-md-1 col-xs-3">
        <button type="button" id="cleanBtn" class="fa fa-eraser fa-2x" title="Limpiar" style="height: 37px; width:74px;"></button>
    </div>

    <!--
    <div class="col-sm-3 col-md-1 col-xs-3">
        <button type="button" id="backBtn" class="fa fa-mail-reply fa-2x" title="Volver" style="height: 37px; width:74px;"
        onclick="location.href='<?=site_url("form_service/initActDynamicForms")?>'"></button>
    </div>
    -->
    <?php
}

// FORM RELOAD
if(!(int)$data['reloaded']) {
    ?>
    <div class="col-sm-12 col-md-12 col-xs-12 " align="left">
    <?php
}
?>

    <div id="divBloqueo2" style="position: absolute; width: 98%; height: 100%;  background-color: rgba(255, 255, 255, 0.22);  z-index: 1; text-align: center; display: none;"></div>

    <input type="hidden" class="form-control" id="currpage" value="<?=$contreg?>">
    <input type="hidden" class="form-control" id="form_id" value="<?=$data["formId"]?>">
    <input type="hidden" class="form-control" id="status" value="<?=$data["status"]?>">
    <input type="hidden" class="form-control" id="valDistancia" value="400">
    
    <div id="tabCDI">

        <div align="left">
            <ul class="col-sm-12 col-md-12 col-xs-12 nav nav-tabs">
                <li class="<?php if($data["status"] == 2) echo "active";?>"><a href="#" id="pendiente" class="colorBlanco" onclick="changeStatus(2)"><strong>Pendiente</strong></a></li>
                <li class="<?php if($data["status"] == 3) echo "active";?>"><a href="#" id="aprobado" class="colorBlanco" onclick="changeStatus(3)"><strong>Aprobado</strong></a></li>
                <li class="<?php if($data["status"] == 4) echo "active";?>"><a href="#" id="rechazado" class="colorBlanco" onclick="changeStatus(4)"><strong>Rechazado</strong></a></li>
            </ul>
        </div>
  
        <table id="TablaFormEntries" class="table table-striped tablaA" style="position: relative; right: 3.5%;">
            <thead>
                <tr class="listrabajos">
                    <?php
                    foreach ($data['header'] AS $val) {  
                        $name = $val['Name'];
                        $showCol = true;
                        if(substr($name, strlen($name)-2, 2) == "_X") {
                            $showCol = false;
                        }
                        if($showCol){
                            ?>
                            <th><?=$name?></th>
                            <?php
                        }
                    } 
                    ?> 
                </tr>
            </thead>

            <tbody id="tbodyFormEntries">
                <?php
                $i = 0;
                foreach($data["data"] AS $row) {
                    $cols = $row["ListValue"]["Property"];
                    ?>
                    <tr class="trFormEntries" id="<?=$i?>">
                    <?php
                    $x = 0;
                    foreach($cols AS $col) {
                        $val = $col["Value"];
                        $nam = $col["Name"];

                        $showCol = true;
                        if(substr($nam, strlen($nam)-2, 2) == "_X" || $nam == "UBICACION" || $nam == "MAPA" || $nam == "PDF") {
                            $showCol = false;
                        }

                        if($val == "") {
                           $val = " "; 
                        }

                        if($showCol) {
                            ?>
                            <td onclick="showForm('<?=$i?>')" style="cursor:pointer"><?=$val?></td>
                            <?php
                        }
                        if($nam == "UBICACION") {
                            
                            $tmp = explode("|", $val);
                            $color = $tmp[0];
                            $distance = sprintf("%01.2f", $tmp[1]);
                            ?>
                            <td align="center"><button type="button" class="ubicacionTec" style="background-color: <?php echo $color?>; margin-top: 0px;
                            border-radius: 200px 200px 200px 200px; border: 0px solid #000000; width: 22px; height: 21px;
                            box-shadow: 4px 4px 5px rgba(44, 108, 162, 0.65);" id="<?=$distance?>" ></button></td>
                            <?php
                        }
                        if($nam == "MAPA") {
                            ?>
                            <td><button type="button" class="fa fa-file-image-o fa-2x VerMapaCasa" id="map_<?=$i?>"></button></td>
                            <?php
                        }
                        if($nam == "PDF") {
                            ?>
                            <td><button type="button" class="fa fa-file-pdf-o fa-2x VerPdf" id="pdf_<?=$i?>"></button></td>
                            <?php
                        }
                        if(!$showCol) {
                            ?>
                            <input type="hidden" name="<?=$x."_".$nam?>" id="<?=$x."_".$nam?>" value="<?=$val?>">
                            <?php
                        }
                        
                        
                        $x++;
                    }
                    ?>
                        <input type="hidden" name="<?=$i."_headid"?>" id="<?=$i."_headid"?>" value="<?=$cols[3]["Value"]?>">
                        <input type="hidden" name="<?=$i."_numpet"?>" id="<?=$i."_numpet"?>" value="<?=$cols[2]["Value"]?>">
                        <input type="hidden" name="<?=$i."_formid"?>" id="<?=$i."_formid"?>" value="<?=$cols[5]["Value"]?>">
                        <input type="hidden" name="<?=$i."_imei"?>" id="<?=$i."_imei"?>" value="<?=$cols[4]["Value"]?>">
                        <input type="hidden" name="<?=$i."_fecha"?>" id="<?=$i."_fecha"?>" value="<?=$cols[0]["Value"]?>">
                        <input type="hidden" name="<?=$i."_tecname"?>" id="<?=$i."_tecname"?>" value="<?=$cols[1]["Value"]?>">
                    </tr>
                    <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    <?php
    if(!(int)$data["filtered"]) {
        ?>
        <div class="col-sm-12 col-md-12 col-xs-12" style="text-align: right;">
            <?php if($contadorPag > 1) { ?>
                <i type="button" id="atras" class="btn btn-success fa fa-arrow-left fa-2x" aria-hidden="true"></i>
            <?php } ?>

            <span class="label" id="cp" style="font-size: 12px;color: white;"><?=$contadorPag?></span>
            <span class="label" id="pal" style="font-size: 12px;color: white;"><?=$palabra?></span>
            <span class="label" id="pg" style="font-size: 12px;color: white;"><?=$pag?></span>   

            <?php if($contadorPag < $pag) { ?>
                <i type="button" id="adelante" class="btn btn-success fa fa-arrow-right fa-2x" aria-hidden="true" ></i>
            <?php } ?>

            <div align="left"></div>
        </div>
        <?php
    }
    ?>
    <div id="reloadTimerDiv">
        <?php
        //$loadPopUp      = "bandejaPopup";

        $xdataStr       = "{'valorCheck' : 1, 'currPage' : '".$contreg."', 'filtered' : '".(int)$data["filtered"]."', 'search_txt' : '".$data["search_txt"]."', 'formId' : '".$data["formId"]."', 'stat' : '".$data["status"]."' }";
        $ifStr          = "";
        $funcStr        = "";
        $servCall       = base_url()."index.php/form_service/reloadBandeja";
        $loadPosTop     = "160px";
        $loadPosLeft    = "45%";
        createReloadTimer($xdataStr, $servCall, "tabCDI", (int)LOAD_TIME_QT, $funcStr, $ifStr, base_url(), $loadPosTop, $loadPosLeft);
        ?>
    </div>
    <script>
        function showForm(idx) {
            var headId  = $("#"+idx+"_headid").val();
            var formId  = $("#"+idx+"_formid").val();
            var imei    = $("#"+idx+"_imei").val();
            var fecha   = $("#"+idx+"_fecha").val();
            var numpet  = $("#"+idx+"_numpet").val();
            var tecname = $("#"+idx+"_tecname").val();

            clearTimeout(timer);
            $("#divBloqueo2").css("display", "block");
            $('#divBloqueo2').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

            <?php
            if((int)$data["formId"] == 12)
                $compare = 0;
            else
                $compare = 1;
            ?>

            data = {
                    'headId'    : headId,
                    'formId'    : formId,
                    'imei'      : imei,
                    'numpet'    : numpet,
                    'tecname'   : tecname,
                    'fecha'     : fecha,
                    'empty'     : 0,
                    'compare'   : <?=$compare?>
                   };

            postUrl = base_url + "index.php/form_service/showForm";
            $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                dataType: "text",
                success: function(result) {
                    if(result != "NOK") {
                        $("#stopTime").val(0);
                        $("#divBloqueo2").css("display", "none");
                        $("#divBloqueo2").html("");
                        $("#divShowForm").html(result);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });
        }
        
        function changeStatus(stat) {
            var formId  = $("#form_id").val();
            var statStr = $(this).attr("id");

            clearTimeout(timer);
            $("#stopTime").val(1);
            $("#divBloqueo2").css("display", "block");
            $('#divBloqueo2').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

            data = {
                    'stat'      : stat,
                    'filtered'  : 0,
                    'formId'    : formId
                   };

            postUrl = base_url + "index.php/form_service/filterBandeja";
            $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                dataType: "text",
                success: function(result) {
                    $("#stopTime").val(0);
                    $("#divBloqueo2").css("display", "none");
                    $("#tabCDI").html(result);
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });
        }
        $(document).ready(function() {
            $("#filterBtn").click(function(e){
                var search_txt = $("#search_txt").val();
                var formId     = $("#form_id").val();
                var stat       = $("#status").val();

                clearTimeout(timer);
                $("#stopTime").val(1);
                $("#divBloqueo2").css("display", "block");
                $('#divBloqueo2').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

                data = {
                        'stat'          : stat,
                        'search_txt'    : search_txt,
                        'filtered'      : 1,
                        'formId'        : formId
                       };

                postUrl = base_url + "index.php/form_service/filterBandeja";
                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function(result) {
                        $("#stopTime").val(0);
                        $("#divBloqueo2").css("display", "none");
                        $("#divBloqueo2").html("");
                        $("#tabCDI").html(result);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
            });

            $("#cleanBtn").click(function(e){
                $("#search_txt").val("");
                var formId = $("#form_id").val();
                var stat   = $("#status").val();

                clearTimeout(timer);
                $("#stopTime").val(1);
                $("#divBloqueo2").css("display", "block");
                $('#divBloqueo2').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

                data = {
                        'stat'      : stat,
                        'filtered'  : 0,
                        'formId'    : formId
                       };

                postUrl = base_url + "index.php/form_service/filterBandeja";
                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function(result) {
                        $("#stopTime").val(0);
                        $("#divBloqueo2").css("display", "none");
                        $("#tabCDI").html(result);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
            });
            
            $(".VerMapaCasa").click(function(e){   
                clearTimeout(timer);
                $("#stopTime").val(1);
                $("#divBloqueo2").css("display", "block");

                var id = $(this).attr("id");
                id = id.split("_");
                id = id[1];
                
                var npet = $("#"+id+"_numpet").val();
                var headId = $("#"+id+"_headid").val();

                data = {
                        'headId' : headId,
                        'npet' : npet
                        };

                postUrl = base_url + "index.php/form_service/popUpFormMapa";
                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function(result) { 
                         $("#modalPrincipal").html(result);
                         $("#modalcasacerrada").modal("show");
                         $("#divBloqueo2").css("display", "none");
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
            });
            
            $(".VerPdf").click(function(e){

                var id = e.currentTarget.attributes.id.value;
                var idx = id.split("_");
                idx = idx[1];
                var headId  = $("#"+idx+"_headid").val();
                var formId  = $("#"+idx+"_formid").val();
                var imei    = $("#"+idx+"_imei").val();
                var fecha   = $("#"+idx+"_fecha").val();
                var numpet  = $("#"+idx+"_numpet").val();
                var tecname = $("#"+idx+"_tecname").val();

                clearTimeout(timer);
                $("#stopTime").val(1);
                $("#divBloqueo2").css("display", "block");
                $('#divBloqueo2').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

                data = {'tmp' : 1};

                postUrl = base_url + "index.php/form_service/showPDF?headId="+headId;
                $.ajax({
                  type: "POST",
                  url: postUrl,
                  data: data,
                  dataType: "text",
                  success: function(result) {
                        $("#stopTime").val(0);
                        $("#divBloqueo2").css("display", "none");
                        var url = postUrl;
                        var win = window.open(url, '_blank');
                        win.focus();
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
            });
        });
    </script>    
    </div>

    <div id="modalPrincipalCSUb"></div>
    <div id="modalPrincipalCS"></div>
    <div id="modalPrincipal"></div>
<?php
// FORM RELOAD
if(!(int)$data['reloaded']) {
    ?>
    </div>
    <?php
}
?>
</div>