<!--
<script src="<?php echo base_url();?>assets/js/casa/casacerrada.js"></script>
<script src="<?php echo base_url();?>assets/js/bandejaNoCableado/paginacion.js"></script>
<script src="<?php echo base_url();?>assets/js/casa/tipo_visita.js"></script>
<script src="<?php echo base_url();?>assets/js/casa/popup_ubicacion.js"></script>
<script src="<?php echo base_url();?>assets/js/dataTables.js"></script>
-->
<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/dataTables.css"> -->

<style>

</style>

<?php
$form    = $data["formConfig"];
$formPre = $data["formConfigPre"];

if((int)$data["empty"]) {
   // $backUrl = site_url("form_service/formSearch")."?formId=".$data["formId"];
} else {
   // $backUrl = site_url("form_service/initDynamicForms")."?formId=".$data["formId"];
}
?>
<!-- <div style="background-color:#0071BC;"> -->
<div style="">
<!--<div id="divBloqueo2" style="position: absolute; width: 98%; height: 100%;  background-color: rgba(255, 255, 255, 0.22);  z-index: 1; text-align: center; display: none;"></div>-->
<div><br></div>
<div class="col-sm-12 col-md-12 col-xs-12" align="right">
<!--     <button type="button" class="fa fa-mail-reply fa-2x" title="Volver" style="height: 37px; width:74px;" onclick="location.href='<?=$backUrl?>'"></button> -->
    <!--<button type="button" id="backBtn" class="fa fa-mail-reply fa-2x" title="Volver" style="height: 37px; width:74px;"></button>-->
    <input type="hidden" class="form-control" id="form_id" value="<?=$data["formId"]?>">
    <input type="hidden" class="form-control" id="imei" value="<?=$data["imei"]?>">
    <input type="hidden" class="form-control" id="numpet" value="<?=$data["numpet"]?>">
</div>

<center>
<div class="list-group" id="divBloqueo2" style="background-color:white; width:<?=$data["width"]?>;">
    <div class="form-group" align="center" style="font-family: calibri;font-size: 26px !important; margin-top: 23px;color:black;">
        <strong><?=$form["title"]?></strong>
    </div>
    
    <div class="form-group" align="center" style="font-family: calibri;font-size: 26px !important;color:black;">
        <strong>Usuario:</strong> <?=$data["tecName"]?><br> <strong>Fecha Peticion:</strong> <?=$data["fecha"]?>
    </div>

  
    
    <?php
    $dataStr = "";
    if(!(int)$data["compare"]) {
        foreach($form["section"] AS $section) {

            foreach($section["question"] AS $question) {
                ?>
                <div class="form-group" id="question_<?=$question["question_id"]?>">

                    <?php
                    if(!(int)$data["empty"]) {
                        if(isset($question["value"])) {
                            foreach($question["value"] AS $value) {
                                ?>
                                <div class="col-sm-4 col-md-4 col-xs-4" align="left" style="height: 33px;">
                                    <p><strong><?=$question["text"]?>:</strong></p>
                                </div>
                                <div class="col-sm-8 col-md-8 col-xs-8" align="left" style="height: 33px;">
                                    <?php
                                    if($question["inputType"] == "Imagen") {
                                        ?>
                                        <button type="button" class="fa fa-file-image-o fa-2x VerImagen" style="margin-top: 0px;border-style: none;background: white;" id="<?=$value?>" ></button></td>
                                        <?php
                                    } else {
                                        echo $value;
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="col-sm-4 col-md-4 col-xs-4" align="left" style="height: 33px;">
                                <p><strong><?=$question["text"]?>:</strong></p>
                            </div>
                            <div class="col-sm-8 col-md-8 col-xs-8" align="left" style="height: 33px;">
                                No ingresado
                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <div class="col-sm-4 col-md-4 col-xs-4" align="left" style="height: 33px;">
                            <p><strong><?=$question["text"]?>:</strong></p>
                        </div>
                        <div class="col-sm-8 col-md-8 col-xs-8" align="left" style="height: 33px;">
                            <?php
                            $rdnl  = ""; 
                            $value = "XXX";
                            if($question["pretext"] != "" && $question["pretext"] != "null") {
                                $value = $question["pretext"];
                            }
                            $rdnl  = "readonly";
                            if((int)$question["category_id"] == 1) {
                                $rdnl  = "";
//                                $value = "Ingreso";
                                $value = "";
                            }
                            if($value == "XXX" && (int)$question["category_id"] != 1)
                                $value = "Sin informacion";
                            
                            if($question["input_type_id"] == 1) {
                                ?>
                                <input type="text" style="width:100%" name="questionData_<?=$question["question_id"]?>" id="questionData_<?=$question["question_id"]?>" value="<?=$value?>" <?=$rdnl?>>
                                <?php
                                $dataStr .= "'questionData_".$question["question_id"]."': $('#questionData_".$question["question_id"]."').val(),";
                            } else if($question["input_type_id"] == 2) {
                                ?>
                                <textarea style="width:100%; height:33px;" name="questionData_<?=$question["question_id"]?>" id="questionData_<?=$question["question_id"]?>" <?=$rdnl?>><?=$value?></textarea>
                                <?php
                                $dataStr .= "'questionData_".$question["question_id"]."': $('#questionData_".$question["question_id"]."').html(),";
                            } else if($question["input_type_id"] == 3) {
                                ?>
                                <input type="text" style="width:100%" name="questionData_<?=$question["question_id"]?>" id="questionData_<?=$question["question_id"]?>" value="<?=$value?>" onblur='validateNumber("<?=$question["question_id"]?>")' <?=$rdnl?>>
                                <?php
                                $dataStr .= "'questionData_".$question["question_id"]."': $('#questionData_".$question["question_id"]."').val(),";
                            } else if($question["input_type_id"] == 4) {
                                ?>
                                <input type="text" style="width:100%" name="questionData_<?=$question["question_id"]?>" id="questionData_<?=$question["question_id"]?>" value="<?=$value?>" onblur='validateNumber("<?=$question["question_id"]?>")' <?=$rdnl?>>
                                <?php
                                $dataStr .= "'questionData_".$question["question_id"]."': $('#questionData_".$question["question_id"]."').val(),";
                            } else if($question["input_type_id"] == 5) {
                                ?>
                                <select style="width:100%">
                                    <option>Ingreso</option>
                                    <?php
                                    if(isset($question["option"])) {
                                        foreach($question["option"] AS $option) {
                                            if($option["text"] != "") {
                                                ?>
                                                <option><?=$option["text"]?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                                <?php
                            } else if($question["input_type_id"] == 11) {
                                echo $value." (Imagen)";
                            } else {
                                echo $value;
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>

                </div>
                <br>
                <?php 
            }
        }
        if((int)$data["empty"] && $data["formId"] == 9) {
            ?>
            <div class="col-sm-12 col-md-12 col-xs-12" align="left" style="height: 33px;"></div>
            <div class="modal-footer">
                <div class="col-sm-12 col-md-12 col-xs-12" align="center">
                    
                    <button type="button" id="numgen" class="btn btn-success" style="width: 180px;">Generar Folio</button>   
                </div>       
            </div>
            <?php
        }
    } else {
        ?>
        <div class="col-sm-6 col-md-6 col-xs-6" align="center">
            <?php
            foreach($formPre["section"] AS $sectionPre) {
                foreach($sectionPre["question"] AS $questionPre) {
                    ?>
                    <div class="form-group" id="question_<?=$questionPre["question_id"]?>">
                        <div class="col-sm-4 col-md-4 col-xs-4" align="left" style="height: 33px;">
                            <p><strong><?=$questionPre["text"]?>:</strong></p>
                        </div>
                        <div class="col-sm-8 col-md-8 col-xs-8" align="left" style="height: 33px;">
                            <?php
                            $value = "XXX";
                            if($questionPre["pretext"] != "" && $questionPre["pretext"] != "null")
                                $value = $questionPre["pretext"];
                            if((int)$questionPre["category_id"] == 1)
                                $value = "Ingreso";
                            if($value == "XXX" && (int)$questionPre["category_id"] != 1)
                                $value = "Dato no encontrado";
                            echo $value;
                            ?>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-sm-6 col-md-6 col-xs-6" align="center">
            <?php
            foreach($form["section"] AS $section) {
                foreach($section["question"] AS $question) {
                    ?>
                    <div class="form-group" id="question_<?=$question["question_id"]?>">
                        <?php
                        if(isset($question["value"])) {
                            foreach($question["value"] AS $value) {
                                ?>
                                <div class="col-sm-4 col-md-4 col-xs-4" align="left" style="height: 33px;">
                                    <p><strong><?=$question["text"]?>:</strong></p>
                                </div>
                                <div class="col-sm-8 col-md-8 col-xs-8" align="left" style="height: 33px;">
                                    <?php
                                    if($question["inputType"] == "Imagen") {
                                        ?>
                                        <button type="button" class="fa fa-file-image-o fa-2x VerImagen" style="margin-top: 0px;border-style: none;background: white;" id="<?=$value?>" ></button></td>
                                        <?php
                                    } else {
                                        echo $value;
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="col-sm-4 col-md-4 col-xs-4" align="left" style="height: 33px;">
                                <p><strong><?=$question["text"]?>:</strong></p>
                            </div>
                            <div class="col-sm-8 col-md-8 col-xs-8" align="left" style="height: 33px;">
                                No ingresado
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <br>&nbsp;<br>
        <br>&nbsp;<br>
        <div class="form-group">
            <div class="col-sm-12 col-md-12 col-xs-12" align="center">
                <br>&nbsp;<br>
            </div>
        </div>
        <div class="modal-footer">
            <?php
            if((int)$data["status"] == 2) {
                ?>
                <br>&nbsp;<br>
                <div class="col-sm-6 col-md-6 col-xs-6" align="center">
                    <button type="button" id="deny" class="btn btn-danger" style="width: 180px;">Rechazar</button>   
                </div> 
                <div class="col-sm-6 col-md-6 col-xs-6" align="center">
                    <button type="button" id="accept" class="btn btn-success" style="width: 180px;">Aprobar</button>   
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }
        
    ?>
</div> 
</center>
<script>
    function validateKeyNumber(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || (charCode > 57 && charCode != 173)))
            return false;

        return true;
    }
    
    function validateKeyFloat(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || (charCode > 57 && charCode != 190 && charCode != 110 && charCode != 173)))
            return false;

        return true;
    }
    
    function validateNumber(qid) {
        var val = $("#questionData_"+qid).val();
        if(!isFinite(val)) {
            $("#questionData_"+qid).val('');
        }
    }

    function setFormStatus(stat) {
        $("#divBloqueo2").css("display", "block");
        $('#divBloqueo2').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

        var obs = $("#observacion").html();
        
        data = {
                'stat'    : stat,
                'headId'  : '<?=$data["headId"]?>',
                'obs'     : obs
               };

        postUrl = base_url + "index.php/form_service/setDynamicFormStatus";
        $.ajax({
            type: "POST",
            url: postUrl,
            data: data,
            dataType: "text",
            success: function(result) {                
                $("#divBloqueo2").css("display", "none");
                if(result != "NOK") {                     
                    //swal("Alerta ToolBox!", "Guardado con exito!", "success"); 
                    //$("#backBtn").trigger("click");  
                    swal({  title: "Alerta ToolBox!",                                              
                        text: "Datos guardado con exito!",                                               
                        type: "success",                                               
                        showCancelButton: false,                                               
                        confirmButtonText: "OK",                                               
                        closeOnConfirm: false },                                             
                        function(isConfirm) {                                                   
                            if(isConfirm) {                                                        
                               //location.reload();     
                               location.href='<?=$backUrl?>';
                            } else {                                                        
                               location.reload();                                                   
                            }                                                            
                        }                                        
                    );
                } else {
                    swal("Alerta ToolBox!", "Tuve problemas en actualizar los datos!", "error");
                }
                //$("#divBloqueo2").css("display", "none");                
                
            },
            error: function(xhr, ajaxOptions, thrownError) {}
        });
    }

    $(document).ready(function() {

        $(".VerImagen").click(function(e){  
            $("#divBloqueo2").css("display", "block");
            $("#img").attr("src", <?=URL_IMAGES_DF?> + $(this).attr("id"));
            $("#modalQ_OnLine").modal("show");
            $("#divBloqueo2").css("display", "none");
        });

//        $("#backBtn").click(function(e){  
//            var formId = $("#form_id").val();
//
//            $("#divBloqueo2").css("display", "block");
//            $('#divBloqueo2').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');
//
//            data = {
//                    'formId'    : formId,
//                    'empty'     : '<?=(int)$data["empty"]?>'
//                   };
//
//            <?php
            if(!(int)$data["empty"]) {
                ?>//
//                postUrl = base_url + "index.php/form_service/initDynamicForms";
//                <?php
            } else {
                ?>//
//                postUrl = base_url + "index.php/form_service/initActDynamicForms";
//                <?php
            }
            ?>//
//            $.ajax({
//                type: "POST",
//                url: postUrl,
//                data: data,
//                dataType: "text",
//                success: function(result) {
//                    $("#divBloqueo2").css("display", "none");
//                    $("#formDiv").html(result);
//                },
//                error: function(xhr, ajaxOptions, thrownError) {}
//            });       
        //});

        $("#numgen").click(function(e){  
            var formId = $("#form_id").val();
            var imei   = $("#imei").val();
            var numpet = $("#numpet").val();

            $("#divBloqueo2").css("display", "block");
            $('#divBloqueo2').html('<p style="text-align: -webkit-center;position:relative;z-index:9999"><img src="'+base_url_gif+'" /></p>');

            data = {
                    <?=$dataStr?>
                    'formId'    : formId,
                    'imei'      : imei,
                    'numpet'    : numpet
                   };

            postUrl = base_url + "index.php/form_service/generateAnswerHeader";
            $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                dataType: "text",
                success: function(result) {
                    console.log(result);
                    $("#divBloqueo2").css("display", "none");
                    if(result != "NOK") {
                        //swal("Alerta ToolBox!", "Se ha generado el folio: "+result, "success");
                        swal({  title: "Alerta ToolBox!",                                              
                        text: "Datos guardado con exito!",                                               
                        type: "success",                                               
                        showCancelButton: false,                                               
                        confirmButtonText: "OK",                                               
                        closeOnConfirm: false },                                             
                        function(isConfirm) {                                                   
                            if(isConfirm) {                                                        
                                //location.reload();     
                                location.href='<?=$backUrl?>';
                            } else {                                                        
                                location.reload();                                                   
                                }                                                            
                            }                                        
                        );
                    } else {
                        swal("Alerta ToolBox!", "Tuve problemas en generar el folio.", "error");
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {}
            });
        });

        $("#deny").click(function(e){  
            setFormStatus(4);
        });

        $("#accept").click(function(e){  
            setFormStatus(3);
        });
    });
</script>
<div id="modalPrincipalCSUb"></div>
<div id="modalPrincipalCS"></div>
<!--
<div id="modalPrincipal">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <div class="modal fade" id="modalQ_OnLine" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document" style="width: 550px;">
            <div class="modal-content">
                <div class="modal-header " align="center" >
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Imagen</h4>
                </div>
                <div class="modal-body" style="height: 730px;">
                    <div id="frmingreso">
                        <img id="img" src="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->
</div>