<?php 


	$hasData = false;
	if(isset($sla)){
		$hasData = true;
	}

?>
<div class="list">
<div class="container-fluid menu-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php echo $title ?></h1>			
			</div>
		</div>			
	</div>		
</div>
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('sla/edit/'.$sla['id'], array('id'=>'edit-form'));
		echo form_hidden('sla[id]', $sla['id']);
	}else{
		echo form_open('sla/create'); 	
	}	
	?>
	<!-- <div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-default blue">Carga Masiva</button>	
			</div>	
		</div>
	</div> -->
	<div class="row">

		<div class="col-md-3">			
			<div class="form-group">
				<label for="sla[empresa]">Empresa:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'sla[empresa]', 'class'=> 'form-control', 'value'=> set_value('sla[empresa]', isset($sla['empresa'])? $sla['empresa'] :''))); ?>
				<?php echo form_error('sla[empresa]'); ?>
			</div>
		</div>
		<div class="col-md-3">			
			<div class="form-group">
				<label for="sla[contrato]">Contrato:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'sla[contrato]', 'class'=> 'form-control', 'value'=> set_value('sla[contrato]', isset($sla['contrato'])? $sla['contrato'] :''))); ?>
				<?php echo form_error('sla[contrato]'); ?>
			</div>
		</div>
		<div class="col-md-3">			
			<div class="form-group">
				<label for="sla[name]">Nombre Sla:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'sla[name]', 'class'=> 'form-control', 'value'=> set_value('sla[name]', isset($sla['name'])? $sla['name'] :''))); ?>
				<?php echo form_error('sla[name]'); ?>
			</div>
		</div>
		<div class="col-md-3">			
			<div class="form-group">
				<label for="sla[description]">Cantidad de Horas</label>

				<?php echo form_input(array('type'=>'text', 'name'=>'sla[description]', 'class'=> 'form-control', 'value'=> set_value('sla[description]', isset($sla['description'])? $sla['description'] :''))); ?>
				<?php echo form_error('sla[description]'); ?>

			</div>
		</div>	
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-buttons">
				<button type="submit" disabled="true" class="btn btn-default blue">Carga Masiva</button>	
				<button type="submit" class="btn btn-default blue">Guardar</button>	
			</div>	
		</div>
	</div>


	
