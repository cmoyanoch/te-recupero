<script>
	var stados  = <?php print_r(json_encode($estados[0])); ?>;
	var sinAsig = <?php print_r(json_encode($sinAsig[0])); ?>;
	var sla     = <?php print_r(json_encode($sla)); ?>;

</script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="https://use.fontawesome.com/32535cb184.js"></script>
<style>
	
	.highcharts-credits{
		display: none !important;
	}
</style>

<div class="list">
	<ol class="breadcrumb" style="margin-top: 0px;">
		<li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
		<li><a href="#"><strong>Monitoreo</strong></a></li>
		<li class="active"><strong> Dashboard </strong></li>
	</ol>
</div>
<div id="tabla-nav" class="container">
	<div class="row bg-info" style='padding-right: 0px;padding-left: 0px;'>
		<?php echo form_open(site_url('monitoring/index'), array('method'=>'get')); ?>
		<div class="col-md-12 bg-primary">
			<div class="col-md-6">
				<h5>Filtro de busqueda</h5>
			</div>
			<div class="col-md-6" style="text-align: right;padding-top: 5px;cursor: pointer;" onclick="openFullScreen()">
				<i id="start-nav" class="fa fa-play-circle fa-lg" ></i>
				<i id="stop-nav"  class="fa fa-stop-circle fa-lg" style="display: none;"></i>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-3 form-group">
				<label for="periodo">Periodos</label>
				<select name="periodo" id="periodo" class="form-control">
					<option value="1"   <?php if( $_REQUEST['periodo'] == "1"  ) echo "selected"; ?>>Mes Actual</option>
					<option value="30"  <?php if( $_REQUEST['periodo'] == "30" ) echo "selected"; ?>>30 días</option>
					<option value="60"  <?php if( $_REQUEST['periodo'] == "60" ) echo "selected"; ?>>60 días</option>
					<option value="90"  <?php if( $_REQUEST['periodo'] == "90" ) echo "selected"; ?>>90 días</option>
					<option value="180" <?php if( $_REQUEST['periodo'] == "180") echo "selected"; ?>>180 días</option>
					<option value="365" <?php if( $_REQUEST['periodo'] == "365") echo "selected"; ?>>365 días</option>
				</select>
			</div>

         <div class="col-md-3 form-group">
             <label for="cliente">Cliente</label>
             <select name="cliente" id="cliente" class="form-control">
                 <option value="0">Seleccione Cliente</option>
                 <?php foreach ($clients as $client) { ?>
                     <option value="<?php echo  $client['id_empresa']; ?>"
                         <?php if($client['id_empresa'] == $_REQUEST['cliente']) echo "selected"; ?> >
                         <?php echo $client['nombre_empresa'] ; ?>
                     </option>
                 <?php } ?>
             </select>
         </div>
			<?/*
			<div class="col-md-3 form-group" >
				<label for="zona">Zonas</label>
				<select id="zona" name="zona" class="form-control">
					<option value="" selected> Selecciones una Zona </option>
					<?php foreach ($filterZones as $zone) { ?>
						<option value="<?php echo  $zone['id_zona']; ?>"
							<?php if($zone['id_zona'] == $_REQUEST['zona']) echo "selected"; ?> >
							<?php echo $zone['name'] ; ?>
						</option>
					<?php } ?>
				</select>
			</div>
			*/?>
			<div class="col-md-2 form-group">
				<button id="buscar" name="buscar" style="margin-top: 22px;" class="btn btn-primary">Buscar </button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
	<div class="col-md-12"><br></div>
	<div class="row" style='padding-right: 0px;padding-left: 0px;'>
		<table class="table table-bordered table-responsive">
			<thead>
			<tr>
				<th class="active"  style="width: 170px;background-color: #D3D3D3;">Clientes</th>
				<th class="active"  style="background-color: #D3D3D3;">Total</th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;" >Total Cerrado</th>
				<th class="success" style="background-color: #6296E9; color:#FFF;">Cerrado dentro de SLA</th>
				<th class="danger"  style="background-color: #6296E9; color:#FFF;">Cerrado fuera de SLA</th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"> % Cerrado dentro de SLA</th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"> % Cerrado fuera de SLA</th>
				<th class="Warning" style="background-color: #FCA600; color:#FFF;"> Total Pend</th>
				<th class="info"    style="background-color: #D3D3D3;"> Inc</th>
				<th class="info"    style="background-color: #D3D3D3;"> Req</th>
				<th class="success" style="background-color: #008101; color:#FFF;"> Dentro SLA</th>
				<th class="danger"  style="background-color: #B1221F; color:#FFF;"> Fuera SLA</th>
				<th class="info"    style="background-color: #FFFF01;"> Hoy </th>
				<th class="info"    style="background-color: #FFFF01;"> 1 a 3 días </th>
				<th class="info"    style="background-color: #FFFF01;"> 4 a 7 días </th>
				<th class="info"    style="background-color: #FFFF01;"> 2 Sem </th>
				<th class="info"    style="background-color: #FFFF01;"> 3 Sem </th>
			</tr>
			</thead>
			<tbody>
			<?php
				$totalMensual = 0;
				$totalCerrado = 0;
				$totalPend = 0;
				$inc = 0;
				$hoy = 0;
				$unoatres = 0;
				$cuatroasiete = 0;
				$dossem = 0;
				$tressem = 0;
				
				$totCerradoDentroSla   = 0;
				$totCerradoFueraSla    = 0;
				$totDentroSla   = 0;
				$totFueraSla    = 0;
				
				$cc = 0;
				foreach ($response['cust'] as $row) {
					
					?>
					<tr>
						<?php if($row['result']!=null) { ?>
							<?php if($paramEmp!=null) { ?>
								<th class="active"><a href="<?php echo base_url()."/monitoring/groupDashInc/".$row['result']['id_cust']."/".$paramEmp; ?>"><?php echo $row['name']; ?></a></th>
							<?php } else {
								$_NAV .= $row['result']['id_cust'].",";
								?>
								<th class="active" onclick="firstNivel('<?=$row['result']['id_cust']?>')"><a href="#"><?php echo $row['name']; ?></a></th>
							<?php } ?>
						<?php } else { ?>
							<th class="active" ><?php echo $row['name']; ?></th>
						<?php } ?>
						<th class="active"><?php echo $row['result']['Total Mensual']; ?></th>
						<th class="info" style="background-color: #6296E9; color:#FFF;" ><?php echo $row['result']['Total Cerrado']; ?></th>
						<th class="success" ><?php echo $row['result']['Cerrado Dentro SLA']; ?></th>
						<th class="danger" ><?php echo $row['result']['Cerrado Fuera SLA']; ?></th>
						<th class="info" ><?php
								if($row['result']['Total Mensual']!=0) {
									$cc++;
									echo  round($row['result']['Cerrado Dentro SLA']*100 / $row['result']['Total Cerrado'],2)." %";
								}?></th>
						<th class="info" ><?php
								if($row['result']['Total Mensual']!=0) {
									echo round($row['result']['Cerrado Fuera SLA']*100 / $row['result']['Total Cerrado'],2)." %";
								} ?></th>
						<th class="Warning" style="background-color: #FCA600; color:#FFF;"><?php echo $row['result']['Total Pendiente']; ?></th>
						<th class="info" ><?php echo $row['result']['Inc']; ?></th>
						<th class="info" ><?php echo $row['result']['Req']; ?></th>
						<th class="success" style="background-color: #008101; color:#FFF;"><?php echo $row['result']['Dentro SLA']; ?></th>
						<th class="danger"  style="background-color: #B1221F; color:#FFF;"><?php echo $row['result']['Fuera SLA']; ?></th>
						<th class="info" ><?php echo $row['result']['Hoy']; ?></th>
						<th class="info" ><?php echo $row['result']['1 a 3 dias']; ?></th>
						<th class="info" ><?php echo $row['result']['4 a 7 dias']; ?></th>
						<th class="info" ><?php echo $row['result']['2 Sem']; ?></th>
						<th class="info" ><?php echo $row['result']['3 Sem']; ?></th>
					</tr>
					<?php
					$totalMensual =  $totalMensual + $row['result']['Total Mensual'];
					$totalCerrado =  $totalCerrado + $row['result']['Total Cerrado'];
					$totalPend =  $totalPend + $row['result']['Total Pendiente'];
					$inc =  $inc + $row['result']['Inc'];
					$hoy = $hoy + $row['result']['Hoy'];
					$unoatres = $unoatres + $row['result']['1 a 3 dias'];
					$cuatroasiete = $cuatroasiete + $row['result']['4 a 7 dias'];
					$dossem =  $dossem +  $row['result']['2 Sem'];
					$tressem = $tressem + $row['result']['3 Sem'];
					
					$totCerradoDentroSla   = $totCerradoDentroSla + $row['result']['Cerrado Dentro SLA'];
					$totCerradoFueraSla    = $totCerradoFueraSla  + $row['result']['Cerrado Fuera SLA'];
					$totDentroSla          = $totDentroSla        + round($row['result']['Cerrado Dentro SLA']*100 / $row['result']['Total Cerrado'],2);
					$totFueraSla           = $totFueraSla         + round($row['result']['Cerrado Fuera SLA']*100 / $row['result']['Total Cerrado'],2);
					
					$dentrosla = $dentrosla + $row['result']['Dentro SLA'];
					$fuerasla = $fuerasla + $row['result']['Fuera SLA'];
				}
			
			?>
			</tbody>
			<tfoot>
				<th class="active"   style="background-color: #046DB9; color:#FFF;">Total General</th>
				<th class="active"  style="background-color: #046DB9; color:#FFF;"><?php echo $totalMensual ?></th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $totalCerrado ?></th>
				<th class="success" style="background-color: #6296E9; color:#FFF;"><?php echo $totCerradoDentroSla?></th>
				<th class="danger"  style="background-color: #6296E9; color:#FFF;"><?php echo $totCerradoFueraSla?></th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"></th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"></th>
				<th class="Warning" style="background-color: #FCA600; color:#FFF;"><?php echo $totalPend ?></th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $inc?></th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;">0</th>
				<th class="success" style="background-color: #008101; color:#FFF;"><?php echo $dentrosla;?></th>
				<th class="danger"  style="background-color: #B1221F; color:#FFF;"><?php echo $fuerasla;?></th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $hoy ?></th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $unoatres ?></th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $cuatroasiete ?></th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $dossem ?></th>
				<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $tressem ?></th>
			</tfoot>
		</table>
	</div>
	<div class="row " style='padding-right: 0px;padding-left: 0px;'>
		<div class="col-md-2">
			<div class="col-md-12" style='padding-right: 0px;padding-left: 0px;'>
				<div id="containerCircular3" style="width: 145px; height: 145px; max-width: 170px;margin: 3 auto"></div>
			</div>
			<div class="col-md-12" style='padding-right: 0px;padding-left: 0px;'>
				<div id="containerCircular4" style="width: 145px; height: 145px; max-width: 170px;margin: 3 auto"></div>
			</div>
		</div>
		<div class="col-lg-8" >
			<div class="col-lg-12" >
				<h3 style="margin-left:175px;">Porcentaje cumplimiento de SLA</h3>
			</div>
			<div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
				<div id="container1" style="min-width: 200px; height: 258px; max-width: 320px; margin: 3 auto"></div>
			</div>
			<div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
				<div id="container2" style="min-width: 200px; height: 258px; max-width: 320px;  margin: 3 auto"></div>
			</div>
			<div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
				<div id="container3" style="min-width: 200px; height: 258px; max-width: 320px;  margin: 3 auto"></div>
			</div>
			<div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
				<div id="container4" style="min-width: 200px; height: 258px; max-width: 320px;  margin: 3 auto"></div>
			</div>
		</div>
	</div>
</div>
<br>
<input type="hidden" id="filterPeriod" value="<?=$_REQUEST['periodo']?>">
<input type="hidden" id="filterCustomer" value="<?=$_REQUEST['customer']?>">
<input type="hidden" id="backCust"     value="">

<script>
	/*Total */
	containerCircular3();
	containerCircular4();
	/* Gauge 1 */
	<?php
	$x = 0;
	foreach( $response['cust'] as $cust) {
	
	$valueReloj = round($cust['result']['Cerrado Dentro SLA'] * 100 / $cust['result']['Total Cerrado'],2);
	if($x == 0 ){
		$div = "container1";
		echo  "container1();";
	}
	
	if($x == 1 ){
		$div = "container2";
		echo  "container2();";
	}
	
	if($x == 2 ){
		$div = "container3";
		echo  "container3();";
	}
	
	if($x == 3 ){
		$div = "container4";
		echo  "container4();";
	}
	$x++;
	
	?>
	function <?=$div?>() {
		Highcharts.chart('<?=$div?>', {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				exporting: { enabled: false },
				title: {
					text: '<?=$cust['name']?>',
					style: {
						color: '#000',
						fontWeight: 'bold',
						fontSize: '12px'
					}
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					background: [{
						backgroundColor: {
							linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				credits: {
					enabled: false
				},
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
					tickPixelInterval: 20,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: ''
					},
					plotBands: [{
						from: 0,
						to: 75,
						color: '#DF5353' // red
					}, {
						from: 75,
						to: 90,
						color: '#DDDF0D' // yellow
					}, {
						from: 90,
						to: 100,
						color: '#55BF3B' // green
					}]
				},
				series: [{
					name: 'Speed',
					data: [<?=$valueReloj?>],
					tooltip: {
						valueSuffix: '% Zona'
					},
					dataLabels: {
						enabled: true,
						style: {
							fontWeight:'bold',
							fontSize: '22px'
						}
					}
				}]
			},
			
		function (chart) { });
	}
	<?
	}
	?>

	/* Number With Circle */
	function containerCircular1(){
		
		Highcharts.chart('containerCircular1', {
			chart: {
				renderTo: 'container',
				type: 'pie'
			},
			exporting: { enabled: false },
			title: {
				text: 'Total Cerrado<br><strong><?=$totalCerrado?></strong>',
				align: 'center',
				verticalAlign: 'middle',
				y: 0,
				style: {
					fontSize: '14px'
				}
			},
			plotOptions: {
				pie: {
					shadow: false
				}
			},
			series: [{
				name: 'Total Cerrados',
				data: [
					[ "Completed", <?=$totalCerrado?>],
					{
						"name": "",
						"y": 40,
						"color": 'rgba(0,0,0,0)'
					}
				],
				size: '100%',
				innerSize: '88%',
				showInLegend:false,
				dataLabels: {
					enabled: false
				}
			}]
		}, function (chart) {
			// if (!chart.renderer.forExport) {
			//     setInterval(function () {
			//         var point = chart.series[0].points[0],
			//             newVal,
			//             inc = Math.round((Math.random() - 0.5) * 20);
			
			//         newVal = point.y + inc;
			//         if (newVal < 0 || newVal > 200) {
			//             newVal = point.y - inc;
			//         }
			
			//         point.update(newVal);
			
			//     }, 3000);
			// }
		});
		
	}
	function containerCircular2(){
		
		Highcharts.chart('containerCircular2', {
			chart: {
				renderTo: 'container',
				type: 'pie'
			},
			exporting: { enabled: false },
			title: {
				text: 'Total Pend<br><strong><?=$totalPend?></strong>',
				align: 'center',
				verticalAlign: 'middle',
				y: 0,
				style: {
					fontSize: '14px'
				}
			},
			plotOptions: {
				pie: {
					shadow: false
				}
			},
			series: [{
				name: 'Total Pendientes',
				data: [
					[ "Completed", <?=$totalPend?>],
					{
						"name": "",
						"y": 40,
						"color": 'rgba(0,0,0,0)'
					}
				],
				size: '100%',
				innerSize: '88%',
				showInLegend:false,
				dataLabels: {
					enabled: false
				}
			}]
		}, function (chart) {
			// if (!chart.renderer.forExport) {
			//     setInterval(function () {
			//         var point = chart.series[0].points[0],
			//             newVal,
			//             inc = Math.round((Math.random() - 0.5) * 20);
			
			//         newVal = point.y + inc;
			//         if (newVal < 0 || newVal > 200) {
			//             newVal = point.y - inc;
			//         }
			
			//         point.update(newVal);
			
			//     }, 3000);
			// }
		});
		
	}
	function containerCircular3(){
		
		Highcharts.chart('containerCircular3', {
			chart: {
				renderTo: 'container',
				type: 'pie'
			},
			exporting: { enabled: false },
			title: {
				text: 'Cerrado<br>Dentro SLA<br><strong><?=$totCerradoDentroSla?></strong>',
				align: 'center',
				verticalAlign: 'middle',
				y: 0,
				style: {
					fontSize: '14px'
				}
			},
			plotOptions: {
				pie: {
					shadow: false
				}
			},
			series: [{
				name: 'Cerrado Dentro SLA',
				data: [
					[ "Completed", <?=$totCerradoDentroSla?>],
					{
						"name": "Incomplete",
						"y": <?=$totCerradoFueraSla?>,
						"color": 'rgba(0,0,0,0)'
					}
				],
				size: '100%',
				innerSize: '88%',
				showInLegend:false,
				dataLabels: {
					enabled: false
				}
			}]
		}, function (chart) {
			// if (!chart.renderer.forExport) {
			//     setInterval(function () {
			//         var point = chart.series[0].points[0],
			//             newVal,
			//             inc = Math.round((Math.random() - 0.5) * 20);
			
			//         newVal = point.y + inc;
			//         if (newVal < 0 || newVal > 200) {
			//             newVal = point.y - inc;
			//         }
			
			//         point.update(newVal);
			
			//     }, 3000);
			// }
		});
		
	}
	function containerCircular4(){
		
		Highcharts.chart('containerCircular4', {
			chart: {
				renderTo: 'container',
				type: 'pie'
			},
			exporting: { enabled: false },
			title: {
				text: 'Cerrado<br>Fuera SLA <br><strong><?=$totCerradoFueraSla?></strong>',
				align: 'center',
				verticalAlign: 'middle',
				y: 0,
				style: {
					fontSize: '14px'
				}
			},
			plotOptions: {
				pie: {
					shadow: false
				}
			},
			series: [{
				name: 'Cerrado Fuera SLA',
				data: [
					[ "Completed", <?=$totCerradoFueraSla?>],
					{
						"name": "",
						"y": 40,
						"color": 'rgba(0,0,0,0)'
					}
				],
				size: '100%',
				innerSize: '88%',
				showInLegend:false,
				dataLabels: {
					enabled: false
				}
			}]
		}, function (chart) {
			// if (!chart.renderer.forExport) {
			//     setInterval(function () {
			//         var point = chart.series[0].points[0],
			//             newVal,
			//             inc = Math.round((Math.random() - 0.5) * 20);
			
			//         newVal = point.y + inc;
			//         if (newVal < 0 || newVal > 200) {
			//             newVal = point.y - inc;
			//         }
			
			//         point.update(newVal);
			
			//     }, 3000);
			// }
		});
		
	}
	
	/* Source */
	function firstNivel(customer){
		
		var periodo = $('#filterPeriod').val();
		$('#backCust').val(customer);
		
		datos = { "idcustomer" : customer,
			       "periodo"    : periodo,
		          "modo"       : "customer"}
		
		postUrl = "<?php echo site_url('/monitoring/groupDashInc/'); ?>";
		
		$.ajax({
			type: "GET",
			url: postUrl,
			data: datos,
			dataType: "text",
			success: function(result) {
				$('#tabla-nav').html('')
				$('#tabla-nav').html(result);
				
			},
			error: function(xhr, ajaxOptions, thrownError) {
			
			}
		});
	}
	
	function twoNivel(group){
		
		var periodo = $('#filterPeriod').val();
		var empresa = $('#idempresa').val();
		
		
		datos = { "idgroup"   : group,
					 "idempresa" : empresa,
					 "periodo"   : periodo,
		          "modo" : "customer"};
		
		postUrl = "<?php echo site_url('/monitoring/tecnicoDashInc/'); ?>";
		
		$.ajax({
			type: "GET",
			url: postUrl,
			data: datos,
			dataType: "text",
			success: function(result) {
				
				$('#tabla-nav').html('')
				$('#tabla-nav').html(result);
				
				
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	
	function returnForNav(filterZona){
		
		var periodo = $('#filterPeriod').val();
		
		datos = { "zona"      : filterZona,
			"idempresa" : null,
			"periodo"   : periodo};
		
		postUrl = "<?php echo site_url('/monitoring/returnForNav/'); ?>";
		
		$.ajax({
			type: "GET",
			url: postUrl,
			data: datos,
			dataType: "text",
			success: function(result) {
				
				$('#tabla-nav').html('')
				$('#tabla-nav').html(result);
				
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	
	function openFullScreen(){
		
		if((document.webkitIsFullScreen==false)||(document.isFullScreen==false)  ){
			
			(document.documentElement.webkitRequestFullScreen||document.documentElement.requestFullScreen).call(document.documentElement);
			console.log('Open function startNavigation() : ');
			
			$('#stop-nav').show();
			$('#start-nav').hide();
			
			startNavigation('start');
			
		}else{
			(document.webkitCancelFullScreen||document.cancelFullScreen).call(document);
			console.log('Open function stopNavigation() : ');
			
			$('#start-nav').show();
			$('#stop-nav').hide();
			
			startNavigation('stop');
			
		}
	}
	
	function startNavigation(mode){
		
		var filterZona  = $('#zona').val();
		
		var idxZona     = '<?=substr($_NAV, 0, -1)?>';
		var listZona    = idxZona.split(',');
		var dualNumber  = listZona.length * 2;
		var firstNumber = listZona.length;
		var i = 0;
		var z = 0;
		
		if(mode == "start"){
			
			setInterval(function(){
				
				if(i % 2 == 0){
					firstNivel(listZona[z]);
					z = z + 1;
				}else{
					returnForNav(filterZona);
				}
				i = i + 1;
				
				if(firstNumber == z){
					z = 0;
				}
				
				if(dualNumber == i){
					i = 0;
				}
				
			}, 10000);
			
			setInterval(function(){
				if((document.webkitIsFullScreen == false) || (document.isFullScreen==false)  ) {
					console.log('-- NOT FULLSCREEN -- ');
					location.reload();
					
				}
				
			}, 1000);
			
		}else{
			location.reload();
		}
	}
	
	function backAjaxFirst(){
		location.reload();
	}
	
	function backAjaxTwo(){
		idcustomer = $('#backCust').val();

		firstNivel(idcustomer)
	}

</script>