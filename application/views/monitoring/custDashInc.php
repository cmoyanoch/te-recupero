<br>
<div class="container" id="tabla-nav">
	<div class="row" style='padding-right: 0px;padding-left: 0px;'>
		<div class="col-md-12">
			<div class="col-md-1">
				<input type="hidden"  name="idzone" id="idzone" value="<?=$data["idzone"]?>">
				<input type="hidden"  name="idempresa" id="idempresa" value="<?=$data["idempresa"]?>">
				<button type="button" onclick="backAjaxFirst();" class="btn btn-success">atrás</button>
			</div>
		</div>
	</div>
	<br>
	<div class="row" style='padding-right: 0px;padding-left: 0px;'>
		<table class="table table-bordered table-responsive">
			<thead>
			<tr>
				<th class="active" style="width: 170px;background-color: #D3D3D3;">Cliente</th>
				<th class="active" style="background-color: #D3D3D3;">Total Mensual</th>
				<th class="info" style="background-color: #6296E9; color:#FFF;" >Total Cerrado</th>
				<th class="success" style="background-color: #6296E9; color:#FFF;">Cerrado dentro de SLA</th>
				<th class="danger" style="background-color: #6296E9; color:#FFF;">Cerrado fuera de SLA</th>
				<th class="info" style="background-color: #6296E9; color:#FFF;"> % Cerrado dentro de SLA</th>
				<th class="info" style="background-color: #6296E9; color:#FFF;"> % Cerrado fuera de SLA</th>
				<th class="Warning" style="background-color: #FCA600; color:#FFF;"> Total Pend</th>
				<th class="info" style="background-color: #D3D3D3;"> Inc</th>
				<th class="info" style="background-color: #D3D3D3;"> Req</th>
				<th class="success" style="background-color: #008101; color:#FFF;"> Dentro SLA</th>
				<th class="danger"  style="background-color: #B1221F; color:#FFF;"> Fuera SLA</th>
				<th class="info" style="background-color: #FFFF01;"> Hoy </th>
				<th class="info" style="background-color: #FFFF01;"> 1 a 3 días </th>
				<th class="info" style="background-color: #FFFF01;"> 4 a 7 días </th>
				<th class="info" style="background-color: #FFFF01;"> 2 Sem </th>
				<th class="info" style="background-color: #FFFF01;"> 3 Sem </th>
			</tr>
			</thead>
			<tbody>
			<?php
				
				$totalMensual = 0;
				$totalCerrado = 0;
				$totalPend = 0;
				$inc = 0;
				$hoy = 0;
				$unoatres = 0;
				$cuatroasiete = 0;
				$dossem = 0;
				$tressem = 0;
				
				$totCerradoDentroSla   = 0;
				$totCerradoFueraSla    = 0;
				$totDentroSla   = 0;
				$totFueraSla    = 0;
				foreach ($data['dashInc'] as $row) { ?>
					<tr>
						<?php if($paramEmp!=null) { ?>
							<th class="active" ><a href="<?php echo base_url()."/monitoring/tecnicoDashInc/".$row['id_cust']."/".$paramEmp; ?>"><?php echo $row['empresa']; ?></a></th>
						<?php } else { ?>
							<th class="active" onclick="twoNivel('<?=$row['id_cust']?>', 'bigBoss')"><a href="#"><?php echo $row['empresa']; ?></a></th>
						<?php } ?>
						<th class="active"><?php echo $row['Total Mensual']; ?></th>
						<th class="info" style="background-color: #6296E9; color:#FFF;"><?php echo $row['Total Cerrado']; ?></th>
						<th class="success" ><?php echo $row['Cerrado Dentro SLA']; ?></th>
						<th class="danger" ><?php echo $row['Cerrado Fuera SLA']; ?></th>
						<th class="info" ><?php
								if($row['Total Cerrado']!=0) {
									echo round($row['Cerrado Dentro SLA']*100 / $row['Total Cerrado'],2)." %";
								} else {
									echo "0 %";
								}?></th>
						<th class="info" ><?php
								if($row['Total Cerrado']!=0) {
									echo round($row['Cerrado Fuera SLA']*100 / $row['Total Cerrado'],2)." %";
								} else {
									echo "0 %";
								}?></th>
						<th class="Warning" style="background-color: #FCA600; color:#FFF;" ><?php echo $row['Total Pendiente']; ?></th>
						<th class="info" ><?php echo $row['Inc']; ?></th>
						<th class="info" ><?php echo $row['Req']; ?></th>
						<th class="success" style="background-color: #008101; color:#FFF;"><?php echo $row['Dentro SLA']; ?></th>
						<th class="danger"  style="background-color: #B1221F; color:#FFF;"><?php echo $row['Fuera SLA']; ?></th>
						<th class="info" ><?php echo $row['Hoy']; ?></th>
						<th class="info" ><?php echo $row['1 a 3 dias']; ?></th>
						<th class="info" ><?php echo $row['4 a 7 dias']; ?></th>
						<th class="info" ><?php echo $row['2 Sem']; ?></th>
						<th class="info" ><?php echo $row['3 Sem']; ?></th>
					</tr>
					<?php
					$totalMensual =  $totalMensual + $row['Total Mensual'];
					$totalCerrado =  $totalCerrado + $row['Total Cerrado'];
					$totalPend =  $totalPend + $row['Total Pendiente'];
					$inc =  $inc + $row['Inc'];
					$hoy = $hoy + $row['Hoy'];
					$unoatres = $unoatres + $row['1 a 3 dias'];
					$cuatroasiete = $cuatroasiete + $row['4 a 7 dias'];
					$dossem  =  $dossem + $row['2 Sem'];
					$tressem = $tressem + $row['3 Sem'];
					
					$totCerradoDentroSla   = $totCerradoDentroSla + $row['Cerrado Dentro SLA'];
					$totCerradoFueraSla    = $totCerradoFueraSla  + $row['Cerrado Fuera SLA'];
					$totDentroSla          = $totDentroSla        + $row['Dentro SLA'];
					$totFueraSla           = $totFueraSla         + $row['Fuera SLA'];
				} ?>
			</tbody>
			<tfoot>
			<th class="active"  style="background-color: #046DB9; color:#FFF;">Total General</th>
			<th class="active"  style="background-color: #046DB9; color:#FFF;"><?php echo $totalMensual ?></th>
			<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $totalCerrado ?></th>
			<th class="success" style="background-color: #6296E9; color:#FFF;"><?php echo $totCerradoDentroSla ?></th>
			<th class="danger"  style="background-color: #6296E9; color:#FFF;"><?php echo $totCerradoFueraSla?></th>
			<th class="info"    style="background-color: #6296E9; color:#FFF;"></th>
			<th class="info"    style="background-color: #6296E9; color:#FFF;"></th>
			<th class="Warning" style="background-color: #FCA600; color:#FFF;"><?php echo $totalPend ?></th>
			<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $inc?></th>
			<th class="info"    style="background-color: #6296E9; color:#FFF;">0</th>
			<th class="success" style="background-color: #008101; color:#FFF;"><?php echo $totDentroSla;?></th>
			<th class="danger"  style="background-color: #B1221F; color:#FFF;"><?php echo $totFueraSla;?></th>
			<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $hoy ?></th>
			<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $unoatres ?></th>
			<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $cuatroasiete ?></th>
			<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $dossem ?></th>
			<th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $tressem ?></th>
			</tfoot>
		</table>
	</div>
	<div class="row" style='padding-right: 0px;padding-left: 0px;'>
		<div class="col-md-2">
			<div class="col-md-12" style='padding-right: 0px;padding-left: 0px;'>
				<div id="containerCircular3" style="width: 145px; height: 145px; max-width: 170px;margin: 3 auto"></div>
			</div>
			<div class="col-md-12" style='padding-right: 0px;padding-left: 0px;'>
				<div id="containerCircular4" style="width: 145px; height: 145px; max-width: 170px;margin: 3 auto"></div>
			</div>
		</div>
		<div class="col-lg-8" >
			<div class="col-lg-12" >
				<h3 style="margin-left:175px;">Porcentaje Cerrado Fuera De SLA</h3>
			</div>
			<?php
				$y = 1;
				foreach ($data['dashInc'] as $row) { ?>
					<div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
						<div id="container<?=$y?>" style="min-width: 200px; height: 258px; max-width: 320px; margin: 3 auto"></div>
					</div>
					<?php $y++; } ?>
		</div>
	</div>
</div>
<script>
	<?php
	$z = 1;
	foreach ($data['dashInc'] as $row) {
	echo "container{$z}();";
	$valueReloj = round($row['Cerrado Fuera SLA'] * 100 / $row['Total Cerrado'],2);
	?>
	function container<?=$z?>() {
		Highcharts.chart('container<?=$z?>', {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				exporting: { enabled: false },
				title: {
					text: '<?=$row['group']?>',
					style: {
						color: '#000',
						fontWeight: 'bold',
						fontSize: '12px'
					}
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					background: [{
						backgroundColor: {
							linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				credits: {
					enabled: false
				},
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
					tickPixelInterval: 20,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: ''
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 75,
						color: '#DDDF0D' // yellow
					}, {
						from: 75,
						to: 100,
						color: '#DF5353' // red
					}]
				},
				series: [{
					name: 'Speed',
					data: [<?=$valueReloj?>],
					tooltip: {
						valueSuffix: '% Zona'
					},
					dataLabels: {
						enabled: true,
						style: {
							fontWeight:'bold',
							fontSize: '18px'
						}
					}
				}]
// }
			},
			// Add some life
			function (chart) {
				/*  if (!chart.renderer.forExport) {
						setInterval(function () {
							 var point = chart.series[0].points[0],
								  newVal,
								  inc = Math.round((Math.random() - 0.5) * 20);

							 newVal = point.y + inc;
							 if (newVal < 0 || newVal > 200) {
								  newVal = point.y - inc;
							 }

							 point.update(newVal);

						}, 3000);
				  }*/
			});
	}
	
	<?php $y++;     $z++;} ?>
	
	
	containerCircular3();
	containerCircular4();
	
	function containerCircular1(){
		
		Highcharts.chart('containerCircular1', {
			chart: {
				renderTo: 'container',
				type: 'pie'
			},
			exporting: { enabled: false },
			title: {
				text: 'Total Cerrado<br><strong>45</strong>',
				align: 'center',
				verticalAlign: 'middle',
				y: 0,
				style: {
					fontSize: '14px'
				}
			},
			plotOptions: {
				pie: {
					shadow: false
				}
			},
			series: [{
				name: 'Browsers',
				data: [
					[ "Completed", 45],
					{
						"name": "Incomplete",
						"y": 40,
						"color": 'rgba(0,0,0,0)'
					}
				],
				size: '100%',
				innerSize: '90%',
				showInLegend:false,
				dataLabels: {
					enabled: false
				}
			}]
		}, function (chart) {
			// if (!chart.renderer.forExport) {
			//     setInterval(function () {
			//         var point = chart.series[0].points[0],
			//             newVal,
			//             inc = Math.round((Math.random() - 0.5) * 20);
			
			//         newVal = point.y + inc;
			//         if (newVal < 0 || newVal > 200) {
			//             newVal = point.y - inc;
			//         }
			
			//         point.update(newVal);
			
			//     }, 3000);
			// }
		});
		
	}
	function containerCircular2(){
		
		Highcharts.chart('containerCircular2', {
			chart: {
				renderTo: 'container',
				type: 'pie'
			},
			exporting: { enabled: false },
			title: {
				text: 'Total Pend<br><strong>71</strong>',
				align: 'center',
				verticalAlign: 'middle',
				y: 0,
				style: {
					fontSize: '14px'
				}
			},
			plotOptions: {
				pie: {
					shadow: false
				}
			},
			series: [{
				name: 'Browsers',
				data: [
					[ "Completed", 71],
					{
						"name": "Incomplete",
						"y": 40,
						"color": 'rgba(0,0,0,0)'
					}
				],
				size: '100%',
				innerSize: '90%',
				showInLegend:false,
				dataLabels: {
					enabled: false
				}
			}]
		}, function (chart) {
			// if (!chart.renderer.forExport) {
			//     setInterval(function () {
			//         var point = chart.series[0].points[0],
			//             newVal,
			//             inc = Math.round((Math.random() - 0.5) * 20);
			
			//         newVal = point.y + inc;
			//         if (newVal < 0 || newVal > 200) {
			//             newVal = point.y - inc;
			//         }
			
			//         point.update(newVal);
			
			//     }, 3000);
			// }
		});
		
	}
	function containerCircular3(){
		
		Highcharts.chart('containerCircular3', {
			chart: {
				renderTo: 'container',
				type: 'pie'
			},
			exporting: { enabled: false },
			title: {
				text: 'Dentro SLA <br><strong><?=$totDentroSla?></strong>',
				align: 'center',
				verticalAlign: 'middle',
				y: 0,
				style: {
					fontSize: '14px'
				}
			},
			plotOptions: {
				pie: {
					shadow: false
				}
			},
			series: [{
				name: 'Browsers',
				data: [
					[ "Completed", <?=$totDentroSla?>],
					{
						"name": "Incomplete",
						"y": 40,
						"color": 'rgba(0,0,0,0)'
					}
				],
				size: '100%',
				innerSize: '90%',
				showInLegend:false,
				dataLabels: {
					enabled: false
				}
			}]
		}, function (chart) {
			// if (!chart.renderer.forExport) {
			//     setInterval(function () {
			//         var point = chart.series[0].points[0],
			//             newVal,
			//             inc = Math.round((Math.random() - 0.5) * 20);
			
			//         newVal = point.y + inc;
			//         if (newVal < 0 || newVal > 200) {
			//             newVal = point.y - inc;
			//         }
			
			//         point.update(newVal);
			
			//     }, 3000);
			// }
		});
		
	}
	function containerCircular4(){
		
		Highcharts.chart('containerCircular4', {
			chart: {
				renderTo: 'container',
				type: 'pie'
			},
			exporting: { enabled: false },
			title: {
				text: 'Fuera SLA <br><strong><?=$totFueraSla?></strong>',
				align: 'center',
				verticalAlign: 'middle',
				y: 0,
				style: {
					fontSize: '14px'
				}
			},
			plotOptions: {
				pie: {
					shadow: false
				}
			},
			series: [{
				name: '',
				data: [
					[ "Completed", <?=$totFueraSla?>],
					{
						"name": "",
						"y": 0,
						"color": 'rgba(0,0,0,0)'
					}
				],
				size: '100%',
				innerSize: '90%',
				showInLegend:false,
				dataLabels: {
					enabled: false
				}
			}]
		}, function (chart) {
			// if (!chart.renderer.forExport) {
			//     setInterval(function () {
			//         var point = chart.series[0].points[0],
			//             newVal,
			//             inc = Math.round((Math.random() - 0.5) * 20);
			
			//         newVal = point.y + inc;
			//         if (newVal < 0 || newVal > 200) {
			//             newVal = point.y - inc;
			//         }
			
			//         point.update(newVal);
			
			//     }, 3000);
			// }
		});
		
	}
	
</script>
