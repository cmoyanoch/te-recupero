<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script async type="text/javascript">
    var chart;
    $(document).ready(function () {

        var url = document.location.origin;
        var contenedorResultado = $('[data-model=container]');
        var cajaBusqueda = $('[data-model=caja-busqueda]');
        var estructura = contenedorResultado.find('[data-model=struct-model]').clone();
        var selectCliente = cajaBusqueda.find("[data-attribute=select-cliente]");
        var selectZona = cajaBusqueda.find("[data-attribute=select-zona]");
        var selectGrupo = cajaBusqueda.find("[data-attribute=select-grupo]");
        var selectPeriodo = cajaBusqueda.find("[data-attribute=select-periodo]");
        var selectPregunta = cajaBusqueda.find("[data-attribute=select-pregunta]");
        var data = {};
        function setFilter(respuesta) {
            var grupos = respuesta.data.grupos;
            var clientes = respuesta.data.clientes;
            var zonas = respuesta.data.zonas;
            var preguntas = respuesta.data.preguntas;
            data = respuesta.data;
            setSelectDom(selectCliente, clientes, 'Cliente');
            setSelectDom(selectGrupo, grupos, 'Grupo');
            setSelectDom(selectZona, zonas, 'Zona');
            setSelectDom(selectPregunta, preguntas, 'Pregunta');
        }

        function llenarGraph1(totales) {

            Highcharts.setOptions({
                colors: ['green', 'red']
            });
            // Build the chart
            console.log(totales);
            chart = new Highcharts.chart({
                chart: {
                    renderTo: 'chart1',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Nivel de satisfacción'
                },
                tooltip: {
                    formatter: function () {

                        return '<b>' + this.point.name + '</b>: ' + this.percentage + ' %';
                    }
                    /*pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'*/
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            },
                            connectorColor: 'silver'
                        }
                    }
                },
                series: [{
                        name: 'Nivel de satisfacción',
                        data: [
                            {name: "Porcentaje satisfacción", y: parseFloat(totales)},
                            {name: "Porcentaje Insatisfacción", y: parseFloat((100 - parseFloat(totales)))}
                        ]
                    }]
            });
        }

        function setBusqueda(respuesta) {

            console.log('Set Respuesta Busqueda');
            contenedorResultado.empty();
            console.log(respuesta.data);
            objClientes = estructura.clone();
            objClientes.find('[data-attribute=head-title]').html(respuesta.data.head_title);
            objClientes.find('[data-attribute=title]').html(respuesta.data.titulo);
            objTabla = objClientes.find('tbody tr');
            objClientes.find('tbody tr').remove();
            $.each(respuesta.data.clientes, function (i, cliente) {

                console.log(cliente);
                var dato = objTabla.clone();
                dato.find('[data-attribute=nombre]').attr('data-id', cliente.id);
                dato.find('[data-attribute=nombre]').html(cliente.cliente);
                dato.find('[data-attribute=nombre-grupo]').html(cliente.grupo);
                dato.find('[data-attribute=nombre-zona]').html(cliente.zona);
                dato.find('[data-attribute=nombre-tecnico]').html(cliente.tecnico);
                dato.find('[data-attribute=nombre-question]').html(cliente.question);
                dato.find('[data-attribute=valor-resp]').html(getPercentValue(cliente.totalpreguntas, cliente.value) + '%');
                dato.find('[data-attribute=nombre]').on('click', function (e) {
                    step = step + 1;
                    console.log(" Pasos : " + step);
                    console.log(" data-id : " + $(this).attr('data-id'));
                    switch (step) {
                        case 1:
                            activarBuscarZona($(this).attr('data-id'));
                            break;
                        case 2:
                            activarBuscarGrupo($(this).attr('data-id'));
                            break;
                        case 3:
                            activarBuscarTecnico($(this).attr('data-id'));
                            break;
                        case 4:
                            activarBuscarIncident($(this).attr('data-id'));
                            break;
                        case 5:
                            e.preventDefault();
                            break;
                    }
                });
                objClientes.find('tbody').append(dato);
            });
            contenedorResultado.append(objClientes);
            if (respuesta.data.clientes.length == 0) {
                swal("Mensaje iTr@ces", "Sin Datos !! ", "info");
            }

        }

        function activarBuscarZona(id) {
            console.log('Comenzamos la busqueda por pasos : activarBuscarZona() ');
            filtro = {
                'periodo': selectPeriodo.val(),
                'cliente': id,
                'zona': selectZona.val()
            };
            valid = getValidation();
            if (valid) {
                resp = processAjax('GET', url + '/auris/totalesByZona', 'application/json', filtro, 'json', false);
                resp.done(function (respuesta) {
                    console.log('Respuesta correcta', respuesta);
                    setBusqueda(respuesta);
                    // setFilter(respuesta);
                }).fail(function (xhr) {
                    console.error('Error en el controlador');
                    console.log('ERROR:', xhr);
                }).always(function () {
                    console.log('Proceso Finalizado');
                });
                console.log('Terminamos la busqueda');
            }
        }

        function activarBuscarGrupo(id) {
            console.log('Comenzamos la busqueda por pasos : activarBuscarGrupo(' + id + ') ');
            filtro = {
                'periodo': selectPeriodo.val(),
                'cliente': id,
                'zona': selectZona.val()
            };
            valid = getValidation();
            if (valid) {
                resp = processAjax('GET', url + '/auris/totalesByGrupo', 'application/json', filtro, 'json', false);
                resp.done(function (respuesta) {
                    console.log('Respuesta correcta', respuesta);
                    setBusqueda(respuesta);
                    // setFilter(respuesta);
                }).fail(function (xhr) {
                    console.error('Error en el controlador');
                    console.log('ERROR:', xhr);
                }).always(function () {
                    console.log('Proceso Finalizado');
                });
                console.log('Terminamos la busqueda');
            }
        }

        function totales(respuesta)
        {
            console.log("Imprimiendo totales");
            console.log(respuesta);
            var total = 0;
            var valor = 0;
            var div = 0;
            console.log(respuesta.data);
            $.each(respuesta.data.clientes, function (i, cliente)
            {
                valor += parseInt(cliente.totalpreguntas) + parseInt(valor);
                div += parseInt(cliente.value) + parseInt(div);
            });
            console.log(valor);
            console.log(div);
            total = getPercentValue(valor, div);
            console.log(parseFloat(total));
            llenarGraph1(total);
        }
        function getPercentValue(total, valor) {
            porcentaje = (valor * 100) / (total * 5);
            if (isNaN(porcentaje))
                porcentaje = 0;
            return parseFloat(porcentaje).toFixed(2);
        }

        function setSelectDom(select, data, controlNombre) {
            option = null;
            option = select.find('option');
            select.empty();
            var dom = option.clone();
            dom.html('-- Todos --');
            dom.attr('value', 0);
            select.append(dom);
            dom = null;
            $.each(data, function (i, value) {
                var dom = option.clone();
                dom.html(value.nombre);
                dom.attr('value', value.id);
                select.append(dom);
                dom = null;
            });
        }

        function getFiltros() {
            resp = processAjax('POST', url + '/Informeencuesta/filtros', 'application/json', {}, 'json', false);
            resp.done(function (respuesta) {
                console.log('Respuesta correcta', respuesta);
                setFilter(respuesta);
            }).fail(function (xhr) {
                console.error('Error en el controlador');
                console.log('ERROR:', xhr);
            }).always(function () {
                console.log('Proceso Finalizado');
            });
        }

        function getValidation() {
            periodo = selectPeriodo.val();
            isValid = true;
            if (periodo == "0") {
                swal("Mensaje iTr@ces", "Debe seleccionar un periodo.", "info");
                isValid = false;
            }
            return isValid;
        }

        function processAjax(type, url, contentType, data, dataType, async = true) {

            return $.ajax({
                type: type,
                url: url,
                data: data,
                contentType: contentType,
                dataType: dataType,
                async: async,
                cache: false,
            });
        }

        function activaBuscar() {
            step = 0;
            $('#buscar').click(function (e) {

                $('#struct-box').show();
                e.preventDefault();
                filtro = {
                    'periodo': selectPeriodo.val(),
                    'cliente': selectCliente.val(),
                    'grupo': selectGrupo.val(),
                    'zona': selectZona.val(),
                    'pregunta': selectPregunta.val()
                };
                valid = getValidation();
                if (valid) {
                    resp = processAjax('GET', url + '/informeencuesta/getQuestionByCustomer', 'application/json', filtro, 'json', false);
                    resp.done(function (respuesta) {
                        console.log('Respuesta correcta', respuesta);
                        setBusqueda(respuesta);
                        totales(respuesta);
                        // setFilter(respuesta);
                    }).fail(function (xhr) {
                        console.log('ERROR:', xhr);
                    }).always(function () {
                        console.log('Proceso Finalizado');
                    });
                    console.log('Terminamos la busqueda');
                }
            });
        }

        function run() {
            contenedorResultado.find('[data-model=struct-model]').remove();
            getFiltros();
            activaBuscar();
        }

        run();
    });</script>
<style type="text/css">
    #chart1 {
        min-width: 40%;
        max-width: 80%;
        height: 35%;
        margin: 0 auto
    }
    div.panel-heading {
        background-color: #3fa9f5 !important;
        color: white !important;
    }

    .breadcrumb {
        margin-top: 0px;
    }

    .c-pointer:hover {
        cursor: pointer;
        background-color: #95b5ef !important;
    }

    .success {
        background-color: #6693c1 !important;
    }

    .info {
        background-color: #82bb7b !important;
    }


</style>

<div class="list">
    <ol class="breadcrumb">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a
                href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Monitoreo</strong></a></li>
        <li class="active"><strong> Productividad </strong></li>
    </ol>
</div>
<!-- Dashboard Produvtividad y Analisis -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 bg-info" data-model="caja-busqueda" id="caja-busqueda">
            <div class="row">
                <div class="col-md-12 bg-primary">
                    <h4>Filtro de busqueda</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group" data-model="select-periodo">
                    <label for="periodo">Periodos</label>
                    <select name="periodo" id="periodo" class="form-control" data-attribute="select-periodo">
                        <option value="0">Seleccione Periodo</option>
                        <option value="1">Mes Actual</option>
                        <option value="30">30 días</option>
                        <option value="60">60 días</option>
                        <option value="90">90 días</option>
                        <option value="180">180 días</option>
                        <option value="365">365 días</option>
                    </select>
                </div>
                <div class="col-md-12 form-group" data-model="select-cliente">
                    <label for="cliente">Cliente</label>
                    <select name="cliente" id="cliente" class="form-control" data-attribute="select-cliente">
                        <option value="0">Seleccione Cliente</option>
                    </select>
                </div>
                <div class="col-md-12 form-group" data-model="select-grupo">
                    <label for="grupo">Grupo</label>
                    <select name="grupo" id="grupo" class="form-control" data-attribute="select-grupo">
                        <option value="0">Seleccione Grupo</option>
                    </select>
                </div>
                <div class="col-md-12 form-group" data-model="select-zona">
                    <label for="zona">Zonas</label>
                    <select name="zona" id="zona" class="form-control" data-attribute="select-zona">
                        <option value="0">Seleccione Zona</option>
                    </select>
                </div>
                <div class="col-md-12 form-group" data-model="select-pregunta">
                    <label for="pregunta">Preguntas</label>
                    <select name="pregunta" id="pregunta" class="form-control" data-attribute="select-pregunta">
                        <option value="0">Seleccione Pregunta</option>
                    </select>
                </div>
                <div class="col-md-12 form-group">
                    <button id="buscar" name="buscar" class="btn btn-primary">Buscar</button>
                </div>
            </div>
        </div>
        <div style="display:none" class="col-md-9" id="struct-box">
            <div class="row" data-model="container">
                <div class="col-md-12" data-model="struct-model">
                    <div class="col-md-12 bg-primary">
                        <h4 data-attribute="title" id="HI"></h4>
                    </div>
                    <table class="table" data-model="content-table">
                        <thead>
                            <tr>
                                <th class="info" data-attribute="head-title">[nombre]</th>
                                <th class="info">Grupo resolutor</th>
                                <th class="info">Zona</th>
                                <th class="info">Tecnico</th>
                                <th class="info">Pregunta</th>
                                <th class="success">Respuesta</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="info c-pointer" data-attribute="nombre"></td>
                                <td class="info" data-attribute="nombre-grupo"></td>
                                <td class="info" data-attribute="nombre-zona"></td>
                                <td class="info" data-attribute="nombre-tecnico"></td>
                                <td class="info" data-attribute="nombre-question"></td>
                                <td class="success" data-attribute="valor-resp"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="chart1"></div>
</div>