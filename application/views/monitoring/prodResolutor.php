

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>




<script src="https://code.highcharts.com/highcharts-more.js"></script>



<div class="container ">
    <div class="row">
        <div class="col-md-6">
            <!-- <?php //echo form_open(site_url('monitoring/index'), array('method'=>'get')); ?>
            <div class="input-group">
                <?php //echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué país deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>                    
                <span class="input-group-btn">
                    <button class="btn btn-default blue" type="submit">Buscar</button>
                </span>
            </div>
            <?php //echo form_close(); ?> -->
        </div>
        <div class="col-md-6 button-content">                               
            <!-- <a href="<?php //echo site_url('monitoring/create'); ?>" class="btn btn-default blue">Crear</a>                 -->
        </div>
    </div>
  
    <div class="row" style='padding-right: 0px;padding-left: 0px;'>
       <br>
       <br>
      
        <table class="table">
        <thead>
         <tr>
            <th class="info">Zona   </th>
            <th class="info">Grupo Resolutor    </th>
            <th class="info">Total Tareas   </th>
            <th class="success">Tareas Asignadas   </th>
            <th class="success">% Tareas Asignadas </th>
            <th class="danger">Tareas No Asignadas    </th>
            <th class="danger">% Tareas No Asignadas  </th>
            <th class="info">Total Tareas Finalizadas   </th>
            <th class="info">% Finalizados Vs Total Tareas  </th>
            <th class="info">% Finalizados vs Tareas Asignadas  </th>
            <th class="info">Responsable</th>            
          </tr>
        </thead>
        <tbody>
           <tr>
               <td  class="">Centro   </td>
                <td class="">EUS_ZONA NORTE IV REGION     </td>
                <td class="">55   </td>
                <td class="">54   </td>
                <td class="">98,2%    </td>          
                <td class="">1    </td>
                <td class="">1,8%     </td>
                <td class="">15   </td>
                <td class="">27,3%    </td>
                <td class="">27,8%    </td>
                <td class="">Juan Carlos Cubillos - Marcelo Varas  </td>    
          </tr> 
          <tr>
            <td class="">  Centro </td>
            <td class="">EUS_ZONA V REGION    </td>
            <td class="">208      </td>
            <td class="">205      </td>
            <td class="">98,6%    </td>          
            <td class="">3    </td>
            <td class="">1,4%     </td>
            <td class="">21   </td>
            <td class="">10,1%    </td>
            <td class="">10,2%    </td>
            <td class="">Juan Carlos Cubillos - Marcelo Varas  </td>    
        </tr>  
        <tr>
            <td class=""> Claro Instalaciones     </td>
            <td class=""> GT_TELMEX INST. Z RM   </td>
            <td class=""> 113    </td>
            <td class=""> 112    </td>
            <td class=""> 99,1%  </td>          
            <td class=""> 1  </td>
            <td class=""> 0,9%   </td>
            <td class=""> 49     </td>
            <td class=""> 43,4%  </td>
            <td class=""> 43,8%  </td>
            <td class=""> Daniel Vega </td>    
        </tr>  
         <tr>
            <td class=""> Claro Reparaciones      </td>
            <td class=""> GT_TELMEX REP. Z RM    </td>
            <td class=""> 108    </td>
            <td class=""> 94     </td>
            <td class=""> 87,0%  </td>          
            <td class=""> 14     </td>
            <td class=""> 13,0%  </td>
            <td class=""> 68     </td>
            <td class=""> 63,0%  </td>
            <td class=""> 72,3%  </td>
            <td class=""> Marco Morales Chávez </td>    
        </tr> 
         <tr>
            <td class=""> EUS CORREOS VTR SAVAL   </td>
            <td class=""> EUS CORREOS VTR SAVAL  </td>
            <td class=""> 25     </td>
            <td class=""> 25     </td>
            <td class=""> 100,0%     </td>          
            <td class=""> 0  </td>
            <td class=""> 0,0%   </td>
            <td class=""> 0  </td>
            <td class=""> 0,0%   </td>
            <td class=""> 0,0%   </td>
            <td class=""> Christopher Saavedra </td>    
        </tr> 
        <tr>
            <td class="">EUS_BCI_RM    </td>
            <td class="">EUS_BCI_RM   </td>
            <td class="">22   </td>
            <td class="">20   </td>
            <td class="">90,9%    </td>          
            <td class="">2    </td>
            <td class="">9,1%     </td>
            <td class="">0    </td>
            <td class="">0,0%     </td>
            <td class="">0,0%     </td>
            <td class="">Gislainne Miranda  </td>    
        </tr> 
         <tr>
            <td class=""> EUS_REGION METROPOLITANA 2      </td>
            <td class=""> EUS_REGION METROPOLITANA 2     </td>
            <td class=""> 154    </td>
            <td class=""> 152    </td>
            <td class=""> 98,7%  </td>          
            <td class=""> 2  </td>
            <td class=""> 1,3%   </td>
            <td class=""> 141    </td>
            <td class=""> 91,6%  </td>
            <td class=""> 92,8%  </td>
            <td class=""> Natalia Meneses </td>    
        </tr>
        <tr>
            <td class="">EUS_RESIDENTE FALABELLA       </td>
            <td class="">EUS_RESIDENTE FALABELLA      </td>
            <td class="">4    </td>
            <td class="">2    </td>
            <td class="">50,0%    </td>          
            <td class="">2    </td>
            <td class="">50,0%    </td>
            <td class="">0    </td>
            <td class="">0,0%     </td>
            <td class="">0,0%     </td>
            <td class="">Dagoberto Saez  </td>    
        </tr> 
         <tr>
            <td class=""> EUS_SERVIDORES Y COMUNICACIONES     </td>
            <td class=""> EUS_SERVIDORES Y COMUNICACIONES    </td>
            <td class=""> 0  </td>
            <td class=""> 0  </td>
            <td class=""> 0  </td>          
            <td class=""> 0  </td>
            <td class=""> 0  </td>
            <td class=""> 0  </td>
            <td class=""> 0  </td>
            <td class=""> 0  </td>
            <td class=""> Loreto Pizarro </td>    
        </tr>
        <tr>
            <td class="">Metropolitana   </td>
            <td class="">EUS_REGION METROPOLITANA     </td>
            <td class="">39   </td>
            <td class="">39   </td>
            <td class="">100,0%   </td>          
            <td class="">0    </td>
            <td class="">0,0%     </td>
            <td class="">15   </td>
            <td class="">38,5%    </td>
            <td class="">38,5%    </td>
            <td class="">Loreto Pizarro  </td>    
        </tr>
        <tr>
            <td class="">Metropolitana  </td>
            <td class="">EUS_REGION METROPOLITANA 3  </td>
            <td class="">17  </td>
            <td class="">17  </td>
            <td class="">100,0%  </td>          
            <td class="">0   </td>
            <td class="">0,0%    </td>
            <td class="">13  </td>
            <td class="">76,5%   </td>
            <td class="">76,5%   </td>
            <td class="">Daisy Oyarzun - Bernardo Matus </td>    
        </tr> 
        <tr>
            <td class=""> Metropolitana  </td>
            <td class=""> EUS_REGION VI RANCAGUA     </td>
            <td class=""> 103    </td>
            <td class=""> 103    </td>
            <td class=""> 100,0%     </td>          
            <td class=""> 0  </td>
            <td class=""> 0,0%   </td>
            <td class=""> 84     </td>
            <td class=""> 81,6%  </td>
            <td class=""> 81,6%  </td>
            <td class=""> Daisy Oyarzun </td>    
        </tr> 
         <tr>
            <td class=""> Norte   </td>
            <td class=""> EUS_ZONA NORTE I Y III REGION  </td>
            <td class=""> 128    </td>
            <td class=""> 106    </td>
            <td class=""> 82,8%  </td>          
            <td class=""> 22     </td>
            <td class=""> 17,2%  </td>
            <td class=""> 1  </td>
            <td class=""> 0,8%   </td>
            <td class=""> 0,9%   </td>
            <td class=""> Jorge Toro </td>    
        </tr> 
        <tr>
            <td class=""> Norte   </td>
            <td class=""> EUS_ZONA NORTE II REGION   </td>
            <td class=""> 178    </td>
            <td class=""> 136    </td>
            <td class=""> 76,4%  </td>          
            <td class=""> 42     </td>
            <td class=""> 23,6%  </td>
            <td class=""> 0  </td>
            <td class=""> 0,0%   </td>
            <td class=""> 0,0%   </td>
            <td class=""> Jaime Guerra </td>    
        </tr> 
         <tr>
            <td class="">Sur    </td>
            <td class="">EUS_ZSUR CONCEPCION-LOS ANGELES  </td>
            <td class="">158      </td>
            <td class="">158      </td>
            <td class="">100,0%   </td>          
            <td class="">0    </td>
            <td class="">0,0%     </td>
            <td class="">70   </td>
            <td class="">44,3%    </td>
            <td class="">44,3%    </td>
            <td class="">Alex Perez  </td>    
        </tr>
        <tr>
            <td class="">Sur    </td>
            <td class="">EUS_ZSUR TALCA-CHILLAN-TEMUCO    </td>
            <td class="">226      </td>
            <td class="">226      </td>
            <td class="">100,0%   </td>          
            <td class="">0    </td>
            <td class="">0,0%     </td>
            <td class="">174      </td>
            <td class="">77,0%    </td>
            <td class="">77,0%    </td>
            <td class="">Nelson Miranda  </td>    
        </tr> 
         <tr>
            <td class="">Sur   </td>
            <td class="">EUS_ZSUR VALDIVIA-PUNTA ARENAS  </td>
            <td class="">176     </td>
            <td class="">176     </td>
            <td class="">100,0%  </td>          
            <td class="">0   </td>
            <td class="">0,0%    </td>
            <td class="">131     </td>
            <td class="">74,4%   </td>
            <td class="">74,4%   </td>
            <td class="">Rodolfo Vergara </td>    
        </tr>  
         <tr>
            <td class="info"> Total General  </td>
            <td class="info">  </td>
            <td class="info"> 1.714  </td>
            <td class="success"> 1.625  </td>
            <td class="success"> 94,8%  </td>          
            <td class="danger"> 89     </td>
            <td class="danger"> 5,2%   </td>
            <td class="info"> 782    </td>
            <td class="info"> 45,6%  </td>
            <td class="info"> 48,1%  </td>
            <td class="info">  </td>    
        </tr> 
           
         
        </tbody>
      </table>
        
       
    </div>
   

  
</div>

