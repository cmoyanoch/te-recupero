<div class="list">
    <ol class="breadcrumb" style="margin-top: 0px;">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Monitoreo</strong></a></li>
        <li class="active"><strong> Dashboard </strong></li>
    </ol>
</div>
<div id="tabla-nav" class="container">
    <div class="row bg-info" style='padding-right: 0px;padding-left: 0px;'>
        <?php echo form_open(site_url('monitoring/index'), array('method'=>'get')); ?>
        <div class="col-md-12 bg-primary">
            <div class="col-md-6">
                <h5>Filtro de busqueda</h5>
            </div>
            <div class="col-md-6" style="text-align: right;padding-top: 5px;cursor: pointer;" onclick="openFullScreen()">
                <i id="stop-nav" class="fa fa-stop-circle fa-lg"></i>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3 form-group">
                <label for="periodo">Periodos</label>
                <select name="periodo" id="periodo" class="form-control">
                    <option value="0">Seleccione Periodo</option>
                    <option value="30"  <?php if( $_REQUEST['periodo'] == "30" ) echo "selected"; ?>>Mes Actual</option>
                    <option value="30"  <?php if( $_REQUEST['periodo'] == "30" ) echo "selected"; ?>>30 días</option>
                    <option value="60"  <?php if( $_REQUEST['periodo'] == "60" ) echo "selected"; ?>>60 días</option>
                    <option value="90"  <?php if( $_REQUEST['periodo'] == "90" ) echo "selected"; ?>>90 días</option>
                    <option value="180" <?php if( $_REQUEST['periodo'] == "180") echo "selected"; ?>>180 días</option>
                    <option value="365" <?php if( $_REQUEST['periodo'] == "365") echo "selected"; ?>>365 días</option>
                </select>
            </div>
            <?/*
            <div class="col-md-3 form-group">
                <label for="cliente">Cliente</label>
                <select name="cliente" id="cliente" class="form-control">
                    <option value="0">Seleccione Cliente</option>
                    <?php foreach ($filterClient as $client) { ?>
                        <option value="<?php echo  $client['id_empresa']; ?>"
                            <?php if($client['id_empresa'] == $_REQUEST['cliente']) echo "selected"; ?> >
                            <?php echo $client['nombre_empresa'] ; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            */?>
            <div class="col-md-3 form-group" >
                <label for="zona">Zonas</label>

                <select id="zona" name="zona" class="form-control" >
                    <option value="" selected> Selecciones una Zona </option>
                    <?php foreach ($data['filterZones']as $zone) { ?>
                        <option value="<?php echo  $zone['id_zona']; ?>"
                            <?php if($zone['id_zona'] == $_REQUEST['zona']) echo "selected"; ?> >
                            <?php echo $zone['name'] ; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-2 form-group">
                <button id="buscar" name="buscar" style="margin-top: 22px;" class="btn btn-primary">Buscar </button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <div class="col-md-12"><br></div>
    <div class="row" style='padding-right: 0px;padding-left: 0px;'>
        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <th class="active"  style="width: 170px;background-color: #D3D3D3;">Zona</th>
                <th class="active"  style="background-color: #D3D3D3;">Total Mensual</th>
                <th class="info"    style="background-color: #6296E9; color:#FFF;" >Total Cerrado</th>
                <th class="success" style="background-color: #6296E9; color:#FFF;">Cerrado dentro de SLA</th>
                <th class="danger"  style="background-color: #6296E9; color:#FFF;">Cerrado fuera de SLA</th>
                <th class="info"    style="background-color: #6296E9; color:#FFF;"> % Cerrado dentro de SLA</th>
                <th class="info"    style="background-color: #6296E9; color:#FFF;"> % Cerrado fuera de SLA</th>
                <th class="Warning" style="background-color: #FCA600; color:#FFF;"> Total Pend</th>
                <th class="info"    style="background-color: #D3D3D3;"> Inc</th>
                <th class="info"    style="background-color: #D3D3D3;"> Req</th>
                <th class="success" style="background-color: #008101; color:#FFF;"> Dentro SLA</th>
                <th class="danger"  style="background-color: #B1221F; color:#FFF;"> Fuera SLA</th>
                <th class="info"    style="background-color: #FFFF01;"> Hoy </th>
                <th class="info"    style="background-color: #FFFF01;"> 1 a 3 días </th>
                <th class="info"    style="background-color: #FFFF01;"> 4 a 7 días </th>
                <th class="info"    style="background-color: #FFFF01;"> 2 Sem </th>
                <th class="info"    style="background-color: #FFFF01;"> 3 Sem </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $totalMensual = 0;
            $totalCerrado = 0;
            $totalPend = 0;
            $inc = 0;
            $hoy = 0;
            $unoatres = 0;
            $cuatroasiete = 0;
            $dossem = 0;
            $tressem = 0;

            $totCerradoDentroSla   = 0;
            $totCerradoFueraSla    = 0;
            $totDentroSla   = 0;
            $totFueraSla    = 0;

            $cc = 0;
            foreach ($data['response']['zonas'] as $row) {

                ?>
                <tr>
                    <?php if($row['result']!=null) { ?>
                        <?php if($paramEmp!=null) { ?>
                            <th class="active"><a href="<?php echo base_url()."/monitoring/groupDashInc/".$row['result']['id_zona']."/".$paramEmp; ?>"><?php echo $row['name']; ?></a></th>
                        <?php } else {
                            $_NAV .= $row['result']['id_zona'].",";
                            ?>
                            <th class="active" onclick="firstNivel('<?=$row['result']['id_zona']?>')"><a href="#"><?php echo $row['name']; ?></a></th>
                        <?php } ?>
                    <?php } else { ?>
                        <th class="active" ><?php echo $row['name']; ?></th>
                    <?php } ?>
                    <th class="active"><?php echo $row['result']['Total Mensual']; ?></th>
                    <th class="info" style="background-color: #6296E9; color:#FFF;" ><?php echo $row['result']['Total Cerrado']; ?></th>
                    <th class="success" ><?php echo $row['result']['Cerrado Dentro SLA']; ?></th>
                    <th class="danger" ><?php echo $row['result']['Cerrado Fuera SLA']; ?></th>
                    <th class="info" ><?php
                        if($row['result']['Total Mensual']!=0) {
                            $cc++;
                            echo  round($row['result']['Cerrado Dentro SLA']*100 / $row['result']['Total Cerrado'],2)." %";
                        }?></th>
                    <th class="info" ><?php
                        if($row['result']['Total Mensual']!=0) {
                            echo round($row['result']['Cerrado Fuera SLA']*100 / $row['result']['Total Cerrado'],2)." %";
                        } ?></th>
                    <th class="Warning" style="background-color: #FCA600; color:#FFF;"><?php echo $row['result']['Total Pendiente']; ?></th>
                    <th class="info" ><?php echo $row['result']['Inc']; ?></th>
                    <th class="info" ><?php echo $row['result']['Req']; ?></th>
                    <th class="success" style="background-color: #008101; color:#FFF;"><?php echo $row['result']['Dentro SLA']; ?></th>
                    <th class="danger"  style="background-color: #B1221F; color:#FFF;"><?php echo $row['result']['Fuera SLA']; ?></th>
                    <th class="info" ><?php echo $row['result']['Hoy']; ?></th>
                    <th class="info" ><?php echo $row['result']['1 a 3 dias']; ?></th>
                    <th class="info" ><?php echo $row['result']['4 a 7 dias']; ?></th>
                    <th class="info" ><?php echo $row['result']['2 Sem']; ?></th>
                    <th class="info" ><?php echo $row['result']['3 Sem']; ?></th>
                </tr>
                <?php
                $totalMensual =  $totalMensual + $row['result']['Total Mensual'];
                $totalCerrado =  $totalCerrado + $row['result']['Total Cerrado'];
                $totalPend =  $totalPend + $row['result']['Total Pendiente'];
                $inc =  $inc + $row['result']['Inc'];
                $hoy = $hoy + $row['result']['Hoy'];
                $unoatres = $unoatres + $row['result']['1 a 3 dias'];
                $cuatroasiete = $cuatroasiete + $row['result']['4 a 7 dias'];
                $dossem =  $dossem +  $row['result']['2 Sem'];
                $tressem = $tressem + $row['result']['3 Sem'];

                $totCerradoDentroSla   = $totCerradoDentroSla + $row['result']['Cerrado Dentro SLA'];
                $totCerradoFueraSla    = $totCerradoFueraSla  + $row['result']['Cerrado Fuera SLA'];
                $totDentroSla          = $totDentroSla        + round($row['result']['Cerrado Dentro SLA']*100 / $row['result']['Total Cerrado'],2);
                $totFueraSla           = $totFueraSla         + round($row['result']['Cerrado Fuera SLA']*100 / $row['result']['Total Cerrado'],2);

                $dentrosla = $dentrosla + $row['result']['Dentro SLA'];
                $fuerasla = $fuerasla + $row['result']['Fuera SLA'];
            }

            ?>
            </tbody>
            <tfoot>
            <th class="active"   style="background-color: #046DB9; color:#FFF;">Total General</th>
            <th class="active"  style="background-color: #046DB9; color:#FFF;"><?php echo $totalMensual ?></th>
            <th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $totalCerrado ?></th>
            <th class="success" style="background-color: #6296E9; color:#FFF;"><?php echo $totCerradoDentroSla?></th>
            <th class="danger"  style="background-color: #6296E9; color:#FFF;"><?php echo $totCerradoFueraSla?></th>
            <th class="info"    style="background-color: #6296E9; color:#FFF;"></th>
            <th class="info"    style="background-color: #6296E9; color:#FFF;"></th>
            <th class="Warning" style="background-color: #FCA600; color:#FFF;"><?php echo $totalPend ?></th>
            <th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $inc?></th>
            <th class="info"    style="background-color: #6296E9; color:#FFF;">0</th>
            <th class="success" style="background-color: #008101; color:#FFF;"><?php echo $dentrosla;?></th>
            <th class="danger"  style="background-color: #B1221F; color:#FFF;"><?php echo $fuerasla;?></th>
            <th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $hoy ?></th>
            <th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $unoatres ?></th>
            <th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $cuatroasiete ?></th>
            <th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $dossem ?></th>
            <th class="info"    style="background-color: #6296E9; color:#FFF;"><?php echo $tressem ?></th>
            </tfoot>
        </table>
    </div>
    <div class="row " style='padding-right: 0px;padding-left: 0px;'>
        <div class="col-md-2">
            <div class="col-md-12" style='padding-right: 0px;padding-left: 0px;'>
                <div id="containerCircular3" style="width: 145px; height: 145px; max-width: 170px;margin: 3 auto"></div>
            </div>
            <div class="col-md-12" style='padding-right: 0px;padding-left: 0px;'>
                <div id="containerCircular4" style="width: 145px; height: 145px; max-width: 170px;margin: 3 auto"></div>
            </div>
        </div>
        <div class="col-lg-8" >
            <div class="col-lg-12" >
                <h3 style="margin-left:175px;">Porcentaje Cerrado Fuera De SLA</h3>
            </div>
            <div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
                <div id="container1" style="min-width: 200px; height: 258px; max-width: 320px; margin: 3 auto"></div>
            </div>
            <div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
                <div id="container2" style="min-width: 200px; height: 258px; max-width: 320px;  margin: 3 auto"></div>
            </div>
            <div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
                <div id="container3" style="min-width: 200px; height: 258px; max-width: 320px;  margin: 3 auto"></div>
            </div>
            <div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
                <div id="container4" style="min-width: 200px; height: 258px; max-width: 320px;  margin: 3 auto"></div>
            </div>
	        <div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
		        <div id="container5" style="min-width: 200px; height: 258px; max-width: 320px;  margin: 3 auto"></div>
	        </div>
	        <div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
		        <div id="container6" style="min-width: 200px; height: 258px; max-width: 320px;  margin: 3 auto"></div>
	        </div>
	        <div class="col-lg-3" style='padding-right: 0px;padding-left: 0px;'>
		        <div id="container7" style="min-width: 200px; height: 258px; max-width: 320px;  margin: 3 auto"></div>
	        </div>
        </div>
    </div>
</div>
<br>
<script>
    /*Total */
    containerCircular3();
    containerCircular4();
    /* Gauge 1 */
    <?php
    $x = 0;
    foreach( $data['response']['zonas'] as $zonas) {

    $valueReloj = round($zonas['result']['Cerrado Fuera SLA'] * 100 / $zonas['result']['Total Cerrado'],2);
    if($x == 0 ){
        $div = "container1";
        echo  "container1();";
    }

    if($x == 1 ){
        $div = "container2";
        echo  "container2();";
    }

    if($x == 2 ){
        $div = "container3";
        echo  "container3();";
    }

    if($x == 3 ){
        $div = "container4";
        echo  "container4();";
    }

    if($x == 4 ){
	    $div = "container5";
	    echo  "container5();";
    }

    if($x == 5 ){
	    $div = "container6";
	    echo  "container6();";
    }

    if($x == 6 ){
	    $div = "container7";
	    echo  "container7();";
    }
    $x++;

    ?>
    function <?=$div?>() {
        Highcharts.chart('<?=$div?>', {
                chart: {
                    type: 'gauge',
                    plotBackgroundColor: null,
                    plotBackgroundImage: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                },
                exporting: { enabled: false },
                title: {
                    text: '<?=$zonas['name']?>',
                    style: {
                        color: '#000',
                        fontWeight: 'bold',
                        fontSize: '12px'
                    }
                },
                pane: {
                    startAngle: -150,
                    endAngle: 150,
                    background: [{
                        backgroundColor: {
                            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                [0, '#FFF'],
                                [1, '#333']
                            ]
                        },
                        borderWidth: 0,
                        outerRadius: '109%'
                    }, {
                        backgroundColor: {
                            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                [0, '#333'],
                                [1, '#FFF']
                            ]
                        },
                        borderWidth: 1,
                        outerRadius: '107%'
                    }, {
                        // default background
                    }, {
                        backgroundColor: '#DDD',
                        borderWidth: 0,
                        outerRadius: '105%',
                        innerRadius: '103%'
                    }]
                },
                credits: {
                    enabled: false
                },
                // the value axis
                yAxis: {
                    min: 0,
                    max: 100,
                    minorTickInterval: 'auto',
                    minorTickWidth: 1,
                    minorTickLength: 10,
                    minorTickPosition: 'inside',
                    minorTickColor: '#666',
                    tickPixelInterval: 20,
                    tickWidth: 2,
                    tickPosition: 'inside',
                    tickLength: 10,
                    tickColor: '#666',
                    labels: {
                        step: 2,
                        rotation: 'auto'
                    },
                    title: {
                        text: ''
                    },
                    plotBands: [{
                        from: 0,
                        to: 50,
                        color: '#55BF3B' // green
                    }, {
                        from: 50,
                        to: 75,
                        color: '#DDDF0D' // yellow
                    }, {
                        from: 75,
                        to: 100,
                        color: '#DF5353' // red
                    }]
                },
                series: [{
                    name: 'Speed',
                    data: [<?=(int)$valueReloj?>],
                    tooltip: {
                        valueSuffix: '% Zona'
                    },
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontWeight:'bold',
                            fontSize: '22px'
                        }
                    }
                }]
            },

            function (chart) { });
    }
    <?
    }
    ?>
    /*
    function containeMesaAyuda() {
        Highcharts.chart('containeMesaAyuda', {
            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: 'GT_TELMEX REP. Z RM',
                style: {
                    color: '#000',
                    fontWeight: 'bold',
                    fontSize: '16px'
                }
            },
            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                        backgroundColor: {
                            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                [0, '#FFF'],
                                [1, '#333']
                            ]
                        },
                        borderWidth: 0,
                        outerRadius: '109%'
                    }, {
                        backgroundColor: {
                            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                [0, '#333'],
                                [1, '#FFF']
                            ]
                        },
                        borderWidth: 1,
                        outerRadius: '107%'
                    }, {
                        // default background
                    }, {
                        backgroundColor: '#DDD',
                        borderWidth: 0,
                        outerRadius: '105%',
                        innerRadius: '103%'
                    }]
            },
            credits: {
                enabled: false
            },
            // the value axis
            yAxis: {
                min: 0,
                max: 100,
                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',
                tickPixelInterval: 20,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: ''
                },
                plotBands: [{
                        from: 0,
                        to: 60,
                        color: '#55BF3B' // green
                    }, {
                        from: 60,
                        to: 80,
                        color: '#DDDF0D' // yellow
                    }, {
                        from: 80,
                        to: 100,
                        color: '#DF5353' // red
                    }]
            },
            series: [{
                name: 'Speed',
                data: [35],
                tooltip: {
                    valueSuffix: '% Zona'
                },
                dataLabels: {
                    enabled: true,
                    style: {
                        fontWeight:'bold',
                        fontSize: '22px'
                    }
                }
            }]
// }
        },
// Add some life
                function (chart) {
                    // if (!chart.renderer.forExport) {
                    //     setInterval(function () {
                    //         var point = chart.series[0].points[0],
                    //             newVal,
                    //             inc = Math.round((Math.random() - 0.5) * 20);

                    //         newVal = point.y + inc;
                    //         if (newVal < 0 || newVal > 200) {
                    //             newVal = point.y - inc;
                    //         }

                    //         point.update(newVal);

                    //     }, 3000);
                    // }
                });


    }
    function containerRedDeServicios() {
        Highcharts.chart('containerRedDeServicios', {
            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: 'GT_TELMEX INST. Z RM',
                style: {
                    color: '#000',
                    fontWeight: 'bold',
                    fontSize: '16px'
                }
            },
            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                        backgroundColor: {
                            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                [0, '#FFF'],
                                [1, '#333']
                            ]
                        },
                        borderWidth: 0,
                        outerRadius: '109%'
                    }, {
                        backgroundColor: {
                            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                [0, '#333'],
                                [1, '#FFF']
                            ]
                        },
                        borderWidth: 1,
                        outerRadius: '107%'
                    }, {
                        // default background
                    }, {
                        backgroundColor: '#DDD',
                        borderWidth: 0,
                        outerRadius: '105%',
                        innerRadius: '103%'
                    }]
            },
            credits: {
                enabled: false
            },
            // the value axis
            yAxis: {
                min: 0,
                max: 100,
                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',
                tickPixelInterval: 20,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: ''
                },
                plotBands: [{
                        from: 0,
                        to: 60,
                        color: '#55BF3B' // green
                    }, {
                        from: 60,
                        to: 80,
                        color: '#DDDF0D' // yellow
                    }, {
                        from: 80,
                        to: 100,
                        color: '#DF5353' // red
                    }]
            },
            series: [{
                    name: 'Speed',
                    data: [60],
                    tooltip: {
                        valueSuffix: '% Zona'
                    },
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontWeight:'bold',
                            fontSize: '22px'
                        }
                    }
                }]
// }
        },
        // Add some life
                function (chart) {
                    // if (!chart.renderer.forExport) {
                    //     setInterval(function () {
                    //         var point = chart.series[0].points[0],
                    //             newVal,
                    //             inc = Math.round((Math.random() - 0.5) * 20);

                    //         newVal = point.y + inc;
                    //         if (newVal < 0 || newVal > 200) {
                    //             newVal = point.y - inc;
                    //         }

                    //         point.update(newVal);

                    //     }, 3000);
                    // }
                });
    }
    */
    /* Number With Circle */
    function containerCircular1(){

        Highcharts.chart('containerCircular1', {
            chart: {
                renderTo: 'container',
                type: 'pie'
            },
            exporting: { enabled: false },
            title: {
                text: 'Total Cerrado<br><strong><?=(int)$totalCerrado?></strong>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                style: {
                    fontSize: '14px'
                }
            },
            plotOptions: {
                pie: {
                    shadow: false
                }
            },
            series: [{
                name: 'Total Cerrados',
                data: [
                    [ "Completed", <?=(int)$totalCerrado?>],
                    {
                        "name": "",
                        "y": 40,
                        "color": 'rgba(0,0,0,0)'
                    }
                ],
                size: '100%',
                innerSize: '88%',
                showInLegend:false,
                dataLabels: {
                    enabled: false
                }
            }]
        }, function (chart) {
            // if (!chart.renderer.forExport) {
            //     setInterval(function () {
            //         var point = chart.series[0].points[0],
            //             newVal,
            //             inc = Math.round((Math.random() - 0.5) * 20);

            //         newVal = point.y + inc;
            //         if (newVal < 0 || newVal > 200) {
            //             newVal = point.y - inc;
            //         }

            //         point.update(newVal);

            //     }, 3000);
            // }
        });

    }
    function containerCircular2(){

        Highcharts.chart('containerCircular2', {
            chart: {
                renderTo: 'container',
                type: 'pie'
            },
            exporting: { enabled: false },
            title: {
                text: 'Total Pend<br><strong><?=(int)$totalPend?></strong>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                style: {
                    fontSize: '14px'
                }
            },
            plotOptions: {
                pie: {
                    shadow: false
                }
            },
            series: [{
                name: 'Total Pendientes',
                data: [
                    [ "Completed", <?=(int)$totalPend?>],
                    {
                        "name": "",
                        "y": 40,
                        "color": 'rgba(0,0,0,0)'
                    }
                ],
                size: '100%',
                innerSize: '88%',
                showInLegend:false,
                dataLabels: {
                    enabled: false
                }
            }]
        }, function (chart) {
            // if (!chart.renderer.forExport) {
            //     setInterval(function () {
            //         var point = chart.series[0].points[0],
            //             newVal,
            //             inc = Math.round((Math.random() - 0.5) * 20);

            //         newVal = point.y + inc;
            //         if (newVal < 0 || newVal > 200) {
            //             newVal = point.y - inc;
            //         }

            //         point.update(newVal);

            //     }, 3000);
            // }
        });

    }
    function containerCircular3(){

        Highcharts.chart('containerCircular3', {
            chart: {
                renderTo: 'container',
                type: 'pie'
            },
            exporting: { enabled: false },
            title: {
                text: 'Cerrado<br>Dentro SLA<br><strong><?=(int)$totCerradoDentroSla?></strong>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                style: {
                    fontSize: '14px'
                }
            },
            plotOptions: {
                pie: {
                    shadow: false
                }
            },
            series: [{
                name: 'Cerrado Dentro SLA',
                data: [
                    [ "Completed", <?=(int)$totCerradoDentroSla?>],
                    {
                        "name": "",
                        "y": 40,
                        "color": 'rgba(0,0,0,0)'
                    }
                ],
                size: '100%',
                innerSize: '88%',
                showInLegend:false,
                dataLabels: {
                    enabled: false
                }
            }]
        }, function (chart) {
            // if (!chart.renderer.forExport) {
            //     setInterval(function () {
            //         var point = chart.series[0].points[0],
            //             newVal,
            //             inc = Math.round((Math.random() - 0.5) * 20);

            //         newVal = point.y + inc;
            //         if (newVal < 0 || newVal > 200) {
            //             newVal = point.y - inc;
            //         }

            //         point.update(newVal);

            //     }, 3000);
            // }
        });

    }
    function containerCircular4(){

        Highcharts.chart('containerCircular4', {
            chart: {
                renderTo: 'container',
                type: 'pie'
            },
            exporting: { enabled: false },
            title: {
                text: 'Cerrado<br>Fuera SLA <br><strong><?=(int)$totCerradoFueraSla?></strong>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                style: {
                    fontSize: '14px'
                }
            },
            plotOptions: {
                pie: {
                    shadow: false
                }
            },
            series: [{
                name: 'Cerrado Fuera SLA',
                data: [
                    [ "Completed", <?=(int)$totCerradoFueraSla?>],
                    {
                        "name": "",
                        "y": 40,
                        "color": 'rgba(0,0,0,0)'
                    }
                ],
                size: '100%',
                innerSize: '88%',
                showInLegend:false,
                dataLabels: {
                    enabled: false
                }
            }]
        }, function (chart) {
            // if (!chart.renderer.forExport) {
            //     setInterval(function () {
            //         var point = chart.series[0].points[0],
            //             newVal,
            //             inc = Math.round((Math.random() - 0.5) * 20);

            //         newVal = point.y + inc;
            //         if (newVal < 0 || newVal > 200) {
            //             newVal = point.y - inc;
            //         }

            //         point.update(newVal);

            //     }, 3000);
            // }
        });

    }

</script>