<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>

<div class="container ">
    <div class="row">
        <div class="col-md-6">
            <!-- <?php //echo form_open(site_url('monitoring/index'), array('method'=>'get')); ?>
            <div class="input-group">
                <?php //echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué país deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>                    
                <span class="input-group-btn">
                    <button class="btn btn-default blue" type="submit">Buscar</button>
                </span>
            </div>
            <?php //echo form_close(); ?> -->
        </div>
        <div class="col-md-6 button-content">                               
            <!-- <a href="<?php //echo site_url('monitoring/create'); ?>" class="btn btn-default blue">Crear</a>                 -->
        </div>
    </div>
    <div class="row" style='padding-right: 0px;padding-left: 0px;'>
        <table class="table">
            <thead>
                <tr>
                    <th class="active" >Servicio</th>
                    <th class="active">Total Mensual</th>
                    <th class="info" >Total Cerrado</th>
                    <th class="success" >Cerrado dentro de SLA</th>
                    <th class="danger" >Cerrado fuera de SLA</th>
                    <th class="info" > % Cerrado dentro de SLA</th>
                    <th class="info" > % Cerrado fuera de SLA</th>
                    <th class="Warning" > Total Pend</th>
                    <th class="info" > Inc</th>
                    <th class="info" > Req</th>
                    <th class="success" > Dentro SLA</th>
                    <th class="danger" > Fuera SLA</th>
                    <th class="info" > Hoy </th>
                    <th class="info" > 1 a 3 días </th>
                    <th class="info" > 4 a 7 días </th>
                    <th class="info" > 2 Sem </th>
                    <th class="info" > 3 Sem </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dashInc as $row) { ?>                    
                    <tr>
                        <th class="active" ><a href="<?php echo base_url()."/monitoring/prodZona/".$row['group']; ?>"><?php echo $row['group']; ?></a></th>
                        <th class="active"><?php echo $row['Total Mensual']; ?></th>
                        <th class="info" ><?php echo $row['Total Cerrado']; ?></th>
                        <th class="success" ><?php echo $row['Cerrado Dentro SLA']; ?></th>
                        <th class="danger" ><?php echo $row['Cerrado Fuera SLA']; ?></th>
                        <th class="info" ><?php 
                            if($row['Total Cerrado']!=0) {
                                echo $row['Cerrado Dentro SLA']*100 / $row['Total Cerrado']." %";
                            } else {
                                echo "0 %";
                            }?></th>
                        <th class="info" ><?php 
                            if($row['Total Cerrado']!=0) {
                                echo $row['Cerrado Fuera SLA']*100 / $row['Total Cerrado']." %";
                            } else {
                                echo "0 %";
                            }?></th>
                        <th class="Warning" ><?php echo $row['Total Pendiente']; ?></th>
                        <th class="info" ><?php echo $row['Inc']; ?></th>
                        <th class="info" ><?php echo $row['Req']; ?></th>
                        <th class="success" ><?php echo $row['Dentro SLA']; ?></th>
                        <th class="danger" ><?php echo $row['Fuera SLA']; ?></th>
                        <th class="info" ><?php echo $row['Hoy']; ?></th>
                        <th class="info" ><?php echo $row['1 a 3 dias']; ?></th>
                        <th class="info" ><?php echo $row['4 a 7 dias']; ?></th>
                        <th class="info" ><?php echo $row['2 Sem']; ?></th>
                        <th class="info" ><?php echo $row['3 Sem']; ?></th>              
                    </tr>                    
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

