

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>




<script src="https://code.highcharts.com/highcharts-more.js"></script>



<div class="container ">
	<div class="row">
		<div class="col-md-6">
			<!-- <?php //echo form_open(site_url('monitoring/index'), array('method'=>'get')); ?>
			<div class="input-group">
				<?php //echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué país deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>					
				<span class="input-group-btn">
					<button class="btn btn-default blue" type="submit">Buscar</button>
				</span>
			</div>
			<?php //echo form_close(); ?> -->
		</div>
		<div class="col-md-6 button-content">								
			<!-- <a href="<?php //echo site_url('monitoring/create'); ?>" class="btn btn-default blue">Crear</a>				 -->
		</div>
	</div>
    <div class="row" style='padding-right: 0px;padding-left: 0px;'>
       
         <div class="col-md-12">
           
            <div class="col-md-12" style='padding-right: 0px;padding-left: 0px;'>              
                <div id="containerTiempoSolucionTrazer" style="min-width: 200px; height: 300px; margin: 5 auto"></div>               
            </div>  
        </div>
    </div>
    <div class="row" style='padding-right: 0px;padding-left: 0px;'>
       <br>
       <br>
      
        <table class="table">
        <thead>
         <tr>
                <th class="info">Zona   </th>
                <th class="danger">Total de OT Finalizadas</th>
                <th class="danger">Tiempo Superior a 6 Horas  </th>
                <th class="success">% Finalización más de 6 horas  </th>
                <th class="success">Entre 4 y 6 Horas  </th>
                <th class="success">% Finalización entre 4 y 6 Horas   </th>
                <th class="success">Entre 2 y 4 Horas  </th>
                <th class="success">% Finalización entre 2 y 4 Horas   </th>
                <th class="success">Entre 21 Min y 2 Horas </th>
                <th class="danger">% Finalización entre 21 Min y 2 Horas</th>
                <th class="danger">Tiempo inferior a 20 Minutos   </th>
                <th class="info">% Finalización - 20 Minutos</th>
                <th class="info">OT Canceladas  </th>
                <th class="info">% OT Canceladas    </th>
                <th class="info">Responsable</th>   
          </tr>
        </thead>
        <tbody>
           <tr>
            <td class="">Norte  </td>
            <td class="">1  </td>
            <td class="">0  </td>
            <td class="">0% </td>
            <td class="">0  </td>          
            <td class="">0% </td>
            <td class="">0  </td>
            <td class="">0% </td>
            <td class="">0  </td>
            <td class="">0% </td>
            <td class="">1  </td>
            <td class="">100%   </td>
            <td class="">0  </td>
            <td class="">0% </td>
            <td class="">Felipe Valenzuela</td>            
          </tr>
          <tr>
            <td class="">Centro   </td>
            <td class="">36   </td>
            <td class="">1    </td>
            <td class="">3%   </td>
            <td class="">1    </td>          
            <td class="">3%   </td>
            <td class="">5    </td>
            <td class="">14%      </td>
            <td class="">7    </td>
            <td class="">19%      </td>
            <td class="">22   </td>
            <td class="">61%      </td>
            <td class="">0    </td>
            <td class="">0%   </td>
            <td class="">Felipe Valenzuela  </td>          
          </tr> 
          <tr>
            <td class="">Sur      </td>
            <td class="">375      </td>
            <td class="">24   </td>
            <td class="">6%   </td>
            <td class="">27   </td>          
            <td class="">7%   </td>
            <td class="">46   </td>
            <td class="">12%      </td>
            <td class="">173      </td>
            <td class="">46%      </td>
            <td class="">105      </td>
            <td class="">28%      </td>
            <td class="">0    </td>
            <td class="">0%   </td>
            <td class="">David Giacomozzi  </td>          
          </tr>
          <tr>
           <td class="">Metropolitana    </td>
            <td class="">112     </td>
            <td class="">14  </td>
            <td class="">13%     </td>
            <td class="">4   </td>          
            <td class="">4%  </td>
            <td class="">10  </td>
            <td class="">9%  </td>
            <td class="">46  </td>
            <td class="">41%     </td>
            <td class="">38  </td>
            <td class="">34%     </td>
            <td class="">0   </td>
            <td class="">0%  </td>
            <td class="">Sergio Tapia </td>         
          </tr>  
          <tr>
           <td class="">Claro Instalaciones      </td>
            <td class="">47      </td>
            <td class="">19      </td>
            <td class="">40%         </td>
            <td class="">6       </td>          
            <td class="">13%         </td>
            <td class="">2       </td>
            <td class="">4%      </td>
            <td class="">5       </td>
            <td class="">11%         </td>
            <td class="">15      </td>
            <td class="">32%         </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">Macario Ayala   </td>            
          </tr>   
          <tr>
            <td class="">Claro Reparaciones       </td>
            <td class="">66      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">4       </td>          
            <td class="">6%      </td>
            <td class="">8       </td>
            <td class="">12%         </td>
            <td class="">14      </td>
            <td class="">21%         </td>
            <td class="">40      </td>
            <td class="">61%         </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">Macario Ayala   </td>            
          </tr>   
          <tr>
            <td class="">EUS CORREOS VTR SAVAL        </td>
            <td class="">0       </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>          
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">Victor Hernandez    </td>           
          </tr>  
          <tr>
            <td class="">EUS CORREOS VTR SAVAL        </td>
            <td class="">0       </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>          
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">Victor Hernandez    </td>           
          </tr>  
          <tr>
            <td class="">EUS_BCI_RM      </td>
            <td class="">0       </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>          
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">Carolina Vargas     </td>              
          </tr>   
          <tr>
            <td class="">EUS_BCI_RM      </td>
            <td class="">0       </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>          
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">Carolina Vargas     </td>              
          </tr> 
          <tr>
            <td class="">EUS_REGION METROPOLITANA 2      </td>
            <td class="">141         </td>
            <td class="">19      </td>
            <td class="">13%         </td>
            <td class="">2       </td>          
            <td class="">1%      </td>
            <td class="">2       </td>
            <td class="">1%      </td>
            <td class="">1       </td>
            <td class="">1%      </td>
            <td class="">117         </td>
            <td class="">83%         </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">Luis Suarez     </td>               
          </tr>  
          <tr>
            <td class="">EUS_RESIDENTE FALABELLA         </td>
            <td class="">0       </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>          
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">Jorge Quiroz    </td>                
          </tr> 
          <tr>
            <td class="">EUS_SERVIDORES Y COMUNICACIONES         </td>
            <td class="">0       </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>          
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">0       </td>
            <td class="">0%      </td>
            <td class="">Pablo Navarrete     </td>                
          </tr>   
          <tr>
            <td class="info">Total General       </td>
            <td class="info">778         </td>
            <td class="info">77      </td>
            <td class="info">9,9%        </td>
            <td class="info">44      </td>          
            <td class="info">5,7%        </td>
            <td class="info">73      </td>
            <td class="info">9,4%        </td>
            <td class="info">246         </td>
            <td class="info">31,6%       </td>
            <td class="info">338         </td>
            <td class="info">43,4%       </td>
            <td class="info">0       </td>
            <td class="info">0,0%        </td>
            <td class="info">    </td>                
          </tr>   
           
         
        </tbody>
      </table>
        
       
    </div>
   

  
</div>

<script>



  $( document ).ready(function() {
    containerGlobal('containerTiempoSolucionTrazer','Técnico Residente',99.51,0.49);

    console.log('aca');

});
 



function containerGlobal(container,title,dentro,fuera){
    Highcharts.chart(container, {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Distribución de Tiempos de Solución de Tareas en Trazer'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
     credits: {
          enabled: false
      },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Tiempo superior a 6 horas',
            y: 18.5,
            color:'#f2dede'
        }, {
            name: 'Entre 4 y 6 horas ',
            y: 5.6,
            sliced: true,
            selected: true,
            color:'#dff0d8'
        }, {
            name: 'Entre 2 y 4 horas',
            y: 15.5
        }, {
            name: 'Entre 21 min y 2 Horas',
            y: 28.2
        }, {
            name: 'Tiempo Inferior a 20 Minutos',
            y: 31.8,
            color:'#d9edf7'
        }, {
            name: 'OT Cancelada',
              y: 0
         }]
        }]
    });
     
}





</script>