<!-- <pre>
<?php
// print_r($estados[0]);
// print_r($sinAsig[0]);
 //print_r($sla);
?>
</pre> -->

<script>


var stados = <?php print_r(json_encode($estados[0]));?>;
var sinAsig = <?php print_r(json_encode($sinAsig[0]));?>;
var sla = <?php print_r(json_encode($sla));?>;

console.log(stados);
</script>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>




<script src="https://code.highcharts.com/highcharts-more.js"></script>



<div class="container ">
	<div class="row">
		<div class="col-md-6">
			<!-- <?php //echo form_open(site_url('monitoring/index'), array('method'=>'get')); ?>
			<div class="input-group">
				<?php //echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué país deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>					
				<span class="input-group-btn">
					<button class="btn btn-default blue" type="submit">Buscar</button>
				</span>
			</div>
			<?php //echo form_close(); ?> -->
		</div>
		<div class="col-md-6 button-content">								
			<!-- <a href="<?php //echo site_url('monitoring/create'); ?>" class="btn btn-default blue">Crear</a>				 -->
		</div>
	</div>
    <div class="row" style='padding-right: 0px;padding-left: 0px;'>
      <table class="table">
        <thead>
          <tr>
            <th class="active" >Servicio</th>
            <th class="active">Total Mensual</th>
            <th class="info" >Total Cerrado</th>
            <th class="success" >Cerrado dentro de SLA</th>
            <th class="danger" >Cerrado fuera de SLA</th>
            <th class="info" > % Cerrado dentro de SLA</th>
            <th class="info" > % Cerrado fuera de SLA</th>
            <th class="Warning" > Total Pend</th>
            <th class="info" > Inc</th>
            <th class="info" > Req</th>
            <th class="success" > Dentro SLA</th>
            <th class="danger" > Fuera SLA</th>
            <th class="info" > Hoy </th>
            <th class="info" > 1 a 3 días </th>
            <th class="info" > 4 a 7 días </th>
            <th class="info" > 2 Sem </th>
            <th class="info" > 3 Sem </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="active">Grupo Resolutor 1</td>
            <td>101</td>
            <td class="info" >40</td>
            <td class="success" >37</td>
            <td class="danger" >3</td>          
            <td class="info" >92,5%</td>
            <td class="info" >7,5%</td>
            <td class="Warning" >61</td>
            <td class="info" >35</td>
            <td class="info" >26</td>
            <td class="success" >55</td>
            <td class="danger" >6</td>
            <td class="info" >1</td>
            <td class="info" >12</td>
            <td class="info" >13</td>
            <td class="info" >29</td>
            <td class="info" >33</td>
            
          </tr>   
           <tr>
            <td class="active">Grupo Resolutor 2</td>
            <td>1.459</td>
            <td class="info" >1.335</td>
            <td class="success" >1.251</td>
            <td class="danger" >84</td>          
            <td class="info" >93,7%</td>
            <td class="info" >6,3%</td>
            <td class="Warning" >124</td>
            <td class="info" >33</td>
            <td class="info" >91</td>
            <td class="success" >107</td>
            <td class="danger" >17</td>
            <td class="info" >1</td>
            <td class="info" >55</td>
            <td class="info" >23</td>
            <td class="info" >34</td>
            <td class="info" >50</td>
            
          </tr>     
          <tr>
            <td class="active">Grupo Resolutor 3</td>
            <td>2.229</td>
            <td class="info" >1.408</td>
            <td class="success" >1.385</td>
            <td class="danger" >23</td>          
            <td class="info" >98,4%</td>
            <td class="info" >1,6%</td>
            <td class="Warning" >891</td>
            <td class="info" >135</td>
            <td class="info" >756</td>
            <td class="success" >874</td>
            <td class="danger" >17</td>
            <td class="info" >29</td>
            <td class="info" >254</td>
            <td class="info" >241</td>
            <td class="info" >265</td>
            <td class="info" >313</td>
            
          </tr>       
         
        </tbody>
      </table>
    </div>
    <div class="row" style='padding-right: 0px;padding-left: 0px;'>
       
         <div class="col-md-4">
            <br>
            <div class="col-md-12" style='padding-right: 0px;padding-left: 0px;'>              
                <div id="containerCerradoPendienteultimoMes" style="min-width: 200px; height: 200px; max-width: 600px; margin: 5 auto"></div>               
            </div>  
        </div>
         <div class="col-md-8" >      
        OS PENDIENTES FUERA DE SLA ÚLTIMO MES POR SERVICIO
        <br>
            <div class="col-md-4" style='padding-right: 0px;padding-left: 0px;'>                
                <div id="containerLab" style="min-width: 200px; height: 200px; max-width: 600px; margin: 5 auto"></div>               
            </div> 
             <div class="col-md-4" style='padding-right: 0px;padding-left: 0px;'>                
                <div id="containeMesaAyuda" style="min-width: 200px; height: 200px; max-width: 600px; margin: 5 auto"></div>               
            </div> 
            <div class="col-md-4" style='padding-right: 0px;padding-left: 0px;'>                
                <div id="containerRedDeServicios" style="min-width: 200px; height: 200px; max-width: 600px; margin: 5 auto"></div>               
            </div> 
            <!--  <div class="col-md-3" style='padding-right: 0px;padding-left: 0px;'>                
                <div id="containerTecnicoResidente" style="min-width: 200px; height: 200px; max-width: 600px; margin: 5 auto"></div>               
            </div>  -->
        </div>  
       
    </div>
    <br>
    <div class="row" style='padding-right: 0px;padding-left: 0px;'>
       
         <div class="col-md-4">
            <br>
            <div class="col-md-12" style='padding-right: 0px;padding-left: 0px;'>              
                <div id="containerPendientesDentroFueraultimoMes" style="min-width: 200px; height: 200px; max-width: 600px; margin: 5 auto"></div>               
            </div>  
        </div>
         <div class="col-md-8" >      
        OS DENTRO Y FUERA DE SLA POR SERVICIO ÚLTIMO MES 
        <br>
            <div class="col-md-4" style='padding-right: 0px;padding-left: 0px;'>                
                <div id="containerLabDentroyFuera" style="min-width: 200px; height: 200px; max-width: 600px; margin: 5 auto"></div>               
            </div> 
             <div class="col-md-4" style='padding-right: 0px;padding-left: 0px;'>                
                <div id="containeMesaAyudaDentroyFuera" style="min-width: 200px; height: 200px; max-width: 600px; margin: 5 auto"></div>               
            </div> 
            <div class="col-md-4" style='padding-right: 0px;padding-left: 0px;'>                
                <div id="containerRedDeServiciosDentroyFuera" style="min-width: 200px; height: 200px; max-width: 600px; margin: 5 auto"></div>               
            </div> 
            <!--  <div class="col-md-3" style='padding-right: 0px;padding-left: 0px;'>                
                <div id="containerTecnicoResidenteDentroyFuera" style="min-width: 200px; height: 200px; max-width: 600px; margin: 5 auto"></div>               
            </div>  -->
        </div>  
       
    </div>
    <br>
    <br>
    <br>
	<div class="row">
		<div class="col-md-6">				
			<div id="container3" style="min-width: 310px; height: 260px; max-width: 600px; margin: 0 auto"></div>				
		</div>	
		<div class="col-md-6">                
            <div id="container2" style="min-width: 310px; height: 260px; max-width: 600px; margin: 0 auto"></div>               
        </div> 
        <div class="col-md-6">				
			<div id="container" style="min-width: 310px; height: 260px; max-width: 600px; margin: 0 auto"></div>				
		</div>	
	</div>
</div>

<script>



$(document).ready(function () {

    // Build the chart

    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Tickets Asignados/Sin Asignar'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Tickets no Asignados',
                y: parseInt(sinAsig.sinasignar),
                color: '#FF0000' 
               							
            }, {
                name: 'Tickets Asignados',
                y: parseInt(sinAsig.asignados),
                color: '#55BF3B', 
                sliced: true,
                selected: true
            }]
        }]
    });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Build the chart
    Highcharts.chart('container2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Gráfico por estados'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'abiertos',
                y: parseInt(stados.abierto)
            }, {
                name: 'recibido',
                y: parseInt(stados.recibido)
            }, {
                name: 'en progreso',
                y: parseInt(stados.progress)
            }, {
                name: 'en el sitio',
                y: parseInt(stados.insite)
            }, {
                name: 'resuelto',
                y: parseInt(stados.resuelto)
            }, {
                name: 'cerrado',
                y: parseInt(stados.cerrado),
                sliced: true,
                selected: true
            }]
        }]
    });
//////////////////////////////////////////////////////////////////////////////////////////////////////
    Highcharts.chart('container3', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'SLA OK'
    },
    subtitle: {
        text: 'Simple data'
    },
    xAxis: {
        categories: ['Abiertos', 'Cerrados'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'cantidad de ticket',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' tickets'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    // legend: {
    //     layout: 'vertical',
    //     align: 'right',
    //     verticalAlign: 'top',
    //     x: -40,
    //     y: 80,
    //     floating: true,
    //     borderWidth: 1,
    //     backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
    //     shadow: true
    // },

    // [contCerradoAtiempo] => 0
    // [constCerradoTrde] => 0
    // [contAbiertosOk] => 0
    // [contAbiertosNOK] => 12

    credits: {
        enabled: false
    },
    series: [{
        name: 'ok',
        data: [parseInt(sla.contAbiertosOk), parseInt(sla.contCerradoAtiempo)],
        color: '#55BF3B'
    }, {
        name: 'no ok',
        data: [parseInt(sla.contAbiertosNOK), parseInt(sla.constCerradoTrde)],
        color: '#FF0000',
    }]
});

});

  containerCerradoPendienteultimoMes();
  containerLab();
  containeMesaAyuda();
  containerRedDeServicios();
  //containerTecnicoResidente();

  containerPendientesDentroFueraultimoMes();


  containerGlobal('containerLabDentroyFuera','Grupo Resolutor 1',90.16,9.84);
  containerGlobal('containeMesaAyudaDentroyFuera','Grupo Resolutor 2',86.29,13.21);
  containerGlobal('containerRedDeServiciosDentroyFuera','Grupo Resolutor 3',98.09,1.01);
 // containerGlobal('containerTecnicoResidenteDentroyFuera','Técnico Residente',99.51,0.49);



function containerCerradoPendienteultimoMes(){
    Highcharts.chart('containerCerradoPendienteultimoMes', {
     // Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'GRÁFICA OS CERRADOS VS OS PENDIENTES ÚLTIMO MES'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
     credits: {
          enabled: false
      },
     series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Cerrados',
            y: 56.33,
            color: '#55BF3B'
            
        }, {
            name: 'Pendientes',
            y: 24.03,
            color: '#FF0000',
            sliced: true,
            selected: true
        }]
    }]
});
}

function containerGlobal(container,title,dentro,fuera){
    Highcharts.chart(container, {
     // Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: title
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
     credits: {
          enabled: false
      },
     series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Dentro SLA',
            y: dentro,
            color: '#55BF3B'
        }, {
            name: 'Fuera SLA',
            color: '#FF0000',
            y: fuera,
            sliced: true,
            selected: true
        }]
    }]
});
}


function containerPendientesDentroFueraultimoMes(){
    Highcharts.chart('containerPendientesDentroFueraultimoMes', {
     // Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'GLOBAL OS PENDIENTES DENTRO Y FUERA DE SLA ÚLTIMO MES'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
     credits: {
          enabled: false
      },
     series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Dentro SLA',
            y: 97.30,
            color: '#55BF3B'
        }, {
            name: 'Fuera SLA',
            y: 3.60,
            color: '#FF0000',
            sliced: true,
            selected: true
        }]
    }]
});
}

function containerLab(){
    Highcharts.chart('containerLab', {

    chart: {
        type: 'gauge',
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
    },

    title: {
        text: 'Grupo Resolutor 1'
    },

    pane: {
        startAngle: -150,
        endAngle: 150,
        background: [{
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, '#FFF'],
                    [1, '#333']
                ]
            },
            borderWidth: 0,
            outerRadius: '109%'
        }, {
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, '#333'],
                    [1, '#FFF']
                ]
            },
            borderWidth: 1,
            outerRadius: '107%'
        }, {
            // default background
        }, {
            backgroundColor: '#DDD',
            borderWidth: 0,
            outerRadius: '105%',
            innerRadius: '103%'
        }]
    },
     credits: {
          enabled: false
      },

    // the value axis
    yAxis: {
        min: 0,
        max: 100,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
            step: 2,
            rotation: 'auto'
        },
        title: {
            text: ''
        },
        plotBands: [{
            from: 0,
            to: 60,
            color: '#55BF3B' // green
        }, {
            from: 60,
            to: 80,
            color: '#DDDF0D' // yellow
        }, {
            from: 80,
            to: 100,
            color: '#DF5353' // red
        }]
    },

    series: [{
        name: 'Laboratorio',
        data: [6],
        tooltip: {
            valueSuffix: ''
        }
    }]
// }
},
// Add some life
function (chart) {
    // if (!chart.renderer.forExport) {
    //     setInterval(function () {
    //         var point = chart.series[0].points[0],
    //             newVal,
    //             inc = Math.round((Math.random() - 0.5) * 20);

    //         newVal = point.y + inc;
    //         if (newVal < 0 || newVal > 200) {
    //             newVal = point.y - inc;
    //         }

    //         point.update(newVal);

    //     }, 3000);
    // }
});

      
}

function containeMesaAyuda(){
    Highcharts.chart('containeMesaAyuda', {

    chart: {
        type: 'gauge',
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
    },

    title: {
        text: 'Grupo Resolutor 2'
    },

    pane: {
        startAngle: -150,
        endAngle: 150,
        background: [{
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, '#FFF'],
                    [1, '#333']
                ]
            },
            borderWidth: 0,
            outerRadius: '109%'
        }, {
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, '#333'],
                    [1, '#FFF']
                ]
            },
            borderWidth: 1,
            outerRadius: '107%'
        }, {
            // default background
        }, {
            backgroundColor: '#DDD',
            borderWidth: 0,
            outerRadius: '105%',
            innerRadius: '103%'
        }]
    },
     credits: {
      enabled: false
    },
    // the value axis
    yAxis: {
        min: 0,
        max: 100,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
            step: 2,
            rotation: 'auto'
        },
        title: {
            text: ''
        },
        plotBands: [{
            from: 0,
            to: 60,
            color: '#55BF3B' // green
        }, {
            from: 60,
            to: 80,
            color: '#DDDF0D' // yellow
        }, {
            from: 80,
            to: 100,
            color: '#DF5353' // red
        }]
    },

    series: [{
        name: 'Mesa de Ayuda',
        data: [17],
        tooltip: {
            valueSuffix: ''
        }
    }]
// }
},
// Add some life
function (chart) {
    // if (!chart.renderer.forExport) {
    //     setInterval(function () {
    //         var point = chart.series[0].points[0],
    //             newVal,
    //             inc = Math.round((Math.random() - 0.5) * 20);

    //         newVal = point.y + inc;
    //         if (newVal < 0 || newVal > 200) {
    //             newVal = point.y - inc;
    //         }

    //         point.update(newVal);

    //     }, 3000);
    // }
});

      
}

function containerRedDeServicios(){
    Highcharts.chart('containerRedDeServicios', {

    chart: {
        type: 'gauge',
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
    },

    title: {
        text: 'Grupo Resolutor 3'
    },

    pane: {
        startAngle: -150,
        endAngle: 150,
        background: [{
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, '#FFF'],
                    [1, '#333']
                ]
            },
            borderWidth: 0,
            outerRadius: '109%'
        }, {
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, '#333'],
                    [1, '#FFF']
                ]
            },
            borderWidth: 1,
            outerRadius: '107%'
        }, {
            // default background
        }, {
            backgroundColor: '#DDD',
            borderWidth: 0,
            outerRadius: '105%',
            innerRadius: '103%'
        }]
    },
     credits: {
      enabled: false
    },
    // the value axis
    yAxis: {
        min: 0,
        max: 100,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
            step: 2,
            rotation: 'auto'
        },
        title: {
            text: ''
        },
        plotBands: [{
            from: 0,
            to: 60,
            color: '#55BF3B' // green
        }, {
            from: 60,
            to: 80,
            color: '#DDDF0D' // yellow
        }, {
            from: 80,
            to: 100,
            color: '#DF5353' // red
        }]
    },

    series: [{
        name: 'Red De Servicios',
        data: [17],
        tooltip: {
            valueSuffix: ''
        }
    }]
// }
    },
    // Add some life
    function (chart) {
        // if (!chart.renderer.forExport) {
        //     setInterval(function () {
        //         var point = chart.series[0].points[0],
        //             newVal,
        //             inc = Math.round((Math.random() - 0.5) * 20);

        //         newVal = point.y + inc;
        //         if (newVal < 0 || newVal > 200) {
        //             newVal = point.y - inc;
        //         }

        //         point.update(newVal);

        //     }, 3000);
        // }
    });

      
}

function containerTecnicoResidente(){
    Highcharts.chart('containerTecnicoResidente', {

    chart: {
        type: 'gauge',
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
    },

    title: {
        text: 'Técnico Residente'
    },

    pane: {
        startAngle: -150,
        endAngle: 150,
        background: [{
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, '#FFF'],
                    [1, '#333']
                ]
            },
            borderWidth: 0,
            outerRadius: '109%'
        }, {
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, '#333'],
                    [1, '#FFF']
                ]
            },
            borderWidth: 1,
            outerRadius: '107%'
        }, {
            // default background
        }, {
            backgroundColor: '#DDD',
            borderWidth: 0,
            outerRadius: '105%',
            innerRadius: '103%'
        }]
    },
     credits: {
      enabled: false
    },
    // the value axis
    yAxis: {
        min: 0,
        max: 100,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
            step: 2,
            rotation: 'auto'
        },
        title: {
            text: ''
        },
        plotBands: [{
            from: 0,
            to: 60,
            color: '#55BF3B' // green
        }, {
            from: 60,
            to: 80,
            color: '#DDDF0D' // yellow
        }, {
            from: 80,
            to: 100,
            color: '#DF5353' // red
        }]
    },

    series: [{
        name: 'Técnico Residente',
        data: [1],
        tooltip: {
            valueSuffix: ''
        }
    }]
// }
    },
    // Add some life
    function (chart) {
        // if (!chart.renderer.forExport) {
        //     setInterval(function () {
        //         var point = chart.series[0].points[0],
        //             newVal,
        //             inc = Math.round((Math.random() - 0.5) * 20);

        //         newVal = point.y + inc;
        //         if (newVal < 0 || newVal > 200) {
        //             newVal = point.y - inc;
        //         }

        //         point.update(newVal);

        //     }, 3000);
        // }
    });

      
}
</script>



<script type="text/javascript">
	$(function(){
		<?php if ($search_value): ?>			
			$("#search-value").focus();
			$("#search-value").val($("#search-value").val());
		<?php endif ?>

		/*Al cargar la web selecciona todos los elementos según el estado del principal*/
		if($('#select-all').prop('checked')){
			$('.checkbox-element').prop('checked', true);
			addDeleteButton();
		}else{
			$('.checkbox-element').prop('checked', false);
			$('#delete-button').remove();			
		}
		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		if($('input.checkbox-element:checked').length > 0){
			addDeleteButton();
		}

		/*Función del botón Aceptar del modal al eliminar una categoría*/
		$(document).on('click', '#accept-button-modal.deletemonitoring', function(){
			$param = ($(this).data('param'));
			$form = $('#list-form');
			if($param != null){
				$('.checkbox-element').prop('checked', false);
				$form.append($('<input/>').attr({"type":"hidden", "name":"monitoring[monitoring_id][]", "value":$param}));		             
			}
            $form.submit();
            $('#accept-button-modal').removeClass('deletemonitoring');
			$('#accept-button-modal').removeData('param');
			$('#normal-modal').modal('hide');            
		})

		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		$('#list-form').on('change', function(){			
			if($('input.checkbox-element:checked').length > 0){
				addDeleteButton();
			}else{
				$('#delete-button').remove();				
			}
		});

		/*Selecciona todos los elementos según el estado del principal*/
		$('#select-all').on('change', function(){
			if($(this).prop('checked')){
				$('.checkbox-element').prop('checked', true);				
			}else{
				$('.checkbox-element').prop('checked', false);
				$('#delete-button').remove();				
			}
		});

		/* Crea el botón eliminar */
		function addDeleteButton(){
			if($('.menu-content .button-content').find('#delete-button').length == 0){
				$deleteButton = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'delete-button',"data-title":"Eliminar país(s)" ,"data-message":"¿Estás seguro que deseas eliminar el(las) país(s) seleccionada(s)?", "data-function":"deletemonitoring", "data-toggle":"modal", "data-target":"#normal-modal"}).html('Eliminar');				
				$('.menu-content .button-content').prepend($deleteButton);	
			}			
		}


		

		
	});
</script>