<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script async type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script async type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script async type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script async type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script async type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script async type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script async type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>


<script async type="text/javascript">
    $(document).ready(function() {
        // Activamos las funciones
        var url = document.location.origin;
        var contenedorResultado = $('[data-model=container]');
        var cajaBusqueda = $('[data-model=caja-busqueda]');
        var estructura = contenedorResultado.find('[data-model=struct-model]').clone();
        var selectCliente = cajaBusqueda.find("[data-attribute=select-cliente]");
        var selectZona = cajaBusqueda.find("[data-attribute=select-zona]");
        var selectPeriodo = cajaBusqueda.find("[data-attribute=select-periodo]");
        var data = {};
        var step = 0;

        function setFilter(respuesta){
            var grupos = respuesta.data.grupos;
            var clientes = respuesta.data.clientes;
            var zonas = respuesta.data.zonas;
            data = respuesta.data;

            setSelectDom(selectCliente, clientes, 'Cliente');
            setSelectDom(selectZona, zonas, 'Zona');

            // cajaBusqueda.find('[data-model=select-zona]').after(cajaBusqueda.find('[data-model=select-cliente]'));
            // cajaBusqueda.find('[data-model=select-cliente]').after(cajaBusqueda.find('[data-model=select-zona]'));
        }
        function setBusqueda(respuesta){
            console.log('Set Respuesta Busqueda');
            var total = 0;
            var totalAsignados   = 0;
            var totalNoAsignados = 0;
            var totalCerrados    = 0;
            contenedorResultado.empty();
            console.log(respuesta.data);

            objClientes = estructura.clone();
            objClientes.find('[data-attribute=head-title]').html(respuesta.data.head_title);
            objClientes.find('[data-attribute=title]').html(respuesta.data.titulo);
            objTabla = objClientes.find('tbody tr');
            objClientes.find('tbody tr').remove();

            $.each(respuesta.data.clientes, function(i, cliente){
                var dato = objTabla.clone();

                console.log(cliente);

                dato.find('[data-attribute=nombre]').attr('data-id', cliente.id);
                dato.find('[data-attribute=nombre]').html(cliente.nombre);
                dato.find('[data-attribute=total-tickets]').html(cliente.total);
                dato.find('[data-attribute=total-asignados-porcentaje]').html(getPercentValue(cliente.total,cliente.asignados) + '%');
                dato.find('[data-attribute=total-asignados]').html(cliente.asignados);
                dato.find('[data-attribute=total-noasignados-porcentaje]').html(getPercentValue(cliente.total,cliente.no_asignados) + '%');
                dato.find('[data-attribute=total-noasignados]').html(cliente.no_asignados);
                dato.find('[data-attribute=total-cerrados-porcentaje]').html(getPercentValue(cliente.total,cliente.cerrados) + '%');
                dato.find('[data-attribute=total-cerrados]').html(cliente.cerrados);
                dato.find('[data-attribute=responsable]').html('no data');

                dato.find('[data-attribute=nombre]').on('click',function(e){
                    step = step + 1;
                    console.log(" Pasos : "+ step);
                    console.log(" data-id : "+ $(this).attr('data-id'));
                    switch(step) {
                        case 1: activarBuscarZona($(this).attr('data-id')); break;
                        case 2: activarBuscarGrupo($(this).attr('data-id')); break;
                        case 3: activarBuscarTecnico($(this).attr('data-id')); break;
                        case 4: activarBuscarIncident($(this).attr('data-id')); break;

                        case 5: e.preventDefault(); break;
                    }
                });

                total = parseInt(total) + parseInt(cliente.total);
                totalAsignados   = parseInt(totalAsignados)   + parseInt(cliente.asignados);
                totalNoAsignados = parseInt(totalNoAsignados) + parseInt(cliente.no_asignados);
                totalCerrados    = parseInt(totalCerrados)    + parseInt(cliente.cerrados);

                objClientes.find('tbody').append(dato);
            });
            if(respuesta.data.clientes.length > 0){
                var dato = objTabla.clone();
                dato.find('[data-attribute=nombre]').html('<strong>Total</strong>');
                dato.find('[data-attribute=total-tickets]').html('<strong>' + total + '</strong>');
                dato.find('[data-attribute=total-asignados]').html('<strong>' + totalAsignados + '</strong>');
                dato.find('[data-attribute=total-asignados-porcentaje]').html('<strong>' + getPercentValue(total,totalAsignados) + '%</strong>');
                dato.find('[data-attribute=total-noasignados]').html('<strong>' + totalNoAsignados + '</strong>');
                dato.find('[data-attribute=total-noasignados-porcentaje]').html('<strong>' + getPercentValue(total,totalNoAsignados) + '%</strong>');
                dato.find('[data-attribute=total-cerrados]').html('<strong>' + totalCerrados + '</strong>');
                dato.find('[data-attribute=total-cerrados-porcentaje]').html('<strong>' + getPercentValue(total,totalCerrados) + '%</strong>');

                dato.find('[data-attribute=responsable]').html('');

                objClientes.find('tbody').append(dato);

                contenedorResultado.append(objClientes);
            }
            if(respuesta.data.clientes.length == 0){
                swal("Mensaje iTr@ces", "Sin Datos !! ", "info");
            }
        }
        function setSelectDom(select, data, controlNombre){
            option = null;
            option = select.find('option');
            select.empty();
            var dom = option.clone();
            dom.html('-- Todos --');
            dom.attr('value',0);
            select.append(dom);
            dom = null;
            $.each(data, function(i,value){
                var dom = option.clone();
                dom.html(value.nombre);
                dom.attr('value',value.id);
                select.append(dom);
                dom = null;
            });
        }
        function getPercentValue(total, valor){
            porcentaje = (valor*100) / total;

            if(isNaN(porcentaje))
                porcentaje = 0;
            return parseFloat(porcentaje).toFixed(2);
        }
        function getFiltros(){
            resp = processAjax('POST',url + '/auris/filtros','application/json', {}, 'json',false);
            resp.done(function(respuesta){
                console.log('Respuesta correcta',respuesta);
                setFilter(respuesta);
            }).fail(function(xhr){
                console.error('Error en el controlador');
                console.log('ERROR:', xhr);
            }).always(function(){
                console.log('Proceso Finalizado');
            });
        }
        function getValidation(){
            periodo  = selectPeriodo.val();
            isValid = true;

            if(periodo == "0"){
                swal("Mensaje iTr@ces", "Debe seleccionar un periodo.", "info");
                isValid = false;
            }
            return isValid;
        }
        function processAjax(type, url, contentType, data, dataType, async=true){
            console.info('Util::Procesamos AJAX');
            return $.ajax({
                type: type,
                url: url,
                data: data,
                contentType: contentType,
                dataType: dataType,
                async:async,
                cache: false,
            });
        }
        function activaBuscar(){

            $('#buscar').click(function(e){
                $('#struct-box').show();
                step = 0;
                console.log('activarBusqueda : step ='+step);
                e.preventDefault();
                console.log('Comenzamos la busqueda');

                filtro = {
                    'periodo' : selectPeriodo.val(),
                    'cliente' : selectCliente.val(),
                    'zona' : selectZona.val()
                };
                valid = getValidation();

                if(valid){
                    resp = processAjax('GET',url + '/auris/totalesByCliente','application/json', filtro, 'json',false);
                    resp.done(function(respuesta){
                        console.log('Respuesta correcta',respuesta);
                        setBusqueda(respuesta);
                        // setFilter(respuesta);
                    }).fail(function(xhr){
                        console.error('Error en el controlador');
                        console.log('ERROR:', xhr);
                    }).always(function(){
                        console.log('Proceso Finalizado');
                    });

                    console.log('Terminamos la busqueda');
                }
            });
        }
        function activarBuscarZona(id){
            console.log('Comenzamos la busqueda por pasos : activarBuscarZona() ');
            filtro = {
                'periodo' : selectPeriodo.val(),
                'cliente' : id,
                'zona' : selectZona.val()
            };

            valid = getValidation();

            if(valid) {
                resp = processAjax('GET', url + '/auris/totalesByZona', 'application/json', filtro, 'json', false);
                resp.done(function (respuesta) {
                    console.log('Respuesta correcta', respuesta);
                    setBusqueda(respuesta);
                    // setFilter(respuesta);
                }).fail(function (xhr) {
                    console.error('Error en el controlador');
                    console.log('ERROR:', xhr);
                }).always(function () {
                    console.log('Proceso Finalizado');
                });

                console.log('Terminamos la busqueda');
            }
        }
        function activarBuscarGrupo(id){
            console.log('Comenzamos la busqueda por pasos : activarBuscarGrupo('+id+') ');
            filtro = {
                'periodo' : selectPeriodo.val(),
                'cliente' : id,
                'zona' : selectZona.val()
            };

            valid = getValidation();

            if(valid) {
                resp = processAjax('GET', url + '/auris/totalesByGrupo', 'application/json', filtro, 'json', false);
                resp.done(function (respuesta) {
                    console.log('Respuesta correcta', respuesta);
                    setBusqueda(respuesta);
                    // setFilter(respuesta);
                }).fail(function (xhr) {
                    console.error('Error en el controlador');
                    console.log('ERROR:', xhr);
                }).always(function () {
                    console.log('Proceso Finalizado');
                });

                console.log('Terminamos la busqueda');
            }
        }
        function activarBuscarTecnico(id){
            console.log('Comenzamos la busqueda por pasos : activarBuscarTecnico() ');
            filtro = {
                'periodo' : selectPeriodo.val(),
                'cliente' : id,
                'zona' : selectZona.val()
            };

            valid = getValidation();

            if(valid) {
                resp = processAjax('GET', url + '/auris/totalesByTecnico', 'application/json', filtro, 'json', false);
                resp.done(function (respuesta) {
                    console.log('Respuesta correcta', respuesta);
                    setBusqueda(respuesta);
                    // setFilter(respuesta);
                }).fail(function (xhr) {
                    console.error('Error en el controlador');
                    console.log('ERROR:', xhr);
                }).always(function () {
                    console.log('Proceso Finalizado');
                });

                console.log('Terminamos la busqueda');
            }
        }
        function activarBuscarIncident(id){
            console.log('Cambio de vista a Tickects');
            filtro = { 'cliente' : id };

            cajaBusqueda.hide();
            contenedorResultado.hide();
            //estructura.hide();
	
	
	        if ($.fn.DataTable.isDataTable("#tableIncidentForTec")) {
		        $('#tableIncidentForTec').DataTable().clear().destroy();
	        }
	        
            $('#incidentForTec').show();
	         $.fn.dataTable.ext.errMode = 'none';
            $('#tableIncidentForTec').DataTable( {
	             "paging" : false,
	             "searching" : false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },
                "ajax": {
                    "url": "<?php echo site_url("auris/totalesByIncident") ?>",
                    "type": 'GET',
                    "data": filtro
                },
                "columns": [
                    { "data": "Numero" },
                    { "data": "Tipo" },
                    { "data": "Titulo" },
                    { "data": "Estado" },
                    { "data": "Bitacora" },
                    { "data": "Grupo Resolutor" },
                    { "data": "Tecnico" },
                    { "data": "Ubicacion" },
                    { "data": "Fecha Creación" },
                    { "data": "Fecha Integracion" },
                    { "data": "Fecha Asignado" },
                    { "data": "Fecha Cierre Ticket" },
                    { "data": "Fecha Resolucion" },
                    { "data": "Tiempo Coordinación" },
                    { "data": "Tiempo de Atención Terreno" },
                    { "data": "Tiempo Integracion" }
                ],
                buttons: [
                    'csv',
                    'excel'
                ]
            });
	
	       
	
	        console.log('Terminamos la busqueda y pintar datos');

            $('#btn-back-prd').click(function(e){
	            
                $('#incidentForTec').hide();
                cajaBusqueda.show();
                contenedorResultado.show();
                step = 3;
	            
            });
        }
        function run(){
            contenedorResultado.find('[data-model=struct-model]').remove();
            $('#incidentForTec').hide();
            getFiltros();
            activaBuscar();
        }
        run();
    });

</script>
<style type="text/css">
    div.panel-heading {
        background-color: #3fa9f5 !important;
        color: white !important;
    }
    .breadcrumb{
        margin-top: 0px;
    }
    .c-pointer:hover{
        cursor: pointer;
        background-color: #95b5ef !important;
    }
    .success{ background-color: #6693c1 !important; }
    .info{ background-color: #82bb7b !important; }
    .danger{ background-color: #f95454 !important; }
    .orange{ background-color: #dea02e !important; }

</style>
<div class="list">
    <ol class="breadcrumb">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Monitoreo</strong></a></li>
        <li class="active"><strong> Productividad </strong></li>
    </ol>
</div>
<!-- Dashboard Produvtividad y Analisis -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 bg-info" data-model="caja-busqueda" id="caja-busqueda">
            <div class="row">
                <div class="col-md-12 bg-primary">
                    <h4>Filtro de busqueda</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group" data-model="select-periodo">
                    <label for="periodo">Periodos</label>
                    <select name="periodo" id="periodo" class="form-control" data-attribute="select-periodo">
                        <option value="0">Seleccione Periodo</option>
                        <option value="1">Mes Actual</option>
                        <option value="30">30 días</option>
                        <option value="60">60 días</option>
                        <option value="90">90 días</option>
                        <option value="180">180 días</option>
                        <option value="365">365 días</option>
                    </select>
                </div>
                <div class="col-md-12 form-group" data-model="select-cliente">
                    <label for="cliente">Cliente</label>
                    <select name="cliente" id="cliente" class="form-control" data-attribute="select-cliente">
                        <option value="0">Seleccione Cliente</option>
                    </select>
                </div>
                <div class="col-md-12 form-group" data-model="select-zona">
                    <label for="zona">Zonas</label>
                    <select name="zona" id="zona" class="form-control" data-attribute="select-zona">
                        <option value="0">Seleccione Zona</option>
                    </select>
                </div>
                <div class="col-md-12 form-group">
                    <button id="buscar" name="buscar" class="btn btn-primary">Buscar </button>
                </div>
            </div>
        </div>
        <div style="display: none;" class="col-md-9" id="struct-box">
            <div class="row" data-model="container">
                <div class="col-md-12" data-model="struct-model">
                    <div class="col-md-12 bg-primary">
                        <h4 data-attribute="title"></h4>
                    </div>
                    <table class="table" data-model="content-table">
                        <thead>
                            <tr>
                                <th class="info" data-attribute="head-title">[nombre]</th>
                                <th class="info">Total</th>
                                <th class="success">Asignadas</th>
                                <th class="success">% Asignadas</th>
                                <th class="danger">No Asignada</th>
                                <th class="danger">% No Asignadas</th>
                                <th class="danger orange">Cerrados</th>
                                <th class="danger orange">% Cerrados</th>
                                <th class="info">Responsable</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="info c-pointer" data-attribute="nombre" data-id=""></td>
                                <td class="info" data-attribute="total-tickets"></td>
                                <td class="success" data-attribute="total-asignados"></td>
                                <td class="success" data-attribute="total-asignados-porcentaje"></td>
                                <td class="danger" data-attribute="total-noasignados"> </td>
                                <td class="danger" data-attribute="total-noasignados-porcentaje"></td>
                                <td class="danger orange" data-attribute="total-cerrados"> </td>
                                <td class="danger orange" data-attribute="total-cerrados-porcentaje"></td>
                                <td class="info" data-attribute="responsable"></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="col-md-12" data-model="result"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="incidentForTec" style="display: none;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <h4><strong>Tickets Tecnico</strong></h4>
                    </div>
                    <table id="tableIncidentForTec" class="display compact">
                        <thead>
                        <tr>
                            <th>Numero</th>
                            <th>Tipo</th>
                            <th>Titulo</th>
                            <th>Estado</th>
                            <th>Bitacora</th>
                            <th>Grupo Resolutor</th>
                            <th>Tecnico</th>
                            <th>Ubicacion</th>
                            <th>Fecha Creación</th>
                            <th>Fecha Integracion</th>
                            <th>Fecha Asignado</th>
                            <th>Fecha Cierre Ticket</th>
                            <th>Fecha Resolucion</th>
                            <th>Tiempo Coordinación</th>
                            <th>Tiempo de Atención Terreno</th>
                            <th>Tiempo Integracion</th>
                        </tr>
                        </thead>
                    </table>
                   </div>

            </div>
        </div>
        <div class="col-md-4">
            <button class="btn" id="btn-back-prd">Volver</button>
        </div>
    </div>
</div>