<style>
label{
	font-weight: bold !important;
    color: red;
    font-size: 15px;
}
p, li{
   /*color: white;*/  
}

</style>

<div class="list">
    <ol class="breadcrumb" style="margin-top: 4px;">
        <li title="Volver a la página anterior" onclick="history.back()">
        <i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong> Monitoreo </strong></a></li>
        <li class="active"><a href="active"><strong> Reporte </strong></a></li>
    </ol>
</div>

<?php 
    //var_dump($reporte);
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="tabla-nav">
                <div class="row bg-info" style='padding-right: 0px;padding-left: 0px;'>
                    <?php echo form_open(site_url('monitoreo/reporte'), array('method'=>'get')); ?>
                    <div class="col-md-12 bg-primary">
                        <div class="col-md-6">
                            <h5>Filtro de búsqueda</h5>
                        </div>
                        <div class="col-md-6" style="text-align: right;padding-top: 5px;cursor: pointer;" onclick="openFullScreen()">
                            <i id="start-nav" class="fa fa-play-circle fa-lg" ></i>
                            <i id="stop-nav"  class="fa fa-stop-circle fa-lg" style="display: none;"></i>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div>
                            Seleccione el tipo de reporte:
                        </div>
                        <div>
                            <div class="col-md-2 text-right" style="vertical-align: middle;">
                                <input type="hidden" name="busqueda" value="1">
                            </div>
                            <div class="col-md-2">
                                <label style="width: 100%;display: block;text-align: center;">Mensual: </label>
                                <input type="radio" value="1" class="form-control" name="frecuencia">
                            </div>
                            <div class="col-md-2">
                                <label style="width: 100%;display: block;text-align: center;">Semanal: </label>
                                <input type="radio" value="2" class="form-control" name="frecuencia">                              
                            </div>
                            <div class="col-md-2">
                                <label style="width: 100%;display: block;text-align: center;">Diario: </label>
                                <input type="radio" value="3" class="form-control" name="frecuencia">
                            </div>
                        </div>
                        <div class="row collapse">
                            <div class="col-md-offset-3 col-md-6" style="padding:1em;">
                                <select name="fechaweek" class="form-control">
                                <?php 
                                    $now = new DateTime;

                                    $from = new DateTime; 

                                    $from->modify('-4 weeks');
                                    $from->modify('monday this week');

                                    while($from < $now) {

                                        $fecha = $from->format('d-m-Y');
                                        $from->modify("+6 days");
                                        $fecha2 = $from->format('d-m-Y');
                                        $from->modify("+1 days");


                                        echo '<option value="'.$fecha.'|'.$fecha2.'">';
                                        echo 'Semana: '.$fecha.' - '.$fecha2;
                                        echo '</option>';
                                        
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <input type="submit" value="Buscar" class="btn">
                            </div>
                        </div>

                        <div class="row collapse">
                            <div class="col-md-6 col-md-offset-3" style="padding:1em;">
                                <select name="fechaweek" class="form-control">
                                <?php 
                                    $now = new DateTime;

                                    $from = new DateTime; 

                                    $from->modify('-4 weeks');
                                    $from->modify('monday this week');

                                    while($from < $now) {

                                        $fecha = $from->format('d-m-Y');
                                        $from->modify("+6 days");
                                        $fecha2 = $from->format('d-m-Y');
                                        $from->modify("+1 days");


                                        echo '<option value="'.$fecha.'|'.$fecha2.'">';
                                        echo 'Semana: '.$fecha.' - '.$fecha2;
                                        echo '</option>';
                                        
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <input type="submit" value="Buscar" class="btn">
                            </div>
                        </div>
                        </div>
                    <?php echo form_close(); ?>
                </div                   <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="tabla-nav">
                                <div class="row bg-info" style='padding-right: 0px;padding-left: 0px;'>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col" class="text-dark"><h3><strong>CANTIDAD DE VIAJES - <?php setlocale(LC_ALL,"es_ES"); echo strftime('%B') ?></strong></h3></th>
                                                    <th scope="col" class="text-dark"></th>
                                                    <?php
                                                    setlocale(LC_ALL,'es_ES.UTF-8');

                                                    //Si es lunes calculamos a partir de la fecha actual
                                                    if (date('w') == 0) {
                                                        $lastm = time();
                                                        //Si es otro dia buscamos la fecha del lunes pasado.
                                                    } else {
                                                        $lastm = strtotime('today');
                                                    }

                                                    //Bucle para mostrar los últimos 7 dias
                                                    for ($i = 6; $i > -1; $i--) {
                                                        //Restamos días a la fecha actual
                                                        $sd = strtotime("-$i day", $lastm);
                                                        //comprobamos si la fecha del bucle es la fecha actual
                                                        if (date('d', $sd) === date('d')) {
                                                            //colocamos una clase para colorear - strftime para mostrar los dias en español
                                                            $dias='<th scope="col" style="color:#FF3600;">' . strftime("%A %d", $sd) . '</th>';
                                                            echo $dias;
                                                        } else {
                                                            //Sino es el día actual imprimimos en negro
                                                            echo '<th>' . strftime("%A %d", $sd) . '</th>' ;
                                                        }
                                                    }
                                                    ?>
                                                    <th scope="col" class="text-dark">Total</th>
                                                </tr>
                                                </thead>
                                                <thead>
                                                <tr>
                                                    <th scope="col" class="text-dark">CAMIONES</th>
                                                    <th scope="col" class="text-dark">CONDUCTOR</th>
                                                    <th scope="col" class="text-dark"></th>
                                                    <th scope="col" class="text-dark"></th>
                                                    <th scope="col" class="text-dark"></th>
                                                    <th scope="col" class="text-dark"></th>
                                                    <th scope="col" class="text-dark"></th>
                                                    <th scope="col" class="text-dark"></th>
                                                    <th scope="col" class="text-dark"></th>
                                                    <th scope="col" class="text-dark"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                            
                                                    <?php echo form_open(site_url('form/change_state'), array('id' => 'list-form')) ?>
                                                    <?php 
                                                          foreach ($reporte as $reportes) {
                                                              
                                                          
                                                     ?>
                                                        <tr>
                                                            <td>
                                                                <?php  echo $reportes['numberTruck'];?>
                                                            </td>
                                                            <td>
                                                                <?php  echo $reportes['name'];?>
                                                            </td>
                                                                <?php 
                                                                    $acum=0;
                                                                    foreach ($reportes['info'] as $key) 
                                                                { ?>
                                                            <td class="text-center">
                                                                <?php 
                                                                        echo $key['cantidad'];
                                                                        $acum= $acum + $key['cantidad'];
                                                                    ?>
                                                            </td>
                                                            <?php } ?>

                                                            <td class="text-center">
                                                                <?php echo $acum;?>
                                                            </td>

                                                        </tr>
                                                <?php
                                                }
                                                /* } else {
                                                    if ($search_value != ""){
                                                        echo "<p class='message-no-data'>No existen resultados con los criterios de búsqueda ingresados.</p>";
                                                        echo "<p class='message-no-data link'><a href='" . site_url('form/index') . "'>Volver al inicio</a></p>";
                                                    } else {
                                                        echo "<p class='message-no-data'>El sistema aun no registra información.</p>";
                                                    }
                                                }*/ ?>

                                                 <?php echo form_close(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


