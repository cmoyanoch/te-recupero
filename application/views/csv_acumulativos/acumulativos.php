<script src="<?php echo base_url();?>assets/js/csv_acumulativos/paginacion_acumulativos.js"></script>
<script src="<?php echo base_url();?>assets/js/dataTables.js"></script> 


<link rel="stylesheet" href="<?php echo base_url();?>assets/css/dataTables.css">

<script type="text/javascript">
$(document).ready(function() {
    $('#Tabla_acumulativos').DataTable(
      {
      "bPaginate": false,
      "bLengthChange": false,
      "bFilter": false,
      "bSort": false,
      "bInfo": false,
      "bAutoWidth": false }
      );
} );

</script>

<style type="text/css">
#Tabla_acumulativos{
  margin: auto;
}
.div1{
  margin: auto;
}
</style>


<?php

    $Creg = $data['CReg'];             //CANTIDAD TOTAL DE REGISTROS QUE CONTENGA LA CONSULTA. ARRAY
    $registrosTot = $Creg['cantReg'];  //CANTIDAD TOTAL DE REGISTROS QUE CONTENGA LA CONSULTA NUMERO
    $contreg = $data['contadorPag'];   //TRAE COMO ARRAY EL NUMERO DE LA PAGINA EN EL QUE VAMOS
    $contadorPag = $contreg;           //RECIBE COMO NUMERO LA PAGINA EN EL QUE VAMOS
    $pag = $registrosTot/PAGINADO_CANT;
    $pag = floor($pag);
    $resto = $registrosTot%PAGINADO_CANT;
    
    if ($resto>0 || $pag==0) {
      $pag = $pag+1;
    }

    $palabra = ' Página de ';

    // print_r($registrosTot);
          
?>




<div id="divBloq" style="  position: absolute; width: 100%; height: 100%;  background-color: rgba(255, 255, 255, 0.22);  z-index: 1;
 text-align: center; display: none;"></div>

<div id="tab_acumulativos">


  <div class="col-sm-12 col-md-12 col-xs-12" id="filtros_correoCli">
   <div class="col-sm-12 col-md-3 col-xs-12"></div>
   <div class="col-sm-12 col-md-2 col-xs-12" style="width: 210px;" align ="left">
     <label  style="color: white;">Desde:</label>  <br>
     <input type="text" id="datepicker_Desde" style="width:200px;height:37px" readonly>
   </div>

   <div class="col-sm-12 col-md-2 col-xs-12" style="width: 210px;" align ="left">
     <label  style="color: white;">Hasta:</label>  <br>
     <input type="text" id="datepicker_Hasta" style="width:200px;height:37px" readonly>
   </div>

   <div class="col-sm-12 col-md-2 col-xs-12" style="width: 50px;" align ="left">
     <i type="button" id="filtrar_acumulativos" title="Buscar" style="margin-top: 20px;" class="btn btn-success fa fa-search fa-2x" aria-hidden="true"></i>
   </div>

   <div class="col-sm-12 col-md-3 col-xs-12" style="width: 50px;" align ="left">
    <i type="button" id="reCargar" title="Recargar" class="btn btn-success fa fa-refresh fa-2x" aria-hidden="true" style="margin-top: 20px;"></i>
   </div>

  </div>

 <div class="col-sm-12 col-md-9 col-xs-12 btn-group" >
  <br>
 </div>

  <input type="hidden" class="form-control" id="FiltroF" value="0"> 
  <input type="hidden" class="form-control" id="idTipoRep" value="<?=$data['idTipoRep'];?>"> 

  <table id="Tabla_acumulativos" class="table table-striped tablaA" style="width: 100%;">

  <thead>
      <tr class="listrabajos">
          <th style="width: 10%;"><input type="checkbox" id="selTodos" title="Seleccionar Todos" class="todosDoc" ></th>
          <th style="width: 70%;">DOCUMENTOS</th>           
          <th style="width: 20%;">DESCARGAR</th>            
      </tr>
   </thead>
   
   <tbody id="Cargar_correoCli">

        <?php
          foreach ($data['datos'] as $val) {
        ?>
            
            <tr class="tr_correoCli" id="<? echo $val['ReportName'] ?>" style="">
                <td><input type="checkbox" id="<?=$val['Id'];?>" class="select_acumulativo" nombre="<? echo $val['ReportName'] ?>"></td>
                <td style="width: 500px;"><?=strtoupper($val['ReportName']);?></td>
                <td><a href="<?=CSV_DIR.$val['ReportName'];?>" download="" class="btn btn-primary fa fa-cloud-download D_acumulativos" >  Descargar</a></td>			
            </tr>
        <?php
          }
        ?> 

  </table>

  <div class="col-sm-11 col-md-11 col-xs-11" style="text-align: right;">
  <? if($contadorPag > 1){ ?>
   <i type="button" id="atras" class="btn btn-success fa fa-arrow-left fa-2x" aria-hidden="true"></i>
  <? } ?>

   <span class="label" id="cp" style="font-size: 12px;color: black;" ><?php echo $contadorPag ?></span>
   <span class="label" id="pal" style="font-size: 12px;color: black;" ><?php echo $palabra ?></span>
   <span class="label" id="pg" style="font-size: 12px;color: black;" ><?php echo $pag ?></span>   
   
   <? if($contadorPag < $pag){ ?>
    <i type="button" id="adelante" class="btn btn-success fa fa-arrow-right fa-2x" aria-hidden="true" ></i>
   <? } ?>
  </div>


</div>

<div id="modal_acumulativos"></div>