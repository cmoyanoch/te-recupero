<pre>
<?php 
	$hasData = false;
	if(isset($grouptec)){
		$hasData = true;
	}
//print_r($grouptec);
?>

</pre>
<div class="list">
<div class="container-fluid menu-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php echo $title ?></h1>			
			</div>
		</div>			
	</div>		
</div>
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('groupTec/edit/'.$grouptec['grouptec_id'], array('id'=>'edit-form'));
		echo form_hidden('grouptec[grouptec_id]', $grouptec['grouptec_id']);
	}else{
		echo form_open('groupTec/create'); 	
	}	
	?>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-group">
				<label for="grouptec[name]">Nombre Cuadrilla:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'grouptec[name]', 'class'=> 'form-control', 'value'=> set_value('grouptec[name]', isset($grouptec['name'])? $grouptec['name'] :''))); ?>
				<?php echo form_error('grouptec[name]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-default blue">Guardar</button>	
			</div>	
		</div>
	</div>
