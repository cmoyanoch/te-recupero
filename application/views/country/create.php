<?php 
	$hasData = false;
	if(isset($country)){
		$hasData = true;
	}

?>
<div class="list">
<div class="container-fluid menu-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php echo $title ?></h1>			
			</div>
		</div>			
	</div>		
</div>
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('country/edit/'.$country['country_id'], array('id'=>'edit-form'));
		echo form_hidden('country[country_id]', $country['country_id']);
	}else{
		echo form_open('country/create'); 	
	}	
	?>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-group">
				<label for="country[name]">Nombre país:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'country[name]', 'class'=> 'form-control', 'value'=> set_value('country[name]', isset($country['name'])? $country['name'] :''))); ?>
				<?php echo form_error('country[name]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-default blue">Guardar</button>	
			</div>	
		</div>
	</div>
