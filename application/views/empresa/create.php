<div class="list">
    <ol class="breadcrumb" style="margin-top: 4px;">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Mantenedores</strong></a></li>
        <li><a href="#"><strong>Cerco Electronico</strong></a></li>
        <li class="active"><strong>Editar</strong></li>
    </ol>               
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('empresa/edit/'.$empresa['id_empresa'], array('id'=>'edit-form'));
		echo form_hidden('empresa[id_empresa]', $empresa['id_empresa']);
	}else{
		echo form_open('empresa/create'); 	
	}	
	?>
    <div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label for="empresa[radio_cerco_1]">Cerco electrónico 1 (mts):</label>
					<?php echo form_input(array('type'=>'text', 'name'=>'empresa[radio_cerco]','pattern'=>'[0-9]+','title'=>'Ingrese solo caracteres numéricos', 'class'=> 'form-control', 'value'=> set_value('empresa[radio_cerco]', isset($empresa['radio_cerco'])? $empresa['radio_cerco'] :''))); ?>
					<?php echo form_error('empresa[radio_cerco_1]'); ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="empresa[radio_cerco_2]">Cerco electrónico 2 (mts):</label>
					<?php echo form_input(array('type'=>'text', 'name'=>'empresa[radio_cerco_2]','pattern'=>'[0-9]+','title'=>'Ingrese solo caracteres numéricos', 'class'=> 'form-control', 'value'=> set_value('empresa[radio_cerco_2]', isset($empresa['radio_cerco_2'])? $empresa['radio_cerco_2'] :''))); ?>
					<?php echo form_error('empresa[radio_cerco_2]'); ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="empresa[radio_cerco_3]">Cerco electrónico 3 (mts):</label>
					<?php echo form_input(array('type'=>'text', 'name'=>'empresa[radio_cerco_3]','pattern'=>'[0-9]+','title'=>'Ingrese solo caracteres numéricos', 'class'=> 'form-control', 'value'=> set_value('empresa[radio_cerco_3]', isset($empresa['radio_cerco_3'])? $empresa['radio_cerco_3'] :''))); ?>
					<?php echo form_error('empresa[radio_cerco_3]'); ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="empresa[radio_cerco_4]">Cerco electrónico 4 (mts):</label>
					<?php echo form_input(array('type'=>'text', 'name'=>'empresa[radio_cerco_4]','pattern'=>'[0-9]+','title'=>'Ingrese solo caracteres numéricos', 'class'=> 'form-control', 'value'=> set_value('empresa[radio_cerco_4]', isset($empresa['radio_cerco_4'])? $empresa['radio_cerco_4'] :''))); ?>
					<?php echo form_error('empresa[radio_cerco_4]'); ?>
				</div>
			</div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="form-buttons" style="text-align: left;">
                    <button type="button" onclick="history.back()" class="btn btn-success">atrás</button>
                </div>
            </div>    
            <div class="col-md-6">
                <div class="form-buttons">
                    <button type="submit" class="btn btn-default blue">Guardar</button>	
                </div>
            </div>	
        </div>
    </div>
