<div class="list">
    
    <ol class="breadcrumb" style="margin-top: 4px;">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Mantenedores</strong></a></li>
        <li><a href="#"><strong>Cerco Electronico</strong></a></li>
        <li class="active"><strong> Buscar </strong></li>
    </ol>               
    
	<div class="container-fluid menu-content">
		<div class="container">
			<div class="row" style="display:none;">
				<div class="col-md-6">
					<?php echo form_open(site_url('empresa/index'), array('method'=>'get')); ?>
					<div class="input-group" style="display:none;">
						<?php echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué Empresa deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>					
              <span class="input-group-btn" style="display:none;">
							<button class="btn btn-default blue" type="submit">Buscar</button>
						</span>
					</div>
					<?php echo form_close(); ?>
				</div>
				<div class="col-md-6 button-content" style="display:none;">
					<a href="<?php echo site_url('empresa/create'); ?>" class="btn btn-default blue">Crear</a>				
				</div>
			</div>			
		</div>		
	</div>
	<div class="container ">
		<div class="row">
			<div class="col-md-12">				
				<?php if (count($empresaes) > 0){ ?>
				<div class="table-responsive">
					<?php echo form_open(site_url('empresa/delete'), array('id'=>'list-form')) ?>										
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
								<td>Cerco electrónico 1 </td>
								<td>Cerco electrónico 2 </td>
								<td>Cerco electrónico 3 </td>
								<td>Cerco electrónico 4 </td>
								<td class="with-icon"></td>
							</tr>				
						</thead>
						<tbody>
							<?php foreach ($empresaes as $keyempresa => $empresa): ?>							
								<tr>
									<td><?php echo $empresa['radio_cerco'] ?> mts</td>
									<td><?php echo $empresa['radio_cerco_2'] ?> mts</td>
									<td><?php echo $empresa['radio_cerco_3'] ?> mts</td>
									<td><?php echo $empresa['radio_cerco_4'] ?> mts</td>
									<td style="display:none;">
										<?php if ($empresa['can_delete']): ?>
											<a title="Eliminar" href="" data-title="Eliminar país" data-message="¿Estás seguro que deseas eliminar la país <?php echo $empresa['nombre_empresa'] ?>?" data-function="deleteempresa" data-param="<?php echo $empresa['id_empresa'] ?>" data-toggle="modal" data-target="#normal-modal">
												<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete' ?>"></use></svg>
											</a>	
										<?php endif ?>										
									</td>
									<td>									
										<a title="Editar" href="<?php echo site_url('empresa/edit/'.$empresa['id_empresa']) ?>">
											<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-edit' ?>"></use></svg>
										</a>										
									</td>								
								</tr>							
							<?php endforeach ?>						
						</tbody>
					</table>
				</div>
				<?php echo form_close(); ?>				
				<?php }else{ 
					if ($search_value != "") {
						echo "<p class='message-no-data'>No existen resultados con los criterios de búsqueda ingresados.</p>";
						echo "<p class='message-no-data link'><a href='".site_url('empresa/index')."'>Volver al inicio</a></p>";
					}else{
						echo "<p class='message-no-data'>El sistema aun no registra información.</p>";
					}	
				} ?>				
			</div>	
		</div>		
		<div class="row">
			<div class="col-md-12 pagination-content">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<?php echo $pagination; ?>					
					</ul>
				</nav>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		<?php if ($search_value): ?>			
			$("#search-value").focus();
			$("#search-value").val($("#search-value").val());
		<?php endif ?>

		/*Al cargar la web selecciona todos los elementos según el estado del principal*/
		if($('#select-all').prop('checked')){
			$('.checkbox-element').prop('checked', true);
			addDeleteButton();
		}else{
			$('.checkbox-element').prop('checked', false);
			$('#delete-button').remove();			
		}
		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		if($('input.checkbox-element:checked').length > 0){
			addDeleteButton();
		}

		/*Función del botón Aceptar del modal al eliminar una categoría*/
		$(document).on('click', '#accept-button-modal.deleteempresa', function(){
			$param = ($(this).data('param'));
			$form = $('#list-form');
			if($param != null){
				$('.checkbox-element').prop('checked', false);
				$form.append($('<input/>').attr({"type":"hidden", "nombre_empresa":"empresa[id_empresa][]", "value":$param}));		             
			}
            $form.submit();
            $('#accept-button-modal').removeClass('deleteempresa');
			$('#accept-button-modal').removeData('param');
			$('#normal-modal').modal('hide');            
		})

		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		$('#list-form').on('change', function(){			
			if($('input.checkbox-element:checked').length > 0){
				addDeleteButton();
			}else{
				$('#delete-button').remove();				
			}
		});

		/*Selecciona todos los elementos según el estado del principal*/
		$('#select-all').on('change', function(){
			if($(this).prop('checked')){
				$('.checkbox-element').prop('checked', true);				
			}else{
				$('.checkbox-element').prop('checked', false);
				$('#delete-button').remove();				
			}
		});

		/* Crea el botón eliminar */
		function addDeleteButton(){
			if($('.menu-content .button-content').find('#delete-button').length == 0){
				$deleteButton = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'delete-button',"data-title":"Eliminar país(s)" ,"data-message":"¿Estás seguro que deseas eliminar el(las) país(s) seleccionada(s)?", "data-function":"deleteempresa", "data-toggle":"modal", "data-target":"#normal-modal"}).html('Eliminar');				
				$('.menu-content .button-content').prepend($deleteButton);	
			}			
		}
		

		
	});
</script>