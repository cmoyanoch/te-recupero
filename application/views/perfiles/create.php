<style>

h4 {
	font-family: 'TitilliumWeb-bold';
	font-size: 16px;
	color: #fff;
}
.checkb {
	font-family: 'TitilliumWeb-bold';
	font-size: 14px;
	color: #fff;
}
</style>

<?php
if(empty($perfil)){
$nombre_perfil = array(
					  'name'        => 'nombre_perfil',
					  'id'          => 'nombre_perfil',
					  'placeholder' => 'Nombre Perfil',
					  'class'		=> 'form-control'
					);
$action = 'perfiles/save';
$id_perfil=0;
}else{
$nombre_perfil = array(
					  'name'        => 'nombre_perfil',
					  'id'          => 'nombre_perfil',
					  'value'		=> $perfil[0]->nombre_perfil,
					  'class'		=> 'form-control'
					);
$action = 'perfiles/save/';
$id_perfil=$perfil[0]->id_perfil;
}
$attributes = array('class' => 'pure-form pure-form-aligned', 'id' => 'paginasform', 'method' => 'post');
?>

<div class="list">
    <ol class="breadcrumb" style="margin-top: 4px;">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Mantenedores</strong></a></li>
        <li><a href="#"><strong>Usuarios</strong></a></li>
        <li><a href="#"><strong>Perfiles</strong></a></li>
        <li class="active"><strong> Crear </strong></li>
    </ol>              
    
</div>

<div class="container">	


	<?php 
	echo form_open($action, $attributes);
	echo form_hidden('id_perfil', $id_perfil);
	?>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label style="color: #fff" for="nombre_perfil">Nombre perfil:</label>
				<?php echo form_input($nombre_perfil); ?>
			</div>
		</div>
	</div>
	<div class="row"">
		<div class="col-md-3">
			<h4>Permisos del perfil</h4>
			<?php $cont=0; 
			foreach ($group as $row) {
				if($cont == 10){
					echo '</div><div class="col-md-3"><h4>&nbsp;</h4>';
					$cont=0;
				}
				?>

			<div class="checkbox">
				<label>
					<?php echo form_checkbox(array('name'=>'permisions[]', 'value'=>$row->id,'id'=>$row->id,'style'=>'color:#fff;'),$row->description,
					in_array($row->id, $permisos)); echo $row->description;?>
				</label>
			</div>


			<?php $cont++; }?>
		</div>
            
            
            
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-buttons">
				<button type="submit" class="btn btn-success">Guardar</button>
			</div>
		</div>
	</div>
</div>