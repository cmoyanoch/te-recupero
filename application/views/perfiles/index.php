<style>
label{
	font-weight: bold !important;
  
    font-size: 15px;
}
 .icon{fill:#3fa9f5; width: 22px; height: 22px;}
</style>

<div class="list">
    <ol class="breadcrumb" style="margin-top: 4px;">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Mantenedores</strong></a></li>
        <li><a href="#"><strong>Usuarios</strong></a></li>
        <li class="active"><strong> Perfiles </strong></li>
    </ol>            
    <br>
</div>
<div align="center">						
    <a href="<?php echo site_url('perfiles/create'); ?>" class="btn btn-default blue" style="width:  110px;"><i class="fa fa-plus"></i><strong> Crear</strong></a>				
</div>

	<div class="container ">
		<div class="row">
			<div class="col-md-12">				
				<?php if (count($perfil) > 0){ ?>
				<div class="table-responsive">
					<?php echo form_open(site_url('perfiles/delete'), array('id'=>'list-form')) ?>										
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
								<td class="cell-selection"><?php echo form_checkbox('select-all', '', FALSE, array('id'=>'select-all')); ?></td>								
								<td>Nombre perfil</td>
								<td class="with-icon"></td>
							</tr>				
						</thead>
						<tbody>
							<?php foreach ($perfil as $row): ?>							
								<tr>								
									<td><?php echo form_checkbox('user[user_id][]', $row->id_perfil, FALSE,array('class'=> 'checkbox-element'));?></td>
									<td><?php echo $row->nombre_perfil; ?></td>	
									<td style="text-align: right;"><a title="Eliminar" href="<?php echo site_url('perfiles/delete/').$row->id_perfil;?>"  onclick="return confirm('Esta seguro, eliminara el perfil?')">
											<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete' ?>"></use></svg>
										</a>
										<a title="Editar" href="<?php echo site_url('perfiles/edit/').$row->id_perfil;?>">
											<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-edit' ?>"></use></svg>
										</a></td>
								</tr>							
							<?php endforeach ?>						
						</tbody>
					</table>
				<?php echo form_close(); ?>
				</div>			
				<?php }else{ 
						echo "<p class='message-no-data link'><a style='color:#fff;' href='".site_url('perfiles/create')."'>Crear nuevo</a></p>";
				} ?>				
			</div>	
		</div>
	</div>
</div>