<style>
.text{color: #fff;}
</style>

<?php 

//        echo '<pre>';
//    if($resultado){
//        print_r("exito");
//        print_r("*****");
//        print_r($resultado);
//    }
//    
//    if($error){
//        print_r("error");
//        print_r("--------");
//        print_r($error);
//    }

?>

<div class="list">
            <ol class="breadcrumb" style="margin-top: 0px;">
                <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
                <li><a href="#"><strong>Ticket</strong></a></li>
                <li class="active"><strong>Carga Masiva</strong></li>
            </ol>            
    <br> <hr>
</div>
<div class="container">

        <div class="form-group">
            <a href="<?php echo site_url('assets/csvejemplo.csv');?>"><button class="btn btn-success"  type="submit"><i class="fa fa-cloud-download"></i><strong> Descargar Ejemplo</strong></button></a>
        </div>
    
	<?php echo form_open_multipart('cargamasiva/do_upload');?>

        <div id="tabCarga" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center" hidden="" style="display: block;">
            <table style="width: auto;">
                <tbody>
                    <tr>
                        <td>
                            <input type="hidden" name="MAX_FILE_SIZE" value="50000000">
                            <input  class="form-control" type="file" name="userfile" autofocus="" size="20" />
                        </td>
                        <td>
                            <div class="col-sm-8 col-md-8 col-xs-8 col-lg-9">
                                <button type="submit" title="Cargar Archivo Seleccionado" class="btn btn-default blue"><strong>Cargar</strong></button>	
                            </div>
                        </td>
                        <td></td>       
                    </tr>
                </tbody>
            </table> 
            <div class="progress" id="barra" align="center" style="width: 40%;height: 5%;border-color: white;border-style: groove;" hidden="">
                <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    <span class="sr-only">100% Complete</span>
                </div>
            </div>
        </div>    
    
	<?php echo form_close();?>
    
        
</div>



<?php 
	$hasData = false;
	if(isset($resultado)){?>

	<div class="container ">
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">									
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
                                                            <td><strong>Detalle Carga</strong></td>
							</tr>				
						</thead>
						<tbody>
							<?php foreach ($resultado as $row){?>
								<tr>		
                                                                    <?php if(!empty($row['error'])){ ?>
									<td><?php echo "ERROR: No se pudo insertar incident N°: ".$row['id']." (ya existe)." ?></td>
                                                                    <?php }else{ ?>    
									<td><?php echo "ÉXITO: Id de Ingreso ".$row['id']['message'] ?></td>	
                                                                    <?php } ?>    
								</tr>							
							<?php } ?>						
						</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>

<?php
}
//new dBug($resultado);exit();
?>