<?php
/**
 * Created by PhpStorm.
 * User: Multiprint
 * Date: 03-09-2018
 * Time: 18:12
 */?>
<style>
   #exTab2 h3 {
      color : white;
      background-color: #428bca;
      padding : 5px 15px;
   }
</style>
<div class="list" style="height: 35px !important;">
   <ol class="breadcrumb" style="margin-top: 4px;">
      <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i>
	      <a href="#" ><strong style="color:black;"> Voltar</strong></a>
      </li>
      <li><a href="#" style="color:#6a479a;"><strong>Ticket</strong></a></li>
      <li class="active" style="color:#6a479a;"><strong>Administrar Ticket</strong></li>
   </ol>
</div>
<input type="hidden" name="search_value"     id="search_value" value="<?php echo $search_value;?>"/>
<input type="hidden" name="id_estado"        id="id_estado" />
<input type="hidden" name="name_estado"      id="name_estado"/>
<input type="hidden" name="id_zona"          id="id_zona"/>
<input type="hidden" name="name_grupo"       id="name_grupo"/>
<input type="hidden" name="name_tecnico"     id="name_tecnico"/>
<input type="hidden" name="name_coordinador" id="name_coordinador"/>
<input type="hidden" name="persistent_id"    id="persistent_id"/>
<input type="hidden" name="address_ticket"    id="address_ticket"/>

<div id="exTab2" class="container">
   <ul class="nav nav-tabs" id="sub_modulos">
      <li class="active">
         <a  href="#4" data-toggle="tab" id="mod_estado"><strong>Actualizar Estado</strong></a>
      </li>
   </ul>

   <div class="tab-content">
      <div class="tab-pane" id="4">
          <div id="cambio_est" style="padding: 20px; margin: 20px"><?php include("actualizarEstado.php");  ?></div>
      </div>
   </div>
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-buttons" style="text-align: left;">
                <a href="<?php echo site_url('tracking/tickets'); ?>" class="btn btn-success">Volver</a>
            </div>
        </div>
    </div>
</div>

