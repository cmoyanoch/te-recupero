<script src="https://use.fontawesome.com/32535cb184.js"></script>
<div class="list">
	<div class="container-fluid menu-content">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
				<button type="button"  onclick="history.back()" class="btn btn-success">atrás</button>
				</div>
				<div class="col-md-6">
					<?php echo form_open(site_url('tracking/tickets'), array('method'=>'get')); ?>
					<div class="input-group">
						<?php echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué ticket deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>					
						<span class="input-group-btn">
							<button class="btn btn-default blue" type="submit">Buscar</button>
						</span>
					</div>
					<?php echo form_close(); ?>
				</div>
				<!-- <div class="col-md-6 button-content">								
					<a href="<?php //echo site_url('tracking/create'); ?>" class="btn btn-default blue">Crear</a>				
				</div> -->
			</div>			
		</div>		
	</div>
	<div class="container ">
		<div class="row">
			<div class="col-md-12">				
				<?php if (count($tickets) > 0){ ?>
				<div class="table-responsive">
					<?php echo form_open(site_url('tracking/delete'), array('id'=>'list-form')) ?>										
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
								<!-- <td class="cell-selection"><?php //echo form_checkbox('select-all', '', FALSE, array('id'=>'select-all')); ?></td> -->
								<!-- <td>SLA</td>								-->
								<td>Carga</td>
								<td>Titulo</td>
								<td>Fecha Creación</td>							
								<td>Fecha Actualización</td>	
								<td>Estado</td>						
								<td>Encargado</td>
								<!-- <td>PDF</td> -->
								<td>Bitácora</td>
							</tr>				
						</thead>
						<tbody>
							<?php foreach ($tickets as $keytracking => $ticket): ?>							
								<tr data-id ="<?php echo $ticket['id']; ?>">
									

									<?php
									/*
									if($ticket['idstatus']!='Cerrado'){

										 $fechaTecnico = $ticket['datecreation'];
										 $fechaactual= date ("Y-m-d h:i:s");
										 $restrictionDate = strtotime($fechaTecnico);
								         $now             = strtotime(date('Y-m-d H:i:s'));
								         $diferencia      = $now - $restrictionDate;
								         $diferencia	  =  ($diferencia/60)/60;
								         	if($diferencia>$ticket['sla']){
								         		$color = 'background-color:#ff4747';
								         	}else{
								         		$color = 'background-color:#54da54';
								         	}


											$arrTime =  (explode(".",$diferencia));
											$arrTime[1]= substr($arrTime[1],0,2);
											$arrTime[1]= intval($arrTime[1]*0.6);
										?>
											
											<td style='<?php echo $color?>'><strong><?php echo $arrTime[0].':'.$arrTime[1].' / SLA('.$ticket['sla'].' h)' ; ?></strong></td> 

												<!-- <td style='<?php //echo $color?>'><?php //echo $arrTime[1].' de '.$ticket['sla'].' hrs' ; ?></td> -->
										<?php
									}else{

										 $fechaTecnico = $ticket['datecreation'];  
										 $fechaactual= $ticket['updatetime'];
										 $restrictionDate = strtotime($fechaTecnico);
								         $now             = strtotime($fechaactual);
								         $diferencia      = $now - $restrictionDate; 
								         $diferencia = ($diferencia/60)/60;
								         	if($diferencia>$ticket['sla']){
								         		$color = 'background-color:#ff4747;';
								         	}else{
								         		$color = 'background-color:#54da54';
								         	}

								         	$arrTime =  (explode(".",$diferencia));
											$arrTime[1]= substr($arrTime[1],0,2);
											$arrTime[1]= intval($arrTime[1]*0.6);
										?>
											
											<td style='<?php echo $color?>'><strong><?php echo $arrTime[0].':'.$arrTime[1].' / SLA('.$ticket['sla'].' h)' ; ?></strong></td> 

										<?php
									}
								*/
									?>
									<td><?php echo $ticket['incident']; ?></td>
															
									<td><?php echo $ticket['title'] ?></td>
									<td><?php echo $ticket['datecreation'] ?></td>
									<td><?php echo $ticket['updatetime'] ?></td>
									<td><?php echo $ticket['idstatus'] ?></td>		
									<td><?php echo $ticket['name'] ?></td>		
									<!-- <td><?php //echo $ticket['dateclose'] ?></td> -->
									<td style='display:none'> 
										<button type="button" class="btn btn-default detalletrackingulario" id='<?php echo $ticket['imei'].'|'.$ticket['id_tracking'].'|'. $ticket['HeadId'].'|'.$ticket['name'] ; ?>'>
									   		 <span class="glyphicon glyphicon-list-alt"></span> Ver trackingulario
									 	</button>
									</td>
									<td style='display:none'> 
										<button type="button" class="btn btn-default detalleIncidencia" id='<?php echo $ticket['id']; ?>'>
									   		 <span class="glyphicon glyphicon-list-alt"></span> Detalle
									 	</button>
									</td>

									<?php if($ticket['HeadId'] != '' || $ticket['HeadId'] != null){?>
									
									 <td><button type="button" class="fa fa-file-pdf-o fa-2x VerPdf" id='<?php echo $ticket['incident'].'|'.$ticket['title'].'|'.$ticket['imei'].'|'.$ticket['id_tracking'].'|'. $ticket['HeadId'].'|'.$ticket['name'] ;  ?>'></button></td>
									<?php }else{?>
									 <td> </td>
									<?php }?>
									
									<td> 
									 	<button type="button" class="btn btn-default detalleTrazabilidad " id='<?php echo $ticket['incident']; ?>'>
									   		 <span class="glyphicon glyphicon-list-alt"></span> ver
									 	</button>
									</td> 
																
								</tr>							
							<?php endforeach ?>						
						</tbody>
					</table>
				</div>
				<?php echo form_close(); ?>				
				<?php }else{ 
					if ($search_value != "") {
						echo "<p class='message-no-data'>No existen resultados con los criterios de búsqueda ingresados.</p>";
						echo "<p class='message-no-data link'><a href='".site_url('tracking/tickets')."'>Volver al inicio</a></p>";
					}else{
						echo "<p class='message-no-data'>El sistema aun no registra información.</p>";
					}	
				} ?>				
			</div>	
		</div>		
		<div class="row">
			<div class="col-md-12 pagination-content">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<?php echo $pagination; ?>					
					</ul>
				</nav>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		<?php if ($search_value): ?>			
			$("#search-value").focus();
			$("#search-value").val($("#search-value").val());
		<?php endif ?>

		/*Al cargar la web selecciona todos los elementos según el estado del principal*/
		if($('#select-all').prop('checked')){
			$('.checkbox-element').prop('checked', true);
			addDeleteButton();
		}else{
			$('.checkbox-element').prop('checked', false);
			$('#delete-button').remove();			
		}
		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		if($('input.checkbox-element:checked').length > 0){
			addDeleteButton();
		}

		/*Función del botón Aceptar del modal al eliminar una categoría*/
		$(document).on('click', '#accept-button-modal.deletetracking', function(){
			$param = ($(this).data('param'));
			$form = $('#list-form');
			if($param != null){
				$('.checkbox-element').prop('checked', false);
				$form.append($('<input/>').attr({"type":"hidden", "name":"tracking[tracking_id][]", "value":$param}));		             
			}
            $form.submit();
            $('#accept-button-modal').removeClass('deletetracking');
			$('#accept-button-modal').removeData('param');
			$('#normal-modal').modal('hide');            
		})

		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		$('#list-form').on('change', function(){			
			if($('input.checkbox-element:checked').length > 0){
				addDeleteButton();
			}else{
				$('#delete-button').remove();				
			}
		});

		/*Selecciona todos los elementos según el estado del principal*/
		$('#select-all').on('change', function(){
			if($(this).prop('checked')){
				$('.checkbox-element').prop('checked', true);				
			}else{
				$('.checkbox-element').prop('checked', false);
				$('#delete-button').remove();				
			}
		});

		/* Crea el botón eliminar */
		function addDeleteButton(){
			if($('.menu-content .button-content').find('#delete-button').length == 0){
				$deleteButton = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'delete-button',"data-title":"Eliminar país(s)" ,"data-message":"¿Estás seguro que deseas eliminar el(las) país(s) seleccionada(s)?", "data-function":"deletetracking", "data-toggle":"modal", "data-target":"#normal-modal"}).html('Eliminar');				
				$('.menu-content .button-content').prepend($deleteButton);	
			}			
		}
		

		
	});
</script>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Detalle Ticket</h4>
        </div>
         <div class="modal-body">
          <p id="tableDetailTicket">The <strong>show</strong> method shows the modal and the <strong>hide</strong> method hides the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnAceptaAsig" class="btn btn-default" data-dismiss="modal">Aceptar</button>
        </div>
      </div>
      
    </div>
  </div>

    <div class="modal fade" id="myModaltracking" role="dialog"> 
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">trackingulario</h4>
        </div>
         <div class="modal-body">
          <p id="tableDetailtracking">The <strong>show</strong> method shows the modal and the <strong>hide</strong> method hides the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnAceptaAsig" class="btn btn-default" data-dismiss="modal">Aceptar</button>
        </div>
      </div>
      
    </div>
  </div>





<script type="text/javascript">
	$(function(){
		<?php if ($search_value): ?>			
			$("#search-value").focus();
			$("#search-value").val($("#search-value").val());
		<?php endif ?>

		/*Al cargar la web selecciona todos los elementos según el estado del principal*/
		if($('#select-all').prop('checked')){
			$('.checkbox-element').prop('checked', true);
			addDeactivateButton();
		}else{
			$('.checkbox-element').prop('checked', false);
			$('#deactivate-button').remove();
			$('#activate-button').remove();
		}
		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		if($('input.checkbox-element:checked').length > 0){
			addDeactivateButton();
		}

		/* Funcionalidad del botón del modal "Aceptar" para activar/desactivar trackingulario */
		$(document).on('click', '#accept-button-modal.changeState', function(){
			$param = ($(this).data('param')).split(",");

			$tracking = $('#list-tracking');
			if($param.length == 2){
				$tracking.append($('<input/>').attr({"type":"hidden", "name":"tracking[tracking_id][]", "value":$param[1]}));
				$('.checkbox-element').prop('checked', false);				
			}
			$tracking.append($('<input/>').attr({"type":"hidden", "name":"tracking[action]", "value":$param[0]}));				             
            $tracking.submit();
            $('#accept-button-modal').removeClass('changeState');
			$('#accept-button-modal').removeData('param');
			$('#normal-modal').modal('hide');            
		})

		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		$('#list-tracking').on('change', function(){			
			if($('input.checkbox-element:checked').length > 0){
				addDeactivateButton();
			}else{
				$('#deactivate-button').remove();
				$('#activate-button').remove();
			}
		});

		/*Selecciona todos los elementos según el estado del principal*/
		$('#select-all').on('change', function(){
			if($(this).prop('checked')){
				$('.checkbox-element').prop('checked', true);				
			}else{
				$('.checkbox-element').prop('checked', false);
				$('#deactivate-button').remove();
				$('#activate-button').remove();
			}
		});

		/*Crea el botón desactivar*/
		function addDeactivateButton(){
			if($('.menu-content .button-content').find('#deactivate-button').length == 0){
				$buttonDeactivate = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'deactivate-button',"data-title":"Desactivar trackingulario(s)" ,"data-message":"¿Estás seguro que deseas desactivar el(los) trackingulario(s) seleccionado(s)?", "data-function":"changeState", "data-toggle":"modal", "data-target":"#normal-modal", "data-param":"0"}).html('Desactivar');
				$buttonActivate = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'activate-button',"data-title":"Activar trackingulario(s)","data-message":"¿Estás seguro que deseas activar el(los) trackingulario(s) seleccionado(s)?", "data-function":"changeState", "data-toggle":"modal", "data-target":"#normal-modal", "data-param":"1"}).html('Activar');
				$('.menu-content .button-content').prepend($buttonActivate);	
				$('.menu-content .button-content').prepend($buttonDeactivate);	
			}			
		}

		function getDetailTicket(id){
	
			dataDetailTicket={"idTicket" : id}
			 postUrl = 	"<?php echo site_url('Tracking/getDetailTickettracking'); ?>";
			console.log(postUrl);
			$.ajax({
			          type: "POST",
			          url: postUrl,
			          data: dataDetailTicket,
			          dataType: "text",
			          success: function(result) { 
					 // console.log(result);
					 $("#tableDetailTicket").html(result);
		    $("#myModal").modal("show");

					  $('#tableDetail').html(result);
			          },
			            error: function(xhr, ajaxOptions, thrownError) {
			              
			            }
			        });
		}

		function getDetailtracking(imei,idtracking,headId,name){

                   data = {
                     'tecname'   : name,
                    'trackingId'    : idtracking,
                    'imei'      : imei,
                    'fecha'    : '2017-09-28T15:46:19-03:00',
                    'numpet'    : 1,                 
                    'headId'   : headId
                   };
                
                
               	postUrl = 	"<?php echo site_url('Tracking/showtracking'); ?>";   
                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function(result) {
                        $("#divBloqueoPopup").css("display", "none");
                        $("#divBloqueoPopup").html("");
                        if(result != "NOK") {
                           
                             $("#tableDetailtracking").html(result);
		   					 $("#myModaltracking").modal("show");


                        } else {
                            swal("Alerta ToolBox!", "No hay datos!", "error");
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
		}


		

		$(".detalleIncidencia").click(function(){
		    var idDetailTicket = this.id;
		    getDetailTicket(idDetailTicket);
		
		    
		});$(".detalletrackingulario").click(function(){
		    var idDetailtracking = this.id;
		    var arrayDatos = idDetailtracking.split("|");
		    console.log(idDetailtracking);
		    getDetailtracking(arrayDatos[0],arrayDatos[1],arrayDatos[2],arrayDatos[3]);
		    	
		   
		
		    
		});

		$(".detalleTrazabilidad").click(function(){
			 var imei = this.id;
			 console.log(imei)
			 dataUser = {"imei" : imei};     
			 console.log(dataUser);
			 postUrl = 	"<?php echo site_url('Tracking/getTrazabilidad'); ?>";   

			 console.log("postUrl");
			 console.log(postUrl);
		     //postUrl = base_url + "index.php/Tracking/getTrazabilidad";         
		    // postUrl = base_url + "index.php/Tracking/getTrazabilidad";         
		       $.ajax({
		          type: "POST",
		          url: postUrl,
		          data: dataUser,
		          dataType: "text",
		          success: function(result) { 

		          	console.log(result);

				    //$("#tableDetailTicket").html(result);
				     $("#tableDetailTicket").html(result).toggle().toggle();
				    $("#myModal").modal("show");
		         console.log(result);
		          	
		          },
		            error: function(xhr, ajaxOptions, thrownError) {
		              
		            }
		        });
		})

		$(".VerPdf").click(function(e){

			//console.log(e);
			console.log("this");
			console.log(this);
			var id = this.id;
			var idx = id.split("|");
			console.log(idx);

			headId = idx[4];
			console.log(headId);
                data = {
                			'incident' : idx[0],
                			'title' : idx[1],
                			'imei' : idx[2],
                			'trackingId' : idx[3],
                			'headId' : idx[4],
                			'tecname' : idx[5],

           				 };

           				 console.log(data)

                // postUrl = base_url + "index.php/Tracking/showPDF?headId="+headId;
                postUrl = 	"<?php echo site_url('Tracking/showPDF').'?headId='; ?>"+headId;  
                console.log(postUrl);
                $.ajax({
                  type: "POST",
                  url: postUrl,
                  data: data,
                  dataType: "text",
                  success: function(result) {
                  	console.log(result);
                        // $("#stopTime").val(0);
                        // $("#divBloqueo2").css("display", "none");
                        // var url = postUrl;
                        var win = window.open(postUrl, '_blank');
                        win.focus();
                    },
                    error: function(xhr, ajaxOptions, thrownError) {}
                });
            });
		

		
	});
</script>