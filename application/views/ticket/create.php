<?php
	$hasData = false;
	if (isset($ticket)){
		$hasData = true;
	}
	
?>
<style>
	label {
		font-weight: bold !important;
		/*color: white;*/
		font-size: 15px;
	}
</style>
<head>
	<meta charset="UTF-8">
</head>
<div class="list">
	<ol class="breadcrumb" style="margin-top: -13px;">
		<li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a
				href="#"><strong style="color:black;"> Volver</strong></a></li>
		<li><a href="#"><strong>Carga</strong></a></li>
		<li class="active"><strong>Crear Carga</strong></li>
	</ol>
	<br>
	<hr>
</div>
<div class="container">
	<?php
		if (isset($edit)){
			echo form_open('ticket/edit/' . $ticket['id'], array('id' => 'edit-form'));
			echo form_hidden('ticket[id]', $ticket['id']);
		} else {
			echo form_open('ticket/createTicket');
		}
	?>
	<!-- <div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-default blue">Carga Masiva</button>	
			</div>	
		</div>
	</div> -->
	<input type="hidden" name="ticket[dirmanual]" id='dirmanual' value="">
	<div class="row">
		<!--<div class="col-md-3">
			<div class="form-group">
				<label for="ticket[type_incident]">Tipo de Carga:</label>
				<select name="ticket[type_incident]" id="type_incident" class="form-control selectPequeno">
					<option value="1" <?php if( $ticket['type_incident'] == 1 )  echo "selected" ?> >Pallet Completo</option>
					<option value="2" <?php if( $ticket['type_incident'] == 2 )  echo "selected" ?> >Pallet Mixto</option>
				</select>
			</div>
		</div>-->
		<div class="col-md-3">
			<div class="form-group">
				<label for="ticket[incident]">N° Carga:</label>
				<?php echo form_input(array('type' => 'text', 'name' => 'ticket[incident]', 'pattern' => '[A-Za-z0-9]+', 'title' => 'Ingrese solo caracteres alfanuméricos sin acentos', 'class' => 'form-control', 'value' => set_value('ticket[incident]', isset($ticket['incident']) ? $ticket['incident'] : $id[0]['id']))); ?>
				<?php echo form_error('ticket[incident]'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="tec[zona]">Zona:</label>
				<select id="selecZona" name="ticket[zona]" class="form-control selectPequeno">
					<option value="">Seleccione una Zona</option>
					<?php foreach ($zona as $indice => $value) {
						if ($value['id_zona'] != ""){
							?>
							<option value="<?php echo $value['id_zona'] ?>"> <?php echo $value['name'] ?> </option>
						<?php }
					} ?>
				</select>
				<?php echo form_error('ticket[zona]'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="ticket[grupo]">Grupos:</label>
				<select id="selectGrupos" name='ticket[grupo]' class="form-control selectPequeno">
					<option value="99999">Seleccione un grupo</option>
				</select>
				<?php echo form_error('ticket[grupo]'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="ticket[conductor]">Camion</label>
				<select name="ticket[conductor]" class="form-control" id="selectConductor">
					<option value="" selected="selected">Seleccione un Conductor</option>				
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		
		<div class="col-md-3">
			<div class="form-group">
				<label for="ticket[area]">Area:</label>
				<?php echo form_dropdown('ticket[area]', $area, set_value('ticket[area]', isset($ticket['area']) ? $ticket['area'] : ''), array('class' => 'form-control', 'id' => 'selectArea')); ?>
				<?php echo form_error('ticket[area]'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="ticket[bahia]">Bahia:</label>
				<?php echo form_dropdown('ticket[bahia]', $cliente, set_value('ticket[bahia]', isset($ticket['bahia']) ? $ticket['bahia'] : ''), array('class' => 'form-control', 'id' => 'selectBahia')); ?>
				<?php echo form_error('ticket[bahia]'); ?>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="ticket[title]">Comentario:</label>
				<?php echo form_input(array('type' => 'text', 'name' => 'ticket[title]', 'class' => 'form-control', 'value' => set_value('ticket[title]', isset($ticket['title']) ? $ticket['title'] : ''))); ?>
				<?php echo form_error('ticket[title]'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-buttons">
					<button type="submit" class="btn btn-success save-create">Guardar</button>
				</div>
			</div>
		</div>
	</div>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJc-O9rkJ6lxxGGKHXzukR519_4JGoPDA&libraries=places,visualization,drawing"
		sync defer></script>
	
	<script type="text/javascript">
		
		$(document).ready(function () {
			
			$('.save-create').on('click', function(){
				$('.save-create').hide();
			});
			
			
			$("#selecServicio").change(function (e) {
				var txtServicio = $("#selecServicio").val();
				getCategoria(txtServicio);
			})
			
			$("#selecCategoria").change(function (e) {
				var txtServicio = $("#selecServicio").val();
				var txtCategoria = $("#selecCategoria").val();
				getSla(txtServicio, txtCategoria);
			})
			
			initialize();
			
		});
		
		var direcciones = '';
		var slaGloval = '';
		
		
		$('#selectArea').on('change', function(event){
			
			var id_area = $(this).val();
			
			data = { 'id_area': id_area };
			
			postUrl = "<?php echo site_url('Ticket/getBahia'); ?>";
			
			$.ajax({
				type: "POST",
				url: postUrl,
				data: data,
				dataType: "text",
				success: function (result) {
					
					$('#selectBahia').empty();
					
					var obj = JSON.parse(result);
					
					$.each(obj, function(i, val){
						$('#selectBahia').append('<option value="'+val.id+'"">'+ val.name +'</option>');
					});
					
				},
				error: function (xhr, ajaxOptions, thrownError) { }
			
			});
			
		});
		
		function getCategoria(txtServicio) {
			data = {
				'txtServicio': txtServicio
			};
			if (txtServicio != null) {
				postUrl = "<?php echo site_url('Ticket/getCategoria'); ?>";
				$.ajax({
					type: "POST",
					url: postUrl,
					data: data,
					dataType: "text",
					success: function (result) {
						var subzona = (JSON.parse(result));
						var html = "<select>"
						html += "<option value=''> Seleccione un Categoria</option>"
						for (var i = 0; i < subzona.length; i++) {
							html += "<option value=" + subzona[i].categoria + ">" + subzona[i].categoria + "</option>"
						}
						html += "<select>"
						if (result == 'NOK') {
							console.log('aca !!!')
							swal("Mensaje ToolBox", "No se encuentran datos para esta busqueda.", "warning")
						} else {
							console.log('aca2 !!!')
							$("#selecCategoria").html(html);
							$("#selecSla").html("<option value=''>Seleccione un Tipo </option>");
							$("#sladesc").val("");
						}
						;
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
				});
			}
		}
		
		function getSla(txtServicio, txtCategoria) { //sla = tipo
			data = {
				'txtServicio': txtServicio,
				'txtCategoria': txtCategoria
			};
			if (txtServicio != null && txtCategoria != null) {
				postUrl = "<?php echo site_url('Ticket/getSla'); ?>";
				$.ajax({
					type: "POST",
					url: postUrl,
					data: data,
					dataType: "text",
					success: function (result) {
						var sla = (JSON.parse(result));
						slaGloval = sla;
						var html = "<select>"
						html += "<option value=''> Seleccione un Tipo</option>"
						for (var i = 0; i < sla.length; i++) {
							html += "<option value=" + sla[i].id + ">" + sla[i].tipo + "</option>"
						}
						html += "<select>"
						if (result == 'NOK') {
							console.log('aca !!!')
							swal("Mensaje ToolBox", "No se encuentran datos para esta busqueda.", "warning")
						} else {
							console.log('aca2 !!!')
							$("#selecSla").html(html);
							$("#sladesc").val("");
						}
						;
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
				});
			}
		}
		
		$("#selecSla").change(function (e) {
			var id = $("#selecSla").val();
			console.log(id);
			console.log(direcciones);
			
			for (var i = 0; i < slaGloval.length; i++) {
				console.log(slaGloval[i].id);
				
				if (slaGloval[i].id == id) {
					console.log('dentro del if')
					$('#sladesc').val(slaGloval[i].tds + ' ' + slaGloval[i].sla)
				}
			}
		});
		
		
		$("#selecZona").change(function (e) {
			var idzona = $("#selecZona").val();
			data = {
				'idzona': idzona,
				'idzona2': 'Región Metropolitana'
			};
			
			
			if (idzona != null) {
				
				postUrl = "<?php echo site_url('Ticket/getGrupos'); ?>";
				$.ajax({
					type: "POST",
					url: postUrl,
					data: data,
					dataType: "text",
					success: function (result) {
						var subzona = (JSON.parse(result));
						var html = "<select>"
						html += "<option value=''> Seleccione un grupo</option>"
						for (var i = 0; i < subzona.length; i++) {
							html += "<option value=" + subzona[i].id + ">" + subzona[i].name + "</option>"
						}
						html += "<select>"
						
						if (result == 'NOK') {
							console.log('aca !!!')
							swal("Mensaje ToolBox", "No se encuentran datos para esta busqueda.", "warning")
						} else {
							console.log('aca2 !!!')
							$("#selectGrupos").html(html);
							$("#selectDireccion").html("<option value=''>Seleccione una dirección</option>");
							$("#directions").val('');

						}
						;
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
				});
				
			}
		});
		
		$("#selectGrupos").change(function (e) {
			e.preventDefault();

			if($(this).val().length > 0) {
				$("#selectConductor").empty();
				var data = {
					'idZona'  : $("#selecZona").val(),
					'idGroup' : $(this).val()
				}
				postUrl = "<?php echo site_url('Ticket/getConductor'); ?>";
				$.ajax({
					type: "POST",
					url: postUrl,
					data: data,
					dataType: "text",
					success : function (data) {
						$("#selectConductor").append("<option>Seleccione un Conductor</option>");
						data = JSON.parse(data);
						for (var i = 0; i < data.length; i++) {
							$("#selectConductor")
							.append($("<option></option>")
         					.attr("value",data[i].id)
         					.text(data[i].name));
						}
					}, 
					error: function (xhr, ajaxOptions, thrownError) {

					}
				});
			} else {
				$("#selectConductor").empty();
				$("#selectConductor").append("<option>Seleccione un Conductor</option>");
			}
		});
		
		
		$("#selectDireccion").change(function (e) {
			var id = $("#selectDireccion").val();
			console.log(id);
			console.log(direcciones);
			
			if (id == '' || id == '99999') {
				$('#directions').val('');
			} else {
				
				for (var i = 0; i < direcciones.length; i++) {
					console.log(direcciones[i].id);
					
					if (direcciones[i].id == id) {
						console.log('dentro del if')
						$('#directions').val(direcciones[i].address)
					}
				}
				
			}
		});
		
		
		$("#directions").blur(function () {
			dir = $(this).val();
			console.log(dir);
			cliente = $('#selectCliente').val()
			idZona = $('#selecZona').val()
			dirmanual = $('#selectDireccion').val()
			console.log('val combo = ' + dirmanual)
			
			if (dirmanual == '' || dirmanual == '99999') {
				
				dataLatLng = {
					'direction': dir,
					'cliente': cliente,
					'idZona': idZona,
				};
				
				postUrl = "<?php echo site_url('Ticket/getLatLng'); ?>";
				$.ajax({
					type: "POST",
					url: postUrl,
					data: dataLatLng,
					dataType: "text",
					success: function (result) {
						
						if (result == 'ZERO_RESULTS' || result == 'OVER_QUERY_LIMIT') {
							console.log("direccion no valida reingrese");
							
							// chachtr de los tirtos
							$('#directions').val('');
							$('#directions').attr('placeholder', "Ingrese una dirección válida");
							$('#directions').css('border-color', 'red')
							$('#directions').focus();
						} else {
							console.log(result);
							$('#dirmanual').val(result);
							
						}
						
						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
				});
				
			}
			
			
		});
		
		$("#selectCliente").change(function () {
			
			console.log('acaaaaa');
			idCliente = $(this).val();
			
			
			if (idCliente != '') {
				
				dataCliente = {
					'idCliente': idCliente
				};
				
				postUrl = "<?php echo site_url('Ticket/services'); ?>";
				$.ajax({
					type: "POST",
					url: postUrl,
					data: dataCliente,
					dataType: "text",
					success: function (result) {
						console.log(result);
						
						var servicio = (JSON.parse(result));
						var html = "<select>"
						html += "<option value=''> Seleccione un grupo</option>"
						for (var i = 0; i < servicio.length; i++) {
							html += "<option value=" + servicio[i].servicio + ">" + servicio[i].servicio + "</option>"
						}
						html += "<select>"
						
						$("#selecServicio").html(html);
						
						
						// if (result=='ZERO_RESULTS' || result=='OVER_QUERY_LIMIT' ){
						// 	console.log("direccion no valida reingrese");
						
						// 	// chachtr de los tirtos
						// 	$('#directions').val('');
						// 	$('#directions').attr('placeholder',"Ingrese una dirección válida");
						// 	$('#directions').css('border-color','red')
						// 	$('#directions').focus();
						// }else{
						// console.log(result);
						// $('#dirmanual').val(result);
						
						// }
						
						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
				});
				
			}
			
			
		});
	</script>
	<script type="text/javascript">
		
		// de Google places API para ayudar a los usuarios rellenar la información.
		
		var placeSearch, autocomplete, autocomplete_textarea;
		var componentForm = {
			street_number: 'short_name',
			route: 'long_name',
			locality: 'long_name',
			administrative_area_level_1: 'short_name',
			country: 'long_name',
			postal_code: 'short_name'
		};
		
		function initialize() {
			// Cree el objeto de autocompletado, restringiendo la búsqueda
			autocomplete = new google.maps.places.Autocomplete(
				(document.getElementById('autocomplete')),
				{types: ['geocode']});
			// Cuando el usuario selecciona una dirección en el menú desplegable,
			// rellena los campos de dirección en el formulario.
			google.maps.event.addListener(autocomplete, 'place_changed', function () {
				fillInAddress();
			});
			
			
			autocomplete_textarea = new google.maps.places.Autocomplete((document.getElementById('autocomplete_textarea')),
				{types: ['geocode']}
			);
			google.maps.event.addListener(autocomplete_textarea, 'place_changed', function () {
				fillInAddress_textarea();
			});
		}
		
		function fillInAddress_textarea() {
			var place = autocomplete_textarea.getPlace();
			console.log(place.formatted_address);
			console.log(JSON.stringify(place));
			$('#autocomplete_textarea').val(place.formatted_address);
		}
		
		
		function fillInAddress() {
			// Obtener los detalles de lugar el objeto de autocompletado.
			var place = autocomplete.getPlace();
			console.log(JSON.stringify(place));
			for (var component in componentForm) {
				document.getElementById(component).value = '';
				document.getElementById(component).disabled = false;
			}
			
			// Recibe cada componente de la dirección de los lugares más detalles
			// y llena el campo correspondiente en el formulario.
			for (var i = 0; i < place.address_components.length; i++) {
				var addressType = place.address_components[i].types[0];
				if (componentForm[addressType]) {
					var val = place.address_components[i][componentForm[addressType]];
					document.getElementById(addressType).value = val;
				}
			}
		}
		
		
		//ubicación geográfica del usuario,
		
		function geolocate() {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (position) {
					var geolocation = new google.maps.LatLng(
						position.coords.latitude, position.coords.longitude);
					var circle = new google.maps.Circle({
						center: geolocation,
						radius: position.coords.accuracy
					});
					autocomplete.setBounds(circle.getBounds());
					autocomplete_textarea.setBounds(circle.getBounds());
				});
			}
		}
		
		$.datepicker.regional['es'] = {
			closeText: 'Cerrar',
			prevText: '<',
			nextText: '>',
			currentText: 'Hoy',
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
			weekHeader: 'Sm',
			//dateFormat: 'yy-mm-dd',
			dateFormat: 'dd-mm-yy',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
		$.datepicker.setDefaults($.datepicker.regional['es']);
		$.datepicker.regional['es'] = {minDate: '0D', maxDate: '1M'};
		$.datepicker.setDefaults($.datepicker.regional['es']);
		
		$("#fecha").datepicker();
		$('#datetimepicker1').datepicker();
		
		function validaAgenda() {
			
			if ($("#chkAgenda").prop("checked")) {
				$('#groupFecha').show();
				$('#tkAgenda').val('1'); // preventivo
				
			} else {
				$('#groupFecha').hide();
				$('#tkAgenda').val('0'); // correctivo
			}
		}
	
	</script>


	


	
