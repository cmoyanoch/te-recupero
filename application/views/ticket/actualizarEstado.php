<?php
/**
 * Created by PhpStorm.
 * User: Multiprint
 * Date: 05-09-2018
 * Time: 12:30
 */
?>
<style type="text/css">
    .tab-content > .tab-pane {
    display: block;}
    .panel-primary > .panel-heading
    {
        background: #E61D2A;
        border-color: #E61D2A;
    }
</style>
<?php echo form_open('Ticket/detail_Ticket/'.$search_value, array('name' => 'form1', 'id' => 'form1'));?>
<div class="panel panel-primary">
   <div class="panel-heading">Actualizar Estado</div>
   <div class="panel-body">
      <div class="row">
         <div class="col-md-12">
            <div class="col-md-6">
               <strong>Ticket</strong>
               <div id=""><?php echo $search_value;?></div>
            </div>
            <div class="col-md-6">
               <strong>Estado Actual</strong>
               <div id="edo_act_value"></div>
            </div>
         </div>
      </div>
      <div class="row" style="padding-top: 10px">
         <div class="col-md-12">
            <div class="col-md-6">
               <strong>Nuevo Estado</strong>
               <select id="sig_estados" name="sig_estados" class="form-control selectPequeno"">

               </select>
            </div>
            <div class="col-md-6">

            </div>
         </div>
      </div>
      <div class="row" style="padding-top: 10px">
         <div class="col-md-12">
            <div class="col-md-12">
                <strong>Comentario</strong>
               <?php echo form_textarea(array('name'=>'txt_com','class'=> 'form-control','id' => 'txt_com','rows' => '3', 'cols' => '30', 'style' => 'width: 100%')); ?>
            </div>
         </div>
      </div>
      <div class="row" style="padding-top: 10px">
         <div class="col-md-12">
            <div class="col-md-6">
               <!--div class="form-buttons" style="text-align: left;">
                  <a href="<?php echo site_url('tracking/tickets'); ?>" class="btn btn-success">atrás</a>
               </div-->
            </div>
            <div class="col-md-6">
               <div class="form-buttons">
                  <button type="submit" class="btn btn-default blue" id="id_guardar">Guardar</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php echo form_close(); ?>

<script>
    $(document).ready( function() {

         getInfo();

          getEstado();

        $('#id_guardar').on('click', function (event){
            event.preventDefault();
            $(this).tab('show');
            var edo_actual = $("#id_estado").val();
            if(edo_actual == 6 || edo_actual == 9 || edo_actual == 5) /* No se actualiza estado, si el estado es: Cerrado(6), Cancelado(9) */
            {
                swal({
                        title: "itr@ces",
                        text: "No se puede actualizar estado al ticket, su estado actual no lo permite!",
                        type: "info",
                        showCancelButton: false,
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    },
                    function(isConfirm){
                        if(isConfirm){
                            $form = $('#form5');
                            $form.submit();
                        }
                    }
                );
            }
               
        });

        function getEstado(){
            var search = $("#search_value").val();
            var postUrl       = "<?php echo site_url('Ticket/getEstado'); ?>";
            var data          = { 'id_ticket' : search};

            $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                cache: false,
                dataType: "text",
                success: function (result) {
                    $('#sig_estados option').remove();
                    var obj = JSON.parse(result);
                    $("#edo_act_value").text(obj.estado_actual);
                    if( (obj.estado_actual).toLowerCase() == 'Cerrado'.toLowerCase() || (obj.estado_actual).toLowerCase() == "Cancelado".toLowerCase() )
                    {
                        $("#sig_estados").attr('disabled','disabled');
                        $("#txt_com").attr('disabled','disabled');
                        $("#id_guardar").attr('disabled','disabled');
                    }
                    else
                    {
                        $.each(obj.estados, function(i, row){
                            $('<option/>', {
                                'value': row.id,
                                'text': row.name
                            }).appendTo('#sig_estados');
                            //$("#sig_estados").append('<option value='+row.id+'>'+row.name+'</option>');
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("itr@ces", "ERROR! VUELVA A INTENTARLO", "error");
                }
            });
        }

        $("#id_guardar").on('click', function (event) {
            event.preventDefault();
            var save = true;
            var sig_estados = $('#sig_estados').val();
            var txt_com = $('#txt_com').val();

            if(sig_estados == 0 || sig_estados == undefined){
                swal("itr@ces", "Você deve selecionar o estado para atribuir o ticket!", "info");
                save = false;
            }
            if(jQuery.trim(txt_com) == '' || txt_com == undefined){
                swal("itr@ces", "Você deve inserir uma Descrição!", "info");
                save = false;
            }
            if(!save){
                return false;
            }else{
                $('#loader-wrapper').show();
                var search = $("#search_value").val();
                var persistent_id = $('#persistent_id').val();
                var estado_actual = $('#name_estado').val();
                var formData = new FormData(document.getElementById("form1"));
                formData.append("id_ticket", search);
                formData.append("persistent_id", persistent_id);
                formData.append("estado_actual", estado_actual);
                formData.append("estado_nuevo", $("#sig_estados option:selected").text());
                //formData.append("str_grupos", id_grupo);
                var url = "<?php echo site_url('Ticket/actEstado')?>";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: formData,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#loader-wrapper').hide();
                        var obj = JSON.parse(data);
                        //console.log(obj);
                        if (persistent_id) {
                            if (obj[0].Value == "OK") {
                                swal({
                                        title: "itr@ces",
                                        text: obj[1].Value,
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonText: "OK",
                                        closeOnConfirm: false
                                    },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            $form = $('#form1');
                                            $form.submit();
                                        }
                                    }
                                );
                            }
                            else{
                                swal("itr@ces", obj[1].Value, "error");
                                $("#sig_estados").val(0);
                                $("#txt_com").val('');
                            }
                        }
                        else{
                            if(obj == 'OK'){
                                swal({
                                        title: "itr@ces",
                                        text: "Estado atualizado com sucesso!",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonText: "OK",
                                        closeOnConfirm: false
                                    },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            $form = $('#form1');
                                            $form.submit();
                                        }
                                    }
                                );
                            }
                            else
                            {
                                swal("itr@ces", "Erro ao atualizar o status do ticket!", "error");
                                $("#sig_estados").val(0);
                                $("#txt_com").val('');

                            }
                        }

                    },
                    error: function (r) {
                        swal("itr@ces", "ERROR! VUELVA A INTENTARLO", "error");
                        $("#sig_estados").val(0);
                        $("#txt_com").val('');
                    }
                });
                return true;
            }

        });

    });


        function getInfo(){
            $('#loader-wrapper').show();
            var search  = $("#search_value").val();
            var postUrl = "<?php echo site_url('Ticket/getInfo'); ?>";
            var data    = { 'id_ticket' : search};

            $.ajax({
                type: "POST",
                url: postUrl,
                data: data,
                cache: false,
                dataType: "text",
                success: function (result) {
                    var obj = JSON.parse(result);
                    var tipo;
                    $("#ticket_value").text(obj.incident);
                    $("#ticket_value").text(obj.incident);
                    $("#coordinador_value").text(obj.whole_name);
                    if(obj.name)
                        $("#tecnico_value").text(obj.name);
                    else
                        $("#tecnico_value").text("");
                    $("#estado_value").text(obj.estado);
                    if(obj.type_incident == 1)
                        tipo = "Incidente";
                    else if(obj.type_incident == 2)
                        tipo = "Requerimiento";
                    $("#tipo_value").text(tipo);

                    /* INPUTS HIDDEN index */
                    $("#name_estado").val(obj.estado);
                    $("#id_estado").val(obj.idstatus);
                    $("#id_zona").val(obj.id_zona);
                    $("#name_grupo").val(obj.grupo);
                    $("#name_tecnico").val($("#tecnico_value").text());
                    $("#address_ticket").val(obj.address);
                    $("#name_coordinador").val(obj.whole_name);
                     $("#persistent_id").val(obj.persistent_id);
                    /* FIN INPUTS HIDDEN index */

                    $("#grupo_value").text(obj.grupo);
                    $("#titulo_value").text(obj.title);
                    $("#descripcion_value").text(obj.description);
                    

                    $("#address_value").text(obj.address);
                    $("#lat_value").text(obj.coordX);
                    $("#lng_value").text(obj.coordY);

                    $("#fcreacion_value").text(obj.fcreacion);
                    $("#fact_value").text(obj.fact);
                    $("#fcierre_value").text(obj.fcierre);

                    $("#fagenda_value").text(obj.fagenda);

                    $('#loader-wrapper').hide();

                },
                error: function (xhr, ajaxOptions, thrownError) { }
            });
        }
   
</script>