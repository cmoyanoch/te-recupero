<style>
.text{color: #fff;}
</style>
<div class="list">
	<div class="container-fluid menu-content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Carga masiva de ticket</h1>			
				</div>
			</div>			
		</div>		
	</div>
</div>
<div class="container">

	<?php echo form_open_multipart('cargamasiva/do_upload');?>
	<div class="row">
		<div class="col-md-6">			
			<div class="form-group">
				<label for="file" class="text">Seleccione el archivo csv:</label>
				<input class="text" type="file" name="userfile" size="20" />
			</div>
		</div>
		<div class="col-md-6">			
			<div class="form-group">
				<label for="file" class="text">Descarga un csv ejemplo:</label>
				<a href="<?php echo site_url('assets/csvejemplo.csv');?>"><div class="text">  Descargar aqui</div></a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-default blue">Guardar</button>	
			</div>	
		</div>
	</div>

	<?php echo form_close();?>
</div>



<?php 
	$hasData = false;
	if(isset($resultado)){?>

	<div class="container ">
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">									
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
								<!-- <td class="cell-selection"><?php //echo form_checkbox('select-all', '', FALSE, array('id'=>'select-all')); ?></td>	 -->							
								<td>Tickets cargados</td>
								<td class="with-icon"></td>
							</tr>				
						</thead>
						<tbody>
							<?php foreach ($resultado as $row){?>
								<tr>										
									<td><?php echo $row['error'] ?></td>
									<td><?php echo $row['id']['message'] ?>			
									</td>							
								</tr>							
							<?php } ?>						
						</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>

<?php
}
//new dBug($resultado);exit();
?>