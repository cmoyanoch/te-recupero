<?php 
	$hasData = false;
	if(isset($zona)){
		$hasData = true;
	}

?>
<div class="list">
<div class="container-fluid menu-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php echo $title ?></h1>			
			</div>
		</div>			
	</div>		
</div>
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('zona/edit/'.$zona['id_zona'], array('id'=>'edit-form'));
		echo form_hidden('zona[id_zona]', $zona['id_zona']);
	}else{
		echo form_open('zona/create'); 	
	}	
	?>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-group">
				<label for="zona[name]">Nombre Región:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'zona[name]', 'class'=> 'form-control', 'value'=> set_value('zona[name]', isset($zona['name'])? $zona['name'] :''))); ?>
				<?php echo form_error('zona[name]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-default blue">Guardar</button>	
			</div>	
		</div>
	</div>
