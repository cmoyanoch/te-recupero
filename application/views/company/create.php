<?php 
	$hasData = false;
	if(isset($company)){
		$hasData = true;
	}

?>

<style>
label{
	font-weight: bold !important;
    color: white;
    font-size: 15px;
}
</style>

<div class="list">
<div class="container-fluid menu-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php echo $title ?></h1>			
			</div>
		</div>			
	</div>		
</div>
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('company/edit/'.$company['company_id'], array('id'=>'edit-form'));
		echo form_hidden('company[company_id]', $company['company_id']);
	}else{
		echo form_open('company/create'); 	
	}	
	?>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-group">
				<label for="company[name]">Nombre empresa:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'company[name]', 'class'=> 'form-control', 'value'=> set_value('company[name]', isset($company['name'])? $company['name'] :''))); ?>
				<?php echo form_error('company[name]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-default blue">Guardar</button>	
			</div>	
		</div>
	</div>
