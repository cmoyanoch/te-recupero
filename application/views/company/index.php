<div class="list">
	<div class="container-fluid menu-content">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<?php echo form_open(site_url('company/index'), array('method'=>'get')); ?>
					<div class="input-group">
						<?php echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué empresa deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>					
						<span class="input-group-btn">
							<button class="btn btn-default blue" type="submit">Buscar</button>
						</span>
					</div>
					<?php echo form_close(); ?>
				</div>
				<div class="col-md-6 button-content">								
					<a href="<?php echo site_url('company/create'); ?>" class="btn btn-default blue">Crear</a>				
				</div>
			</div>			
		</div>		
	</div>
	<div class="container ">
		<div class="row">
			<div class="col-md-12">				
				<?php if (count($companies) > 0){ ?>
				<div class="table-responsive">
					<?php echo form_open(site_url('company/delete'), array('id'=>'list-form')) ?>										
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
								<td class="cell-selection"><?php echo form_checkbox('select-all', '', FALSE, array('id'=>'select-all')); ?></td>								
								<td>Empresa</td>
								<td class="with-icon"></td>
								<td class="with-icon"></td>
							</tr>				
						</thead>
						<tbody>
							<?php foreach ($companies as $keyCompany => $company): ?>							
								<tr>								
									<td><?php echo $company['can_delete'] ? form_checkbox('company[company_id][]', $company['company_id'], FALSE,array('class'=> 'checkbox-element')) : '';?></td>			
									<td><?php echo $company['name'] ?></td>
									<td>
										<?php if ($company['can_delete']): ?>
											<a title="Eliminar" href="" data-title="Eliminar empresa" data-message="¿Estás seguro que deseas eliminar la empresa <?php echo $company['name'] ?>?" data-function="deleteCompany" data-param="<?php echo $company['company_id'] ?>" data-toggle="modal" data-target="#normal-modal">
												<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete' ?>"></use></svg>
											</a>	
										<?php endif ?>										
									</td>
									<td>									
										<a title="Editar" href="<?php echo site_url('company/edit/'.$company['company_id']) ?>">
											<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-edit' ?>"></use></svg>
										</a>										
									</td>								
								</tr>							
							<?php endforeach ?>						
						</tbody>
					</table>
				</div>
				<?php echo form_close(); ?>				
				<?php }else{ 
					if ($search_value != "") {
						echo "<p class='message-no-data'>No existen resultados con los criterios de búsqueda ingresados.</p>";
						echo "<p class='message-no-data link'><a href='".site_url('company/index')."'>Volver al inicio</a></p>";
					}else{
						echo "<p class='message-no-data'>El sistema aun no registra información.</p>";
					}	
				} ?>				
			</div>	
		</div>		
		<div class="row">
			<div class="col-md-12 pagination-content">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<?php echo $pagination; ?>					
					</ul>
				</nav>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		<?php if ($search_value): ?>			
			$("#search-value").focus();
			$("#search-value").val($("#search-value").val());
		<?php endif ?>

		/*Al cargar la web selecciona todos los elementos según el estado del principal*/
		if($('#select-all').prop('checked')){
			$('.checkbox-element').prop('checked', true);
			addDeleteButton();
		}else{
			$('.checkbox-element').prop('checked', false);
			$('#delete-button').remove();			
		}
		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		if($('input.checkbox-element:checked').length > 0){
			addDeleteButton();
		}

		/*Función del botón Aceptar del modal al eliminar una categoría*/
		$(document).on('click', '#accept-button-modal.deleteCompany', function(){
			$param = ($(this).data('param'));
			$form = $('#list-form');
			if($param != null){
				$('.checkbox-element').prop('checked', false);
				$form.append($('<input/>').attr({"type":"hidden", "name":"company[company_id][]", "value":$param}));		             
			}
            $form.submit();
            $('#accept-button-modal').removeClass('deleteCompany');
			$('#accept-button-modal').removeData('param');
			$('#normal-modal').modal('hide');            
		})

		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		$('#list-form').on('change', function(){			
			if($('input.checkbox-element:checked').length > 0){
				addDeleteButton();
			}else{
				$('#delete-button').remove();				
			}
		});

		/*Selecciona todos los elementos según el estado del principal*/
		$('#select-all').on('change', function(){
			if($(this).prop('checked')){
				$('.checkbox-element').prop('checked', true);				
			}else{
				$('.checkbox-element').prop('checked', false);
				$('#delete-button').remove();				
			}
		});

		/* Crea el botón eliminar */
		function addDeleteButton(){
			if($('.menu-content .button-content').find('#delete-button').length == 0){
				$deleteButton = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'delete-button',"data-title":"Eliminar empresa(s)" ,"data-message":"¿Estás seguro que deseas eliminar el(las) empresa(s) seleccionada(s)?", "data-function":"deleteCompany", "data-toggle":"modal", "data-target":"#normal-modal"}).html('Eliminar');				
				$('.menu-content .button-content').prepend($deleteButton);	
			}			
		}
		

		
	});
</script>