<div class="list">
    <div class="container-fluid menu-content">
        <div class="container">
            <div class="row col-md-12">
                <div class="col-md-6">
                    <?php echo form_open(site_url('categoryCA/index'), array('method'=>'get')); ?>
                    <div class="input-group">
                        <?php echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué categoría deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>
                        <span class="input-group-btn">
							<button class="btn btn-default blue" type="submit">Buscar</button>
						</span>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="col-md-6 button-content">
                    <a href="<?php echo site_url('categoryCA/create'); ?>" class="btn btn-default blue">Crear</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container ">
        <div class="row">
            <div class="col-md-12">
                <?php if (count($ListCategory) > 0){ ?>
                    <div class="table-responsive">
                        <table class="table table-hover" id="list-table">
                            <thead>
                            <tr>
                                <td>Nombre</td>
                                <td>Nombre Corto</td>
                                <td>Hora</td>
                                <td class="with-icon"></td>
                                <td class="with-icon"></td>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($ListCategory as $category): ?>
                                    <tr>
                                        <td><?php echo $category['name'] ?></td>
                                        <td><?php echo $category['name_short'] ?></td>
                                        <td><?php echo $category['hour'] ?></td>
                                        <td>
                                            <a title="Eliminar" href="" data-title="Eliminar Categoria CA" data-message="¿Estás seguro que deseas eliminar la Categoria <?php echo $category['name'] ?>?" data-function="deleteCountry" data-param="<?php echo $category['id'] ?>" data-toggle="modal" data-target="#normal-modal">
                                                <svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete' ?>"></use></svg>
                                            </a>
                                        </td>
                                        <td>
                                            <a title="Editar" href="<?php echo site_url('country/edit/'.$category['id']) ?>">
                                                <svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-edit' ?>"></use></svg>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>