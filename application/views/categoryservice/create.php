<?php 
	$hasData = false;
	if(isset($categoriaservice)){
		$hasData = true;
	}

?>
<div class="list">
<div class="container-fluid menu-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php echo $title ?></h1>			
			</div>
		</div>			
	</div>		
</div>
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('categoryservice/edit/'.$category_service[0]->id, array('id'=>'edit-form'));
		echo form_hidden('category_service[id]', $category_service[0]->id);
	}else{
		echo form_open('categoryservice/create'); 	
	}	
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="category_service[nombre]">Nombre Categoria padre:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'category_service[nombre]', 'class'=> 'form-control', 'value'=> set_value('category_service[nombre]', isset($category_service[0]->nombre)? $category_service[0]->nombre :''))); ?>
				<?php echo form_error('category_service["nombre"]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-default blue">Guardar</button>	
			</div>	
		</div>
	</div>
