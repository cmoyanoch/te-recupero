<div class="list">
  <ol class="breadcrumb" style="background-color: #1E3652 !important;color:#fff">
    	  <div class="card-header" >Crear Factoring</div>
    </ol>               
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('zona/edit/'.$zona['id_empresa'], array('id'=>'edit-form'));
		echo form_hidden('zona[id_empresa]', $zona['id_empresa']);
	}else{
		echo form_open('zona/create'); 	
	}	
	?>
	<div class="row">

		<div class="col-md-6">			
			<div class="form-group">
				<label for="zona[name]">Nombre Factoring:</label>
	<?php echo form_input(array('type'=>'text', 
	'name'=>'zona[name]', 
	'class'=> 'form-control', 
	'value'=> set_value('zona[name]', isset($zona['nombre_empresa'])? $zona['nombre_empresa'] :'')

  )); ?>
				<?php echo form_error('zona[name]'); ?>
			</div>
		</div>

		<div class="col-md-6">			
			<div class="form-group">
				<label for="zona[rut]">Rut Factoring:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'zona[rut]', 'class'=> 'form-control', 'value'=> set_value('zona[rut]', isset($zona['rut_empresa'])? $zona['rut_empresa'] :''))); ?>
				<?php echo form_error('zona[rut]'); ?>
			</div>
		</div>

	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-default blue">Guardar</button>	
			</div>	
		</div>
	</div>
