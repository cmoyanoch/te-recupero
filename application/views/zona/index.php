<div class="list">  
  <ol class="breadcrumb" style="background-color: #1E3652 !important;color:#fff">
    	  <div class="card-header" >Mantenedor de Factoring</div>
    </ol>               
    
	<div class="container-fluid menu-content">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<?php echo form_open(site_url('zona/index'), array('method'=>'get')); ?>
					<div class="input-group">
						<?php echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué factoring deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>					
						<span class="input-group-btn">
							<button class="btn btn-default blue" type="submit">Buscar</button>
						</span>
					</div>
					<?php echo form_close(); ?>
				</div>
				<div class="col-md-6 button-content">								
					<a href="<?php echo site_url('zona/create'); ?>" class="btn btn-default blue">Crear</a>				
				</div>
			</div>			
		</div>		
	</div>
	<div class="container ">
		<div class="row">
			<div class="col-md-12">				
				<?php if (count($zonaes) > 0){ ?>
				<div class="table-responsive">
					<?php echo form_open(site_url('zona/delete'), array('id'=>'list-form')) ?>										
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
							
								<td>Factoring</td>
								<td>Rut</td>
							<!--	<td class="with-icon"></td> -->
								<td class="with-icon"></td>
							</tr>				
						</thead>
						<tbody>
							<?php foreach ($zonaes as $keyzona => $zona): ?>							
								<tr>								
			
									<td><?php echo $zona['nombre_empresa'] ?></td>
									<td><?php echo $zona['rut_empresa'] ?></td>

									<td>
		<a title="Eliminar" onclick="return confirm('¿Estás seguro que deseas eliminar el factoring <?php echo $zona['nombre_empresa'] ?>?');" href="<?php echo site_url('Zona/delete/?id=') .$zona['id_empresa']; ?>" data-title="Eliminar factoring">
		<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete' ?>"></use></svg></a>	

									</td>
					
								</tr>							
							<?php endforeach ?>						
						</tbody>
					</table>
				</div>
				<?php echo form_close(); ?>				
				<?php }else{ 
					if ($search_value != "") {
						echo "<p class='message-no-data'>No existen resultados con los criterios de búsqueda ingresados.</p>";
						echo "<p class='message-no-data link'><a href='".site_url('zona/index')."'>Volver al inicio</a></p>";
					}else{
						echo "<p class='message-no-data'>El sistema aun no registra información.</p>";
					}	
				} ?>				
			</div>	
		</div>		
		<div class="row">
			<div class="col-md-12 pagination-content">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<?php echo $pagination; ?>					
					</ul>
				</nav>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){

		<?php if ($search_value): ?>			
			$("#search-value").focus();
			$("#search-value").val($("#search-value").val());
		<?php endif ?>
		

		
	});
</script>