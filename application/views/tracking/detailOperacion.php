<!-- <pre> -->
<?php
//print_r($historicoTicket);
?>
<!-- </pre> -->
<div class="container-fluid" style="width:100%;" >
	
	<div class="container">
		<div class="row">
			<div class="col">				
				<div class="table-responsive">
															
					<table class="table table-hover" id="list-table" >
						<thead style="font-size:60%;" >
							<tr>
                                                           <th>   </th>

							  <th>   </th>	
							</tr>				
						</thead>

						<tbody style="font-size:65%;"  >
						<?php foreach ($data['DetalleOpperacion'] as $keyForm => $campo): ?>	
						<tr>
						   <td><strong>Nombre Cliente</strong></td> 
						   <td><?php echo $campo['nombre_cliente']; ?></td>
						</tr>	
						<tr>
							<td><strong>R.U.T</strong></td> 
						      <td><?php echo $campo['rut_cliente']; ?></td>
					        </tr>

						<tr>
							<td><strong>Dirección</strong></td> 
							 <td><?php echo $campo['direccion_cliente']; ?></td>
						</tr>		
<tr style="background-color:#1E3652;"   ><th></th><th></th></tr>

                                       
						<tr>
							<td><strong>N° Operación</strong></td> 
							 <td><?php echo $campo['nro_opera']; ?></td>
						</tr>	
						<tr>
							<td><strong>Fecha ingreso operación</strong></td> 
							 <td><?php echo $campo['date_ingreso']; ?></td>
						</tr>
						<tr>
							<td><strong>Estado</strong></td> 
							 <td><?php echo $campo['name']; ?></td>
						</tr>							
						<tr>
							<td><strong>Fecha de otorgamiento</strong></td> 
							 <td><?php echo $campo['date_opera']; ?></td>
						</tr>	
						<tr>
							<td><strong>Tipo de Documento</strong></td> 
							 <td><?php echo $campo['nom_documento']; ?></td>
						</tr>

 <tr style="background-color:#1E3652;"   ><th></th><th></th></tr>

						<tr>
							<td><strong>Cantidad documentos</strong></td> 
							 <td><?php echo $campo['cant_doc']; ?></td>
						</tr>	
						<tr>
							<td><strong>Monto de documento(s)</strong></td> 
							 <td><?php echo $campo['monto_doc']; ?></td>
						</tr>
 <tr style="background-color:#1E3652;"   ><th></th><th></th></tr>

						<tr>
							<td><strong>Monto anticipado</strong></td> 
							 <td><?php 
							$monto = $campo['monto_doc'] * $campo['porc_ant'] /100;
							 echo $monto; ?></td>
						</tr>	
						<tr>
							<td><strong>Diferencia de precio</strong></td> 
							 <td><?php 
                                 
                                 $dif = $campo['monto_doc'] - $monto; 


							 echo $dif; ?></td>
						</tr>
						<tr>
							<td><strong>Precios de la cesión</strong></td> 
							 <td><?php echo $campo['preci_ces']; ?></td>
						</tr>							
						<tr>
							<td><strong>Monto no Financiado</strong></td> 
							 <td><?php echo $campo['monto_nof']; ?></td>
						</tr>	
						<tr>
							<td><strong>Monto adelanto</strong></td> 
							 <td><?php echo $campo['monto_adelan']; ?></td>
						</tr>

 <tr style="background-color:#1E3652;"   ><th></th><th></th></tr>

						<tr>
							<td><strong>Comision</strong></td> 
							 <td><?php echo $campo['comi_total']; ?></td>
						</tr>
						<tr>
							<td><strong>Gastos</strong></td> 
							 <td><?php echo $campo['monto_gasto']; ?></td>
						</tr>							
						<tr>
							<td><strong>Iva</strong></td> 
							 <td><?php

                             $iva = $campo['comi_total'] * 0.19;


							  echo $iva; ?></td>
						</tr>	
						<tr>
							<td><strong>Monto aplicado</strong></td> 
							 <td><?php echo $campo['monto']; ?></td>
						</tr>
						 <tr style="background-color:#1E3652;"   ><th></th><th></th></tr>
                                                 <tr>
						    <td><strong>Liquido a girar $</strong></td> 
						    <td><?php echo $campo['total_girado']; ?></td>
						</tr>

                        <?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>	
		</div>		
	</div>
</div>
