<script src="https://use.fontawesome.com/32535cb184.js"></script>


  <?php echo form_open(site_url('tracking/tickets'), array('method' => 'get')); ?>
  <div class="card border-primary mb-3" >
  <div class="card-header"  style="background-color: #1E3652 !important;color:#fff" >Filtro de busqueda</div>
  <div class="card-body text-primary">
  <div class="form-row">

  <div class="form-group col-md-4"> <?php echo form_dropdown('factoriFilter', $factoriFilter, set_value('factoriFilter',isset($factoriFilter['factoriFilter']) ? $factoriFilter['factoriFilter'] : ''),array('class' => 'form-control', 'name' => 'filterFacto', 'id' => 'filterFacto')); ?><?php echo form_error('factoriFilter'); ?></div>

  <div class="form-group col-md-4">
 <?php echo form_dropdown('clienteFilter', $clienteFilter, set_value('clienteFilter',isset($clienteFilter['clienteFilter']) ? $clienteFilter['clienteFilter'] : ''),array('class' => 'form-control', 'name' => 'filterCliente', 'id' => 'filterCliente'));?><?php echo form_error('clienteFilter'); ?></div>


  <div class="form-group col-md-3"> <?php echo form_input(array('type' => 'text', 'name' => 'filterOpera', 'class' => 'form-control', 'placeholder' => '¿Qué Operacion deseas buscar?', 'value' => $search_value, 'id' => 'filterOpera')) ?></div>

  <div class="form-group col-md-1"><button class="btn btn-success"  type="submit">Buscar</button></div>   
  </div>  
  </div>   
  </div>
  <?php echo form_close(); ?>





	<div class="container-fluid ">
		<div class="row">
			<div class="col-md-12">

					
					<div class="table-responsive">
						<?php echo form_open(site_url('tracking/delete'), array('id' => 'list-form')) ?>
						<table class="table table-hover table-responsive" id="list-table">
							<thead style="font-size:60%;" >

<th>Cliente</th>
<th>Rut</th>
<th>Nº Operación</th>
<th>Fecha Operación</th>
<th>Documentos</th>
<th>Cant. Documentos</th>
<th>Monto Documentos</th>
<th>Monto Financiado</th>
<th>Diferencia Precio</th>
<th>Valor Compra</th>
<th>Monto no Financiado</th>
<th>Comisión Total</th>
<th>IVA Comisión</th>
<th>Monto Gastos</th>
<th>Monto Adelanto</th>
<th>Monto Aplicado</th>
<th>Total Girado</th>
                   					
							</thead>
							<tbody>
			
<?php foreach ($tickets as $keytracking => $ticket){ ?>
<tr data-id="<?php echo $ticket['id']; ?>" style="font-size:60%;" >    

<td><?php echo $ticket['nombre_empresa']; ?></td>
<td><?php echo $ticket['rut_empresa']; ?></td>
<td><button type="button" class="btn btn-default detalleOperacion " id='<?php echo $ticket['nro_opera']; ?>' style="font-size:110%;"  > <?php echo $ticket['nro_opera']; ?></button> </td>
<td><?php echo $ticket['date_opera']; ?></td>
<td><?php echo $ticket['tipo_doc']; ?></td>

<td><button type="button" class="btn btn-default detalleDocumento " id='<?php echo $ticket['nro_opera']; ?>' style="font-size:110%;"  > <?php echo $ticket['total_doc']; ?></button> </td>

<td><?php echo $ticket['monto_doc']; ?></td>
<td><?php echo $ticket['mto_nomant']; ?></td>
<td><?php echo $ticket['mto_difpre']; ?></td>
<td><?php echo $ticket['precio_cesion']; ?></td>
<td><?php echo $ticket['mto_noant']; ?></td>
<td><?php echo $ticket['comi_total']; ?></td>
<td><?php echo $ticket['iva_comi']; ?></td>
<td><?php echo $ticket['monto_gasto']; ?></td>
<td><?php echo $ticket['monto_adelan']; ?></td>
<td><?php echo $ticket['monto_aplica']; ?></td>
<td><?php echo $ticket['total_girado']; ?></td>

</tr>
<?php } ?> 
							</tbody>
						</table>
					</div>
					<?php echo form_close(); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 pagination-content">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<?php echo $pagination; ?>
					</ul>
				</nav>
			</div> 
		</div>
	</div>

<script type="text/javascript">
	
	$(document).ready(function () {
		
		
		
		$("#filterFacto").change(function (e) {
			var idzona = $("#filterFacto").val();
			data = {
				'idzona': idzona,
				'idzona2': 'Región Metropolitana'
			};
			
			
			if (idzona != 0) {
				
				postUrl = "<?php echo site_url('Ticket/getGrupos'); ?>";
				$.ajax({
					type: "POST",
					url: postUrl,
					data: data,
					dataType: "text",
					success: function (result) {
						var subzona = (JSON.parse(result));
						var html = "<select>"
						html += "<option value=''> Seleccione un grupo</option>"
						for (var i = 0; i < subzona.length; i++) {
							html += "<option value=" + subzona[i].id + ">" + subzona[i].name + "</option>"
						}
						html += "<select>"
						
						if (result == 'NOK') {
							console.log('aca !!!')
							swal("Mensaje", "No se encuentran datos para esta busqueda.", "warning")
						} else {
							console.log('aca2 !!!')
							$("#filterGroup").html(html);
							
						}
						;
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
				});
				
			}else{
				
				$("#filterGroup option").remove();
				$("#filterGroup").append("<option value=''>Seleccione un grupo</option>");
				
			}
		});
		
		
		
		
		
		
		<?php if ($search_value): ?>
		$("#search-value").focus();
		$("#search-value").val($("#search-value").val());
		<?php endif ?>
		
		
		
	});

</script>

<div class="modal fade" id="myModal" role="dialog"  style="width:100%;"  >
	<div class="modal-dialog modal-xl">
	    <div class="modal-content">
             <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal">&times;</button> 
                   <h4 class="modal-title">DETALLE LIQUIDACIÓN FACTORING</h4>
              </div>
	          <div class="modal-body">
	              <p id="tableDetailDocumento">The <strong>show</strong> method shows the modal and the <strong>hide</strong>method hides the modal.</p>
	          </div>
	          <div class="modal-footer">
	               <button type="button" id="btnAceptaAsig" class="btn btn-default" data-dismiss="modal">Aceptar</button>
	         </div>
         </div>
	</div>
</div>






<div class="modal fade" id="myModal2" role="dialog"  style="width:100%;"  >
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">LIQUIDACION FACTORING</h4>
			</div>
			<div class="modal-body">
				<p id="tableDetailOperacion">The <strong>show</strong> method shows the modal and the <strong>hide</strong>
					method hides the modal.</p>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnAceptaAsig" class="btn btn-default" data-dismiss="modal">Aceptar</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">






	$(function () {
	

         	<?php if ($search_value): ?>
		$("#search-value").focus();
		$("#search-value").val($("#search-value").val());
		<?php endif; ?>
			
		
		$(".detalleDocumento").click(function () {
			var oper = this.id;
			dataUser = {"oper": oper};
			postUrl = "<?php echo site_url('Tracking/getDocumento'); ?>";
			
			$.ajax({
				type: "POST",
				url: postUrl,
				data: dataUser,
				dataType: "text",
				success: function (result) {
					$("#tableDetailDocumento").html(result).toggle().toggle();
					$("#myModal").modal("show");
				},
				error: function (xhr, ajaxOptions, thrownError) {
				
				}
			});
		});



            	$(".detalleOperacion").click(function () {
			var oper = this.id;

                       
			dataUser = {"oper": oper};
			postUrl = "<?php echo site_url('Tracking/getOperacion'); ?>";
			
			$.ajax({
				type: "POST",
				url: postUrl,
				data: dataUser,
				dataType: "text",
				success: function (result) {    
					$("#tableDetailOperacion").html(result).toggle().toggle();
					$("#myModal2").modal("show"); 
				},
				error: function (xhr, ajaxOptions, thrownError) {
				
				}
			});
		});



		
		$(".VerPdf").click(function (e) {
			var id = this.id;
			var idx = id.split("|");
			headId = idx[4];
			data = {
				'incident': idx[0],
				'title': idx[1],
				'imei': idx[2],
				'trackingId': idx[3],
				'headId': idx[4],
				'tecname': idx[5]
			};
			postUrl = "<?php echo site_url('Tracking/showPDF') . '?headId='; ?>" + headId;
			console.log(postUrl);
			$.ajax({
				type: "POST",
				url: postUrl,
				data: data,
				dataType: "text",
				success: function (result) {
					var win = window.open(postUrl, '_blank');
					win.focus();
				},
				error: function (xhr, ajaxOptions, thrownError) {
				}
			});
		});
	});


</script>
	
