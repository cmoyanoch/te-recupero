<!-- <pre> -->
<?php
//print_r("tipo = ".$tipo);

//print_r($historicoTicket);

?>
<!-- </pre> -->
<div class="container-fluid" style="width:100%;">



<div class="row">
<div class="col">

 <span class="float-right">
<button type="button" class="btn btn-success btn-sm documento"  > +  Documento</button>
<button type="button" class="btn btn-danger btn-sm savedoc"  >Guardar Documentos</button>


</span>
    </div>
</div>
   	
	<div class="container">
		<div class="row">
			<div class="col">				
                                <?php echo form_open('#', array('id' => 'form_in')); ?>
				<?php if (count($data['historicoTickets']) >= 0){ ?>
				<div class="table-responsive">
					<table class="table table-hover" id="list-table2">
						<thead style="font-size:60%;" >
							<tr>
							        <th>Deudor</th>
		         			        	<th>Rut Deudor </th>
								<th>Fecha Emisión </th>
								<th>Fecha Vencimiento</th>
								<th>Días </th>
								<th>Numero Documento</th>
								<th>Codigo Banco</th>
								<th>Monto Documento </th>
								<th>Porcentaje Anticipo </th>
								<th>Mto.Nom.Ant. </th>
								<th>Monto NO Anticipado</th>
								<th>Monto Diferencia Precio</th>
								<th>Precio Cesión </th>
								<th>Mto.Ant. S/Dctos. </th>
								
								
							</tr>				
						</thead>
                                          	<tbody>   
							<?php foreach ($data['historicoTickets'] as $keyForm => $historicoTicket): ?>							
							<tr data-id ="<?php echo $form['form_id']; ?>" style="font-size:60%;" >								
									
							<td><?php echo $historicoTicket['deudor'] ?></td>
							<td class="text-center"><?php echo $historicoTicket['rut_deudor'] ?></td>
							<td class="text-center"><?php echo $historicoTicket['date_emisor'] ?></td>
							<td class="text-center"><?php echo $historicoTicket['date_vto'] ?></td>
									
							<td class="text-center"><?php echo $historicoTicket['dias'] ?></td>
							<td class="text-center"><?php echo $historicoTicket['nro_doc'] ?></td>
							<td class="text-center"><?php echo $historicoTicket['cod_bco'] ?></td>
						        <td class="text-center"><?php echo number_format($historicoTicket['monto_doc']) ?></td>	


							<td class="text-center"><?php echo $historicoTicket['porc_ant'] ?></td>
							<td class="text-center"><?php echo number_format($historicoTicket['mto_nomant']) ?></td>
							<td class="text-center"><?php echo number_format($historicoTicket['mto_noant']) ?></td>
							<td class="text-center"><?php echo number_format($historicoTicket['mto_difpre']) ?></td>	

							<td class="text-center"><?php echo number_format($historicoTicket['precio_cesion']) ?></td>
							<td class="text-center"><?php echo number_format($historicoTicket['mto_antsdctos']) ?></td>	
	              <td><button type="button" class="btn btn-default fa fa-trash-o delete-doc"  id="<?php echo $historicoTicket['id'] ?>" ></button></td>
								
								</tr>							
							<?php endforeach ?>  						
						</tbody>
					</table>
				</div>
					  <?php echo form_close(); ?>		
				<?php }else{ 
					if ($search_value != "") {
						echo "<p class='message-no-data'>No existen resultados con los criterios de búsqueda ingresados.</p>";
						echo "<p class='message-no-data link'><a href='".site_url('form/index')."'>Volver al inicio</a></p>";
					}else{
						echo "<p class='message-no-data'>El sistema aun no registra información.</p>";
					}	
				} ?>				
			</div>	
		</div>		
		<div class="row">
			<div class="col-md-12 pagination-content">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<?php echo $pagination; ?>					
					</ul>
				</nav>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">




       $('.delete-doc').on('click', function(){
            
            var doc = $(this).attr('id');
            var tipo = "<?php echo $tipo; ?>"; 
            swal({
                title: "itr@ces",
                text: "Desea Eliminar Documento !",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                
                data = { 'doc': doc,'tipo':tipo };
                postUrl = "<?php echo site_url('Tracking/deleteDoc'); ?>";
                
                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function (result) {


                   // alert(JSON.stringify(result));                   

                        if(result == 'OK'){
                            swal({
                                title: "Te Recupero",
                                text: "Documento Eliminado",
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                id:"OK"
                            }, function (isConfirm) {
                                    if (isConfirm) { 
                                    location.reload();                          
                                    $('#myModal').on('hidden.bs.modal', function (e) {
	                            $(this).removeData('bs.modal');
	                            $(this).find('.modal-content').empty();
                                  })
                                                  }
                            });
                        }else{
                            swal({
                                title: "Te Recupero",
                                text: "Error, contacte a un administrador de Te Recupero !! ",
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: false
                            });
                        } 
                    },
                    error: function (xhr, ajaxOptions, thrownError) { }
                });
            });
        });
        






      	$(".savedoc").click(function () {
                        
                        var dato = document.getElementsByClassName("form-control form-control-sm");
                        var e = parseInt(dato.length) / parseInt(14);                       
                        var x = document.getElementById("form_in");
                        var o=1;
                        var a =[];

                        window['fila'+e] = []; 
                       
                        for (var  i = 0; i <= dato.length; i++) {
                        if( x.elements[i].value != "Quitar" ){ 
                             window['fila'+e].push(x.elements[i].value.replace(".", ""));
                         
                        
    
                         // alert(x.elements[i].value+" i = "+i+" o = "+o);  
                            o++;     
                       
                        if(o > 14){
                             o=1; 
                            
                             a.push(eval('fila'+e));   
                             e--;      
                             window['fila'+e] = []; }
                           }}
                        
                     
                       //  alert(JSON.stringify(a));                 
         
                        var ndoc = "<?php echo $oper; ?>"; 
                        var tipo = "<?php echo $tipo; ?>";

                      // alert(tipo);

                  	dataUser = {"datos": a,"ndoc":ndoc,"tipo":tipo};
			postUrl = "<?php echo site_url('Tracking/documento'); ?>";
			                         
        		$.ajax({
				type: "POST",
				url: postUrl,
				data: dataUser,
				dataType: "text",
				success: function (result) {
		
                                 	if (result == 'OK') {
					swal({
						title: "Mensaje TeRecupero!",
						text: "SE ASOCIARON  CORRECTAMENTE LOS DOCUMENTOS A LA OPERACION",
						type: "success",
						showCancelButton: false,
						confirmButtonText: "OK",
						closeOnConfirm: false
						},
						function (isConfirm) {
							if (isConfirm) {
							//	$("#modalusuario_web").modal("hide");
								location.reload();
							} else {
							//	$("#modalusuario_web").modal("hide");
								location.reload();
							}
						});
				} else {
					swal("Error", "Ha ocurrido un error", "error");
				}
                                },
				error: function (xhr, ajaxOptions, thrownError) {
				
				}
			});

                 });










        $(".documento").click(function () {

         var data = document.getElementsByClassName("form-control form-control-sm");        
         var o = parseInt(data.length) / parseInt(14);
          //alert(o);
          if(o == 0){ y=1  }else{ y = parseInt(1)+ parseInt(o);  }  
         //alert(y);
         // Obtiene una referencia a la tabla
         var tableRef = document.getElementById('list-table2').getElementsByTagName('tbody')[0];

         // Obtiene el numero de filas de la tabla 
      
         var x= tableRef.rows.length;
         // Inserta una fila en la tabla, en el índice 0
         var newRow   = tableRef.insertRow(0);

         var celda=[]
         var campo=[]
         var e = 0
        

         for (var i=1;i<=15;i++){ 

         celda[i]=newRow.insertCell(e);

         campo[i] = document.createElement("input");
         campo[i].className= "form-control form-control-sm";
         campo[i].style.height="20px";
         campo[i].style.fontSize = "60%";
         campo[i].style.padding = "0";
         campo[i].name="campo"+i+"[]";
         campo[i].style.textAlign = "center";
         campo[i].type = "text";
         campo[i].size="8";
         campo[i].maxLength = "10";
         campo[i].style.width="100%";
        
 

         switch (i) {
           case 1:
         campo[i].style.textTransform = "uppercase";
         campo[i].size="35";
         campo[i].maxLength = "28";
            break;

            case 2:
         campo[i].size="9";
         campo[i].maxLength = "10";  
            break; 

            case 3:
            case 4:
         campo[i].type ="date";
         campo[i].style.width="87%";
            break; 

            case 5:
            case 9:
         campo[i].size="1";
         campo[i].maxLength = "2";
            break;


            case 6:
         campo[i].size="2";
         campo[i].maxLength = "4";
            break;
         
            case 15:
         campo[i].type = "button";
         campo[i].className = "btn btn-default quitar";
        
         campo[i].style.width="90%";
         campo[i].size="30";
        
     // campo[i].backgroundImage="";
        campo[i].value = "Quitar";
        campo[i].onclick = $(function () {
           $(document).on('click', '.quitar', function (event) {
           event.preventDefault();
          $(this).closest('tr').remove();
              });
});
            break;
                 }         
         
         celda[i].appendChild(campo[i]);
          e++;
  
            }


       });





      function Quitar() {        
    
           var tableRef = document.getElementById('list-table2');
           var a= tableRef.rows.length;
           var fila = this.parentNode.parentNode;
           var tbody = tableRef.getElementsByTagName("tbody")[0];
           tbody.removeChild(fila);

      }














</script>
