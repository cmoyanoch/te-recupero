

<div class="container-fluid" style="width:100%;">

  
<div class="form-row" >
 <?php echo form_open('#', array('id' => 'form_ope')); ?>


  <div class="col-xs-6 col-sm-3">

          <div class="input-group input-group-sm mb-3" >
              <div class="input-group-prepend" >
               <span class="input-group-text" id="inputGroup-sizing-sm" >Factoring</span>
              </div>
               <?php  echo form_dropdown('fact', $data['fact'], set_value('fact'),array('class' => 'form-control form-control-sm', 'name' => 'factori', 'id' => 'factori', 'aria-label' => 'Factoring','aria-describedby' => 'inputGroup-sizing-sm', 'style' => 'font-size:70%')); ?><?php echo form_error('fact'); ?>
          </div> <!--Factoring -->  

          <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm" >Fecha Operacion</span>
                </div>
                <?php echo form_input(array('type' => 'date', 'name' => 'DateO','id' => 'DateO','class' => 'form-control form-control-sm','aria-label' => 'Fecha de Factorig','aria-describedby' => 'inputGroup-sizing-sm')); ?><?php echo form_error('DateO'); ?>
          </div> <!-- Fecha Operacion -->

          <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm" >Monto a Giro</span>
                </div>
                      <?php echo form_label('', 'liqui', array( 'name' => 'liqui', 'id' => 'liqui','class' => 'form-control form-control-sm','placeholder' => 'REAL')); ?>
                      <?php echo form_error('liqui'); ?>  
          </div> <!--Monto a Giro -->

          <div class="input-group input-group-sm mb-3">
             <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm" >IVA Comisión</span>
               </div>
                        <?php echo form_label('', 'ivaCo', array( 'name' => 'ivaCo', 'id' => 'ivaCo','class' => 'form-control form-control-sm','placeholder' => 'REAL')); ?>
                        <?php echo form_error('ivaCo'); ?>  
          </div>  <!--IVA -->

          <div class="input-group input-group-sm mb-3">
                 <div class="input-group-prepend">
                 <span class="input-group-text" id="inputGroup-sizing-sm" >Tasa real o Efectiva</span>
                 </div>
                 <?php echo form_label('', 'Trea', array( 'name' => 'Trea', 'id' => 'Trea','class' => 'form-control form-control-sm','placeholder' => 'REAL')); ?>
                 <?php echo form_error('Trea'); ?>  
          </div> <!--Tasa real o Efectiva -->     
  </div> 

  <div class="col-xs-6 col-sm-3">	

          <div class="input-group input-group-sm mb-3">
           <div class="input-group-prepend">
           <span class="input-group-text" id="inputGroup-sizing-sm" >Cliente</span>
             </div>
            <?php echo form_dropdown('clie',$data['clie'], set_value('clie') ,array('class' => 'form-control form-control-sm', 'name' => 'cliente', 'id' => 'cliente','aria-label' => 'Cliente','aria-describedby' => 'inputGroup-sizing-sm','style' => 'font-size:70%')); ?><?php echo form_error('clie'); ?>
          </div> <!-- Cliente -->   

          <div class="input-group input-group-sm mb-3">
              <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm" >Documento</span>
              </div>
              <?php echo form_dropdown('tdoc', $data['tdoc'], set_value('tdoc'),array('class' => 'form-control form-control-sm', 'name' => 'tipdocu', 'id' => 'tipdocu','style' => 'font-size:70%')); ?><?php echo form_error('tdoc'); ?>
          </div> <!--Documento -->

          <div class="input-group input-group-sm mb-3">
                   <div class="input-group-prepend">
                   <span class="input-group-text" id="inputGroup-sizing-sm" >Monto Dif.Pre</span>
                   </div>
                   <?php echo form_label('', 'mdifp', array( 'name' => 'mdifp', 'id' => 'mdifp','class' => 'form-control form-control-sm','placeholder' => 'REAL')); ?>
                   <?php echo form_error('mdifp'); ?>  
          </div> <!--Monto Diferencia de Precio -->  

          <div class="input-group input-group-sm mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm" >Tasa Dif.Pre</span>
              </div>
              <?php echo form_input(array('type' => 'text', 'name' => 'tdips','id' => 'tdips', 'class' => 'form-control form-control-sm','placeholder' => 'SUGERIDA' ))?><?php echo form_error('tdips'); ?>  

              <?php echo form_label('', 'TdiPr', array( 'name' => 'TdiPr', 'id' => 'TdiPr','class' => 'form-control form-control-sm','placeholder' => 'REAL')); ?>
                  <?php echo form_error('TdiPr'); ?>  
          </div>  <!--Tasa dif. Pre -->     

          <div class="input-group input-group-sm mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm" >Total cobrado</span>
              </div>

              <?php echo form_label('', 'tcobra', array( 'name' => 'tcobra', 'id' => 'tcobra','class' => 'form-control form-control-sm','placeholder' => 'REAL')); ?>
                  <?php echo form_error('tcobra'); ?>  
          </div>  <!--Total cobrado  --> 
  </div>  

  <div class="col-xs-6 col-sm-3">	

          <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm" >RUT</span>
                </div>
                 <?php echo form_input(array('type' => 'text','name' => 'rut','id' => 'rut','class' => 'form-control form-control-sm','aria-label' => 'Rut','aria-describedby' => 'inputGroup-sizing-sm')); ?><?php echo form_error('rut'); ?> 
          </div> <!-- RUT-->  

          <div class="input-group input-group-sm mb-3">
               <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm" >Monto operación</span>
                </div>
                <?php echo form_input(array('type' => 'text', 'name' => 'mmopera', 'id' => 'mmopera', 'class' => 'form-control form-control-sm','placeholder' => 'SUGERIDO')); ?><?php echo form_error('mmopera'); ?>
                 <?php echo form_label('', 'mopera', array( 'name' => 'mopera', 'id' => 'mopera','class' => 'form-control form-control-sm','placeholder' => 'REAL')); ?>
                  <?php echo form_error('mopera'); ?>   
          </div> <!--Monto operación-->

          <div class="input-group input-group-sm mb-3">
           <div class="input-group-prepend">
           <span class="input-group-text" id="inputGroup-sizing-sm" >Monto Comisión</span>
           </div>
            <?php echo form_input(array('type' => 'text', 'name' => 'ctotal', 'id' => 'ctotal', 'class' => 'form-control form-control-sm','placeholder' => 'INGRESAR')); ?>
             <?php echo form_error('ctotal'); ?>
          </div> <!--Monto Comisión -->  

          <div class="input-group input-group-sm mb-3">
           <div class="input-group-prepend">
           <span class="input-group-text" id="inputGroup-sizing-sm" >Tasa Comisión</span>
           </div>
             <?php echo form_label('', 'tcom', array( 'name' => 'tcom', 'id' => 'tcom','class' => 'form-control form-control-sm','placeholder' => 'REAL')); ?>
             <?php echo form_error('tcom'); ?>  
          </div> <!--Tasa Comisión -->

          <div class="input-group input-group-sm mb-3">
              <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm" >Porcentaje de anticipo</span>
              </div>
              <?php echo form_input(array('type' => 'text', 'name' => 'pora','id' => 'pora', 'class' => 'form-control form-control-sm','placeholder' => 'INGRESAR'))?>
              <?php echo form_error('pora'); ?>  
          </div> <!--Porcentaje de anticipo -->
  </div>   

  <div class="col-xs-6 col-sm-3">

          <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm" >Nº Operación</span>
                </div>
                 <?php echo form_input(array('type' => 'text','name' => 'noper','class' => 'form-control form-control-sm','aria-label' => 'Nº Operación','aria-describedby' => 'inputGroup-sizing-sm','style' => '')); ?><?php echo form_error('noper'); ?>
          </div> <!-- Nº Operación-->             

          <div class="input-group input-group-sm mb-3">
                   <div class="input-group-prepend">
                   <span class="input-group-text" id="inputGroup-sizing-sm" >Monto anticipado</span>
                   </div>
                   <?php echo form_input(array('type' => 'text', 'name' => 'mmadela', 'id' => 'mmadela','class' => 'form-control form-control-sm','placeholder' => 'SUGERIDO')); ?>
                     <?php echo form_error('mmadela'); ?>
                   <?php echo form_label('', 'madela', array( 'class' => 'form-control form-control-sm','name' => 'madela', 'id' => 'madela','placeholder' => 'Real')); ?><?php echo form_error('madela'); ?>   
          </div> <!--Monto anticipado-->

          <div class="input-group input-group-sm mb-3">
               <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm" >Monto Gastos</span>
                </div>
                <?php echo form_input(array('type' => 'text', 'name' => 'mgasto', 'id' => 'mgasto', 'class' => 'form-control form-control-sm','placeholder' => 'INGRESAR')); ?><?php echo form_error('mgasto'); ?>
          </div> <!--Monto Gastos-->

          <div class="input-group input-group-sm mb-3">
               <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm" >Tasa Gastos</span>
                </div>
                <?php echo form_label('', 'tgasto', array( 'name' => 'tgasto', 'id' => 'tgasto','class' => 'form-control form-control-sm','placeholder' => 'REAL')); ?>
                <?php echo form_error('tgasto'); ?>  
          </div><!-- Tasa Gastos -->

         <div class="input-group input-group-sm mb-3"> 
            <div class="input-group-prepend" >
             <span class="input-group-text" id="inputGroup-sizing-sm" >N° Documentos</span>
              </div>
                   <?php echo form_label('', 'ndoc', array( 'name' => 'ndoc', 'id' => 'ndoc','class' => 'form-control form-control-sm','placeholder' => '')); ?>
                <?php echo form_error('tgasto'); ?> 
              <a role="button" href="#" class="btn btn-rounded btn-success btn-sm documento" id="documento" > <i class="fas far fa-plus-square pl-1"></i>&nbsp;Documento</a>
         </div>          
  </div> 

  <?php echo form_close(); ?>
</div>

<div class="form-row" id="tabla" style="visibility: hidden;" >

			<?php echo form_open('#', array('id' => 'form_doc')); ?>	
        <div class="table-responsive-sm table-mt-auto" style="height:230px;background:#fff;overflow-y:scroll;width:100%;" >


					<table class="table table-bordered table-sm" id="list-table-2" >
						<thead class="thead-light" style="font-size:70%;" >

            <tr>
                <th colspan="9" style="visibility:hidden;" id="th"></th>
                <th colspan="2">Monto dif. Pre</th>
                <th colspan="3" style="visibility:hidden;"></th>
            </tr>

						<tr>

							  <th>Deudor</th>

		         		<th>Rut Deudor</th>

								<th>Fecha Vencimiento</th>	

								<th>Días</th>

								<th>Numero Documento</th>

								<th id="cban">Codigo Banco</th>

								<th>Monto Documento</th>

								<th>Monto Anticipado</th>

								<th>Monto NO Anticipado</th>
                <th>Tasa dif. Pre</th>       
								<th>Sugerida</th>
                <th>Real</th>
                <th>Liquido</th>
                <th>Tasa Real</th>

						    <th></th>
						</tr>				
						</thead><tbody></tbody>						       
					</table>					
				</div>
        <?php echo form_close(); ?>	
</div>	
</div>	


<script type="text/javascript">

 $(".saveoperacion").click(function () {
                     

                    var x =   document.forms['form_ope'].getElementsByClassName("form-control form-control-sm");
                    var data = [];
                       
                    for (var  i = 0; i < x.length; i++) {

                             data.push(x[i].value);  
                     }

                      // alert(JSON.stringify(data));

   

                    var table = document.getElementById('list-table-2');     
                    var n =  table.getElementsByTagName('tbody')[0].rows.length;
                    var a = n;
                    var e =  document.getElementById("form_doc");
                    var o=1;
                    var dato = [];
                    window['fila'+n] = [];


                    for (var  i = 0; i < e.length; i++) {

                        window['fila'+n].push(e.elements[i].value);  
                        o++;   

                        if( o > 8 && a > 1 ){

                             o=1; 
                             dato.push(eval('fila'+n));   
                             n--;      
                             window['fila'+n] = [];

                            }


                        if( o == 8 && a == 1 ){

                             dato.push(eval('fila'+n)); 

                            }
                     }

                   //alert(JSON.stringify(dato));

                   var tipo = "<?php echo $data['tipo'];?>";  
                   dataUser = {"data": data, "dato": dato,"tipo":tipo};
                   postUrl = "<?php echo site_url('Tracking/operacion'); ?>";
  
                    $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: dataUser,
                    dataType: "text",
                    success: function (result) {

                      alert(JSON.stringify(result));
            
                   if (result == 'OK') {
                       swal({
                       title: "Mensaje Terecupero!",
                       text: "SE CREO CORRECTAMENTE LA OPERACION",
                       type: "success",
                       showCancelButton: false,
                       confirmButtonText: "OK",
                       closeOnConfirm: false
                      },
                       function (isConfirm) {
                            if (isConfirm) {
                          //  $("#modalusuario_web").modal("hide");
                             location.reload();
                             } else {
                          //  $("#modalusuario_web").modal("hide");
                              location.reload();
                            }});
                         } else {
                         swal("Error", "Ha ocurrido un error", "error");
                        }


                                },
                          error: function (xhr, ajaxOptions, thrownError) {
         
                      } 
                       }); 
  });

 $("#cliente").change(function () {

   
                   var data = $("#cliente").val();  
                   dataUser = {"data": data};
                   postUrl = "<?php echo site_url('Tracking/getRut'); ?>";
  
                    $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: dataUser,
                    dataType: "text",
                    success: function (result) {

                   var cliente = (JSON.parse(result));

                   $("#rut").val(cliente[0].rut);
                  
 
                                },
                          error: function (xhr, ajaxOptions, thrownError) {
         
                      }}); 
  });

  $("#tdips").change(function () {
          
         

         if($("#tdips").val()){

         MontoR(); 

         var table = document.getElementById('list-table-2');     
         var n =  table.getElementsByTagName('tbody')[0].rows.length;
         var tota = [];
         var tdips = $("#tdips").val();

         if(n){

          for (var i = 0; i < n; i++) { 
           
           tota[i] = $("#campo10_"+i).text(); 
           tota[i] = tota[i].replace(/%/, '');
               

          if($("#tdips").val() && tota[i]){

          var color="red";

         if(tdips == tota[i]){

          var color="green";

         }

         $("#campo10_"+i).css('background', color);
         $("#campo10_"+i).attr('placement', 'top');
         $("#campo10_"+i).attr('title', tota[i]);

         } 

             }}}
  });

  $("#tipdocu").change(function () {

        if($("#tipdocu").val()){

         var tableRef = document.getElementById('list-table-2');     
         var n =  tableRef.getElementsByTagName('tbody')[0].rows.length;


         var doc = $("#tipdocu").val();
         var disp = "none";
         var th = "9";


         if(doc == 3){

            disp = ""; 
            th = "10";

         } 

            document.getElementById('cban').style.display = disp;
            document.getElementById("th").colSpan = th;

         if(n){
          for (var i = 0; i < n; i++) {   

          document.getElementById('td6_'+i).style.display = disp;   

             }}}
  });

 $("#mgasto").change(function () {

        if($("#mgasto").val()){

         var tableRef = document.getElementById('list-table-2');     
         var n =  tableRef.getElementsByTagName('tbody')[0].rows.length;

         if(n){
          for (var i = 0; i < n; i++) {         
              resta(i);
           }} 
        suma2();  
         }
  });

 $("#ctotal").change(function () {

     var valor = $("#ctotal").val()
     if(valor){

     valor = valor.replace(/\./g,''); 
     var iva = Math.trunc(parseInt(valor) * 0.19);

     $("#ivaCo").text(iva);


     var tableRef = document.getElementById('list-table-2');     
     var n =  tableRef.getElementsByTagName('tbody')[0].rows.length;

         if(n){
          for (var  i = 0; i <  n; i++) {         
              resta(i);
           }} 

      suma2();     

     }else{

      $("#ivaCo").text("");
     }   
  });

 $("#DateO").change(function () {

         var tableRef = document.getElementById('list-table-2');     
         var n =  tableRef.getElementsByTagName('tbody')[0].rows.length;

         if(n){

          for (var  i = 0; i <  n; i++) {
         
              fecha(this,i);

           }} 
  });

 $("#TdiP").change(function () {

         var tableRef = document.getElementById('list-table-2');     
         var n =  tableRef.getElementsByTagName('tbody')[0].rows.length;

         if(n){

          for (var  i = 0; i <  n; i++) {
         
              logica(1,i);

           }} 
  });

 $("#rut").blur(function () {

    if(this.value){      

      var rut = this.value;
      var T = rut.replace(/\./g,'');

      var M=0,S=1;
      for(;T;T=Math.floor(T/10))
       S=(S+T%10*(9-M++%6))%11;

       var dv = S?S-1:+'k';

       rut = rut.replace(/^(\d{2})(\d{3})(\d{3})$/, '$1.$2.$3');
       rut = rut+"-"+dv;
      $("#rut").val(rut);    
    }
  });

 $(".documento").click(function () {


         //var TdPre = $("#TdiP").val();
         var dateOpe = $("#DateO").val();
         var ctotal  = $("#ctotal").val();
         var mgasto  = $("#mgasto").val();
         var pora    = $("#pora").val();
         var tdips   = $("#tdips").val();

         //  if(dateOpe && ctotal && mgasto && pora && tdips ){

         var doc = $("#tipdocu").val();
         document.getElementById('tabla').style.visibility =""; 

         // Obtiene una referencia a la tabla
         var tableRef = document.getElementById('list-table-2').getElementsByTagName('tbody')[0];
         // Obtiene el numero de filas de la tabla  
         var x = tableRef.rows.length;
         // Inserta una fila en la tabla, en el índice 0

         var filas = parseInt(x) + parseInt(1);

         $("#ndoc").text(filas);

         var newRow   = tableRef.insertRow(0);

         var celda = []
         var campo = []
         var div   = []         
         var e = 0
        

         for (var i=1;i<=15;i++){ 

         celda[i]=newRow.insertCell(e);
         //celda[i].className= "input-group";
         //celda[i].style.border = "0";
          
         campo[i] = document.createElement("input");
         campo[i].className= "form-control input-sm";
         campo[i].type = "text";
         campo[i].style.padding = "0px 2px 0px 2px";
         campo[i].style.fontSize = "60%";
         
 

         switch (i) {
           case 1:
         campo[i].style.textTransform = "uppercase"; 
         campo[i].maxLength = "22";
         campo[i].style.width = "108px";
         //celda[i].style.width = "14%";
         
            break;

            case 2:
         campo[i].size="10";
         campo[i].maxLength = "12";
         campo[i].style.width = "80px";
         campo[i].style.fontSize = "80%";   
         campo[i].style.justifyContent = "center";      
         campo[i].addEventListener("blur",function(){rut(this,x)},false);
            break; 

            case 3:
         campo[i].type ="date";       
         campo[i].addEventListener("change",function(){fecha(this,x)},false);
         campo[i].style.fontWeight ="400";
         campo[i].style.fontSize = "60%";
         campo[i].style.width = "90px";
         // celda[i].style.width="11%";
            break;

            case 5:      
         campo[i].maxLength = "4"; 
         campo[i].style.width = "27px";
         //celda[i].style.width="4%";
            break;

            case 6:           
         campo[i].maxLength = "4";
         campo[i].style.width = "27px";
         celda[i].id="td6_"+x;
         if(doc != 3){
         celda[i].style.display = "none"; 
         document.getElementById('cban').style.display = "none"; 
         } 

            break;

            case 7:
         campo[i].maxLength = "10";
         campo[i].style.width = "55px";
          // celda[i].style.width="9%";  
         campo[i].addEventListener("change",function(){logica(7,x)},false);
            break;



            case 11:
         campo[i].maxLength = "10";
         campo[i].style.width = "55px";
         campo[i].addEventListener("change",function(){logica(11,x)},false);
            break;


            case 4: 
            case 8:
            case 9:
            case 10:
            case 12:
            case 13:
            case 14:
           campo[i] = document.createElement("LABEL");
           campo[i].style.display = "inline-flex";
           campo[i].style.alignItems = "center";
           campo[i].style.justifyContent = "center";
           campo[i].style.fontSize = "60%";
            break;
        
            case 15:

         campo[i] = document.createElement("i");
         campo[i].className = "fas fa-times-circle text-red";
         campo[i].style.color="red";
         campo[i].style.fontSize = "18px";
         campo[i].style.display = "inline-flex";
         campo[i].style.alignItems = "center";
         campo[i].style.justifyContent = "center";
           // celda[i].style.width="4%"

           // campo[i].backgroundImage="";
          campo[i].addEventListener("click",function(){quitar(this,event,x)},false);   

                  
            break;
                 } 

          campo[i].style.height="23px"
          campo[i].id="campo"+i+"_"+x;
          campo[i].name="campo"+i+"_"+x;

         
          celda[i].appendChild(campo[i]);

           e++;
  
            }

              /* }else{

                         var mens = "";

                       if(!dateOpe){  mens += "\n"+"Fecha Operacion";  }
                       if(!ctotal){   mens += "\n"+" Monto Comisión";  }
                       if(!mgasto){   mens += "\n"+" Monto Gastos";  }
                       if(!tdips){    mens += "\n"+" Tasa dif. Pre Sugerida";  }                        
                       if(!pora){     mens += "\n"+" Porcentaje de anticipo";  }

                       swal({
                       title: "Mensaje Terecupero!",
                       text: "FALTA INGRESAR LOS SIGUIENTES DATOS"+"\n"+"PARA ASOCIAR DOCUMENTOS A LA OPERACION :"+mens,
                       type: "error",
                       showCancelButton: false,
                       confirmButtonText: "OK",
                       closeOnConfirm: false
                      });

              } */
  });


function logica(value,i) { 

         var campo = "";
         var a     = "";
         var b     = "";  
         var c     = "";	
         var campo7 = $("#campo7_"+i).val(); 
         var pora = $("#pora").val(); 
         var campo11 = $("#campo11_"+i).val(); 

         pora  = pora.replace(/\%/g,''); 
         campo7  = campo7.replace(/\./g,''); 
         campo11 = campo11.replace(/\./g,'');

         if(pora && campo7 ){

         a = parseInt(campo7) * parseInt(pora)/100; 
         b = parseInt(campo7) - parseInt(a);

         $("#campo8_"+i).text(Math.round(a)); 
         $("#campo9_"+i).text(Math.round(b)); 

         suma1(i);

         }else{
      
         $("#campo8_"+i).text(a); 
         $("#campo9_"+i).text(b); 
         $("#campo13_"+i).text(a);

         }



         if( campo11 && a && $("#campo4_"+i).text() ){


         var c1 = parseInt(a) / parseInt(30);
         var c2 = parseInt(campo11) / eval($("#campo4_"+i).text());

             c  = parseInt(c2) / parseInt(c1);
             c  =  c * parseInt(100);

          var exp = -1;

          c = c.toString().split('e');
          c = Math['ceil'](+(c[0] + 'e' + (c[1] ? (+c[1] - exp) : -exp)));
   
          c = c.toString().split('e');
          c =+(c[0] + 'e' + (c[1] ? (+c[1] + exp) : exp));
  

         $("#campo10_"+i).text(c+"%"); 

         if($("#tdips").val() && c){

          var color="red";

         if( $("#tdips").val() == c ){

            color="green";

         }

              $("#campo10_"+i).css('background', color);
              $("#campo10_"+i).css('color', '#FFFFFF');
              $("#campo10_"+i).attr('placement', 'top');
              $("#campo10_"+i).attr('title', c);

         }

          
         suma1(i); 
         suma2();         
         tasa();

         } else {

         $("#campo10_"+i).text(c);

         } 

         resta(i);           
}

function quitar(value,event,i) {



    
            var mopera  = eval($("#mopera").text());
            var madela  = eval($("#madela").text());
            var mdifp   = eval($("#mdifp").text());
            var liqui   = eval($("#liqui").text());
            var tcobra  = eval($("#tcobra").text());

    

            var campo7 = $("#campo7_"+i).val();
            campo7 = campo7.replace(/\./g,'');

            var campo8 = eval( $("#campo8_"+i).text() );

            var campo11 = $("#campo11_"+i).val();
            campo11 = campo11.replace(/\./g,'');

            var campo13 = eval( $("#campo13_"+i).text() );


            if($("#campo7_"+i).val()){  
            campo7 = Math.round(mopera - parseInt(campo7));

            }

            if($("#campo8_"+i).text()){             
            campo8  = Math.round(madela - campo8);

            }     

            if($("#campo11_"+i).val()){  
            campo11= Math.round(mdifp - parseInt(campo11));  
            var sum = liqui - parseInt(campo11);
 
            }   

            if($("#campo13_"+i).text()){  

            var total = campo13 + tcobra;

            }

          
           $("#mopera").text(campo7);
           $("#madela").text(campo8); 
           $("#mdifp").text(campo11);

           $("#liqui").text(total);
           $("#tcobra").text(sum);


           var filas = eval($("#ndoc").text()) - parseInt(1);
           $("#ndoc").text(filas);

           if( filas < 1 ){

           document.getElementById('tabla').style.visibility ="hidden"; 

            }


           event.preventDefault();
           $(value).closest('tr').remove();
}

function suma1(i) {

	       var table = document.getElementById('list-table-2');     
         var n     =  table.getElementsByTagName('tbody')[0].rows.length;
         
         if(n == 1){ i = 0; }

         var color1  = "red";
         var color2  = "red";

         var campo7  = 0;
         var campo8  = 0;
         var campo11 = 0;
         var campo13 = 0;
         var total = 0;
         var sum = 0; 

         var camp7  = []
         var camp8  = []
         var camp11 = []
         var camp13 = []

         var mmadela = $("#mmadela").val();
         var mmopera = $("#mmopera").val();
         var ctotal  = $("#ctotal").val();
         var mgasto  = $("#mgasto").val();        

          if(ctotal && mgasto ){

          sum   = parseInt(ctotal.replace(/\./g,'')) + eval($("#ivaCo").text()) + parseInt(mgasto.replace(/\./g,''));

          }


          mmopera = mmopera.replace(/\./g,'');
          mmadela = mmadela.replace(/\./g,'');

           

            if(n > 1){

          for (var  a = 0; a <  n; a++) {

            camp7[a] = $("#campo7_"+a).val();
            camp7[a]  = camp7[a].replace(/\./g,'');

            camp11[a] = $("#campo11_"+a).val();
            camp11[a]  = camp11[a].replace(/\./g,'');


            if(a != i ){

            if($("#campo7_"+a).val()){
            campo7  = campo7 + parseInt(camp7[a]);
            }

            if($("#campo8_"+a).text()){
            campo8  = campo8 + eval($("#campo8_"+a).text());
            }

            if($("#campo11_"+a).val()){ 
            campo11 = campo11 + parseInt(camp11[a]); 
            }

            if($("#campo13_"+a).text()){
            campo13 = campo13 + eval($("#campo13_"+a).text());    
            }

              }
            }

           }else{


            camp7[i] = $("#campo7_"+i).val();
            camp7[i] = camp7[i].replace(/\./g,'');

            camp8[i] = eval( $("#campo8_"+i).text() );

            camp11[i] = $("#campo11_"+i).val();
            camp11[i] = camp11[i].replace(/\./g,'');

            camp13[i] = eval( $("#campo13_"+i).text() );

           } 


             alert("suma1 camp13 = "+camp13[i]);

            if($("#campo7_"+i).val()){  
            campo7 = Math.round(campo7 + parseInt(camp7[i]));

            }

            if($("#campo8_"+i).text()){             
            campo8  = Math.round(campo8 + camp8[i]);

            }     

            if($("#campo11_"+i).val()){  
            campo11= Math.round(campo11 + parseInt(camp11[i]));  
            sum = sum + campo11;
 
            }   

            if($("#campo13_"+i).text()){  
            campo13 = Math.round(campo13 + camp13[i]);  
            total = campo13 - sum;

            }

             alert("suma1 total = "+total);
          
           $("#mopera").text(campo7);
           $("#madela").text(campo8); 
           $("#mdifp").text(campo11);

           $("#liqui").text(total);
           $("#tcobra").text(sum);
 

           if( mmopera == campo7 ){ 

             var color1  = "green";

           }

              $("#mopera").css('background', color1);
              $("#mopera").css('color', '#FFFFFF');
              $("#mopera").attr('placement', 'top');
              $("#mopera").attr('title', campo7);   

  
           if( mmadela == campo8  ){   
         
               var color2  = "green";
           }

              $("#madela").css('background', color2);
              $("#madela").css('color', '#FFFFFF');
              $("#madela").attr('placement', 'top');
              $("#madela").attr('title', campo8);                 
}

function suma2() {


         var table = document.getElementById('list-table-2');     
         var n     =  table.getElementsByTagName('tbody')[0].rows.length;
    
         var dav   = [];
         var div   = [];
         var res   = [];
         var campo11 = []         
       

         if(n){

          for (var i = 0; i < n; i++) {

           if($("#campo4_"+i).text() && $("#campo8_"+i).text() && $("#campo11_"+i).val() ){

            campo11[i] = $("#campo11_"+i).val(); 
            campo11[i] = campo11[i].replace(/\./g,'') 

            div[i] = campo11[i] / eval($("#campo8_"+i).text());
            div[i] = (div[i] / eval($("#campo4_"+i).text())) * 30;
            dav[i] = div[i].toFixed(4)*100;

            if(dav[i].toString().length > 4){ 
            
             res[i] = dav[i].toFixed(2);

              }else{ res[i] = dav[i]; }

             tasa();

           }else{ 

            res[i] = 0;
           }
         
     
            $("#campo14_"+i).text(res[i]+"%");

            } 

             
            }
}

function resta(i) {


           var total   = "";
           var campo9  = $("#campo8_"+i).text();
           var campo11 = $("#campo11_"+i).val();

           if( campo9 && campo11 ){            
           
            campo9 = campo9.replace(/\./g,''); 
            campo11 = campo11.replace(/\./g,'');              

             total = eval(campo9) - parseInt(campo11);   

             }


            $("#campo13_"+i).text(total);  

             suma2();        
             suma1();  
}

function tasa() {


         var table = document.getElementById('list-table-2');     
         var n     =  table.getElementsByTagName('tbody')[0].rows.length;
         var tota  = 0;
         var tcoba = 0;
         var totar = 0;
         var suma  = 0;
         var div   = 0;
         var dia   = 0;
         var campo11 = []

         var mgasto = $("#mgasto").val(); 
             mgasto = mgasto.replace(/\./g,'');  

         var ctotal = $("#ctotal").val(); 
             ctotal = ctotal.replace(/\./g,''); 

         var mmadela = $("#mmadela").val(); 
             mmadela = mmadela.replace(/\./g,'');

         var mdifp = $("#mdifp").text(); 


         if(n){
             for (var i = 0; i < n; i++) {

              campo11[i]  = $("#campo11_"+i).val(); 
              campo11[i]  = campo11[i].replace(/\./g,''); 

               suma = parseInt(suma) + eval($("#campo4_"+i).text());
               tcoba = parseInt(tcoba) + parseInt(campo11[i]);

             }
         }

     alert("tasa 1= "+tcoba+" "+$("#mmadela").val()+" "+suma);
         

          if( tcoba && $("#mmadela").val() && suma ){ 


            alert("tasa 2= "+tcoba+" "+$("#mmadela").val()+" "+suma);

            div = parseInt(tcoba) / parseInt(mmadela); 

            alert("tasa div = "+div);

            dia = suma / n; 
            dia = Math.round(dia);
        

            if(div && dia){ 

              var dav = parseInt(div) / parseInt(dia) * parseInt(30);

                 alert("tasa dav 1 = "+dav); 
                             
                  dav = dav * 100;

                 alert("tasa dav 2 = "+dav);


               if(dav.toString().length > 4){ 
            
                   tota = dav.toFixed(2);

                }else{

                   tota = dav;

                }  

               $("#Trea").text(tota+"%");

              } 
          }

       
          if($("#ctotal").val() && $("#mmadela").val()){  

               var tcom = parseInt(ctotal) / parseInt(mmadela); 
                   tcom = tcom.toFixed(4)*parseInt(100);

               if(tcom.toString().length > 4){ 
            
               var tcomi = tcom.toFixed(2);

               }else{ 

                tcomi = tcom; 

                }   

               $("#tcom").text(tcomi+"%");
           }

          if($("#mgasto").val() && $("#mmadela").val()){ 


                   var tgas = parseInt(mgasto) / parseInt(mmadela); 
                       tgas = tgas.toFixed(4)*parseInt(100);

                   if(tgas.toString().length > 4){ 
            
                   var tgast = tgas.toFixed(2);

                   }else{ 

                    tgast = tgas; 

                   }   

                    $("#tgasto").text(tgast+"%");
           }

          if($("#mdifp").text() && $("#mmadela").val() && dia ){  

                   var c1 = parseInt(mmadela) / parseInt(30);
                   var c2 = eval(mdifp) / parseInt(dia);
                       c = parseInt(c2) / parseInt(c1) ;      
                       c = c.toFixed(4) * parseInt(100) ;

                       /*
                          redondea el decimal

                       c = Math.round(c);

                       */

                   if(c.toString().length > 4){ 
            
                       totar = c.toFixed(3);

                      }else{ totar = c; }   

                    $("#TdiPr").text(totar+"%");

                    MontoR();
           }                     
}

function MontoR() {


         var table = document.getElementById('list-table-2');     
         var n     =  table.getElementsByTagName('tbody')[0].rows.length;
         var tota  = [];
         var tcoba = [];
         var monto = [];
         var montos = [];
         var porce  = $("#pora").val(); 
         var color  = "red";
         var x  = $("#tdips").val() / 100;
        
         if(n){

          for (var i = 0; i < n; i++) {


              montos[i] = $("#campo11_"+i).val(); 
              monto[i]  = $("#campo7_"+i).val(); 

              if(montos[i] && monto[i]){ 

              monto[i]  = monto[i].replace(/\./g,'');
              montos[i] = montos[i].replace(/\./g,'');  

              tcoba[i] = parseInt(monto[i]) * parseInt(porce)/100;     
              tota[i]  = (parseInt(tcoba[i]) * x ) / 30;
              tota[i]  = tota[i] * eval($("#campo4_"+i).text()) 
              tota[i]  = Math.sign(tota[i]) * Math.floor(Math.abs(tota[i]) + 0.5);

              $("#campo12_"+i).text(tota[i]);    

              if(montos[i] == tota[i]){ 

                color="green"; 

              }  


              $("#campo12_"+i).css('background', color);
              $("#campo12_"+i).css('color', '#FFFFFF');
              $("#campo12_"+i).attr('placement', 'top');
              $("#campo12_"+i).attr('title', tota[i]);  

                }
              }     

            }
}

function fecha(value,i) {

        
       if($(value).val()){

         var dateVen;
         var dateOpe  = $("#DateO").val();
         var id       = $(value).attr('id'); 
         

         if( id == "campo3_"+i ){

         dateVen = $(value).val();
         
         }else{

         dateVen = $("#campo3_"+i).val();
        
         }
         
                   
        if(Date.parse(dateOpe) >= Date.parse(dateVen)){

          $(value).val("");
          $("#campo4_"+i).text("");

         }else if( dateOpe && dateVen ){
 
  
         dateOpe  = new Date(dateOpe);
         var diaFer = ['2020-01-01','2020-04-10','2020-05-01','2020-05-21','2020-06-29','2020-07-16','2020-09-18','2020-10-12','2020-11-08','2020-12-25'];
         

         if(diaFer.includes(dateVen) == true){
         
         var dateVm  = new Date(dateVen); 
         var dateVm = dateVm.setDate(dateVm.getDate() + 2);
         var dateVm = new Date(dateVm);
         var diaS  = dateVm.getDay(dateVm.getDate());
         
         if(diaS == 6 ){   
         
         dateVm = dateVm.setDate(dateVm.getDate() + 3);
         dateVm = new Date(dateVm);
         
         }else if(diaS == 0 ){   
         
         dateVm = dateVm.setDate(dateVm.getDate() + 2);
         dateVm = new Date(dateVm);
         
         }
       
         var diM = dateVm.getDate();
         var meM = dateVm.getMonth() + 1;
         
         if(diM <= 9 ){ diM = "0"+diM; }
         if(meM <= 9 ){ meM = "0"+meM; }
         
         var dateVen = dateVm.getFullYear() + "-" +meM+ "-" + diM;

         }
         
         
         var f = new Date(dateVen);
         var mes = f.getMonth();                   
         var dia = f.getDate();
         var diaS  = f.getDay(f.setDate(f.getDate() + 1));

         //alert(" dia uno "+dia+"  diaS dos "+ diaS);
 
           if(dia == 28 || dia == 29 || dia == 30 || dia == 31){

            //alert("mes uno = "+mes);
           
           if(mes == 0 ){ 

            mes =  mes + 1; 

            }else{

            mes =  mes + 2;
 
            }
           

           //alert("mes dos = "+mes);
      
           
           }else{
           
            mes =  mes + 1;
           
           }
            
          // alert("mes tres = "+mes);


           switch (diaS) {
            /*
           case 4:
           f.setDate(f.getDate() + 4);
            break;
            */
            case 5:
            case 6:
           f.setDate(f.getDate() + 3);
            break;

            case 0:
           f.setDate(f.getDate() + 2);
            break; 

            default:
           f.setDate(f.getDate() + 1);
           }
 
         
           var dateLib = f.getFullYear() + "-" +mes+"-" + f.getDate();
           dateLib  = new Date(dateLib);
           var li = new Date(dateLib);
  
           var di = li.getDate();
           var me = li.getMonth() + 1;

           if(di <= 9 ){ di = "0"+di; }
           if(me <= 9 ){ me = "0"+me; }

           var dateT =  di+"-"+me+"-"+li.getFullYear();
           var diasdif  = dateLib.getTime()-dateOpe.getTime();
           diasdif =  Math.abs(diasdif);
           var  dF = Math.round(diasdif/(1000*60*60*24));

           $("#campo4_"+i).text(dF);
           $("#campo4_"+i).attr('placement', 'top');
           $("#campo4_"+i).attr('title', dateT );

           logica(value,i);

          }}else{

           $("#campo4_"+i).text("");

          }           
}

function rut(value,i){  

   if($(value).val()){ 

      var rut = $(value).val();
      var T = rut.replace(/\./g,'');

      var M=0,S=1;
      for(;T;T=Math.floor(T/10))
       S=(S+T%10*(9-M++%6))%11;

       var dv = S?S-1:+'k';

       rut = rut.replace(/^(\d{2})(\d{3})(\d{3})$/, '$1.$2.$3');
       rut = rut+"-"+dv;
      $(value).val(rut);  
    }
}


</script>


























