<style>
	
	.tr_hover {
		border-style: double;
		background-color: #f2f2f2;
	}

</style>

<script>
	var markerCluster;
	var myMap;
	var infoWindows = [];
	var infoWindowsSites = [];
	var markers = [];
	
	var jsonCordenadas = <?php print_r(json_encode($tech));?>;
	var jsonCordenadasSites = <?php print_r(json_encode($sites));?>;
	
	<?php if($map['zona'] != ''){?>
	var zona  <?php print_r('=' . $map['zona']);?>;
	<?php }else{?>
	var zona = '';
	<?php }?>
	
	<?php if($map['grupo'] != ''){?>
	var grupo  <?php print_r('=' . $map['grupo']);?>;
	<?php }else{?>
	var grupo = '';
	<?php }?>
	
	<?php if($map['customer'] != ''){?>
	var customer  <?php print_r('=' . $map['customer']);?>;
	<?php }else{?>
	var customer = '';
	<?php }?>

</script>
<head>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
	<meta charset="utf-8">
	<title>Simple markers</title>
	<style>
		#map {
			height: 100%;
		}
		
		html, body {
			height: 100%;
			margin: 0;
			padding: 0;
		}
		
		#xxxxx {
			font-family: Arial;
			font-size: 13px;
			font-weight: 400;
			padding: 8px;
			background-color: #040404;
			color: white;
			margin: 1px;
			border-radius: 15px; /* In accordance with the rounding of the default infowindow corners. */
		}
	</style>
</head>

<div class="list">
	<?php echo form_open('tracking/index', array('id' => 'formTicket')); ?>
	
	<ol class="breadcrumb" style="margin-top: 0px;">
		<li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a
				href="#"><strong style="color:black;"> Volver</strong></a></li>
		<li class="active"><strong>Mapa</strong></li>
		<br>
	</ol>
	<div class="col-md-12">
		<div class="col-md-3">
			<div class="form-group">
				<select id="selecZona" name="map[zona]" class="form-control selectPequeno">
					<option value="">Seleccione una Zona</option>
					<?php foreach ($zonas as $indice => $value) { ?>
						<option
							value="<?php echo $value['id_zona'] ?>" <?php if ($map['zona'] == $value['id_zona']) echo "selected" ?>>
							<?php echo $value['name'] ?>
						</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<select id="selectGrupos" name="map[grupo]" class="form-control selectPequeno">
					<option value="">Seleccione un Grupo</option>
					<?php foreach ($grupos as $indice => $value) { ?>
						<option
							value="<?php echo $value['id'] ?>" <?php if ($map['grupo'] == $value['id']) echo "selected" ?>>
							<?php echo $value['name'] ?>
						</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<select id="selecTecnico" name="map[tecnico]" class="form-control selectPequeno">
					<option value="">Seleccione un Conductor</option>
					<?php foreach ($tecnico as $indice => $tec) { ?>
						<option value="<?php echo $tec['id'] ?>" <?php if ($map['tecnico'] == $tec['id']) echo "selected" ?> >
							<?php echo $tec['name'] ?>
						</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<button type="submit" class="btn btn-success">Buscar</button>
			<button type="submit" class="btn btn-warning" id="modalInactive">Conductor Inactivos</button>
		</div>
	</div>
	<div class="col-md-12"></div>
	<div class="col-md-8" id="map" style="height: 450px"></div>
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
				<?php echo form_open(site_url('form/index'), array('method' => 'get')); ?>

				<?php echo form_close(); ?>
			</div>
			
		</div>
		<?php if (count($tech) > 0){ ?>
			<div class="table-responsive" style="overflow-x: hidden; height:420px">
				<?php echo form_open(site_url('form/change_state'), array('id' => 'list-form')) ?>
				<table class="table table-hover" id="list-table">
					<tbody>
					<?php
						foreach ($tech as $keyForm => $technical): ?>
							<tr data-id="<?php echo $technical['id']; ?>">
								
								<?php if (is_null($technical['coordx']) || is_null($technical['coordy']) || $technical['coordx'] == "" || $technical['coordy'] == ""){
									$technical['coordx'] = "0.00000";
									$technical['coordy'] = "0.00000";
								} ?>
								
								<?php if (is_null($technical['imei']) || $technical['imei'] == ""){
									$technical['imei'] = "0";
								} ?>
								
								<?php if ($technical['idstatus'] == 'Recibido'){ ?>
									<td id="<?php echo 'oc' . $technical['id']; ?>"
									    id2="<?php echo $technical['coordx'] . "|" . $technical['coordy']; ?>"
									    onclick='posicion2(<?php echo $technical['coordx'] ?>,<?php echo $technical['coordy'] ?>,<?php echo $technical['imei'] ?>)'>
										<img id="<?php echo 'img' . $technical['id']; ?>" title="Ver Ubicación en Mapa"
										     style="height:30px; cursor: pointer;"
										     src="<?php echo site_url('assets/images/contacted.png') ?>" class="positionTech"
										     alt="Cinque Terre"></td>
								<?php } else if ($technical['idstatus'] == 'En El Lugar'){ ?>
									<td id="<?php echo 'oc' . $technical['id']; ?>"
									    id2="<?php echo $technical['coordx'] . "|" . $technical['coordy']; ?>"
									    onclick='posicion2(<?php echo $technical['coordx'] ?>,<?php echo $technical['coordy'] ?>,<?php echo $technical['imei'] ?>)'>
										<img id="<?php echo 'img' . $technical['id']; ?>" title="Ver Ubicación en Mapa"
										     style="height:30px; cursor: pointer;"
										     src="<?php echo site_url('assets/images/onsite.png') ?>" class="positionTech"
										     alt="Cinque Terre"></td>
								<?php } else if ($technical['idstatus'] == 'En Progeso'){ ?>
									<td id="<?php echo 'oc' . $technical['id']; ?>"
									    id2="<?php echo $technical['coordx'] . "|" . $technical['coordy']; ?>"
									    onclick='posicion2(<?php echo $technical['coordx'] ?>,<?php echo $technical['coordy'] ?>,<?php echo $technical['imei'] ?>)'>
										<img id="<?php echo 'img' . $technical['id']; ?>" title="Ver Ubicación en Mapa"
										     style="height:30px; cursor: pointer;"
										     src="<?php echo site_url('assets/images/workinprogress.png') ?>" class="positionTech"
										     alt="Cinque Terre"></td>
								<?php } else if ($technical['idstatus'] == 'Resuelto'){ ?>
									<td id="<?php echo 'oc' . $technical['id']; ?>"
									    id2="<?php echo $technical['coordx'] . "|" . $technical['coordy']; ?>"
									    onclick='posicion2(<?php echo $technical['coordx'] ?>,<?php echo $technical['coordy'] ?>,<?php echo $technical['imei'] ?>)'>
										<img id="<?php echo 'img' . $technical['id']; ?>" title="Ver Ubicación en Mapa"
										     style="height:30px; cursor: pointer;"
										     src="<?php echo site_url('assets/images/resolved.png') ?>" class="positionTech"
										     alt="Cinque Terre"></td>
								<?php } else { ?>
									<td id="<?php echo 'oc' . $technical['id']; ?>"
									    id2="<?php echo $technical['coordx'] . "|" . $technical['coordy']; ?>"
									    onclick='posicion2(<?php echo $technical['coordx'] ?>,<?php echo $technical['coordy'] ?>,<?php echo $technical['imei'] ?>)'>
										<img id="<?php echo 'img' . $technical['id']; ?>" title="Ver Ubicación en Mapa"
										     style="height:30px; cursor: pointer;"
										     src="<?php echo site_url('assets/images/truck.png') ?>" class="positionTech"
										     alt="Cinque Terre"></td>
								<?php } ?>
								
								
								<td style="padding: 3px;" id="<?php echo 'dir' . $technical['id']; ?>"
								    class="techDirections"><?php echo $technical['name'] . ' ' . $technical['surname']; ?>
									<br> <?php echo '<b>N°</b>: '.$technical['numberTruck'].' | <b>Patente</b>: '.$technical['patentTruck']; ?>
									<br> <label id="<?php echo 'lastConTec' . $technical['id']; ?>">Ultima sincronización
										: <?php if ($tech1['updatetimecoord']){
											echo $tech1['updatetimecoord'];
										} else {
											echo 'SinDatos';
										} ?> </label>
								</td>
								<?php
									
									$fechaTecnico = $technical['updatetimecoord'];
									$fechaactual = date("Y-m-d H:i:s");
									
									$restrictionDate = strtotime($fechaTecnico);
									$now = strtotime(date('Y-m-d H:i:s'));
									
									$diferencia = $now - $restrictionDate;
									
									if ( ($technical['coordx'] == 0 && $technical['coordy'] == 0) || $diferencia > 180 ){
										?>
										<!-- <td style="padding: 3px;" id ="<?php //echo 'gps'.$fechaTecnico.'-'.$fechaactual;
										?>" class=""> -->
										<td style="padding: 3px;background-color: #f3a1a1d4;"
										    id="<?php echo 'gps' . $diferencia; ?>"
										    class="" align="center">
											<img id="<?php echo 'gps' . $technical['id']; ?>" style="height:30px"
											     src="<?php echo site_url('assets/images/gpsNOK.png') ?>" class="positionTech"
											     alt="">
										</td>
										<?php
									} else {
										?>
										<td style="padding: 3px;background-color: #2994295e;" id="<?php echo 'gps' . $technical['id']; ?>" class="" align="center">
											<img class="<?php echo 'gps' . $technical['id']; ?>" style="height:30px" src="<?php echo site_url('assets/images/gpsOK.png') ?>" class="positionTech" alt="">
										</td>
									<?php } ?>
								
								<?php if (is_null($technical['bateria']) || $technical['bateria'] == ""){
									$technical['bateria'] = '0';
								} ?>
								
								<?php if ($technical['bateria'] < 5){ ?>
									<td><img id="<?php echo 'bt' . $technical['id']; ?>"
									         src="<?php echo site_url('assets/images/batery1.jpg') ?>" style="height:30px"
									         class=""
									         alt="Cinque Terre"></td>
								<?php } else if ($technical['bateria'] < 26){ ?>
									<td><img id="<?php echo 'bt' . $technical['id']; ?>"
									         src="<?php echo site_url('assets/images/batery2.jpg') ?>" style="height:30px"
									         class=""
									         alt="Cinque Terre"></td>
								<?php } else if ($technical['bateria'] < 60){ ?>
									<td><img id="<?php echo 'bt' . $technical['id']; ?>"
									         src="<?php echo site_url('assets/images/batery3.jpg') ?>" style="height:30px"
									         class=""
									         alt="Cinque Terre"></td>
								<?php } else { ?>
									<td><img id="<?php echo 'bt' . $technical['id']; ?>"
									         src="<?php echo site_url('assets/images/batery4.jpg') ?>" style="height:30px"
									         class=""
									         alt="Cinque Terre"></td>
								<?php } ?>
								<?php if ($technical['incident'] && $technical['idstatus'] > 0){ ?>
									<td>
										<span id="<?php echo 'tk' . $technical['id']; ?>" style="cursor:pointer"
										      class="label label-success asgignarTicket"><?php echo $technical['incident']; ?></span>
									</td>
								<?php } else { ?>
								<?php } ?>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<?php echo form_close(); ?>
		<?php } else {
			if ($search_value != ""){
				echo "<p class='message-no-data'>No existen resultados con los criterios de búsqueda ingresados.</p>";
				echo "<p class='message-no-data link'><a href='" . site_url('form/index') . "'>Volver al inicio</a></p>";
			} else {
				echo "<p class='message-no-data'>El sistema aun no registra información.</p>";
			}
		} ?>
	</div>
</div>
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Detalle Carga</h4>
			</div>
			<div class="modal-body">
				<p id="tableDetailTicket"></p>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnAceptaAsig" class="btn btn-default" data-dismiss="modal">Aceptar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModalDirections" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Ruta</h4>
			</div>
			<div id="mapDirections"></div>
			<div class="modal-footer">
				<button type="button" id="btnAceptaAsig" class="btn btn-default" data-dismiss="modal">Aceptar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="tecnicosModalInactive" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="exampleModalLabel">Tecnico Inactivos</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table table-hover" id="list-table">
					<thead>
						<tr>
							<th>Tecnico</th>
							<th>Tipo de Inactividad</th>
							<th>Comentario</th>
						</tr>
					</thead>
					<tbody>
					<?php
						if(count($tecInactive) > 0){
							
							foreach ($tecInactive as $_tec):?>
							<tr>
								<td><?php echo $_tec['name']; ?></td>
								<td><?php
										switch ($_tec['inactiveType']){
											case 1 : echo 'Desvinculado' ; break;
											case 2 : echo 'Vacaciones' ; break;
											case 3 : echo 'Licencia Medica' ; break;
										} ?>
								</td>
								<td><?php echo $_tec['inactiveCommentary']; ?></td>
							</tr>
							<?php
							endforeach;
						} else { ?>
							<td><span style="font-weight: 600; color:red;">No cuenta con tecnicos Inactivos.</td>
					<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerra</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	
	$("#selecZona").change(function (e) {
		var idzona = $("#selecZona").val();
		
		data = {
			'idzona': idzona,
			'idzona2': 'Región Metropolitana'
		};
		
		postUrl = "<?php echo site_url('Ticket/getGrupos'); ?>";
		$.ajax({
			type: "POST",
			url: postUrl,
			data: data,
			dataType: "text",
			success: function (result) {
				var subzona = (JSON.parse(result));
				var html = "<select>"
				html += "<option value=''> Seleccione un grupo</option>"
				for (var i = 0; i < subzona.length; i++) {
					html += "<option value=" + subzona[i].id + ">" + subzona[i].name + "</option>"
				}
				html += "<select>"
				
				if (result == 'NOK') {
					
					swal("Mensaje", "No se encuentran datos para esta busqueda.", "warning")
				} else {
					
					$("#selectGrupos").html(html);
					$("#selectDireccion").html("<option value=''>Seleccione una dirección</option>");
					$("#directions").val('');
					
				}
				;
			},
			error: function (xhr, ajaxOptions, thrownError) {
			}
		});
		
		
	});
	
	
	$("#selectGrupos").change(function (e) {
		var idzona = $("#selecZona").val();
		var idgrupo = $("#selectGrupos").val();
		
		data = {
			'idzona': idzona,
			'idgrupo': idgrupo
		};
		
		postUrl = "<?php echo site_url('Ticket/getTecnicos'); ?>";
		$.ajax({
			type: "POST",
			url: postUrl,
			data: data,
			dataType: "text",
			success: function (result) {
				
				var subzona = (JSON.parse(result));
				var html = "<select>"
				html += "<option value=''> Seleccione un Tecnico</option>"
				for (var i = 0; i < subzona.length; i++) {
					html += "<option value=" + subzona[i].id + ">" + subzona[i].name + "</option>"
				}
				html += "<select>"
				
				if (result == 'NOK') {
					
					swal("Mensaje", "No se encuentran datos para esta busqueda.", "warning")
				} else {
					
					$("#selecTecnico").html(html);
					
					
				}
				;
			},
			error: function (xhr, ajaxOptions, thrownError) {
			}
		});
		
	});
	
	
	$("#selecZona").change(function (e) {
		var idzona = $("#selecZona").val();
		var idgrupo = $("#selectGrupos").val();
		
		data = {
			'idzona': idzona,
			'idgrupo': ''
		};
		
		postUrl = "<?php echo site_url('Ticket/getTecnicos'); ?>";
		$.ajax({
			type: "POST",
			url: postUrl,
			data: data,
			dataType: "text",
			success: function (result) {
				
				var subzona = (JSON.parse(result));
				var html = "<select>"
				html += "<option value=''> Seleccione un Tecnico</option>"
				for (var i = 0; i < subzona.length; i++) {
					html += "<option value=" + subzona[i].id + ">" + subzona[i].name + "</option>"
				}
				html += "<select>"
				
				if (result == 'NOK') {
					swal("Mensaje", "No se encuentran datos para esta busqueda.", "warning")
				} else {
					$("#selecTecnico").html(html);
				}
				;
			},
			error: function (xhr, ajaxOptions, thrownError) {
			}
		});
	});
	
	$(function () {
		
		$('#selecZona select').val(zona);
		$('#selectGrupos select').val(grupo);
		$('#selecCustomer select').val(customer);
		
		
		initMap("nada", 1, 3);
		
		markers = [];
		for (var i = 0; i < jsonCordenadas.length; i++) {
			name = jsonCordenadas[i].name;
			lati = jsonCordenadas[i].coordx;
			long = jsonCordenadas[i].coordy;
			tel = jsonCordenadas[i].phone;
			lastConn = jsonCordenadas[i].updatetimecoord;
			estado = jsonCordenadas[i].idstatus;
			grupo = jsonCordenadas[i].grp;
			nameState = jsonCordenadas[i].nameState;
			pintaMarker(parseFloat(lati), parseFloat(long), name, tel, lastConn, estado, grupo, nameState)
		}
		
		setCluster();
		
		for (var i = 0; i < jsonCordenadasSites.length; i++) {
			name = jsonCordenadasSites[i].name;
			address = jsonCordenadasSites[i].address;
			lat = jsonCordenadasSites[i].lat;
			lng = jsonCordenadasSites[i].lng;
			region = jsonCordenadasSites[i].region;
			customer = jsonCordenadasSites[i].customer;
		}
		
		
		$('.asgignarTicket').click(function () {
			
			var id = this.id
			text = $('#' + id).text();
			
			if (text == 'Asignar') {
				var idTeck = id.substring(2, id.length);
				var url = "<?php echo site_url('tracking/asignar' . '?idTech='); ?>";
				window.location.href = url + idTeck;
				
			} else {
				var idTeck = id.substring(2, id.length);
				
				getDetailTicketForIdTech(idTeck);
				
			}
		})
		
		$('.techDirections').click(function () {
			var incident = $(this).attr('id');
			var idTech = $(this).attr('idTech');
			
			if (idTech && incident) {
				idTech = idTech.substring(3, idTech.length);
				showModal(incident, idTech);
			}
		})
	});
	
	function posicion2(lati, long, imei) {
		
		if (isNaN(lati) || isNaN(long) || lati == "0.00000" || parseInt(lati) == 0 || long == "0.00000" || parseInt(long) == 0) {
			swal("Mensaje iTr@ces ", "No hay datos de geolocalización...!", "warning");
			return;
		} else {
			var pt = new google.maps.LatLng(lati, long);
			myMap.setCenter(pt);
			myMap.setZoom(20);
		}
	}
	
	function recallMaps() {
		
		var zona = $('#selecZona').val();
		var grupo = $('#selectGrupos option:selected').val();
		var tecnico = $('#selecTecnico option:selected').val();
		
		dataMaps = {
			"imei": "imei",
			"zona": zona,
			"grupo": grupo,
			"tecnico": tecnico,
		};
		
		postUrl = "<?php echo site_url('Tracking/recallMap'); ?>";
		
		$.ajax({
			type: "POST",
			url: postUrl,
			data: dataMaps,
			dataType: "text",
			success: function (result) {
				
				arr = (JSON.parse(result));
				console.log(arr);
				centro = myMap.center;
				zoom = myMap.getZoom();
				displayMarker(arr.tech2, arr.hora);
			},
			error: function (xhr, ajaxOptions, thrownError) {
			
			}
		});
	}
	
	function showModal(incident, idTech) {
		
		console.log(incident + ' :: ' + idTech);
		datashow = {
			"incident": incident,
			"idTech": idTech
		};
		postUrl = "<?php echo site_url('Tracking/showModal'); ?>";
		
		$.ajax({
			type: "POST",
			url: postUrl,
			data: datashow,
			dataType: "text",
			success: function (result) {
				
				$("#mapDirections").html(result).toggle().toggle();
				$("#myModalDirections").modal("show");
				
			},
			error: function (xhr, ajaxOptions, thrownError) {
			
			}
		});
	}
	
	
	function displayMarker(puntos, hora) {
		
		limpiaCluster();
		clearMarkers();
		markers = [];
		
		for (var i = 0; i < puntos.length; i++) {
			id = puntos[i].id;
			name = puntos[i].name;
			lati = puntos[i].coordx;
			long = puntos[i].coordy;
			tel = puntos[i].phone;
			estado = puntos[i].idstatus;
			lastConn = hora;
			bateria = puntos[i].bateria;
			imei = puntos[i].imei;
			grupo = puntos[i].grp;
			nameState = puntos[i].nameState;
			
			validaGps(lastConn, name, hora, id, lati, long);
			
			
			if (bateria < 5) {
				img = "<?php echo site_url('assets/images/batery1.jpg')?>"
				$('#bt' + id).attr('src', img);
			} else if (bateria < 26) {
				img = "<?php echo site_url('assets/images/batery2.jpg')?>"
				$('#bt' + id).attr('src', img);
			} else if (bateria < 60) {
				img = "<?php echo site_url('assets/images/batery3.jpg')?>"
				$('#bt' + id).attr('src', img);
			} else {
				img = "<?php echo site_url('assets/images/batery4.jpg')?>"
				$('#bt' + id).attr('src', img);
			}
			
			switch (estado) {
				case "Recibido":
					icon = "<?php echo site_url('assets/images/contacted.png')?>"
					break;
				case "En El Lugar":
					icon = "<?php echo site_url('assets/images/onsite.png')?>"
					break;
				case "En Progeso":
					icon = "<?php echo site_url('assets/images/workinprogress.png')?>"
					break;
				case "Resuelto":
					icon = "<?php echo site_url('assets/images/resolved.png')?>"
					break;
				default:
					icon = "<?php echo site_url('assets/images/truck.png')?>"
			}
			
			$('#img' + id).attr('src', icon);
			
			if (estado == null || estado == "Cerrado") {
				estado = 'Disponible';
				
				$('#tk' + id).css('display', 'none');
			}
			
			$('#' + id).text(estado);
			$('#bt' + id).text(bateria + ' %')
			
			pintaMarker(parseFloat(lati), parseFloat(long), name, tel, hora, estado, grupo, nameState)
			asignaOnClick(id, parseFloat(lati), parseFloat(long), imei)
			
		}
		setCluster()
	}
	
	function validaGps(lastConn, name, horaActual, id, lati, long) {
		
		var fecActual2 = new Date(horaActual).getTime();
		var fechaTec = new Date(lastConn).getTime();
		
		resultado = (fecActual2 - fechaTec);
		
		console.log('Server 2 : ' + horaActual + ' - Tec Old : ' + lastConn);
		console.log('lati  : ' + lati + ' - long : ' + long);

		if (lastConn == null)
			lastConn = 'Sin Datos';
		
		var img  = '';
		
		if ( ( isNaN(lati) || isNaN(long) || lati == "0.00000" || parseInt(lati) == 0 || long == "0.00000" || parseInt(long) == 0) || resultado > 180000){
			
			var img = "<?php echo site_url('assets/images/gpsNOK.png')?>";
			
			$('.gps' + id).parent().prop("style", "padding: 3px; background-color: #f3a1a1d4;");
			$('.gps' + id).attr('src', img);
			$('#lastConTec' + id).text('Ultima sincronización : ' + lastConn);
		} else {
			
			var img = "<?php echo site_url('assets/images/gpsOK.png')?>";
			
			$('.gps' + id).parent().prop("style", "padding: 3px; background-color: #2994295e;");
			$('.gps' + id).attr('src', img);
			$('#lastConTec' + id).text('Ultima sincronización : ' + lastConn);
		}
		
	}
	
	function calcularDiasAusencia(fechaIni, fechaFin) {
		
		var fechaInicio = new Date(fechaIni).getTime();
		var fechaFin = new Date(fechaFin).getTime();
		
		var diff = fechaFin - fechaInicio;
		
		
		return diff // diaEnMils;
	}
	
	function asignaOnClick(id, lati, long, imei) {
		
		$('#oc' + id).attr('onclick', '').unbind('click');
		$('#oc' + id).click(function () {
			posicion2(lati, long, imei)
		})
	}
	
	function getDetailTicketForIdTech(idTech) {
		
		dataMaps = {"idTech": idTech};
		
		postUrl = "<?php echo site_url('Tracking/getDetailTicketForIdTech'); ?>";
		
		$.ajax({
			type: "POST",
			url: postUrl,
			data: dataMaps,
			dataType: "text",
			success: function (result) {
				$("#tableDetailTicket").html(result).toggle().toggle();
				$("#myModal").modal("show");
				
				
			},
			error: function (xhr, ajaxOptions, thrownError) {
			
			}
		});
	}
</script>


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////    -->
<script>
	
	function initMap(centro, tipo, zoom) {
		
		if (zona == 1) {
			centro = {lat: -26.3772624, lng: -70.3560286} //33.4396456,-70.6603728	
			zoom = 6
		} else if (zona == 2) {
			centro = {lat: -31.396456, lng: -70.6603728} //33.4396456,-70.6603728
			zoom = 8
			
		} else if (zona == 3) {
			centro = {lat: -39.396456, lng: -70.8603728} //33.4396456,-70.6603728
			zoom = 6
			
		} else if (zona == 4) {
			centro = {lat: -33.4396456, lng: -70.6603728} //33.4396456,-70.6603728
			zoom = 12
		} else {
			centro = {lat: -33.4396456, lng: -70.6603728} //33.4396456,-70.6603728
			zoom = 12
		}
		if (tipo == 1) {
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: zoom,
				center: centro
			});
		} else {
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: zoom,
				center: centro
			});
		}
		
		
		myMap = map;
		
		google.maps.event.addDomListener(myMap, 'zoom_changed', function () {
			
		});
		
	};
	
	
	function pintaMarker(lati, longi, name, tel, lastConn, estado, grupo, nameState) {
		
		var contentString;

		if ( estado == 1 || estado == 5 ) {
			estadoAux = "Disponible";
		} else {
			estadoAux = "En Ruta";
		}
		
		if (nameState == null) {
			estadoAux = "Disponible"
		}
		
		
		contentString = '<div id = ""><b>' + 'Técnico: </b>' + name + ' <br>'
		contentString = contentString + '<b>Teléfono: </b>' + tel + ' <br>'
		contentString = contentString + '<b>Grupo: </b>' + grupo + ' <br>'
		contentString = contentString + '<b>Última sincronización: </b><p>' + lastConn + '</p> <br>'
		
		var myLatLngI = {lat: lati, lng: longi};
		var infowindow = new google.maps.InfoWindow({
			content: contentString,
			infoBoxClearance: new google.maps.Size(1, 1),
			maxWidth: 600,
			position: new google.maps.LatLng(-32.0, 149.0)
		});
		
		switch (estadoAux) {
			case "Recibido":
				icon = "<?php echo site_url('assets/images/contacted2.png')?>"
				break;
			case "En El Lugar":
				icon = "<?php echo site_url('assets/images/onsite2.png')?>"
				break;
			case "En Progeso":
				icon = "<?php echo site_url('assets/images/workinprogress2.png')?>"
				break;
			case "Resuelto":
				icon = "<?php echo site_url('assets/images/resolved2.png')?>"
				break;
			default:
				icon = "<?php echo site_url('assets/images/truck.png')?>"
		}
		
		name = name.toUpperCase();
		
		var marker = new google.maps.Marker({
			position: myLatLngI,
			map: myMap,
			icon: icon
		});
		
		marker.addListener('click', function () {
			closeAllInfoWindows();
			infowindow.open(myMap, marker);
		});
		
		markers.push(marker);
		infoWindows.push(infowindow);
	}
	
	function closeAllInfoWindows() {
		for (var i = 0; i < infoWindows.length; i++) {
			infoWindows[i].close();
		}
	}
	
	function closeAllInfoWindowsSites() {
		for (var i = 0; i < infoWindowsSites.length; i++) {
			infoWindowsSites[i].close();
		}
	}
	
	function setCluster() {
		
		markerCluster = new MarkerClusterer(myMap, markers,
			{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
	}
	
	function limpiaCluster() {
		markerCluster.removeMarkers(markers);
	}
	
	
	function clearMarkers() {
		setMapOnAll(null);
		
	}
	
	function setMapOnAll(map) {
		for (var i = 0; i < markers.length; i++) {
			
			markers[i].setMap(map);
		}
	}
	
	setInterval(function () {
		recallMaps()
	}, 10000);

</script>

<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>

<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJc-O9rkJ6lxxGGKHXzukR519_4JGoPDA&libraries=places,visualization,drawing"
	sync defer></script>

<!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJc-O9rkJ6lxxGGKHXzukR519_4JGoPDA&libraries=places,visualization,drawing&callback=initMap"  sync defer>  </script-->


<script>
	
	$(document).ready(function () {
		
		$("#list-table tbody tr").click(function () {
			$("#list-table tbody tr").removeClass("tr_hover");
			$(this).addClass("tr_hover");
		});
		
	});
	
	
	$('#modalInactive').on('click', function(event){
		event.preventDefault();
		
		$('#tecnicosModalInactive').modal();
	});
	
</script>
