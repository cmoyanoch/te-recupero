<script src="https://use.fontawesome.com/32535cb184.js"></script>


  <?php echo form_open(site_url('tracking/tickets'), array('method' => 'get')); ?>
  <div class="card border-primary mb-3" >
  <div class="card-header"  style="background-color: #1E3652 !important;color:#fff" >Operaciones Reales</div>
  <div class="card-body text-primary">
  


<div class="form-row" >

  <div class="form-group col-md-3"> <?php echo form_dropdown('factoriFilter', $factoriFilter, set_value('factoriFilter',isset($factoriFilter['factoriFilter']) ? $factoriFilter['factoriFilter'] : ''),array('class' => 'form-control', 'name' => 'filterFacto', 'id' => 'filterFacto','style' => 'font-size:80%',  'style' => 'width:95%' )); ?><?php echo form_error('factoriFilter'); ?></div>

  <div class="form-group col-md-3">
 <?php echo form_dropdown('clienteFilter', $clienteFilter, set_value('clienteFilter',isset($clienteFilter['clienteFilter']) ? $clienteFilter['clienteFilter'] : ''),array('class' => 'form-control', 'name' => 'filterCliente', 'id' => 'filterCliente','style' => 'font-size:80%',  'style' => 'width:95%'));?><?php echo form_error('clienteFilter'); ?></div>


  <div class="form-group col-md-3">
 <?php echo form_dropdown('docFilter', $docFilter, set_value('docFilter',isset($docFilter['docFilter']) ? $docFilter['docFilter'] : ''),array('class' => 'form-control', 'name' => 'filterDoc', 'id' => 'filterDoc','style' => 'font-size:80%',  'style' => 'width:95%'));?><?php echo form_error('docFilter'); ?></div>


  <div class="form-group col-md-2"> <?php echo form_input(array('type' => 'date', 'name' => 'filterDate', 'class' => 'form-control', 'placeholder' => 'Ingrese el numero de operacion', 'value' => $search_value, 'id' => 'filterDate','style' => 'font-size:70%' )) ?></div>



  <div class="form-group col-md-2"> <?php echo form_input(array('type' => 'text', 'name' => 'filterOpera', 'class' => 'form-control', 'placeholder' => 'Ingrese el numero de operacion', 'value' => $search_value, 'id' => 'filterOpera','style' => 'font-size:190%',  'style' => 'width:80%' )) ?></div>

  <div class="form-group col-md-3"  >
<button class="btn btn-success"  type="submit">Buscar</button>&nbsp;&nbsp;
<button class="btn btn-success creaOperacion" style="width:45%;font-size:80%"  type="button">+ Operacion</button></div>   
  </div>  
 


 </div>   
  </div>
  <?php echo form_close(); ?>





	<div class="container-fluid ">
		<div class="row">
			<div class="col-md-12">

					
					<div class="table-responsive">
						<?php echo form_open(site_url('tracking/delete'), array('id' => 'list-form')) ?>
						<table class="table table-hover table-responsive" id="list-table">
							<thead style="font-size:60%;" >

<th>Cliente</th>
<th>Rut</th>
<th>Nº Operación</th>
<th>Fecha Operación</th>
<th>Documentos</th>
<th>Cant. Documentos</th>
<th>Monto Operación</th>
<th>Monto Anticipado</th>
<th>Monto a Giro</th>
<th>Monto Diferencia de Precio</th>
<th>Monto Comisión</th>
<th>Monto Gastos</th>
<th>IVA Comisión</th>
<th>Tasa dif. Pre Real</th>
<th>Tasa Comisión</th>
<th>Tasa Gastos</th>
<th>Tasa real o Efectiva</th>
<th>Tasa dif. Pre Sugerida</th>
<th>Porcentaje de anticipo</th>
                   					
							</thead>
							<tbody>
			
<?php foreach ($tickets as $keytracking => $ticket){ ?>

<tr data-id="<?php echo $ticket['id']; ?>" style="font-size:60%;" >    

<td><?php echo $ticket['nombre_empresa']; ?></td>
<td><?php echo $ticket['rut_empresa']; ?></td>
<td>
<button type="button" class="btn btn-default detalleOperacion " id='<?php echo $ticket['nro_opera']; ?>' style="font-size:110%;"> 
<?php echo $ticket['nro_opera']; ?></button></td>

<td><?php echo $ticket['date_opera']; ?></td>
<td><?php echo $ticket['avr_documento']; ?></td>

<td><button type="button" class="btn btn-default detalleDocumento " id='<?php echo $ticket['id']; ?>' style="font-size:110%;"> 
<?php echo $ticket['total_doc']; ?></button></td>
<td><?php echo number_format($ticket['monto_doc']); ?></td>

<td><?php echo $ticket['monto_adelan']; ?></td>

<td><?php echo $ticket['total_girado']; ?></td>



<td><?php echo number_format($ticket['mto_difpre']); ?></td>

<td><?php echo number_format($ticket['comi_total']); ?></td>

<td><?php echo number_format($ticket['monto_gasto']); ?></td>

<td><?php echo number_format($ticket['iva_comi']); ?></td>

<td><?php echo number_format($ticket['total_girado']); ?></td>

<td><?php echo number_format($ticket['total_girado']); ?></td>

<td><?php echo number_format($ticket['total_girado']); ?></td>

<td><?php echo number_format($ticket['total_girado']); ?></td>

<td><?php echo number_format($ticket['total_girado']); ?></td>

<td><?php echo number_format($ticket['porc_ant']); ?></td>
<td><button type="button" class="btn btn-default fa fa-trash-o delete-ope"  id="<?php echo $ticket['id']?>" ></button></td>
</tr>
<?php } ?> 
							</tbody>
						</table>
					</div>
					<?php echo form_close(); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 pagination-content">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<?php echo $pagination; ?>
					</ul>
				</nav>
			</div> 
		</div>
	</div>

<script type="text/javascript">
	
	$(document).ready(function () {
		
		
		
			$("#filterFacto").change(function (e) {
			var idFact = $("#filterFacto").val();
			data = {'idFact': idFact };
					
			if (idFact != 0) {
				postUrl = "<?php echo site_url('tracking/getCliente'); ?>";
				$.ajax({
					type: "POST",
					url: postUrl,
					data: data,
					dataType: "text",
					success: function (result) {

                                          	var cliente = (JSON.parse(result));
                                                var html = "<select>"
						html += "<option value=''> Seleccione un Cliente</option>"
						for (var i = 0; i < cliente.length; i++) {
							html += "<option value=" + cliente[i].id + ">" + cliente[i].name + "</option>"
						}
						html += "<select>"
                          
                        			if (result == 'NOK') {
							console.log('aca !!!')
							swal("Mensaje", "No se encuentran datos para esta busqueda.", "warning")
						} else {
							console.log('aca2 !!!')
							$("#filterCliente").html(html);
							
						};  
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
				});
				
			}else{
				
				$("#filterCliente option").remove();
				$("#filterCliente").append("<option value=''>Seleccione un Cliente</option>");
				
			}
		});	
		
		
		
		
		
		<?php if ($search_value): ?>
		$("#search-value").focus();
		$("#search-value").val($("#search-value").val());
		<?php endif ?>
		
		
		
	});

</script>

<div class="modal fade" id="myModal" role="dialog"  style="width:100%;"  >
	<div class="modal-dialog modal-xl">
	    <div class="modal-content">
             <div class="modal-header" style="background-color: #1E3652 !important;color:#fff;">
                   <button type="button" class="close" data-dismiss="modal">&times;</button> 
                   <h4 class="modal-title" style="font-size:16px;align-items:center;justify-content:center;" >DETALLE LIQUIDACIÓN FACTORING</h4>
              </div>
	          <div class="modal-body">
	              <p id="tableDetailDocumento">The <strong>show</strong> method shows the modal and the <strong>hide</strong>method hides the modal.</p>
	          </div>
	          <div class="modal-footer">
	               <button type="button" id="btnAceptaAsig" class="btn btn-default" data-dismiss="modal">Aceptar</button>
	         </div>
         </div>
	</div>
</div>


<div class="modal fade" id="myModal2" role="dialog"  style="width:100%;"  >
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #1E3652 !important;color:#fff;" >
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" style="font-size:16px;align-items:center;justify-content:center;">LIQUIDACION FACTORING</h4>
			</div>
			<div class="modal-body">
				<p id="tableDetailOperacion">The <strong>show</strong> method shows the modal and the <strong>hide</strong>
					method hides the modal.</p>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnAceptaAsig" class="btn btn-default" data-dismiss="modal">Aceptar</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="myModal3" role="dialog" >
	<div class="modal-dialog modal-xl modal-fluid ">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #1E3652 !important;color:#fff;" >
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" style="font-size:16px;align-items:center;justify-content:center;">CREAR OPERACION</h4>
			</div>
			<div class="modal-body">
				<p id="tableCreaOperacion">The <strong>show</strong> method shows the modal and the <strong>hide</strong>
					method hides the modal.</p>
			</div>
			<div class="modal-footer">
		
                <button type="button" class="btn btn-success saveoperacion">Guardar</button>


		<button type="button" id="btnAceptaAsig" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>






<script type="text/javascript">






	$(function () {
	

         	<?php if ($search_value): ?>
		$("#search-value").focus();
		$("#search-value").val($("#search-value").val());
		<?php endif; ?>
			
		
		$(".detalleDocumento").click(function () {
			var oper = this.id;
			dataUser = {"oper": oper};
			postUrl = "<?php echo site_url('Tracking/getDocumento'); ?>";
			
			$.ajax({
				type: "POST",
				url: postUrl,
				data: dataUser,
				dataType: "text",
				success: function (result) {
					$("#tableDetailDocumento").html(result).toggle().toggle();
					$("#myModal").modal("show");
				},
				error: function (xhr, ajaxOptions, thrownError) {
				
				}
			});
		});



       $(".detalleOperacion").click(function () {
			var oper = this.id;
                        var tipo = "<?php echo $tipo; ?>"; 
			dataUser = {"oper": oper,"tipo": tipo};
			postUrl = "<?php echo site_url('Tracking/getOperacion'); ?>";
			
			$.ajax({
				type: "POST",
				url: postUrl,
				data: dataUser,
				dataType: "text",
				success: function (result) {    
					$("#tableDetailOperacion").html(result).toggle().toggle();
					$("#myModal2").modal("show"); 
				},
				error: function (xhr, ajaxOptions, thrownError) {
				
				}
			});
		});


       $(".creaOperacion").click(function () {
			
        		postUrl = "<?php echo site_url('Tracking/createTicket'); ?>";
			    dataUser = {"tipo": 1};
			      $.ajax({
				    type: "POST",
				    url: postUrl,
                    data: dataUser,
				    dataType: "text",
				    success: function (result) {    
					$("#tableCreaOperacion").html(result).toggle().toggle();
					$("#myModal3").modal("show"); 
				},
				error: function (xhr, ajaxOptions, thrownError) {
				
				}
			});               	
		
		});


       $('.delete-ope').on('click', function(){
            
            var ope = $(this).attr('id');
            
            swal({
                title: "itr@ces",
                text: "Desea Eliminar Operacion !",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                
                data = { 'ope': ope };
                postUrl = "<?php echo site_url('Tracking/deleteOpe'); ?>";
                
                $.ajax({
                    type: "POST",
                    url: postUrl,
                    data: data,
                    dataType: "text",
                    success: function (result) {

                        if(result == 'OK'){
                            swal({
                                title: "Te Recupero",
                                text: "Operacion Eliminada",
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                id:"OK"
                            }, function (isConfirm) {
                                    if (isConfirm) { 	location.reload();
                                                              
                                    $('#myModal').on('hidden.bs.modal', function (e) {
	                            $(this).removeData('bs.modal');
	                            $(this).find('.modal-content').empty();
                                  })
                                                  }
                            });
                        }else{
                            swal({
                                title: "Te Recupero",
                                text: "Error, contacte a un administrador de Te Recupero !! ",
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: false
                            });
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) { }
                });
            });
        });




		
		$(".VerPdf").click(function (e) {
			var id = this.id;
			var idx = id.split("|");
			headId = idx[4];
			data = {
				'incident': idx[0],
				'title': idx[1],
				'imei': idx[2],
				'trackingId': idx[3],
				'headId': idx[4],
				'tecname': idx[5]
			};
			postUrl = "<?php echo site_url('Tracking/showPDF') . '?headId='; ?>" + headId;
			console.log(postUrl);
			$.ajax({
				type: "POST",
				url: postUrl,
				data: data,
				dataType: "text",
				success: function (result) {
					var win = window.open(postUrl, '_blank');
					win.focus();
				},
				error: function (xhr, ajaxOptions, thrownError) {
				}
			});
		});
	});


</script>
	
