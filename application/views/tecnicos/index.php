<div class="list">
  <ol class="breadcrumb" style="background-color: #1E3652 !important;color:#fff">
    	  <div class="card-header" >Mantenedor de Clientes</div>
    </ol>               
        
	<div class="container-fluid menu-content">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<?php echo form_open(site_url('tecnicos/index'), array('method'=>'get')); ?>
					<div class="input-group">
						<?php echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué conductor deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>
						<span class="input-group-btn">
							<button class="btn btn-default blue" type="submit">Buscar</button>
						</span>
					</div>
					<?php echo form_close(); ?>
				</div>
				<div class="col-md-6 button-content">								
					<a href="<?php echo site_url('tecnicos/create'); ?>" class="btn btn-default blue">Crear</a>	
					<a href="#" id="getExcelConductores" class="btn btn-success green">Descargar Excel</a>				
				</div>
			</div>			
		</div>		
	</div>
	<div class="container ">
		<div class="row">
			<div class="col-md-12">				
				<?php if (count($tecnicos) > 0){ ?>
				<div class="table-responsive">
					<?php echo form_open(site_url('tecnicos/delete'), array('id'=>'list-form')) ?>										
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
								<td>Nombre Cliente</td>
								<td>Rut</td>
								<td>Direccion</td>
								<td class="with-icon"></td>
							</tr>				
						</thead>
						<tbody>
							<?php foreach ($tecnicos as $keyTec => $tec): ?>							
								<tr>
									<td><?php echo $tec['nomb'] ?></td>
									<td><?php echo $tec['rut'] ?></td>
									<td><?php echo $tec['direc'] ?></td>
									<td>										
										<a title="Eliminar" onclick="return confirm('Estas seguro de eliminar este técnico?');" href="<?php echo site_url('tecnicos/delete/') .$tec['id']; ?>">
											<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete' ?>"></use></svg>
										</a>
									</td>
									<td>						
										<a title="Editar" href="<?php echo site_url('tecnicos/edit/'.$tec['id']) ?>">
											<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-edit' ?>"></use></svg>
										</a>										
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
				<?php echo form_close(); ?>				
				<?php }else{ 
					if ($search_value != "") {
						echo "<p class='message-no-data'>No existen resultados con los criterios de búsqueda ingresados.</p>";
						echo "<p class='message-no-data link'><a href='".site_url('user/index')."'>Volver al inicio</a></p>";
					}else{
						echo "<p class='message-no-data'>El sistema aun no registra información.</p>";
					}	
				} ?>				
			</div>	
		</div>		
		<div class="row">
			<div class="col-md-12 pagination-content">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<?php echo $pagination; ?>					
					</ul>
				</nav>	
			</div>
		</div>
	</div>
</div>

<!--
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="exampleModalLabel">Inactivar Tecnico</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<label>Tipo de Inactividad</label>
						<select id="typeInactive" class="form-control" required>
							<option value="">&lt; Seleccione Por Favor &gt;</option>
							<option value="1">Desvinculado</option>
							<option value="2">Vacaciones</option>
							<option value="3">Licencia Medica</option>
						</select>
					</div>
					<div class="col-md-12">
						<label>Comentario</label>
						<textarea id="commentaryInactive" class="form-control" rows="5" required ></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary save-change">Guardar</button>
			</div>
		</div>
	</div>
</div>
-->
<input type="hidden" id="TecId" value="">
<script>
	
	/*
	function inactiveModal(id){
		
		$('#TecId').val(id);
		$('#exampleModal').modal();
	}
	
	function activeModal(id){
		
		postUrl = "<?php echo site_url('Tecnicos/setActive'); ?>";
		
		data = { 'idTec': id };
		
		swal({
			title: "itr@ces",
			text: "Desea activar el técnico ?",
			type: "info",
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true
		}, function () {
			$.ajax({
				type: "POST",
				url: postUrl,
				data: data,
				dataType: "text",
				success: function (result) {
					
					if(result == "OK"){
						swal({
							title: "itr@ces",
							text: "Tecnico Activado Correctamente",
							type: "success",
							showCancelButton: false,
							closeOnConfirm: false
						}, function (isConfirm) {
							if (isConfirm) { location.reload(); }
						});
					}else{
						swal({
							title: "itr@ces",
							text: "Error ,\n contacte a un administrador de itr@ces !!",
							type: "warning",
							showCancelButton: false,
							closeOnConfirm: false
						});
					}
				},
				error: function (xhr, ajaxOptions, thrownError) { }
			});
		});
	}
	*/
	
	$('.save-change').on('click', function(event){

		event.preventDefault();
		
		var idTec      = $('#TecId').val();
		var type       = $('#typeInactive').val();
		var commentary = $('#commentaryInactive').val();
		
		data = { 'idTec'      : idTec,
					'type'       : type,
					'commentary' : commentary };
		
		if(type.length > 0 && commentary.trim() != '' && idTec.length > 0 ){
			
			postUrl = "<?php echo site_url('Tecnicos/setInactive'); ?>";
			
			swal({
				title: "itr@ces",
				text: "Desea inactivar el técnico ?",
				type: "info",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true
			}, function () {
				$.ajax({
					type: "POST",
					url: postUrl,
					data: data,
					dataType: "text",
					success: function (result) {
						console.log(result);
						
						if(result == "OK"){
							swal({
								title: "itr@ces",
								text: "Tecnico inactivado Correctamente",
								type: "success",
								showCancelButton: false,
								closeOnConfirm: false
							}, function (isConfirm) {
								if (isConfirm) { location.reload(); }
							});
						}else{
							swal({
								title: "itr@ces",
								text: "Error ,\n contacte a un administrador de itr@ces !!",
								type: "warning",
								showCancelButton: false,
								closeOnConfirm: false
							});
						}
					},
					error: function (xhr, ajaxOptions, thrownError) { }
				});
			});
		}else{
			swal({
				title: "itr@ces",
				text: "Debe ingresar datos en los campos !! ",
				type: "warning",
				showCancelButton: false,
				closeOnConfirm: false
			});
		}
	});


	$('#getExcelConductores').on('click', function(event){


			
			postUrl = "<?php echo site_url('tecnicos/getExcelTecnicos'); ?>";
	
				$.ajax({
					type: "GET",
					url: postUrl,
					xhrFields: {
            responseType: 'blob'
                     },
					 success: function (response) {
					
					        var blob = new Blob([response], { type: 'application/vnd.ms-excel' });
				            var downloadUrl = URL.createObjectURL(blob);
				            var a = document.createElement("a");
				            a.href = downloadUrl;
				            a.download = "planilla-clientes.xlsx";
				            document.body.appendChild(a);
				            a.click();
					
					},
					error: function (xhr, ajaxOptions, thrownError) { }
				});
	});
</script>
