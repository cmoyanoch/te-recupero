<div class="list">
  <ol class="breadcrumb" style="background-color: #1E3652 !important;color:#fff">
    	<div class="card-header" >Crear Cliente</div>
    </ol>               
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('tecnicos/edit/'.$tec['id'], array('id'=>'edit-form'));
		echo form_hidden('tec[id]', $tec['id']);
	}else{
		echo form_open('tecnicos/create'); 	
	}	
	?>
	<div class="row">

		<div class="col-md-8">
			<div class="form-group">
				<label for="tec[name]">Nombre Completo:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'tec[name]','title'=>'Ingrese solo caracteres alfabéticos ', 'class'=> 'form-control', 'value'=> set_value('tec[name]', isset($tec['name'])? $tec['name'] :''))); ?>
				<?php echo form_error('tec[name]'); ?>
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
				<label for="tec[rut]">RUT:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'tec[rut]', 'class'=> 'form-control', 'value'=> set_value('tec[rut]', isset($tec['rut'])? $tec['rut'] :''))); ?>
				<?php echo form_error('tec[rut]'); ?>
			</div>
		</div>
	
		<div class="col-md-6">			
			<div class="form-group">
				<label for="tec[direc]">Direccion:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'tec[direc]', 'class'=> 'form-control', 'value'=> set_value('tec[direc]', isset($tec['direc'])? $tec['direc'] :''))); ?>
				<?php echo form_error('tec[direc]'); ?>
			</div>
		</div>


	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-success">Guardar</button>	
			</div>	
		</div>
	</div>
</div>

