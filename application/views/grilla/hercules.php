<script src="../assets/js/stomp.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<style type="text/css">
	.position
	{
		margin:20px 0;
		border-bottom: 1px solid grey;
	}
	.btn
	{
		/*margin-left: 25%;*/
	}
</style>

<div class="list">
    
    <ol class="breadcrumb" style="margin-top: 4px;">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="active"><strong>Grilla Hercules</strong></a></li>
    </ol> 

    <div class="row" style="margin-bottom: 20px;max-width: 100%;">
    	<div class="col-md-6">
	    	<div class="col-md-4">
				<div class="form-group">
					<label for="">Buscar por carga:</label>
					<input type="text" name="search" id="search" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Buscar por N° de camión:</label>
					<input type="text" name="truck" id="truck" class="form-control">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<input style="margin-top: 20px;" type="button" name="searchbtn" id="searchbtn" value="Buscar" class="btn btn-success">
				</div>
			</div>
    	</div>
    	<div class="col-md-2">
    		<div class="form-group">
					<input style="margin-top: 20px;" type="button" name="cleanbtn" id="cleanbtn" value="Limpiar Filtros" class="btn btn-primary">
				</div>
    	</div>
	    <div class="col-md-2 button-content" style="text-align: right;">								
				<a  style="margin-top: 20px;" href="#" id="getExcelGrilla" class="btn btn-success green">Descargar Excel</a>	
		</div>
	</div>             

	<div class="container-fluid">
				
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="container-fluid">
					<div style="background: #E61D2A;color: white; padding: 1em 0;border-radius: 3em;" class="row">
						<div class="col-md-2 col-sm-5 col-xs-5">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">Carga</p>
						</div>
						<!-- <div class="col-md-2 col-sm-5 col-xs-5">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">Conductor</p>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">Camión</p>
						</div> -->
						<div class="col-md-2 col-sm-2 col-xs-2">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">Bahia</p>
						</div>
						<!-- <div class="col-md-3 col-sm-2 col-xs-2">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">Num. Transporte</p>
						</div> -->
<!-- 						<div class="col-md-2 col-sm-3 col-xs-3">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">Distancia</p>
						</div>
						<div class="col-md-1 col-sm-3 col-xs-3">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">Vuelta</p>
						</div> -->
						<div class="col-md-2 col-sm-3 col-xs-3">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">N° Camión</p>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-3">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">Vuelta</p>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-3">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">Fecha Creación</p>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-3">
							<p style="font-size: 1.5vw; text-align: center; align-content: center;font-weight:bolder;">Fecha Act.</p>
						</div>
					</div>

					<div class="row" id="content"></div>
					<!--<div class="row" id="position1" style="display: none;">
						<div class="col-md-5 col-sm-5 col-xs-5" id="conductor"></div>
						<div class="col-md-2 col-sm-2 col-xs-2" id="camion" ></div>
						<div class="col-md-2 col-sm-2 col-xs-2" id="bahia" ></div>
						<div class="col-md-3 col-sm-3 col-xs-3" id="distancia" ></div>
					</div>
					<div class="row" id="position2" style="display: none;">
						<div class="col-md-5 col-sm-5 col-xs-5" id="conductor" ></div>
						<div class="col-md-2 col-sm-2 col-xs-2" id="camion" ></div>
						<div class="col-md-2 col-sm-2 col-xs-2" id="bahia" ></div>
						<div class="col-md-3 col-sm-3 col-xs-3" id="distancia" ></div>
					</div>
					<div class="row" id="position3" style="display: none;">
						<div class="col-md-5 col-sm-5 col-xs-5" id="conductor" ></div>
						<div class="col-md-2 col-sm-2 col-xs-2" id="camion" ></div>
						<div class="col-md-2 col-sm-2 col-xs-2" id="bahia" ></div>
						<div class="col-md-3 col-sm-3 col-xs-3" id="distancia" ></div>
					</div>
					<div class="row" id="position4" style="display: none;">
						<div class="col-md-5 col-sm-5 col-xs-5" id="conductor" ></div>
						<div class="col-md-2 col-sm-2 col-xs-2" id="camion" ></div>
						<div class="col-md-2 col-sm-2 col-xs-2" id="bahia" ></div>
						<div class="col-md-3 col-sm-3 col-xs-3" id="distancia" ></div>
					</div>
					<div class="row" id="position5" style="display: none;">
						<div class="col-md-5 col-sm-5 col-xs-5" id="conductor" ></div>
						<div class="col-md-2 col-sm-2 col-xs-2" id="camion" ></div>
						<div class="col-md-2 col-sm-2 col-xs-2" id="bahia" ></div>
						<div class="col-md-3 col-sm-3 col-xs-3" id="distancia" ></div>
					</div>-->
				</div>
			</div>

		</div>
	</div>
<div class="modal fade" id="myModaltracking" role="dialog">
	<div class="modal-dialog modal-lg">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Detalle de tickets diario</h4>
			</div>
			<div class="modal-body">
				<p >
					  <table class="table table-striped">  
						<thead>
								<tr>

								  <th>Número</th>

							      <th>Carga</th>

							      <th>Descripción</th>

							      <th>Estado</th>

							      <th>Bahía</th>

							      <th>Vuelta</th>

							      <th>Fecha de creación</th>


							    </tr>

						</thead>
						<tbody id="tableDetailtracking">
							
						</tbody>
					</table>
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnAceptaAsig" class="btn btn-default" data-dismiss="modal">Aceptar</button>
			</div>
		</div>
	
	</div>
</div>


</div>


<script type="text/javascript">
	$(document).ready(function () {
		var search ='';
		var alerta = 0;
		var truck ='';


		$('#searchbtn').on('click', function(event){

		alerta = 1;

		search = $('#search').val();
		truck = $('#truck').val();

		init(search,truck);

	});

		$('#cleanbtn').on('click', function(event){



		search ='';
		truck = '';

		init(search,truck);

	});


		var getDistance = function(lat1,lon1,lat2,lon2) {

			if(lat1==null)
			{
				lat1=0.00000;
			}
			else if(lon1==null)
			{
				lon1=0.00000;
			}
			else if(lat2==null)
			{
				lat2=0.00000;
			}
			else if (lon2==null)
			{
				long2=0.0000;
			}
			
			var rad = function(x) {
				return x*Math.PI/180;
			}
			var R = 6371000; //Radio de la tierra en km
			var dLat = rad( lat2 - lat1 );
			var dLong = rad( lon2 - lon1 );
			var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c;
			return d.toFixed(0); //Retorna tres decimales
		}

		 function init (search,truck) {
			var compare = function (a, b) {
				return a.Distancia - b.Distancia;
			};

			$.ajax({
				type : "POST",
				url : "<?php echo site_url('Grilla/getDataInitHercules'); ?>"+"?search="+search+"&truck="+truck,
				success : function (data) {
					data = JSON.parse(data);

					if(data!='NOK')
					{

					var arrayData = [];

					for (var i = 0; i < data.length; i++) {
						arrayData.push({
							'carga' : data[i].incident,
							//'Camion' : data[i].numberTruck,
							'Bahia' : data[i].id_bahia,
							//'idTec' : data[i].idTec,
							//'cantidad3' : data[i].cantidad3,
							'datecreation' : data[i].datecreation,
							'radio_cerco' : data[i].radio_cerco,
							'radio_cerco_2' : data[i].radio_cerco_2,
							'radio_cerco_3' : data[i].radio_cerco_3,
							'radio_cerco_4' : data[i].radio_cerco_4,
							'actualizacion' : data[i].updatetime,
							'numberTruck' : data[i].numberTruck,
							'id_area' : data[i].id_area,
							//'idsap' : data[i].external_id,
							'estado' : data[i].estado,
							'ultimoupdate' : data[i].updatetimecoord,
							'Distancia' : getDistance(data[i].coordEmpX, data[i].coordEmpY, data[i].coordx, data[i].coordy)
						})
					}
					arrayData = arrayData.sort(compare);


					$("#content").empty();
					var ayuda = '';

					var fecha = "";
					var fecha2 = "";
					var fechareal = "";

					for (var i = 0; i < arrayData.length; i++) {


							if(arrayData[i].numberTruck ==null)
							{
								arrayData[i].numberTruck= 'Camión no registrado';
							}



							if(arrayData[i].Distancia>40000)
							{
								arrayData[i].Distancia = 'GPS apagado';
								ayuda = '';
							}
							else
							{
								ayuda = 'Mts';
							}

							console.log(arrayData[i].actualizacion);

							if(arrayData[i].actualizacion!=null)
							{
								fecha = arrayData[i].actualizacion.split(" ");
								fecha2 = fecha[0].split("-");
								fechareal = fecha2[2]+'-'+fecha2[1]+'-'+fecha2[0]+" "+fecha[1];
							}

							fechac = arrayData[i].datecreation.split(" ");
							fechac2 = fechac[0].split("-");
							fechacreal = fechac2[2]+'-'+fechac2[1]+'-'+fechac2[0]+" "+fechac[1];

						$("#content").append('<div class="row position" id="position' + i + '" >'+'<div class="col-md-2 col-sm-4 col-xs-5" id="carga"> '+
								' <p style="font-size: 1.5vw; color: #828282; text-align: center;">' + arrayData[i].carga + '</p> '+
							'</div>'+
							'<div class="col-md-2 col-sm-1 col-xs-2" id="bahia" >'+
								'<p style="font-size: 1.5vw; color: #828282; text-align: center;">' + arrayData[i].Bahia + '</p>'+
							'</div>'+
							'<div class="col-md-2 col-sm-1 col-xs-2" id="bahia" >'+
								'<p style="font-size: 1.5vw; color: #828282; text-align: center;">' + arrayData[i].numberTruck + '</p>'+
							'</div>'+
							'<div class="col-md-2 col-sm-1 col-xs-2" id="bahia" >'+
								'<p style="font-size: 1.5vw; color: #828282; text-align: center;">' + arrayData[i].id_area + '</p>'+
							'</div>'+
							'<div class="col-md-2 col-sm-2 col-xs-3" >'+
								 '<p style="font-size: 1.5vw; color: #828282; text-align: center;">'+fechacreal+'</p>'+
							'</div>'+
							'<div class="col-md-2 col-sm-2 col-xs-3" >'+
								 '<p style="font-size: 1.5vw; color: #828282; text-align: center;">'+fechareal+'</p>'+
							'</div></div>');

						fechareal = "";


							if(arrayData[i].Distancia=='GPS apagado')
							{
								arrayData[i].Distancia = 8000000;
							}

						if (parseInt(arrayData[i].Distancia, 10) <= parseInt(arrayData[i].radio_cerco, 10)) {
							$("#position" + (i) + " div#distancia").css('background-color', '#00A98A');
						} else if (parseInt(arrayData[i].Distancia, 10) > parseInt(arrayData[i].radio_cerco, 10) && parseInt(arrayData[i].Distancia, 10) < parseInt(arrayData[i].radio_cerco_2, 10)) {
							$("#position" + (i) + " div#distancia").css('background-color', '#FFF388');
						} else if (parseInt(arrayData[i].Distancia, 10) > parseInt(arrayData[i].radio_cerco_2, 10) && parseInt(arrayData[i].Distancia, 10) < parseInt(arrayData[i].radio_cerco_3, 10)) {
							$("#position" + (i) + " div#distancia").css('background-color', '#D7793C');
						} else if (parseInt(arrayData[i].Distancia, 10) >= parseInt(arrayData[i].radio_cerco_4, 10)) {
								$("#position" + (i) + " div#distancia").css('background-color', '#EC6767');
						} else
							{
								$("#position" + (i) + " div#distancia").css('background-color', '#EC6767');
							}

						/*$("#position" + (i + 1) + " div#conductor").empty();
						$("#position" + (i + 1) + " div#camion").empty();
						$("#position" + (i + 1) + " div#bahia").empty();
						$("#position" + (i + 1) + " div#distancia").empty();

						$("#position" + (i + 1) + " div#conductor").append('<p style="font-size: 1.5vw; color: #828282; text-align: center;">' + arrayData[i].Conductor + '</p>');
						$("#position" + (i + 1) + " div#camion").append('<p style="font-size: 1.5vw; color: #828282; text-align: center;">' + arrayData[i].Camion + '</p>');
						$("#position" + (i + 1) + " div#bahia").append('<p style="font-size: 1.5vw; color: #828282; text-align: center;">' + arrayData[i].Bahia + '</p>');
						$("#position" + (i + 1) + " div#distancia").append('<p style="font-size: 1.5vw; color: #fff; text-align: center;">' + arrayData[i].Distancia + ' Mts</p>');*/
					}

				}
				else if (data=='NOK' && alerta== 1)
				{
					alert("No existen datos con el criterio de busqueda");
					alerta=0;
				}
				},
				error : function (err) {
					console.log(err);
				}
			});
		}

		

		setInterval(function() { init(search,truck); }, 5000);
	});



	function cerrarticket(id) {
			
			data = {
				'id': id,
			};
			
			
			postUrl = "<?php echo site_url('Grilla/cerrarTicket'); ?>";
			$.ajax({
				type: "POST",
				url: postUrl,
				data: data,
				dataType: "text",
				success: function (result) {
					$("#divBloqueoPopup").css("display", "none");
					$("#divBloqueoPopup").html("");
					if (result != "NOK") {
						
						 swal("Su ticket se cerró", "", "success");
						
					} else {
						swal("Su ticket no se cerró, por favor contactese con el administrador", "No hay datos!", "error");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
				}
			});
		}

	$(document).on("click",".detalleIncidencia", function(){
		var idDetailTicket = this.id;
		 swal({
		    title: "¿Seguro quiere cerrar la carga?",
		    text: "Una vez cerrada no podrá volver a abrirla",
		    type: "warning",
		    showCancelButton: true,
		    confirmButtonColor: '#DD6B55',
		    confirmButtonText: 'Si',
		    cancelButtonText: "No",
		    closeOnConfirm: false,
		    closeOnCancel: false
		 },
		 function(isConfirm){

		   if (isConfirm){
		  
		     cerrarticket(idDetailTicket);
		    

		    } else {
		      swal("No se cerró el ticket seleccionado", "", "error");
		         e.preventDefault();
		    }
		 });
			
			
			
		});


		$('#getExcelGrilla').on('click', function(event){


			
			postUrl = "<?php echo site_url('grilla/getExcelGrillaHercules'); ?>";
	
				$.ajax({
					type: "GET",
					url: postUrl,
					xhrFields: {
            responseType: 'blob'
        },
					success: function (response) {
					
					        var blob = new Blob([response], { type: 'application/vnd.ms-excel' });
				            var downloadUrl = URL.createObjectURL(blob);
				            var a = document.createElement("a");
				            a.href = downloadUrl;
				            a.download = "planilla-grilla.xlsx";
				            document.body.appendChild(a);
				            a.click();
					
					},
					error: function (xhr, ajaxOptions, thrownError) { }
				});

	});

	
</script>