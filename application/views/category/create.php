<?php 
	$hasData = false;
	if(isset($category)){
		$hasData = true;
	}

?>
<!-- <pre> -->
<?php
// print_r($categories);
// echo "*************************";
// print_r($category);

?>
<!-- </pre> -->

<style>
label{
	font-weight: bold !important;
    color: white;
    font-size: 15px;
}
</style>
<div class="list">
    <ol class="breadcrumb" style="margin-top: 4px;">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Mantenedores</strong></a></li>
        <li><a href="#"><strong>Categoría</strong></a></li>
        <li class="active"><strong> Crear </strong></li>
    </ol>               
</div>
<div class="container">	
	<?php 
	if(isset($edit)){
		echo form_open('category/edit/'.$category['category_id'], array('id'=>'edit-form'));
		echo form_hidden('category[category_id]', $category['category_id']);
	}else{
		echo form_open('category/create'); 	
	}	
	?>
	<div class="row">
		<div class="col-md-6">			
			<div class="form-group">
				<label for="category[name]">Nombre categoría:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'category[name]','pattern'=>'[A-Za-z]+','title'=>'Ingrese solo caracteres alfabéticos sin acento ', 'class'=> 'form-control', 'value'=> set_value('category[name]', isset($category['name'])? $category['name'] :''))); ?>
				<?php echo form_error('category[name]'); ?>
			</div>
		</div>
		<div class="col-md-6">			
			<div class="form-group">
				<label for="category[cat_category_id]">Categoría padre:</label>
				<?php echo form_dropdown('category[cat_category_id]', $categories, set_value('category[cat_category_id]', isset($category['cat_category_id']) ? $category['cat_category_id'] : ''), array('class'=>'form-control')); ?>
				<?php echo form_error('category[cat_category_id]'); ?>
			</div>
		</div>	
	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-success">Guardar</button>	
			</div>	
		</div>
	</div>
