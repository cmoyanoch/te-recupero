<?php 
	$hasData = false;
	if(isset($group)){
		$hasData = true;
	}
?>
<div class="list">
  <ol class="breadcrumb" style="margin-top: 4px;background-color: #1E3652 !important;color:#fff">
    	  <div class="card-header" >Crear de Documentos</div>

    </ol>               
</div>
<div class="container">	
	<?php 
	if(isset($edit)){ 
		echo form_open('group/edit/'.$group['id'], array('id'=>'edit-form'));
		echo form_hidden('group[id]', $group['id']);
	}else{
		
		echo form_open('group/create'); 	
	}	
	?>
	<div class="row">

		<div class="col-md-6">			
			<div class="form-group">
				<label for="group[name]">Nombre Documento:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'group[name]','title'=>'Ingrese solo caracteres alfabéticos sin acento ', 'class'=> 'form-control', 'value'=> set_value('group[name]', isset($group['name'])? $group['name'] :''))); ?>
				<?php echo form_error('group[name]'); ?>
			</div>
		</div>

		<div class="col-md-6">			
			<div class="form-group">
				<label for="group[description]">Descripción Documento:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'group[description]','title'=>'Ingrese solo caracteres alfabéticos sin acento ', 'class'=> 'form-control', 'value'=> set_value('group[description]', isset($group['description'])? $group['description'] :''))); ?>
				<?php echo form_error('group[description]'); ?>
			</div>
		</div>
    
</div>
	<div class="row">
		<div class="col-md-12">

            <div class="form-buttons">
                <button type="submit" class="btn btn-default blue">Guardar</button>	
            </div>
        </div>	
		</div>
	</div>
