  <aside class="main-sidebar sidebar-light-success elevation-4"> 
    <!-- Brand Logo -->
      <a style="margin: auto; width: 70%;" href="<?php echo site_url() ?>" class="brand-link">
      <img style="max-width: 100%;" src="<?php echo base_url(); ?>assets/images/elementos-03.png" alt="Logo Terecupero">
      </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block"> 
          <?php //echo $_SESSION['name_user'][0]->first_name; ?><hr/>
          <strong class="morado">
          <?php //echo "PERFIL : ".$_SESSION['name_profile']; ?>
          </strong>
          </a>
        </div>
      </div>
		

      <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">


	     	<li class="nav-item has-treeview">
		            <a href="#" class="nav-link" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fa nav-icon fa-user"></i><p>Administración</p></a>
		
                <ul class="nav nav-treeview">
		            <li><a class="nav-link" href="<?php echo site_url('user/account') ?>">
                <i class="fa nav-icon fa-user"></i><p>Mi cuenta</p></a>
		            </li>
		
		            <li><a class="nav-link" href="<?php echo site_url('user/password') ?>">
                <i class="fa nav-icon fa-key"></i><p>Cambiar contraseña</p></a>
                </li>
		
                <li><a class="nav-link" href="<?php echo site_url('auth/logout') ?>">
                <i class="fa nav-icon fa-close"></i><p>Cerrar Sesión</p></a>
                </li>
		            </ul>
	      </li>
					
	    	<li class="nav-item has-treeview">
		          <a class="nav-link" href="#" class="nav-link" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fas nav-icon fa-money-check-alt" style="color:#1E3652" ></i><p>Operaciones</p></a>
		

 		          <ul class="nav nav-treeview">
		            <li><a class="nav-link" href="<?php echo site_url('tracking/tickets'); ?>">
                <i class="fas nav-icon fa-hand-holding-usd" style="color:#1E3652" ></i><p>Reales</p></a>
                </li>

	             <!--	<li><a class="nav-link" href="<?php echo site_url('tracking/simulado'); ?>">
                <i class="fas fa-comment-dollar" style="color:Purple" ></i>
                <p>Simuladas</p></a></li> -->
	
         	    </ul>
		    </li>
	
	    	<li class="nav-item has-treeview">
		        <a class="nav-link" href="#" class="nav-link" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa nav-icon fa-file-text-o" style="color:#1E3652"></i><p>Mantenedores</p></a>

		        <ul class="nav nav-treeview">
						
		          <!--    <li class="nav-item">
			             <a class="nav-link" href="#"><i class="fa nav-icon fa-archive" style="color:#1E3652"></i><p>Usuarios</p></a>
			             <ul class="nav nav-treeview">
			
                         <li><a class="nav-link" href="<?php echo site_url('user/index'); ?>">
                         <i class="fa nav-icon fa-file-text-o"></i>
                         <p> Buscar</p></a></li>
			
                        <li><a class="nav-link" href="<?php echo site_url('user/create'); ?>">
                         <i class="fa nav-icon fa-file-text-o"></i> 
                         <p>Crear</p></a></li>
		
                      	<li><a class="nav-link" href="<?php echo site_url('perfiles/index'); ?>">
                         <i class="fa nav-icon fa-file-text-o"></i>
			                   <p>Perfiles</p></a></li> 
	                 </ul>
	              </li> --> <!-- Usuarios--> 
						
		            <li class="nav-item">
			              <a class="nav-link" href="#"><i class="fa nav-icon fa-archive" style="color:#1E3652"></i><p>Clientes</p></a>
		                <ul class="nav nav-treeview">
	                 
                        <li><a class="nav-link" href="<?php echo site_url('tecnicos/index'); ?>">
                        <i class="fa nav-icon fa-file-text-o" style="color:#1E3652"></i>
                        <p>Buscar</p></a></li>
			
                        
                        <li><a class="nav-link" href="<?php echo site_url('tecnicos/create'); ?>">
                        <i class="fa nav-icon fa-file-text-o" style="color:#1E3652"></i>
                        <p>Crear</p></a></li>
		                </ul>
	          	  </li><!--Clientes-->

                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa nav-icon fa-archive" style="color:#1E3652"></i><p>Factoring</p></a>
                    <ul class="nav nav-treeview">
                   
                        <li><a class="nav-link" href="<?php echo site_url('zona/index'); ?>">
                        <i class="fa nav-icon fa-file-text-o" style="color:#1E3652"></i>
                        <p>Buscar</p></a></li>
      
                        
                        <li><a class="nav-link" href="<?php echo site_url('zona/create'); ?>">
                        <i class="fa nav-icon fa-file-text-o" style="color:#1E3652"></i>
                        <p>Crear</p></a></li>
                    </ul>
                </li><!--Factoring-->  

                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa nav-icon fa-archive" style="color:#1E3652"></i><p>Documentos</p></a>
                    <ul class="nav nav-treeview">
                   
                        <li><a class="nav-link" href="<?php echo site_url('group/index'); ?>">
                        <i class="fa nav-icon fa-file-text-o" style="color:#1E3652"></i>
                        <p>Buscar</p></a></li>
      
                        
                        <li><a class="nav-link" href="<?php echo site_url('group/create'); ?>">
                        <i class="fa nav-icon fa-file-text-o" style="color:#1E3652"></i>
                        <p>Crear</p></a></li>
                    </ul>
                </li><!--Documentos-->               


            </ul>
        </li>

    </ul>
  </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

<script>
	
	
	$(document).ready(function () {
		$('.dropdown-submenu a.test').on("click", function (e) {
			
			
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
			// console.log('****************************');
		});
	});
</script>
