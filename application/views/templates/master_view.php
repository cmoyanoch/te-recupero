<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>Te Recupero</title>
	<?php echo link_tag('assets/css/bootstrap.css'); ?>
	<?php echo link_tag('assets/css/general.css'); ?>
	<?php echo link_tag('assets/css/jquery.ui.css'); ?>
	<?php echo link_tag('assets/css/sweetalert.css'); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-multiselect.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/multi-select.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/select2.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/adminlte.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome-ie7.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="icon"  href=<?php echo base_url(); ?>"assets/images/logo_final.png" type="image/png" />

	<script>
		var base_url = "<?php echo base_url(); ?>";
		var base_url_gif = "<?php echo LOAD_GIF; ?>";
		var name_pag = "Te Recupero";
	</script>
	
	<!--script src="<?php //echo site_url('assets/js/jquery-3.1.1.min.js') ?>"></script-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/dist/sweetalert.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-multiselect.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.4.custom.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.multi-select.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.quicksearch.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/select2.js"></script>
		<script>
			var base_url = "<?php echo base_url(); ?>";
			var name_pag = "Te Recupero";
		</script>
		<style>
			#loader-wrapper {
				z-index: 1000 !important;
				background: rgba(89,197,199,1) !important;
			}
			body {
				padding: 0px !important;
			}
			.container
            {
            	width: auto;
            }
            .sidenav {
                height: 100vh;
                width: 15%;
                position: fixed;
                z-index: 1;
                top: 0;
                left: 0;
                background-color: #1E3652;
                overflow-x: hidden;
                padding-top: 20px;

            }

            .icon-content
            {
            	display: none;
            }

		</style>
</head>
	<body class="hold-transition sidebar-mini">
<div class="wrapper" >
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background-color: #1E3652;" >
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="far fa-arrow-alt-circle-left"  style="color:white;size:7px;"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
       <li class="nav-item" style="text-align: right;">
                  <img class="" style="max-width: 8%;" src="<?php echo base_url(); ?>assets/images/sdc-logo.png">
           </li>
         </ul>
       </nav> 

 </div>

       <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php echo $menu; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
	</div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-lg-12">
            <div class="card">


	<?php echo $content; ?>
	<?php echo $flashMessage; ?>
	<?php echo $modalMessage; ?>
		</div>
            <!-- /.card -->


              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      <footer class="sticky-footer bg-white">
          <div class="container my-auto">
              <div class="copyright text-center my-auto">
                  <span>Copyright © 2020</span>
              </div>
          </div>
      </footer>

        </div>
        <!-- /.container-fluid -->

	        <!-- Control Sidebar -->
	  <aside class="control-sidebar control-sidebar-dark ">
	    <!-- Control sidebar content goes here -->
	  </aside>
	  <!-- /.control-sidebar -->

	  <!-- Main Footer -->

    </div>
    <!-- /.content -->
</body>
</html>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/js/adminlte.js"></script>
