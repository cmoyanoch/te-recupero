<div class="flash-message">	
	<div class="content">
		<div class="close-icon">
			<?php 
				if (isset($flashMessage)) {
					if ($flashMessage == -1) { ?>
						<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-warning' ?>"></use></svg>
					<?php }else{ ?>
						<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-close' ?>"></use></svg>
					<?php } 
				}
			?>			
		</div>
		<div class="icon-content">
			<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-check' ?>"></use></svg>
		</div>
		<div class="data-content">
			<?php 
				$flashMessageTitle = "";
				$flashMessageText = "";
				if(isset($flashMessage)){
					switch ($flashMessage) {
						case 0:
							$flashMessageTitle = "Eliminar información";
							$flashMessageText = "La información se ha eliminado exitosamente.";
							break;
						case 1:
							$flashMessageTitle = "Guardar información";
							$flashMessageText = "La información se ha guardado exitosamente.";
							break;
						case 2:
							$flashMessageTitle = "Modificar información";
							$flashMessageText = "La información se ha modificado exitosamente.";
							break;	
						case 3:
							$flashMessageTitle = "Activar formulario";
							$flashMessageText = "La información se ha modificado exitosamente.";
							break;
						case 4:
							$flashMessageTitle = "Desactivar formulario";
							$flashMessageText = "La información se ha modificado exitosamente.";
							break;
						case 5:
							$flashMessageTitle = "Activar usuario";
							$flashMessageText = "La información se ha modificado exitosamente.";
							break;
						case 6:
							$flashMessageTitle = "Desactivar usuario";
							$flashMessageText = "La información se ha modificado exitosamente.";
							break;
						case 7:
							$flashMessageTitle = "Acceso Denegado";
							$flashMessageText = "No tienes los permisos para acceder.";
							break;
						case -1:
							$flashMessageTitle = "Error Inesperado";
							$flashMessageText = "Ha ocurrido un error inesperado, intente nuevamente.";
							break;
						default:
							$flashMessageTitle = "";
							$flashMessageText = "";
							break;
					}	
				}				
			?>
			<p class="title"><?php echo $flashMessageTitle ?></p>
			<p class="text"><?php echo $flashMessageText; ?></p>
		</div>
	</div>	
</div>
<script type="text/javascript">
	$(function(){
		$option = <?php echo isset($flashMessage) ? 'true' : 'false' ?>;
		if($option){
			toggleMessage();
		}
		/* Funcionalidad del botón cerrar  */
		$('.flash-message .content .close-icon').on('click', function(){			
			$('.flash-message').hide();
		});

		/* Oculta o muestra flash message */
		function toggleMessage(){
			if($(".flash-message").css('display') == 'none'){
				$(".flash-message").fadeIn("fast", function(){
					$(".flash-message").delay(3000).fadeOut("Fast",function(){});
				});
			}else{
				$(".flash-message").fadeOut("fast", function(){});
			}
		}

	});
</script>
