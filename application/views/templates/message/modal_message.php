<div class="modal fade" id="normal-modal" tabindex="-1" role="dialog" aria-labelledby="normal-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default blue" id="accept-button-modal">Aceptar</button>
                <button type="button" class="btn btn-default blue" data-dismiss="modal">Cancelar</button>                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){        
        $('#normal-modal').on('show.bs.modal', function (event) {                       
            var link = $(event.relatedTarget);
            var toDo = link.data('function');
            var title = link.data('title');            
            var message = link.data('message');
            var param = link.data('param');
            $('#normal-modal .modal-body').html(message);
            $('#normal-modal .modal-title').html(title);
            $('#accept-button-modal').addClass(toDo);
            if(param != null){                
                $('#accept-button-modal').data("param", param.toString());    
            }           

        })
    });
</script>