<script src="<?php echo base_url();?>assets/js/foros/actualizar.js"></script>
<div class="modal fade" id="modalEditForo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header alert alert-success" align="center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Editar Foros</h4>
      </div>
      <div class="modal-body">
       <div id="frmEditar">
    <?php
      $datos = $data['foro'];    
      // echo $datos[0]['id'];
      // echo $datos[0]['Nombre'];
    ?>            
    
    <div class="form-group" align="left">
        <input type="hidden" class="form-control" id="id2" value="<?php echo $datos[0]['id'] ?>"> 
        <label for="inputObs">Autor:</label>
        <input type="" class="form-control" id="autor" value="<?php echo $datos[0]['Nombre'] ?>" readonly> 
        <label for="inputObs">Título:</label>
        <input type="" class="form-control" id="titulo" maxlength="70" onkeypress="return val4(event)" value="<?php echo $datos[0]['title'] ?>"> 
        <div><br></div>
        <label for="inputObs">Descripción:</label>
        <textarea name="Observacion" id="campoObs" rows="10" cols="78" maxlength="200" onkeypress="return val4(event)" style="width: 100%;"> <?php echo $datos[0]['description'] ?> </textarea>
    </div>
    
    

        </div class="form-group" align="left">
        </div>
        
        <div class="modal-footer" align="">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" align="left">
                <button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 180px;">Cancelar</button>   
            </div>
            <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-12" align="right">
                <button type="button" class="btn btn-success" id="btnAct" style="width: 180px;">Actualizar</button>
            </div>
        </div>
        
    </div>
  </div>
</div>

<SCRIPT>
    
    //SOLO ACEPTA NUMEROS Y LETRAS
    function val4(e) {

        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla==8) return true;
        patron =/[A-Za-z\sáéíóú0123456789]/;
        te = String.fromCharCode(tecla);
        return patron.test(te);

    }
    
    
</script>
