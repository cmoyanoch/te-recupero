<script src="<?php echo base_url(); ?>assets/js/dataTables.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.css">
<script src="<?php echo base_url(); ?>assets/js/foros/paginacion.js"></script>
<script src="<?php echo base_url(); ?>assets/js/foros/acciones.js"></script>
<script src="<?php echo base_url(); ?>assets/js/foros/fechas.js"></script>
<script src="<?php echo base_url(); ?>assets/js/foros/buscarPorTitulo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/foros/crearF.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$('#TablaForos').DataTable(
			{
				"bPaginate": false,
				"bLengthChange": false,
				"bFilter": false,
				"bSort": false,
				"bInfo": false,
				"bAutoWidth": false
			}
		);
	});
</script>
<style type="text/css">
	#TablaForos {
		margin: auto;
	}
	
	.div1 {
		margin: auto;
	}
</style>


<?php
	
	$Creg = $data['CReg'];             //CANTIDAD TOTAL DE REGISTROS QUE CONTENGA LA CONSULTA. ARRAY
	$registrosTot = $Creg['cantReg'];  //CANTIDAD TOTAL DE REGISTROS QUE CONTENGA LA CONSULTA NUMERO
	$contreg = $data['contadorPag'];   //TRAE COMO ARRAY EL NUMERO DE LA PAGINA EN EL QUE VAMOS
	$contadorPag = $contreg;           //RECIBE COMO NUMERO LA PAGINA EN EL QUE VAMOS
	$pag = $registrosTot / PAGINADO_CANT;
	$pag = floor($pag);
	$resto = $registrosTot % PAGINADO_CANT;
	if ($resto > 0 || $pag == 0){
		$pag = $pag + 1;
	}
	
	$palabra = ' Página de ';
	
	$fecha = date('d/m/Y');

?>


<div class="list">
	<ol class="breadcrumb" style="margin-top: 0px;">
		<li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a
				href="#"><strong style="color:black;"> Volver</strong></a></li>
		<li><a href="#"><strong>Administración</strong></a></li>
		<li class="active"><strong> Foros </strong></li>
	</ol>
</div>

<div id="divBloqueo"
     style="  position: absolute; width: 98%; height: 100%;  background-color: rgba(255, 255, 255, 0.22); z-index: 1; text-align: center; display: none;"></div>


<div id="tabForos">
	
	<input type="hidden" class="form-control" id="SWfiltro" value="0">
		<div class="col-sm-12 col-md-12 col-xs-12 ">
		
		
		<div class="col-sm-12 col-md-8 col-xs-12" align="left"><br></div>
		<div class="col-sm-12 col-md-1 col-xs-12" align="left">
			<label style="color: white;">Desde:</label>
			<input type="text" value="<? echo $fecha; ?>" id="datepickerInicio" style="width:95px;height:37px" size="30" readonly>
		</div>
		<div class="col-sm-12 col-md-1 col-xs-12" align="left">
			<label style="color: white;">Hasta:</label>
			<input type="text" value="<? echo $fecha; ?>" id="datepickerFinal" style="width:95px;height:37px" size="30" readonly>
		</div>
		<div class="col-sm-12 col-md-2 col-xs-12" align="left" style="margin-top: 21px;">
			<i type="button" id="LimpiarFechas" onclick="limpiar()" class="btn btn-info " aria-hidden="true" style="height: 41px;"><strong>Limpiar</strong></i>
			<i type="button" id="FiltrarLF" class="btn btn-success fa fa-search fa-2x" aria-hidden="true"></i>
			<i type="button" id="reCargar" class="btn btn-success fa fa-refresh fa-2x" aria-hidden="true"></i>
		</div>
		<div class="col-sm-12 col-md-12 col-xs-12"><br>
			<hr style="width:100%;">
		</div>
		
		
		<div class="col-sm-12 col-md-6 col-xs-12 btn-group" style="width: 912px;"></div>
		
		<div class="col-sm-12 col-md-3 col-xs-12 btn-group" align="left" style="width: 206px;">
			<input type="" placeholder="TÍTULO" style="width: 200px;" class="form-control" id="buscarEnTabla"
			       maxlength="35" onkeypress="return val4(event)">
		</div>
		<div class="col-sm-12 col-md-1 col-xs-1 btn-group" align="left" style="width: 75px;">
			<button type="button" id="btnBuscarTitulo" class="btn btn-success">Buscar</button>
			<!--      <i type="button" id="btnBuscar" class="btn btn-success fa fa-search fa-2x" aria-hidden="true" style="margin-top: -7px;"></i>-->
		</div>
		
		<div class="col-sm-12 col-md-1 col-xs-12 " align="left">
			<a class="btn btn-primary" id="AgregaForo" style=" border-color: white;">Agregar Foro</a>
		</div>
		
		
		<input type="hidden" class="form-control" id="swBuscar" value="0">
		
		
		<table id="TablaForos" class="table table-striped tablaA">
			<br><br>
			<thead>
			<tr class="listrabajos">
				<th>AUTOR</th>
				<th>TÍTULO</th>
				<th>DESCRIPCIÓN</th>
				<th>CREACIÓN</th>
				<th>RESPUESTAS</th>
				<th>EDITAR</th>
				<th>ELIMINAR</th>
			</tr>
			
			</thead>
			
			<tbody id="CargarForos">
			
			<?php
				foreach ($data['datos'] as $val) {
					?>
					
					<tr class="trDetalleForos" id="<?= $val['id']; ?>">
						<td><?= strtoupper($val['Nombre']); ?></td>
						<td><?= strtoupper($val['title']); ?></td>
						<? if (strlen($val['description']) > 60){ ?>
							<td id="<?= $val['id']; ?>" style="cursor: pointer;" title="Ver respuestas..."
							    class="tdForos"><?= strtoupper(substr($val['description'], 0, 60) . '.....'); ?></td>
						<? } else { ?>
							<td id="<?= $val['id']; ?>" style="cursor: pointer;" title="Ver respuestas..."
							    class="tdForos"><?= strtoupper(substr($val['description'], 0, 60)); ?></td>
						<? } ?>
						<td><?= $val['fecha']; ?></td>
						<td><?= $val['countAnswer']; ?></td>
						<td>
							<button type="button" id="<?= $val['id']; ?>" class="btn btn-info btnEditar">Editar</button>
						</td>
						<td>
							<button type="button" id="<?= $val['id']; ?>" class="btn btn-danger btnEliminar">Eliminar</button>
						</td>
					
					</tr>
					<?php
				}
			?>
		
		</table>
	
	
	</div>
	
	<div class="col-sm-12 col-md-12 col-xs-12" style="text-align: right; ">
		<? if ($contadorPag > 1){ ?>
			<i type="button" id="atras" class="btn btn-success fa fa-arrow-left fa-2x" aria-hidden="true"></i>
		<? } ?>
		
		<span class="label" id="cp" style="font-size: 12px;color: black;"><?php echo $contadorPag ?></span>
		<span class="label" id="pal" style="font-size: 12px;color: black;"><?php echo $palabra ?></span>
		<span class="label" id="pg" style="font-size: 12px;color: black;"><?php echo $pag ?></span>
		
		<? if ($contadorPag < $pag){ ?>
			<i type="button" id="adelante" class="btn btn-success fa fa-arrow-right fa-2x" aria-hidden="true"></i>
		<? } ?>
	</div>

</div>

<div id="ModuloForos"></div>


<SCRIPT>
	
	function limpiar() {
		document.getElementById("datepickerInicio").value = "";
		document.getElementById("datepickerFinal").value = "";
	}
	
	//SOLO ACEPTA NUMEROS Y LETRAS
	function val4(e) {
		
		tecla = (document.all) ? e.keyCode : e.which;
		if (tecla == 8) return true;
		patron = /[A-Za-z\sáéíóú0123456789]/;
		te = String.fromCharCode(tecla);
		return patron.test(te);
		
	}


</script>
