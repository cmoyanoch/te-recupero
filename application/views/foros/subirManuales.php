<script src="<?php echo base_url();?>assets/js/manuales/paginacion.js"></script>
<script src="<?php echo base_url();?>assets/js/manuales/manuales.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/dataTables.css">
<script src="<?php echo base_url();?>assets/js/dataTables.js"></script>


<?php

      $Creg = $data['CReg'];             //CANTIDAD TOTAL DE REGISTROS QUE CONTENGA LA CONSULTA. ARRAY
      $registrosTot = $Creg['cantReg'];  //CANTIDAD TOTAL DE REGISTROS QUE CONTENGA LA CONSULTA NUMERO
      $contreg = $data['contadorPag'];   //TRAE COMO ARRAY EL NUMERO DE LA PAGINA EN EL QUE VAMOS
      $contadorPag = $contreg;           //RECIBE COMO NUMERO LA PAGINA EN EL QUE VAMOS
      $pag = $registrosTot/10;
      $pag = floor($pag);
      $resto = $registrosTot%10;
      if ($resto>0 || $pag==0) {
        $pag = $pag+1;
      }
      
      $palabra = ' Página de ';

      // $carga = $data['carga'];
      $descarga = $data['descarga'] ;

      // print_r($descarga);

      ?>

      <script type="text/javascript">
      $(document).ready(function() {
        $('#TablaManuales').DataTable(
        {
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": false,
          "bInfo": false,
          "bAutoWidth": false }
          );
      } );

      </script>

      <style type="text/css">
        #TablaManuales{
          margin: auto;
        }
        .div1{
          margin: auto;
        }
      </style>


      <div id="container">
 

        <div class="col-sm-12 col-md-12 col-xs-12" style="font-family: calibri;font-size: 26px !important; margin-top: 23px;" >
          <label style="color: white;font-family: calibri;font-size: 30px;"> Manuales</label>
        </div> 

        <div id="divBloq" style="  position: absolute; width: 98%; height: 100%;  background-color: rgba(255, 255, 255, 0.22);  z-index: 1;
        text-align: center; display: none;"></div>

        <div id="tabManuales2" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="right">
          <form enctype="multipart/form-data" method="post" name="form1" id="form1"> 

            <div id="tabCarga" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center" hidden>
              <table  style="width: auto;">
                <tr>
                  <td><strong style="font-size: 18px;">Formatos permitidos: pdf, csv, doc y docx.</strong></td>
                  <td><strong style="font-size: 18px;">Máximo: 45MB.</strong></td>
                  <td><button type="button" id="ocultar" title="Ocultar" class="btn btn-danger fa fa-minus-square fa-2x"></button></td>
                </tr>
                <tr>
                  <td>
                    <input type='hidden' name='MAX_FILE_SIZE' value='50000000' />
                    <input type="file" class="form-control" name="userfile" id="userfile"  autofocus required/>
                  </td>
                  <td>
                    <div class="col-sm-8 col-md-8 col-xs-8 col-lg-9">
                      <button type='submit' class="btn btn-primary" value='Subir archivo' id="cargaDoc" ><strong> Cargar</strong></button>
                    </div>
                  </td>
                  <td>
                  </td>       
                </tr>
                <td>
                  <input type="text" class="form-control" name="fileName" id="fileName" maxlength="70" required/>
                </td>
                <td><strong style="font-size: 18px;">Descripción del Archivo </strong></td>
                <td></td>
              </table> 
                <div class="progress" id="barra" align="center" style="width: 40%;height: 5%;border-color: white;border-style: groove;" hidden>
                  <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" 
                  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    <span class="sr-only">100% Complete</span>
                  </div>
                </div>
            </div>

            <div id="BtnAM" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="right">
              <button type="button" id="agregarManual" title="Agregar Manual" class="btn btn-success fa fa-plus-square fa-2x"></button>      
            </div>

          </form>

          <!--div class="col-sm-12 col-md-12 col-xs-12"><hr style="width:100%;"></div-->
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="right">
          <?php echo $this->load->view('foros/search'); ?>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" ><br></div> 


        <div id="tabManuales" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  

          <table id="TablaManuales" class="table table-striped tablaA" style="width: 100%;">

            <thead>
              <tr class="listCap">
                <th>NOMBRE DE ARCHIVO</th>
                <th>DESCRIPCIÓN</th>
                <th>FECHA DE CARGA</th>
                <th>QUITAR</th>
                <th>DESCARGAR</th>
              </tr>
            </thead>
  
            <tbody id="CargarManuales">

              <?php
              foreach ($data['listado'] as $val) {

                $fechaIni = explode(" ", $val['DateTime']);
                $fechaDiv = explode("-", $fechaIni[0]);
                $fechafin = $fechaDiv[2].'-'.$fechaDiv[1].'-'.$fechaDiv[0].' '.$fechaIni[1];

                ?>                    
                <tr class="trManuales" id="<?=$val['id'];?>" style="cursor: pointer;">
                  <td><?=strtoupper($val['NombreArchivo']);?></td>
                  <td><input type="" style="border-style: hidden;color: #3385CF; width: 600px;" maxlength="70" class="form-control descManual" id="<? echo $val['id'] ?>" value="<? echo strtoupper($val['Nombre']); ?>"> </td>

                  <td><?=$fechafin;?></td>
                  <td align="center">
                    <a id="<?=$val['id'];?>" class="btn btn-danger fa fa-times fa-2x quitarDoc" aria-hidden="true">  </a>
                  </td>       
                  <td align="center">
                    <a href="<?=DESC_MAN.$val['NombreArchivo'];?>" download="" class="btn btn-success fa fa-cloud-download fa-2x DLWD" valor="<? echo $val['NombreArchivo'] ?>" >  </a>
                  </td>                   
                </tr>
              <?php
              }
              ?>

            </table>

            <div class="col-sm-12 col-md-12 col-xs-12" style="text-align: right;">
              <? if($contadorPag > 1){ ?>
              <i type="button" id="atras" class="btn btn-success fa fa-arrow-left fa-2x" aria-hidden="true"></i>
              <? } ?>

              <span class="label" id="cp" style="font-size: 12px;color: white;" ><?php echo $contadorPag ?></span>
              <span class="label" id="pal" style="font-size: 12px;color: white;" ><?php echo $palabra ?></span>
              <span class="label" id="pg" style="font-size: 12px;color: white;" ><?php echo $pag ?></span>   

              <? if($contadorPag < $pag){ ?>
              <i type="button" id="adelante" class="btn btn-success fa fa-arrow-right fa-2x" aria-hidden="true" ></i>
              <? } ?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br><br></div>

          </div>

          <!-- /////////////////////////////////////////////////////////////////////////////////////////// -->

        </div>

        <div id="ModuloForos"></div>



<!--div class="">
<iframe  src="https://www.youtube.com/embed/sOnqjkJTMaA" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div-->


<script type="text/javascript"> 

$(document).ready(function(){

  $("#agregarManual").on( "click", function() {

    $("#agregarManual").hide("slow");  
    $("#tabCarga").show("slow"); 
    return;


    var file = $('#userfile').val(); 
    if(file=='') 
    { 
      return false;
    } else{
      $("#BtnAM").show("slow");  
    }

  });


  $("#ocultar").on( "click", function() {

    $("#agregarManual").show("slow");  
    $("#tabCarga").hide("slow"); 
    return;

  });


});

</script>