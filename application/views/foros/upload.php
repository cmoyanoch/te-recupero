<?php
	// Vamos a verificar si hay errores. 
	if($_FILES['userfile']['error'] > 0){
		echo 'Hemos encontrado un problema: ';
		switch($_FILES['userfile']['error'])
		{	// Errores de PHP
			// Archivo excedio la propiedad de PHP upload_max_filesize
			case 1: echo 'El archivo es muy grande'; break;  
			// Su tamaño es muy grande (El tamaño dado en el campo invisible)
			case 2: echo 'No se puede aceptar el archivo muy grande'; break; 
			// El archivo se proceso parcialmente
			case 3: echo 'El archivo no fue procesado por completo.'; break;
			// Archivo no se pudo procesarse
			case 4: echo 'No se pudo procesar el archivo.'; break;	   
		}
		exit;
	}
	//  Verificamos si es un archivo de texto 
	if($_FILES['userfile']['type'] != 'text/plain'){
		echo 'Solo se pueden aceptar archivos de texto.';
		exit;
	}
	// Se puede procesar el archivo
	// Poniendolo en una carpeta llamada 'upload'
	$up_folder = '/var/www/html/'.$_FILES['userfile']['name'];
	if(is_uploaded_file($_FILES['userfile']['tmp_name'])){
		if(!move_uploaded_file($_FILES['userfile']['tmp_name'], $up_folder)){
			// No se puede mover el archivo de carpeta
			echo 'Archivo no se puede mover de lugar';
			exit;
		}
	}
	else{
		// Si se llega a este punto, cabe la 
		// posibilidad de que sea un archivo malicioso
		echo 'Problema - aparente ataque en contra del servidor: ';
		echo $_FILES['userfile']['name'];
		exit;
	}
	// Si todas las pruebas anteriores pasan, 
	// desplegamos un mensaje de exito.
	echo 'Archivo procesado correctamente.<br />';
	
	// Vamos a quitar etiquetas(tags) in-necesarias
	$fp = fopen($up_folder, 'r');  // Nota:  'r' es para lectura
	$contents = fread($fp, filesize($up_folder));
	fclose($fp);
	
	$contents = strip_tags($contents); 
	$fp = fopen($upfile, 'w');  // Nota: 'w' es para escribir
	fwrite($fp, $contents);
	fclose($fp);
	
	// Desplegamos el contenido del archivo 
	//  en el navegador.
	echo 'Resumen del archivo procesado: <br /> <br />';
	echo $contents;
	echo '<br /><br />';
 
?>