<script src="<?php echo base_url();?>assets/js/foros/guardarF.js"></script>
<div class="modal fade" id="ModuloCrearForos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-info" align="center">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="exampleModalLabel">Crear Foros</h4>
            </div>
            
            <div class="modal-body">
                <div id="frmCrear">

                <div class="form-group" align="left">
                    <label for="inputTit">Título:</label>
                    <textarea id="titulo" rows="2" cols="75" placeholder="Título" style="width: 100%;height: 35px;" maxlength="70" onkeypress="return val4(event)"> </textarea>
                    <div><br></div>
                    <label for="inputDes">Descripción:</label>
                    <textarea id="descripcion" rows="10" cols="75" placeholder="Descripción" style="width: 100%;" maxlength="200" onkeypress="return val4(event)"> </textarea>
                    <div><br></div>
                </div>

                </div class="form-group" align="left">
                </div>
            <div class="modal-footer" >
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" align="left">
                    <button type="button" class="btn btn-danger" id="btnSalir" data-dismiss="modal" style="width: 180px;">Salir</button>   
                </div>
                <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-12" align="right">
                    <button type="button" class="btn btn-success" id="btnCrear" style="width: 180px;">Crear</button>
                </div>
            </div>
        </div>
    </div>
</div>

<SCRIPT>
    
    //SOLO ACEPTA NUMEROS Y LETRAS
    function val4(e) {

        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla==8) return true;
        patron =/[A-Za-z\sáéíóú0123456789]/;
        te = String.fromCharCode(tecla);
        return patron.test(te);

    }
    
    
</script>
