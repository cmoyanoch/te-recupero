<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<div class="list">
    
    <ol class="breadcrumb" style="margin-top: 4px;">
        <li title="Volver a la página anterior" onclick="history.back()"><i class="fa fa-arrow-left"></i><a href="#"><strong style="color:black;"> Volver</strong></a></li>
        <li><a href="#"><strong>Mantenedores</strong></a></li>
        <li><a href="#"><strong>Prioridades</strong></a></li>
    </ol> 
	<div class="container ">
		<div class="row">
			<div class="col-md-12">				
				<div class="table-responsive">
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
								<td>ID</td>
								<td>Nombre</td>
								<td></td>
							</tr>				
						</thead>
						<tbody>
												
						</tbody>
					</table>
				</div>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-12">
				<button class="btn btn-default" id="newPriority"><i class="fa fa-plus"></i> Nuevo</button>
			</div>
			<div class="col-md-12">
				<p id="message"></p>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModalPriority" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form id="formPriority" autocomplete="off">
					<div class="col-md-6 form-group" id="cajaIdPriority">
						<label for="idPriority">Id</label>
						<input type="text" class="form-control" id="idPriority" name="id" placeholder="Id">
					</div>
					<div class="col-md-6 form-group">
						<label for="namePriority">Nombre</label>
						<input type="text" class="form-control" id="namePriority" name="name" placeholder="123A">
					</div>
				</form>
				
			</div>
			<div class="modal-footer" style="border-top: 0 !important;">
				<button type="button" id="btnAceptar" class="btn btn-default" data-dismiss="modal">Aceptar</button>
			</div>
		</div>
	
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		
		var init = function () {
			$.ajax({
				type: "POST",
				url : "<?php echo site_url('Priority/getDataInit'); ?>",
				dataType: "text",
				success: function (result) {
					result = JSON.parse(result);
					for (var i = 0; i < result.length; i++) {
						$("#list-table tbody").append(
							$("<tr><td>" + result[i].id + "</td><td>" + result[i].NAME + "</td><td style='width: 10%;'><i data-id='" + result[i].id + "' data-name='" + result[i].NAME + "' style='cursor: pointer;' class='fa fa-edit'></i> / <i data-id='" + result[i].id + "' data-name='" + result[i].NAME + "' style='cursor: pointer;' class='fa fa-trash'></i></td></tr>")
						);
					}
					$('#list-table').DataTable({
						"lengthChange": false,
		                "ordering": false,
		                "retrieve": true,
		                "searching": true,
		                "paging": true,
		                "info": true,
		                "language": {
					    	"info": "Mostrando página _PAGE_ de _PAGES_",
					    	"search" : "Buscar",
					    	"infoFiltered": " - (Filtrado de _MAX_ registros)",
					    	"paginate": {
								"next": "Siguiente",
								"previous" : "Anterior"
							}
					  	}
					});

					$(document).on('click', '#list-table .fa-edit',function (e) {
						$("#myModalPriority .modal-title").text("Modificar Prioridad");
						$("#idPriority").val($(this).data('id'));
						$("#namePriority").val($(this).data('name'));

						$("#myModalPriority").modal();
						$("#btnAceptar").on('click', function () {
							var data = {
								'idPriority' : $("#idPriority").val(),
								'namePriority' : $("#namePriority").val()
							};
							updateData(data);	
							setTimeout(function () {
								location.reload();
							}, 1000);						
						})
					});

					$(document).on('click', '#list-table .fa-trash',function (e) {
						var data = {
							'idPriority' : $(this).data('id')
						}
						console.log(data);
						deleteData(data);

						setTimeout(function () {
							location.reload();
						}, 1000);
					})

				},
				error: function (xhr, ajaxOptions, thrownError) {
				
				}				
			});	

			$("#newPriority").on('click', function () {
				$("#idPriority").val("");	
				$("#cajaIdPriority").css('display', 'none');
				$("#namePriority").val("");	

				

				$("#myModalPriority").modal();
				$("#namePriority").on('keypress', function (e) {
					var key = e.keyCode || e.which;
					switch($(this).val().length) {
						case 0:
							if ((key > 47 && key < 58) || key == 8) {
								return true;
							} else {
								return false;
							}
							break;
						case 1: case 2:
							var keyAnterior = $(this).val().charCodeAt($(this).val().length - 1);
							if (keyAnterior == 65 || keyAnterior == 66 || keyAnterior == 97 || keyAnterior == 94) {
								return false;
							}
							if ((key > 47 && key < 58) || key == 8 || key == 65 || key == 66 || key == 97 || key == 98) {
								return true;
							} else {
								return false;
							}
							break;
						case 3:
							var keyAnterior = $(this).val().charCodeAt($(this).val().length - 1);
							if (keyAnterior == 65 || keyAnterior == 66 || keyAnterior == 97 || keyAnterior == 94) {
								return false;
							}
							if (key == 65 || key == 66 || key == 97 || key == 98) {
								return true;
							} else {
								return false;
							}
							break;
						default: 
							return false;
							break;
					}
				});

				$("#btnAceptar").on('click', function () {
					var data = {
						'namePriority' : $("#namePriority").val()
					};
					createData(data);	
					setTimeout(function () {
						location.reload();
					}, 1000);						
				});
			})		
		}

		var deleteData = function (data) {
			$.ajax({
				type: 'POST',
				url: "<?php echo site_url('Priority/daletePriority') ?>",
				dataType: 'text',
				data: data,
				success: function (data) {
					data = JSON.parse(data);
					$("#message").text(data.message);
					$("#message").fadeIn();
					setTimeout(function () {
						$("#message").fadeOut();
					}, 1000);
				},
				error: function (data) {
					console.log(data);
				}
			})
		}

		var updateData = function (data) {
			$.ajax({
				type: 'POST',
				url: "<?php echo site_url('Priority/updatePriority'); ?>",
				dataType: "text",
				data: data,
				success : function (data) {
					data = JSON.parse(data);
					$("#message").text(data.message);
					$("#message").fadeIn();
					setTimeout(function () {
						$("#message").fadeOut();
					}, 1000);
				},
				error : function (data) {
					console.log(data);
				}
			});
		}

		var createData = function (data) {
			$.ajax({
				type: 'POST',
				url: "<?php echo site_url('Priority/createPriority'); ?>",
				dataType: "text",
				data: data,
				success : function (data) {
					data = JSON.parse(data);
					$("#message").text(data.message);
					$("#message").fadeIn();
					setTimeout(function () {
						$("#message").fadeOut();
					}, 1000);
				},
				error : function (data) {
					console.log(data);
				}
			});
		}

		init();
	});
</script>