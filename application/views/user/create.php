<?php
	$hasData = false;
	if(isset($user)){
		$hasData = true;
	}
?>
<div class="list">
  <ol class="breadcrumb" style="background-color: #1E3652 !important;color:#fff">
    	<div class="card-header" >Crear Usuario</div>
    </ol>            
</div>
<div class="container">	
	<?php 
	if(isset($edit)){  	
		echo form_open('user/edit/'.$user['user_id'], array('id'=>'edit-form'));
		echo form_hidden('user[user_id]', $user['user_id']);
	}else{ 
		echo form_open('user/create'); 	
	}	
	?>
	<div class="row">
		<div class="col-md-6">			
			<div class="form-group">
				<label for="user[first_name]">Nombre:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'user[first_name]', 'class'=> 'form-control', 'value'=> set_value('user[first_name]', isset($user['first_name'])? $user['first_name'] :''))); ?>
				<?php echo form_error('user[first_name]'); ?>
			</div>
		</div>
		<div class="col-md-6">			
			<div class="form-group">
				<label for="user[last_name]">Apellidos:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'user[last_name]', 'class'=> 'form-control', 'value'=> set_value('user[last_name]', isset($user['last_name'])? $user['last_name'] :''))); ?>
				<?php echo form_error('user[last_name]'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">			
			<div class="form-group">
				<label for="user[email]">Correo electrónico:</label>
				<?php echo form_input(array('type'=>'text', 'name'=>'user[email]', 'class'=> 'form-control', 'value'=> set_value('user[email]', isset($user['email'])? $user['email'] :''))); ?>
				<?php echo form_error('user[email]'); ?>
			</div>
		</div>
		<div class="col-md-4">			
			<div class="form-group">
				<label for="user[password]">Contraseña:</label>
				<?php echo form_input(array('type'=>'password', 'name'=>'user[password]', 'class'=> 'form-control', 'value'=> set_value('user[password]', isset($user['password'])? $user['password'] :''))); ?>
				<?php echo form_error('user[password]'); ?>
			</div>
		</div>
		<div class="col-md-4">			
			<div class="form-group">
				<label for="user[password_confirm]">Repita contraseña:</label>
				<?php echo form_input(array('type'=>'password', 'name'=>'user[password_confirm]', 'class'=> 'form-control', 'value'=> set_value('user[password_confirm]', isset($user['password_confirm'])? $user['password_confirm'] :''))); ?>
				<?php echo form_error('user[password_confirm]'); ?>
			</div>
		</div>
        <div class="col-md-4">
			<div class="form-group">
				<label for="user[id_perfil]">Perfil: </label>
				<?php echo form_dropdown('user[id_perfil]', $perfil, set_value('user[id_perfil]', isset($user['id_perfil']) ? $user['id_perfil'] : ''), array('class'=>'form-control', 'id' => 'id_perfil')); ?>
				<?php echo form_error('user[id_perfil]'); ?>
			</div>
		</div>





	</div>


	<div class="row" style="display: none;">
		<div class="col-md-3">
			<h4>Permisos del usuario</h4>			
			<div class="checkbox">
				<label>				
					<?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'1', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','1', isset($user['authorizations']) ? (array_search('1', $user['authorizations'])!==false ? true:false) :false ))); ?>
					Crear formularios
				</label>
			</div>
			<div class="checkbox">
				<label>
					<?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'2', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','2', isset($user['authorizations']) ? (array_search('2', $user['authorizations'])!==false ? true:false) :false ))); ?>
					Editar formularios
				</label>
			</div>
			<div class="checkbox">
				<label>
					<?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'3', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','3', isset($user['authorizations']) ? (array_search('3', $user['authorizations'])!==false ? true:false) :false ))); ?>
					Activar/desactivar formularios
				</label>
			</div>
			<div class="checkbox">
				<label>
					<?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'4', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','4', isset($user['authorizations']) ? (array_search('4', $user['authorizations'])!==false ? true:false) :false ))); ?>
					Crear categorías
				</label>
			</div>
			<div class="checkbox">
				<label>
					<?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'5', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','5', isset($user['authorizations']) ? (array_search('5', $user['authorizations'])!==false ? true:false) :false ))); ?>
					Editar categorías
				</label>
			</div>
			<div class="checkbox">
				<label>
					<?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'6', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','6', isset($user['authorizations']) ? (array_search('6', $user['authorizations'])!==false ? true:false) :false ))); ?>
					Eliminar categorías
				</label>
			</div>
			<div class="checkbox">
				<label>
					<?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'7', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','7', isset($user['authorizations']) ? (array_search('7', $user['authorizations'])!==false ? true:false) :false ))); ?>
					Crear usuarios
				</label>
			</div>
			<div class="checkbox">
				<label>
					<?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'8', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','8', isset($user['authorizations']) ? (array_search('8', $user['authorizations'])!==false ? true:false) :false ))); ?>
					Modificar usuarios
				</label>
			</div>
			<div class="checkbox">
				<label>
					<?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'9', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','9', isset($user['authorizations']) ? (array_search('9', $user['authorizations'])!==false ? true:false) :false ))); ?>
					Activar/desactivar usuarios
				</label>
			</div>
			<div class="checkbox">
				<label>
					<?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'10', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','10', isset($user['authorizations']) ? (array_search('10', $user['authorizations'])!==false ? true:false) :false ))); ?>
					Eliminar usuarios
				</label>
			</div>
		</div>
        <div class="col-md-3">
            <h4> Tickets</h4>
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'11', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','11', isset($user['authorizations']) ? (array_search('11', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Crear Tickets
                </label>
            </div>
             <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'12', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','12', isset($user['authorizations']) ? (array_search('12', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Modificar Tickets
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'13', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','13', isset($user['authorizations']) ? (array_search('13', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Eliminar Tickets
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'14', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','14', isset($user['authorizations']) ? (array_search('14', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Consultar Tickets
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'15', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','15', isset($user['authorizations']) ? (array_search('15', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Asignar Tickets
                </label>
            </div>
        </div>
        <div class="col-md-3">
            <h4> Técnicos</h4>
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'16', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','16', isset($user['authorizations']) ? (array_search('16', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Crear Técnicos
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'17', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','17', isset($user['authorizations']) ? (array_search('17', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Modificar Técnicos
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'18', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','18', isset($user['authorizations']) ? (array_search('18', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Eliminar Técnicos
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'14', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','19', isset($user['authorizations']) ? (array_search('19', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Consultar Técnicos
                </label>
            </div>
        </div>
        <div class="col-md-3">
        <h4> Dashboard</h4>
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'20', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','20', isset($user['authorizations']) ? (array_search('20', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Visualizar Dashboard
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <?php echo form_checkbox(array('name'=>'user[authorizations][]', 'value'=>'21', 'disabled'=>'disabled', 'checked'=>set_checkbox('user[authorizations][]','21', isset($user['authorizations']) ? (array_search('21', $user['authorizations'])!==false ? true:false) :false ))); ?>
                    Exportar Dashboard
                            </label>                          
                    </div>               
                </div>

	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-success">Guardar</button>	
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.customers').hide();

        $('#id_grupo').multiselect({
    	    allSelectedText: 'All Selected',
	        maxHeight: 200,
	        includeSelectAllOption: true
	    });
        $('#id_cliente').multiselect({
            allSelectedText: 'All Selected',
            maxHeight: 200,
            includeSelectAllOption: true
        });
        function run() {

            var perfilId = $('#id_perfil').val();

            $('#id_perfil').change(function (e) {
                /* gerentes id    = 3
                 * Jefe servicio  = 4 */
                perfilId = $('#id_perfil').val();
                if (perfilId == 3 || perfilId == 4) {
                    $('.customers').show();
                } else {
                    $('.customers').hide();
                }
            });
            if (perfilId == 3 || perfilId == 4) {
                $('.customers').show();
            } else {
                $('.customers').hide();
            }
        }
        run();
    });


   // $('.multiselect', d.el).multiselect()

</script>