<style>
label{
	font-weight: bold !important;
    color: white;
    font-size: 15px;
}
</style>
<div class="list">
  <ol class="breadcrumb" style="background-color: #1E3652 !important;color:#fff">
    	<div class="card-header" >Mantenedor Usuario</div> 

    </ol>            
    
	<div class="container-fluid menu-content">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<?php echo form_open(site_url('user/index'), array('method'=>'get')); ?>
					<div class="input-group">
						<?php echo form_input(array('type'=>'text', 'name'=>'value', 'class'=>'form-control', 'placeholder'=>'¿Qué usuario deseas buscar?', 'value'=>$search_value, 'id'=>'search-value')) ?>					
						<span class="input-group-btn">
							<button class="btn btn-default blue" type="submit">Buscar</button>
						</span>
					</div>
					<?php echo form_close(); ?>
				</div>
				<div class="col-md-6 button-content">								
					<a href="<?php echo site_url('user/create'); ?>" class="btn btn-default blue">Crear</a>				
				</div>
			</div>			
		</div>		
	</div>
	<div class="container ">
		<div class="row">
			<div class="col-md-12">				
				<?php if (count($users) > 0){ ?>
				<div class="table-responsive">
					<?php echo form_open(site_url('user/delete'), array('id'=>'list-form')) ?>										
					<table class="table table-hover" id="list-table">
						<thead>
							<tr>
								<td class="cell-selection"><?php echo form_checkbox('select-all', '', FALSE, array('id'=>'select-all')); ?></td>								
								<td>Nombre</td>
								<td>Apellidos</td>
								<td>Email</td>
								<td>Perfl</td>
								<td>Último Ingreso</td>
								<td>Estado</td>
								<td class="with-icon"></td>
								<td class="with-icon"></td>
						<!--		<td class="with-icon"></td> -->
							</tr>				
						</thead>
						<tbody>
							<?php foreach ($users as $keyUser => $user): ?>							
								<tr>								
									<td><?php echo form_checkbox('user[user_id][]', $user['user_id'], FALSE,array('class'=> 'checkbox-element'));?></td>			
									<td><?php echo $user['first_name'] ?></td>
									<td><?php echo $user['last_name'] ?></td>
									<td><?php echo $user['email'] ?></td>
									<td><?php echo $user['nombre_perfil'] ?></td>
									<td><?php echo $user['last_login'] ?></td>
									<td><?php echo $user['active'] ?></td>
									<td>										
										<a title="Eliminar" href="" data-title="Eliminar usuario" data-message="¿Estás seguro que deseas eliminar el usuario <?php echo $user['first_name']." ".$user['last_name'] ?>?" data-function="deleteUser" data-param="<?php echo $user['user_id'] ?>" data-toggle="modal" data-target="#normal-modal">
											<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-delete' ?>"></use></svg>
										</a>											
									</td>
									<td>
										<a title="<?php echo $user['active'] == 'Activo' ? 'Desactivar': 'Activar';?>" href="" data-title="<?php echo $user['active'] == 'Activo' ? 'Desactivar usuario': 'Activar usuario';?>" data-message="¿Estás seguro que deseas <?php echo $user['active'] == 'Activo' ? 'desactivar': 'activar';?> al usuario: <?php echo $user['first_name'].' '.$user['last_name'] ?>?" data-function="changeState" data-param="<?php echo ($user['active'] == 'Activo' ? '0': '1') .','. $user['user_id'];?>" data-toggle="modal" data-target="#normal-modal">
											<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#'.($user['active'] == 'Activo' ? 'icon-disable': 'icon-check') ?>"></use></svg>
										</a>
									</td>									
				<!--					<td>									
										<a title="Editar" href="<?php echo site_url('user/edit/'.$user['user_id']) ?>">
											<svg class="icon"><use xlink:href="<?php echo site_url('assets/images/icons.svg').'#icon-edit' ?>"></use></svg>
										</a>										
									</td>	-->							
								</tr>							
							<?php endforeach ?>						
						</tbody>
					</table>
				</div>
				<?php echo form_close(); ?>				
				<?php }else{ 
					if ($search_value != "") {
						echo "<p class='message-no-data'>No existen resultados con los criterios de búsqueda ingresados.</p>";
						echo "<p class='message-no-data link'><a href='".site_url('user/index')."'>Volver al inicio</a></p>";
					}else{
						echo "<p class='message-no-data'>El sistema aun no registra información.</p>";
					}	
				} ?>				
			</div>	
		</div>		
		<div class="row">
			<div class="col-md-12 pagination-content">
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<?php echo $pagination; ?>					
					</ul>
				</nav>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		<?php if ($search_value): ?>			
			$("#search-value").focus();
			$("#search-value").val($("#search-value").val());
		<?php endif ?>

		/*Al cargar la web selecciona todos los elementos según el estado del principal*/
		if($('#select-all').prop('checked')){
			$('.checkbox-element').prop('checked', true);
			addDeleteButton();
		}else{
			$('.checkbox-element').prop('checked', false);
			removeDeleteButton();
		}
		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		if($('input.checkbox-element:checked').length > 0){
			addDeleteButton();
		}

		/* Funcionalidad del botón del modal eliminar usuario */
		$(document).on('click', '#accept-button-modal.deleteUser', function(){
			$param = ($(this).data('param'));
			$form = $('#list-form');
			$form.attr({'action':'<?php echo site_url("user/delete") ?>'})
			if($param != null){
				$('.checkbox-element').prop('checked', false);
				$form.append($('<input/>').attr({"type":"hidden", "name":"user[user_id][]", "value":$param}));		             
			}
            $form.submit();
            $('#accept-button-modal').removeClass('deleteUser');
			$('#accept-button-modal').removeData('param');
			$('#normal-modal').modal('hide');            
		})

		/* Funcionalidad del botón del modal cambiar estado del usuario */
		$(document).on('click', '#accept-button-modal.changeState', function(){
			$param = ($(this).data('param'));
			$form = $('#list-form');
			$form.attr({'action':'<?php echo site_url("user/change_state") ?>'})
			if($param != null){
				$param = ($(this).data('param')).split(",");
				if($param.length == 2){
					$form.append($('<input/>').attr({"type":"hidden", "name":"user[user_id][]", "value":$param[1]}));
					$('.checkbox-element').prop('checked', false);				
				}
				$form.append($('<input/>').attr({"type":"hidden", "name":"user[action]", "value":$param[0]}));				          
				$form.submit();
			}
           
            $('#accept-button-modal').removeClass('disableUser');
			$('#accept-button-modal').removeData('param');
			$('#normal-modal').modal('hide');            
		})

		/* Crea botón desactivar cuando algún checkbox está seleccionado */
		$('#list-form').on('change', function(){			
			if($('input.checkbox-element:checked').length > 0){
				addDeleteButton();
			}else{
				removeDeleteButton();
			}
		});

		/*Selecciona todos los elementos según el estado del principal*/
		$('#select-all').on('change', function(){
			if($(this).prop('checked')){
				$('.checkbox-element').prop('checked', true);				
			}else{
				$('.checkbox-element').prop('checked', false);
				removeDeleteButton();
			}
		});

		/*Quita los botones de opciones (Eliminar, Desactivar, Activar)*/
		function removeDeleteButton(){
			$('#delete-button').remove();				
			$('#deactivate-button').remove();
			$('#activate-button').remove();
		}

		/*Creal los botones de opciones (Eliminar, Desactivar, Activar) */
		function addDeleteButton(){
			if($('.menu-content .button-content').find('#delete-button').length == 0){
				$deleteButton = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'delete-button',"data-title":"Eliminar Usuario(s)" ,"data-message":"¿Estás seguro que deseas eliminar el(los) usuario(s) seleccionado(s)?", "data-function":"deleteUser", "data-toggle":"modal", "data-target":"#normal-modal"}).html('Eliminar');				
				
				$buttonDeactivate = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'deactivate-button',"data-title":"Desactivar usuario(s)" ,"data-message":"¿Estás seguro que deseas desactivar el(los) usuario(s) seleccionado(s)?", "data-function":"changeState", "data-toggle":"modal", "data-target":"#normal-modal", "data-param":"0"}).html('Desactivar');
				$buttonActivate = $('<button/>').attr({'class':'btn btn-default blue', 'type':'button', 'id':'activate-button',"data-title":"Activar usuario(s)","data-message":"¿Estás seguro que deseas activar el(los) usuario(s) seleccionado(s)?", "data-function":"changeState", "data-toggle":"modal", "data-target":"#normal-modal", "data-param":"1"}).html('Activar');	
				
				$('.menu-content .button-content').prepend($buttonActivate);					
				$('.menu-content .button-content').prepend($buttonDeactivate);
				$('.menu-content .button-content').prepend($deleteButton);
			}			
		}
		

		
	});
</script>
