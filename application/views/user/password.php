<?php 
	$hasData = false;
	if(isset($user)){
		$hasData = true;
	}

?>
<style>
label{	
    color: #000;   
}
</style>
<div class="list">
  <ol class="breadcrumb" style="background-color: #1E3652 !important;color:#fff">
    	<div class="card-header" >Cambiar Contraseña</div> 
    </ol>               
</div>
<div class="container">	
	<?php 	
		echo form_open('user/password'); 		
	?>		
	<div class="row">		
		<div class="col-md-6">			
			<div class="form-group">
				<label for="user[password]">Contraseña:</label>
				<?php echo form_input(array('type'=>'password', 'name'=>'user[password]', 'class'=> 'form-control', 'value'=> set_value('user[password]',''))); ?>
				<?php echo form_error('user[password]'); ?>
			</div>
		</div>
		<div class="col-md-6">			
			<div class="form-group">
				<label for="user[password_confirm]">Repita contraseña:</label>
				<?php echo form_input(array('type'=>'password', 'name'=>'user[password_confirm]', 'class'=> 'form-control', 'value'=> set_value('user[password_confirm]',''))); ?>
				<?php echo form_error('user[password_confirm]'); ?>
			</div>
		</div>
	</div>
	
	
	<div class="row">
		<div class="col-md-12">			
			<div class="form-buttons">
				<button type="submit" class="btn btn-success">Guardar</button>	
			</div>	
		</div>
	</div>
	<?php echo form_close(); ?>
</div>

